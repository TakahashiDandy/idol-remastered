<?php
/* Namespace alias. */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

Class Phpmailers{
    private $mail;
    private $config;
    private $CI;
    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->config->load("email");
        /* Create a new PHPMailer object. Passing TRUE to the constructor enables exceptions. */
        $this->mail = new PHPMailer(TRUE);
    }

    function sendEmail($sendToAdd,$sendToname,$subject,$body){
        /* Open the try/catch block. */
        try {
            $this->mail->isSMTP();
            $this->mail->SMTPAuth = TRUE;
            $this->mail->SMTPSecure = $this->CI->config->item("smtp_crypto");
            $this->mail->Host = $this->CI->config->item("smtp_host");
            $this->mail->Port = $this->CI->config->item("smtp_port");
            $this->mail->Username = $this->CI->config->item("smtp_user");
            $this->mail->Password = $this->CI->config->item("smtp_pass");
            $this->mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'=> false,
                    'verify_peer_name'=> false,
                    'allow_self_signed'=> true
                )
            );
            /* Set the mail sender. */
            $this->mail->setFrom($this->CI->config->item("default_email_from"), $this->CI->config->item("default_name_from"));

            /* Add a recipient. */
            $this->mail->addAddress($sendToAdd, $sendToname);

            /* Set the subject. */
            $this->mail->Subject = $subject;

            /* Set the mail message body. */
            $this->mail->Body = $body;

            $this->mail->isHTML(true);

            /* Finally send the mail. */
            $this->mail->send();
        }
        catch (Exception $e)
        {
            /* PHPMailer exception. */
            echo $e->errorMessage();
        }
        catch (\Exception $e)
        {
            /* PHP exception (note the backslash to select the global namespace Exception class). */
            echo $e->getMessage();
        }
    }

}
