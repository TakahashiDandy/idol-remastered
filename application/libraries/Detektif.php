<?php

use Hashids\Hashids;

class Detektif
{
    protected $hashing;

    public function __construct()
    {
        $this->hashing = new Hashids("dandyiqbalmail@diskominfoXbkpp",128);
    }

    public function tutup($id){
        $eid = $this->hashing->encode($id);
        return $eid;
    }

    public function buka($eid){
        $data = $this->hashing->decode($eid);
        if(sizeof($data) > 0){
            return $data[0];
        } else{
            show_404();
        }
    }

    public function telusuri($eid){
        $data = $this->hashing->decode($eid);
        var_dump($data[0]);
    }
}