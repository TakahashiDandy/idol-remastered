<?php


class Report_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    function reportregistrasi($limit=100,$offset=0,$status=0){
        $this->db->select("a.diklat_jadwal_id,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("count(b.nip) as jumlah_pendaftar");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id");
        $this->db->where("a.status_event",$status);
        $this->db->group_by("a.diklat_jadwal_id");
        $this->db->limit($limit,$offset);
        return $this->db->get();
    }

    function generateAcceptedContentReport($jadwalid){
        $status = array(2,4);
        $this->db->where("diklat_jadwal_id",$jadwalid);
        $this->db->where_in("status_registrant",$status);
        return $this->db->get("registrasi_master_reg_online");
    }

    function generateRejectedContentReport($jadwalid){
        $this->db->where("diklat_jadwal_id",$jadwalid);
        $this->db->where("status_registrant",1);
        return $this->db->get("registrasi_master_reg_online");
    }

    function generatePassedContentReport($jadwalid){
        $this->db->select("*");
        $this->db->from("registrasi_master_reg_online a");
        $this->db->join("diklat_trans_riwayat_diklat b", "a.id_registrasi = b.id_registrasi");
        $this->db->where("a.diklat_jadwal_id",$jadwalid);
        $this->db->where("a.status_registrant",4);
        return $this->db->get();
    }

    function generateContentReport($jadwalid){
        $this->db->where("diklat_jadwal_id",$jadwalid);
        return $this->db->get("registrasi_master_reg_online");
    }

    function generateHeaderReport($jadwalid){
        $this->db->select("a.*");
        $this->db->select("count(b.nip) as jumlah_pendaftar");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id");
        $this->db->where("a.diklat_jadwal_id",$jadwalid);
        $this->db->group_by("a.diklat_jadwal_id");
        return $this->db->get();
    }

    function report_rekomendasi($belajar_reg_id,$rekom =0){
        if($rekom == 0){
            $this->db->select("a.*, b.*, c.* ,d.*, e.*, f.*, g.*, h.*, i.*");
            $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id","left");
            $this->db->join("belajar_trans_rekomendasi_detail h","c.rekomendasi_reg_id = h.rekomendasi_reg_id","left");
        } else{
            $this->db->select("a.*, b.*,c.* ,d.*, e.*, f.*, g.*, h.*, i.*");
            $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id","left");
            $this->db->join("belajar_trans_rekomendasi_detail h","c.rekomendasi_reg_id = h.rekomendasi_reg_id","left");
        }

        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id");
        $this->db->join("belajar_master_univ_fakultas f","a.belajar_fakultas_id = f.fakultas_id");
        $this->db->join("belajar_master_univ_jurusan g","a.belajar_jurusan_id = g.jurusan_id");
        $this->db->join("integrasi_master_skpd i","b.instansi = i.nama_dinas");
        $this->db->where("a.belajar_reg_id", $belajar_reg_id);
        return $this->db->get()->result();
    }

    function searchSuratByID($reg_belajar_id,$posisi){
        $this->db->where("reg_belajar_id",$reg_belajar_id);
        $this->db->where("belajar_trans_posisi",$posisi);
        return $this->db->get("belajar_trans_surat")->row();
    }

    function searchKepalaBKPP(){
        $this->db->where("kode_skpd","4.03.01");
        return $this->db->get("integrasi_master_skpd")->row();
    }

    function searchKepalaSetda(){
        $this->db->where("kode_skpd","4.05.02");
        return $this->db->get("integrasi_master_skpd")->row();
    }

    function searchKepalaOPD($instansi){
        $this->db->where("nama_dinas",$instansi);
        return $this->db->get("integrasi_master_skpd")->row();
    }

}