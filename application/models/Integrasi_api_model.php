<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Integrasi_api_model extends CI_Model
{
    var $simpeg = "https://mantra.bandung.go.id/mantra/json/bkd/simpeg/detailpegawai/";
    var $url_bimtek_sosialisasi = "https://kinerja1.bandung.go.id/2019/api/bank-aktivitas/get-data";

    var $headersimpeg = array(
        'User-Agent: MANTRA',
        'AccessKey: 3736hciwks'
    );

    function __construct() {
        parent::__construct();
        $this->load->library(array('ion_auth'));
    }

    function get_detail_by_nip($nip){
        $param = "nip=".$nip;
        //Pendataan ketika user menyinkronkan pada API.
        $ch = curl_init($this->simpeg . $param);
        // To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headersimpeg);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 50000);

        // Get response
        $response = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        if ($curl_errno > 0) {
            return "Error ($curl_errno): $curl_error\n";
        } else {
            //echo "Data received: \n";
            // Decodeg
            return $response;
        }
    }

    function riwayat_diklat($nip){
        return $this->db->query("select a.diklat_jadwal_id, 
                            a.kategori_id,
                            a.diklat_fungsional_id,
                            a.diklat_teknis_id,
                            a.diklat_jenis,
                            a.diklat_mulai,
                            a.diklat_selesai,
                            a.diklat_penyelenggara,
                            a.diklat_tempat,
                            a.diklat_jumlah_jam,
                            a.diklat_usul_no,
                            a.diklat_usul_tgl,
                            (SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis,
                            (SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional,
                            (SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural,
                            b.nip, b.nama_lengkap,
                            c.diklat_sttp_no,
                            c.diklat_sttp_tgl,
                            c.diklat_sttp_pej
                            FROM
                            diklat_trans_jadwal a
                            JOIN registrasi_master_reg_online b
                            JOIN diklat_trans_riwayat_diklat c
                            ON a.diklat_jadwal_id = b.diklat_jadwal_id AND
                            c.id_registrasi = b.id_registrasi
                            WHERE b.status_registrant = 6 AND b.nip = '$nip'
                            GROUP BY b.nip, a.diklat_jadwal_id, a.diklat_jenis");
                            // WHERE CAST(a.diklat_selesai AS DATE) BETWEEN '$tahun-01-01' AND '$tahun-12-31'
    }

    function get_bimtek_sosialisasi_by_nip($nip){
        //Pendataan ketika user menyinkronkan pada API.
        $ch = curl_init($this->url_bimtek_sosialisasi);
        // To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 50000);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "nip=".$nip);

        // Get response
        $response = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        if ($curl_errno > 0) {
            return "Error ($curl_errno): $curl_error\n";
        } else {
            //echo "Data received: \n";
            // Decodeg
            return $response;
        }
    }

    function getMasterPages($folder){
        $client = new GuzzleHttp\Client(); $url = $AccessKey = '';

        if($folder == "diklat/struktural"){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterstruktural/page=1';
            $AccessKey = '0dlxliovku';
        }
        if($folder == 'diklat/fungsional'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterfungsional/page=1';
            $AccessKey = 's9r9hxuwb3';
        }
        if($folder == 'diklat/teknis'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterteknis/page=1';
            $AccessKey = 'ni8vd0bqju';
        }
        if($folder == 'sekolah'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/mastersekolah/page=1';
            $AccessKey = '88fseczey2';
        }
        if($folder == 'tingpend'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/mastertingpend/page=1';
            $AccessKey = 'ahkzj7uedc';
        }
        if($folder == 'fakultas'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterfakultas/page=1';
            $AccessKey = 's3hffhprm9';
        }
        if($folder == 'jurusan'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterjurusan/page=1';
            $AccessKey = 'ebypkcakeq';
        }
        if($folder == 'universitas'){
            $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masteruniversitas/page=1';
            $AccessKey = 'htc59bynn6';
        }
        
        //request the data
        $res = $client->request('GET', $url, [
            'headers' => [
                'User-Agent'    => 'MANTRA',
                'AccessKey'     => $AccessKey       
            ]
        ]);

        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            return $data->response->data->max_page;
        } else {
            return 0;
        }
    }

    // FUNGSI UNTUK INTEGRASI MASTER DIKLAT
    function getMasterDiklatStruktural($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterstruktural/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => '0dlxliovku',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_struktural_kategori");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "kategori_id"=>$datum->kategori_id,
                    "kategori_nama"=>$datum->kategori_nama,
                    "kategori_parent"=>$datum->kategori_parent
                );
                $this->db->insert("cache_master_struktural_kategori",$insertdata);
            }
        }
    }

    function getUpdatedDiklatStruktural(){
        $data = $this->db->query("Select 
                        a.kategori_id as kategori_id, a.kategori_nama as kategori_nama, a.kategori_parent as kategori_parent,
                        b.kategori_id as c_kategori_id, b.kategori_nama as c_kategori_nama, b.kategori_parent as c_kategori_parent
                        FROM diklat_master_struktural_kategori a
                        RIGHT JOIN cache_master_struktural_kategori b
                        ON a.kategori_id = b.kategori_id
                        WHERE (a.kategori_id !=  b.kategori_id OR a.kategori_nama != b.kategori_nama OR a.kategori_parent != b.kategori_parent) OR
                        (a.kategori_id IS NULL AND a.kategori_nama IS NULL AND a.kategori_parent IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "kategori_id" => $item->c_kategori_id,
                "kategori_nama" => $item->c_kategori_nama,
                "kategori_parent" => $item->c_kategori_parent
            );
            if(!is_null($item->kategori_id)){
                $this->db->update("diklat_master_struktural_kategori",$updatedata, array("kategori_id" => $item->c_kategori_id));
            } else{
                $this->db->insert("diklat_master_struktural_kategori",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_struktural_kategori");
    }

    function getMasterDiklatFungsional($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterfungsional/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 's9r9hxuwb3',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_diklat_fungsional");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "diklat_fungsional_id"=>$datum->diklat_fungsional_id,
                    "diklat_fungsional_nm"=>$datum->diklat_fungsional_nm
                );
                $this->db->insert("cache_master_diklat_fungsional",$insertdata);
            }
        }
    }

    function getUpdatedDiklatFungsional(){
        $data = $this->db->query("SELECT 
                                a.diklat_fungsional_id as diklat_fungsional_id, a.diklat_fungsional_nm as diklat_fungsional_nm,
                                b.diklat_fungsional_id as c_diklat_fungsional_id, b.diklat_fungsional_nm as c_diklat_fungsional_nm
                                FROM diklat_master_diklat_fungsional a
                                RIGHT JOIN cache_master_diklat_fungsional b
                                ON a.diklat_fungsional_id = b.diklat_fungsional_id
                                WHERE (a.diklat_fungsional_id !=  b.diklat_fungsional_id OR a.diklat_fungsional_nm != b.diklat_fungsional_nm) OR
                                (a.diklat_fungsional_id IS NULL AND a.diklat_fungsional_nm IS NULL) OR
                                (b.diklat_fungsional_id IS NULL AND b.diklat_fungsional_nm IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "diklat_fungsional_id" => $item->c_diklat_fungsional_id,
                "diklat_fungsional_nm" => $item->c_diklat_fungsional_nm,
            );
            if(!is_null($item->diklat_fungsional_id)){
                $this->db->update("diklat_master_diklat_fungsional",$updatedata, array("diklat_fungsional_id" => $item->c_diklat_fungsional_id));
            } else{
                $this->db->insert("diklat_master_diklat_fungsional",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_diklat_fungsional");
    }

    function getMasterDiklatTeknis($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterteknis/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 'ni8vd0bqju',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_diklat_teknis");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "diklat_teknis_id"=>$datum->diklat_teknis_id,
                    "diklat_teknis_nm"=>$datum->diklat_teknis_nm
                );
                $this->db->insert("cache_master_diklat_teknis",$insertdata);
            }
        }
    }

    function getUpdatedDiklatTeknis(){
        $data = $this->db->query("SELECT 
                                a.diklat_teknis_id as diklat_teknis_id, a.diklat_teknis_nm as diklat_teknis_nm,
                                b.diklat_teknis_id as c_diklat_teknis_id, b.diklat_teknis_nm as c_diklat_teknis_nm
                                FROM diklat_master_diklat_teknis a
                                RIGHT JOIN cache_master_diklat_teknis b
                                ON a.diklat_teknis_id = b.diklat_teknis_id
                                WHERE (a.diklat_teknis_id !=  b.diklat_teknis_id OR a.diklat_teknis_nm != b.diklat_teknis_nm) OR
                                (a.diklat_teknis_id IS NULL AND a.diklat_teknis_nm IS NULL) OR
                                (b.diklat_teknis_id IS NULL AND b.diklat_teknis_nm IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "diklat_teknis_id" => $item->c_diklat_teknis_id,
                "diklat_teknis_nm" => $item->c_diklat_teknis_nm,
            );
            if(!is_null($item->diklat_teknis_id)){
                $this->db->update("diklat_master_diklat_teknis",$updatedata, array("diklat_teknis_id" => $item->c_diklat_teknis_id));
            } else{
                $this->db->insert("diklat_master_diklat_teknis",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_diklat_teknis");
    }

    // FUNGSI UNTUK INTEGRASI MASTER BELAJAR
    function getMasterSekolah($page){
        $client = new GuzzleHttp\Client();
        $res =  $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/mastersekolah/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => '88fseczey2',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_sekolah");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "sekolah_id"=>$datum->sekolah_id,
                    "sekolah_nm"=>$datum->sekolah_nm,
                    "kabupaten_id"=>$datum->kabupaten_id,
                    "sekolah_kab_kota"=>$datum->sekolah_kab_kota,
                );
                $this->db->insert("cache_master_sekolah",$insertdata);
            }
        }
    }

    function getUpdatedSekolah(){
        $data = $this->db->query("SELECT 
                                a.sekolah_id as sekolah_nm, a.sekolah_nm as sekolah_nm, a.kabupaten_id as kabupaten_id, a.sekolah_kab_kota as sekolah_kab_kota,
                                b.sekolah_id as c_sekolah_nm, b.sekolah_nm as c_sekolah_nm, b.kabupaten_id as c_kabupaten_id, b.sekolah_kab_kota as c_sekolah_kab_kota
                                FROM belajar_master_sekolah a
                                RIGHT JOIN cache_master_sekolah b
                                ON a.sekolah_id = b.sekolah_id
                                WHERE (a.sekolah_id !=  b.sekolah_id OR a.sekolah_nm != b.sekolah_nm OR a.kabupaten_id != b.kabupaten_id OR a.sekolah_kab_kota != b.sekolah_kab_kota) OR
                                (a.sekolah_id IS NULL AND a.sekolah_nm IS NULL AND a.kabupaten_id IS NULL AND a.sekolah_kab_kota IS NULL ) OR
                                (b.sekolah_id IS NULL AND b.sekolah_nm IS NULL AND b.kabupaten_id IS NULL AND b.sekolah_kab_kota IS NULL )");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "sekolah_id"=>$item->c_sekolah_id,
                "sekolah_nm"=>$item->c_sekolah_nm,
                "kabupaten_id"=>$item->c_kabupaten_id,
                "sekolah_kab_kota"=>$item->c_sekolah_kab_kota,
            );
            if(!is_null($item->sekolah_id)){
                $this->db->update("belajar_master_sekolah",$updatedata, array("sekolah_id" => $item->c_sekolah_id));
            } else{
                $this->db->insert("belajar_master_sekolah",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_sekolah");
    }

    function getMasterTingPend($page){
        $client = new GuzzleHttp\Client();
        $res =  $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/mastertingpend/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 'ahkzj7uedc',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_tingkat_pend");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "tingpend_id"=>$datum->tingpend_id,
                    "kd_tingpend"=>$datum->kd_tingpend,
                    "nm_tingpend"=>$datum->nm_tingpend,
                    "print_tingpend"=>$datum->print_tingpend,
                    "kode_urut_pend"=>$datum->kode_urut_pend
                );
                $this->db->insert("cache_master_tingkat_pend",$insertdata);
            }
        }
    }

    function getUpdatedTingPend(){
        $data = $this->db->query("SELECT 
                                a.tingpend_id as tingpend_id, a.kd_tingpend as kd_tingpend, a.nm_tingpend as nm_tingpend, a.print_tingpend as print_tingpend, a.kode_urut_pend as kode_urut_pend,
                                b.tingpend_id as c_tingpend_id, b.kd_tingpend as c_kd_tingpend, b.nm_tingpend as c_nm_tingpend, b.print_tingpend as c_print_tingpend, b.kode_urut_pend as c_kode_urut_pend
                                FROM belajar_master_tingkat_pend a
                                RIGHT JOIN cache_master_tingkat_pend b
                                ON a.tingpend_id = b.tingpend_id
                                WHERE (a.tingpend_id !=  b.tingpend_id OR a.kd_tingpend != b.kd_tingpend OR a.nm_tingpend != b.nm_tingpend OR a.print_tingpend != b.print_tingpend OR a.kode_urut_pend != b.kode_urut_pend) OR
                                (a.tingpend_id IS NULL AND a.kd_tingpend IS NULL AND a.nm_tingpend IS NULL AND a.print_tingpend IS NULL AND a.kode_urut_pend IS NULL ) OR
                                (b.tingpend_id IS NULL AND b.kd_tingpend IS NULL AND b.nm_tingpend IS NULL AND b.print_tingpend IS NULL AND b.kode_urut_pend IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "tingpend_id"=>$item->tingpend_id,
                "kd_tingpend"=>$item->kd_tingpend,
                "nm_tingpend"=>$item->nm_tingpend,
                "print_tingpend"=>$item->print_tingpend,
                "kode_urut_pend"=>$item->kode_urut_pend
            );
            if(!is_null($item->tingpend_id)){
                $this->db->update("belajar_master_tingkat_pend",$updatedata, array("tingpend_id" => $item->c_tingpend_id));
            } else{
                $this->db->insert("belajar_master_tingkat_pend",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_tingkat_pend");
    }

    function getMasterFakultas($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterfakultas/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 's3hffhprm9',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_univ_fakultas");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "fakultas_id"=>$datum->fakultas_id,
                    "fakultas_nm"=>$datum->fakultas_nm
                );
                $this->db->insert("cache_master_univ_fakultas",$insertdata);
            }
        }
    }

    function getUpdatedFakultas(){
        $data = $this->db->query("SELECT 
                                a.fakultas_id as fakultas_id, a.fakultas_nm as fakultas_nm,
                                b.fakultas_id as c_fakultas_id, b.fakultas_nm as c_fakultas_nm
                                FROM belajar_master_univ_fakultas a
                                RIGHT JOIN cache_master_univ_fakultas b
                                ON a.fakultas_id = b.fakultas_id
                                WHERE (a.fakultas_id !=  b.fakultas_id OR a.fakultas_nm != b.fakultas_nm) OR
                                (a.fakultas_id IS NULL AND a.fakultas_nm IS NULL) OR
                                (b.fakultas_id IS NULL AND b.fakultas_nm IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "fakultas_id" => $item->c_fakultas_id,
                "fakultas_nm" => $item->c_fakultas_nm,
            );
            if(!is_null($item->fakultas_id)){
                $this->db->update("belajar_master_univ_fakultas",$updatedata, array("fakultas_id" => $item->c_fakultas_id));
            } else{
                $this->db->insert("belajar_master_univ_fakultas",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_univ_fakultas");
    }

    function getMasterJurusan($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masterjurusan/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 'ebypkcakeq',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_univ_jurusan");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "jurusan_id"=>$datum->jurusan_id,
                    "jurusan_nm"=>$datum->jurusan_nm
                );
                $this->db->insert("cache_master_univ_jurusan",$insertdata);
            }
        }
    }

    function getUpdatedJurusan(){
        $data = $this->db->query("SELECT 
                                a.jurusan_id as jurusan_id, a.jurusan_nm as jurusan_nm,
                                b.jurusan_id as c_jurusan_id, b.jurusan_nm as c_jurusan_nm
                                FROM belajar_master_univ_jurusan a
                                RIGHT JOIN cache_master_univ_jurusan b
                                ON a.jurusan_id = b.jurusan_id
                                WHERE (a.jurusan_id !=  b.jurusan_id OR a.jurusan_nm != b.jurusan_nm) OR
                                (a.jurusan_id IS NULL AND a.jurusan_nm IS NULL) OR
                                (b.jurusan_id IS NULL AND b.jurusan_nm IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "jurusan_id" => $item->c_jurusan_id,
                "jurusan_nm" => $item->c_jurusan_nm,
            );
            if(!is_null($item->kategori_id)){
                $this->db->update("belajar_master_univ_jurusan",$updatedata, array("jurusan_id" => $item->c_jurusan_id));
            } else{
                $this->db->insert("belajar_master_univ_jurusan",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_univ_jurusan");
    }

    function getMasterUniversitas($page){
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/masteruniversitas/page='.$page,
                [
                    'headers' => [
                        'User-Agent'    => 'MANTRA',
                        'AccessKey'     => 'htc59bynn6',
                    ]
                ]);
        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            $this->db->truncate("cache_master_universitas");
            foreach ($data->response->data->data as $datum){
                $insertdata = array(
                    "univ_id"=>$datum->univ_id,
                    "univ_nmpti"=>$datum->univ_nmpti,
                    "univ_kota"=>$datum->univ_kota,
                );
                $this->db->insert("cache_master_universitas",$insertdata);
            }
        }
    }

    function getUpdatedUniversitas(){
        $data = $this->db->query("SELECT 
                                a.univ_id as univ_id, a.univ_nmpti as univ_nmpti, a.univ_kota as univ_kota,
                                b.univ_id as c_univ_id, b.univ_nmpti as c_univ_nmpti, b.univ_kota as c_univ_kota
                                FROM belajar_master_universitas a
                                RIGHT JOIN cache_master_universitas b
                                ON a.univ_id = b.univ_id
                                WHERE (a.univ_id !=  b.univ_id OR a.univ_nmpti != b.univ_nmpti OR a.univ_kota != b.univ_kota) OR
                                (a.univ_id IS NULL AND a.univ_nmpti IS NULL AND a.univ_kota IS NULL) OR
                                (b.univ_id IS NULL AND b.univ_nmpti IS NULL AND b.univ_kota IS NULL)");

        //update kalau ada perbedaan dari data cache ke data master.
        foreach ($data->result() as $item){
            $updatedata = array(
                "univ_id" => $item->c_univ_id,
                "univ_nmpti" => $item->c_univ_nmpti,
                "univ_kota" => $item->c_univ_kota,
            );
            if(!is_null($item->c_univ_id)){
                $this->db->update("belajar_master_universitas",$updatedata, array("univ_id" => $item->univ_id));
            } else{
                $this->db->insert("belajar_master_universitas",$updatedata);
            }
        }
        $this->db->query("DELETE FROM cache_master_universitas");
    }

    function apiSKPD($year){
        $client = new Client();
        $header = array(
            'User-Agent' => 'MANTRA',
            'AccessKey' => 'enckwubgyn'
        );
        $url = "https://mantra.bandung.go.id/mantra/json/bpka/master/units/tahun=".$year."&kode_skpd=all";
        $res = $client->request("GET",$url,array("headers"=>$header));
        if($res->getStatusCode() == 200){
            return $res->getBody()->getContents();
        }
    }

    function generateAccountSKPD(){
        $data = json_decode($this->apiSKPD(2019));
        foreach ($data->response->data->data as $items){
            $insertData = array(
                "tahun" => date("y"),
                "kode_skpd" => $items->kode,
                "nama_dinas" => $items->nama
            );
            $this->db->insert("integrasi_master_skpd",$insertData);
        }
    }

    function integrateUser(){
        $this->generateAccountSKPD();
        $this->db->select("a.*,b.kode_skpd,b.nama_dinas");
        $this->db->from("user_master_users a");
        $this->db->join("integrasi_master_skpd b","a.username = b.nama_dinas","left");
        $this->db->where("(a.username is NULL) || (a.username != b.kode_skpd)");
        $data = $this->db->get()->result();
        foreach ($data as $item){
            if($item->username == NULL){
                $nama = explode(" ", $item->nama_dinas);
                $username = $item->kode_skpd;
                $password = "abcdef2019";
                $email = $item->kode_skpd."@abcdef.com";
                $additional_data = array(
                    'first_name' => $nama[0],
                    'last_name' => str_replace(","," ", $nama[1])
                );
                $group = array("3");
                $this->ion_auth->register($username, $password, $email, $additional_data, $group);
                $this->db->where("username",$item->kode_skpd);
                $this->db->update("user_master_users",array(
                    "active"=>1,
                    "activation_selector"=>NULL,
                    "activation_code"=>NULL,
                ));
            }
        }
        return true;
    }

    function generateKadis(){
        $this->db->select("kode_skpd");
        $this->db->where("tahun",date("Y"));
        $list_skpd = $this->db->get("integrasi_master_skpd")->result();
        foreach ($list_skpd as $skpd){
            $client = new Client();
            $header = array(
                'User-Agent' => 'MANTRA',
                'AccessKey' => '0oqdpseonx'
            );
            $url = "https://mantra.bandung.go.id/mantra/json/bkd/simpeg/list_pegawai_skpd/kode_skpd=".$skpd->kode_skpd."&hanya_pejabat_struktural=true";
            $res = $client->request("GET",$url,array("headers"=>$header));
            if($res->getStatusCode() == 200){
                $result = json_decode($res->getBody()->getContents());
                foreach($result->response->data->data as $kadis){
                        $updateKadis = array(
                            "kepala_dinas" => ($kadis->gelar_depan == "" ? "" : $kadis->gelar_depan . " ") . strtoupper($kadis->nama) . ($kadis->gelar_belakang == "" ? "" : " " . $kadis->gelar_belakang),
                            "nip_kepala" => $kadis->nip,
                            "jabatan_kepala" => $kadis->jabatan,
                            "golongan_kepala" => $kadis->nama_golongan,
                            "pangkat_kepala" => $kadis->nama_pangkat
                        );
                        $this->db->update("integrasi_master_skpd",$updateKadis,array("kode_skpd"=>$skpd->kode_skpd));
                    break;
                }
            }
        }
    }

    function dataTransSurat($nip){
        return $this->db->query("SELECT DISTINCT a.belajar_reg_id, a.nip, b.created_at as tmt_mulai, d.masa_tempuh, c.status_perpanjangan
        FROM registrasi_master_belajar a 
        LEFT JOIN belajar_trans_surat b ON a.belajar_reg_id = b.reg_belajar_id
        LEFT JOIN belajar_trans_perpanjangan c ON a.belajar_reg_id = c.belajar_reg_id
        JOIN belajar_master_tingkat_pend d ON a.belajar_tingpend_id = d.tingpend_id
        WHERE (b.created_at IS NOT NULL) AND (c.status_perpanjangan IS NOT NULL) AND a.nip = '".$nip."'")->result();
    }

    function riwayat_pendidikan($nip){
        return $this->db->query("SELECT b.nip, b.belajar_pendidikan_nm, b.belajar_pend_lokasi, a.belajar_trans_riwayat_sttb_ijazah, a.belajar_trans_riwayat_pendidikan_tgl, a.belajar_trans_riwayat_pejabat, d.nama_lengkap
        FROM belajar_trans_riwayat_pendidikan a
        LEFT JOIN registrasi_master_belajar b ON a.registrasi_belajar_id = b.belajar_reg_id
        LEFT JOIN registrasi_master_reg_online c ON a.registrasi_belajar_id = c.id_registrasi
        LEFT join user_trans_detail_user d ON b.nip = d.nip
        WHERE b.status_reg_belajar = 7 AND b.nip = '".$nip."'
        ORDER BY a.belajar_trans_riwayat_pendidikan_tgl DESC")->result();
    }

}