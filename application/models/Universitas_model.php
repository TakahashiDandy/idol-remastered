<?php
/**
 * Created by Visual Studio Code.
 * User: Muhammad Iqbal Tawaqal
 * Date: 31/07/19
 * Time: 10.41
 */

class Universitas_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    public function save($table, $data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }


    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    function list_universitas(){
        return $this->db->get("belajar_master_universitas");
    }

    function search_list_universitas_by_name($name){
        $this->db->where("univ_nmpti LIKE '%".$name."%'");
        return $this->db->get("belajar_master_universitas");
    }

    function list_jurusan(){
        return $this->db->get("belajar_master_univ_jurusan");
    }

    function search_list_jurusan_by_name($name){
        $this->db->where("jurusan_nm LIKE '%".$name."%'");
        return $this->db->get("belajar_master_univ_jurusan");
    }

    function list_tingkat_pend(){
        $this->db->where("kode_urut_pend >= 6");
        $this->db->order_by("kode_urut_pend ASC");
        return $this->db->get("belajar_master_tingkat_pend");
    }

    function search_list_tingkat_pend_by_name($name){
        $this->db->where("nm_tingpend LIKE '%".$name."%' OR print_tingpend LIKE '%".$name."%' ");
        $this->db->where("kode_urut_pend >= 6");
        $this->db->order_by("kode_urut_pend ASC");
        return $this->db->get("belajar_master_tingkat_pend");
    }

    function list_fakultas(){
        return $this->db->get("belajar_master_univ_fakultas");
    }

    function search_list_fakultas_by_name($name){
        $this->db->where("fakultas_nm LIKE '%".$name."%'");
        return $this->db->get("belajar_master_univ_fakultas");
    }

    function listfunc($limit,$offset){
        return $this->db->get("diklat_master_diklat_fungsional",$limit,$offset);
    }

    function listtek($limit,$offset){
        return $this->db->get("diklat_master_diklat_teknis",$limit,$offset);
    }

}