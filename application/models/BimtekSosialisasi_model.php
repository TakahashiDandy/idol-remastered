<?php
/**
 * Created by VisualStudio Code.
 * Name: Muhammad Iqbal Tawaqal
 * Date: 28/06/19
 * Time: 10.15
 */

class BimtekSosialisasi_model extends CI_Model
{
    var $table = "user_trans_bimtek";

    function __construct() {
        parent::__construct();
    }

    public function get_all_data($limit=100,$offset=0,$nip){
        $this->db->select("nip, tanggal, nama_aktifitas as aktifitas, keterangan, volume");
        $this->db->from($this->table);
        $this->db->where("nip",$nip);
        $this->db->order_by("tanggal","DESC");
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query;
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table,$nip){
        $this->db->where("nip",$nip);
        return $this->db->get($table)->num_rows();
    }

    public function count_total_jpl($nip){
        $this->db->select("SUM(volume) as total_jpl");
        $this->db->from($this->table);
        $this->db->where("nip",$nip);
        return $this->db->get()->row();
    }

    //== SEMUA FUNGSI CRUD JADWAL
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_by_nip($nip)
    {
        $this->db->from($this->table);
        $this->db->where('nip',$nip);
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_by_tanggal($tanggal)
    {
        $this->db->from($this->table);
        $this->db->where('tanggal',$tanggal);
        $query = $this->db->get()->result();
        return $query;
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}