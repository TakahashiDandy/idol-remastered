<?php
/**
 * Created by VisualStudio Code.
 * Name: Muhammad Iqbal Tawaqal
 * Date: 28/06/19
 * Time: 10.15
 */

class Master_user_model extends CI_Model
{
    var $table = "user_master_users";

    function __construct() {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    //== SEMUA FUNGSI CRUD JADWAL
    public function get_detail_user_by_username($username)
    {
        $this->db->select("a.id,b.*");
        $this->db->from($this->table." a");
        $this->db->join("user_trans_detail_user b","a.id = b.id_user","left");
        $this->db->where('a.username',$username);
        return $this->db->get()->row();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id',$id);
        $query = $this->db->get();

        return $query->row();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}