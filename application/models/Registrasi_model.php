<?php
/**
 * Created by PhpStorm.
 * User: takahashidandy
 * Date: 18/02/19
 * Time: 10.41
 */

class Registrasi_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    function listregistrasi($limit=100,$offset=0){
        $this->db->select("a.diklat_jadwal_id,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("count(b.nip) as jumlah_pendaftar");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id");
        // $this->db->where("b.status_registrant",0);
        $this->db->where("a.status_event <=","1");
        $this->db->group_by("a.diklat_jadwal_id");
        $this->db->limit($limit,$offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get();
    }

    function detailregistrasi($id){
        $this->db->select("a.id_registrasi,a.nip,a.nama_lengkap");
        $this->db->from("registrasi_master_reg_online a");
        $this->db->join("registrasi_trans_upload b","a.id_registrasi = b.id_registrasi","left");
        $this->db->where("a.diklat_jadwal_id",$id);
        $this->db->where("a.status_registrant",0);
        return $this->db->get();
    }

    function checkstatusregistrasi($id_reg, $id_jadwal_diklat){
        $this->db->select("status_registrant");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("diklat_jadwal_id",$id_jadwal_diklat);
        $this->db->where("id_registrasi",$id_reg);
        return $this->db->get()->row();
    }

    function detailrejectedregistrasi($id){
        $this->db->select("id_registrasi,nip,nama_lengkap,status_registrant");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("diklat_jadwal_id",$id);
        //$this->db->where("status_registrant",0);
        return $this->db->get();
    }

    function detailacceptedregistrasi($id){
        $this->db->select("id_registrasi,nip,nama_lengkap");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("diklat_jadwal_id",$id);
        $this->db->where("status_registrant",2);
        return $this->db->get();
    }


    function listalumni($limit,$offset){
        $this->db->select("a.diklat_jadwal_id,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("count(b.nip) as jumlah_pendaftar");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id");
        $this->db->where("a.status_event",2);
        $this->db->group_by("a.diklat_jadwal_id");
        $this->db->limit($limit,$offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get();
    }

    function detaillulusalumni($id){
        $this->db->select("id_registrasi,nip,nama_lengkap");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("diklat_jadwal_id",$id);
        $this->db->where("status_registrant",4);
        return $this->db->get();
    }

    function detailgagalalumni($id){
        $this->db->select("id_registrasi,nip,nama_lengkap,status_registrant");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("diklat_jadwal_id",$id);
        $this->db->where("status_registrant",2);
        return $this->db->get();
    }

    function updatealumni($data){
        return $this->db->update_batch("registrasi_master_reg_online",$data,"id_registrasi");
    }

    function insertriwayat($data){
        return $this->db->insert_batch("diklat_trans_riwayat_diklat",$data);
    }

    function insertregistrasi($data){
        $this->db->insert("registrasi_master_reg_online",$data);
        return $this->db->insert_id();
    }

    function deleteregistrasi($id_reg){
        $this->db->where("id_registrasi",$id_reg);
        return $this->db->delete("registrasi_master_reg_online");
    }

    function updateregistrasi($data){
        return $this->db->update_batch("registrasi_master_reg_online",$data,"id_registrasi");
    }

    function checkregistrant($iddiklat,$nip){
        $this->db->select("count(nip) as total");
        $this->db->from("registrasi_master_reg_online");
        $this->db->where("nip",$nip);
        $this->db->where("diklat_jadwal_id",$iddiklat);
        return $this->db->get();
    }

    function detailalumni($id){
        $this->db->select("a.id_registrasi,a.nip,a.nama_lengkap,a.status_registrant");
        $this->db->from("registrasi_master_reg_online a");
        $this->db->where("a.diklat_jadwal_id",$id);
        // $this->db->where("a.status_registrant >=",3);
        // $this->db->where("a.status_registrant <=",5);
        return $this->db->get();
    }

    function get_detail_user_by_id($id)
    {
        $this->db->from('user_trans_detail_user');
        $this->db->where('id_user',$id);
        $query = $this->db->get();

        return $query->row();
    }

    function get_detail_user_diklat_by_id($id, $id_registrasi)
    {
        $this->db->select('a.*,b.*');
        $this->db->from('user_trans_detail_user a');
        $this->db->join('registrasi_master_reg_online b',"a.nip = b.nip");
        $this->db->where('a.id_user',$id);
        $this->db->where('b.id_registrasi',$id_registrasi);
        $query = $this->db->get();

        return $query->row();
    }

    function get_detail_alumni_riwayat_diklat($id){
        $this->db->select("a.id_registrasi,a.nip,a.nama_lengkap,a.status_registrant, a.keterangan, b.*");
        $this->db->from("registrasi_master_reg_online a");
        $this->db->join("diklat_trans_riwayat_diklat b","a.id_registrasi = b.id_registrasi");
        $this->db->where("b.id_registrasi",$id);
        $this->db->order_by("b.diklat_id","DESC");
        $this->db->limit("1");
        return $this->db->get();   
    }

    function save_registrasi_trans_upload($data)
    {
        $this->db->insert("registrasi_trans_upload", $data);
        return $this->db->insert_id();
    }

    function delete_registrasi_trans_upload($id_jadwal,$id_registrasi,$id_option)
    {
        $this->db->select("filepath");
        $this->db->where("id_jadwal",$id_jadwal);
        $this->db->where("id_registrasi",$id_registrasi);
        $data["attachment"] = $this->db->get("registrasi_trans_upload")->result_array();
        $this->db->where("id_jadwal",$id_jadwal);
        $this->db->where("id_registrasi",$id_registrasi);
        $this->db->where("id_option",$id_option);
        $data["status"] = $this->db->delete("registrasi_trans_upload");
        return $data;
    }

    function list_registrasi_trans_uploads($id_reg, $condition){
        $this->db->select("*");
        $this->db->from('registrasi_trans_upload');
        $this->db->where('id_registrasi',$id_reg);
        $this->db->where('filepath LIKE '.$condition.'');
        return $this->db->get()->result();
    }

    function update_status_jadwal_diklat($data,$where){
        $this->db->update("diklat_trans_jadwal", $data, $where);
        return $this->db->affected_rows();
    }

    function list_trans_riwayat_diklat($id_reg){
        $this->db->select("*");
        $this->db->from('diklat_trans_riwayat_diklat');
        // $this->db->join('registrasi_trans_upload b', 'a.id_registrasi = b.id_registrasi','left');
        $this->db->limit("1");
        $this->db->order_by("diklat_id","DESC");
        $this->db->where('id_registrasi',$id_reg);
        return $this->db->get()->result();
    }

    function getattachment($id, $id_option)
    {
        $this->db->select("id_attachment,filepath");
        $this->db->where("id_registrasi", $id);
        $this->db->where("id_option", $id_option);
        return $this->db->get("registrasi_trans_upload");
    }

    function removeattachment($id_attachment){
        $this->db->select("filepath");
        $this->db->where("id_attachment", $id_attachment);
        $this->db->where("id_option", 2);
        $data["attachment"] =  $this->db->get("registrasi_trans_upload")->result_array();
        $this->db->where("id_attachment",$id_attachment);
        $data["deleted"] = $this->db->delete("registrasi_trans_upload");
        return $data;
    }

    function removeattachmentsyaratdiklat($id_attachment){
        $this->db->select("filepath");
        $this->db->where("id_attachment", $id_attachment);
        $this->db->where("id_option", 1);
        $data["attachment"] =  $this->db->get("registrasi_trans_upload")->result_array();
        $this->db->where("id_attachment",$id_attachment);
        $data["deleted"] = $this->db->delete("registrasi_trans_upload");
        return $data;
    }

}