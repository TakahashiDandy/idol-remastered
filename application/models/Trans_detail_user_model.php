<?php
/**
 * Created by VisualStudio Code.
 * Name: Muhammad Iqbal Tawaqal
 * Date: 28/06/19
 * Time: 10.15
 */

class Trans_detail_user_model extends CI_Model
{
    var $table = "user_trans_detail_user";

    function __construct() {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    //== SEMUA FUNGSI CRUD JADWAL
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_user',$id);
        $query = $this->db->get();

        return $query->row();
    }

    public function getcount($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_user',$id);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function isActivate($nip, $id){
        $this->db->select("a.active,a.id,b.id_user,b.nip");
        $this->db->from("user_master_users a");
        $this->db->join("user_trans_detail_user b", "a.id = b.id_user");
        $this->db->where("b.nip",$nip);
        $this->db->where("a.id",$id);
        $this->db->where("a.active",1);
        return $this->db->get()->num_rows();
    }

    public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get_data_skpd($nama_instansi){
        $this->db->from("integrasi_master_skpd");
        $this->db->where("nama_dinas",$nama_instansi);
        return $this->db->get()->row();
    }

}