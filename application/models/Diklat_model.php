<?php
/**
 * Created by PhpStorm.
 * User: takahashidandy
 * Date: 18/02/19
 * Time: 10.41
 */

class Diklat_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    function listriwayatdiklat($limit, $offset, $nip)
    {
        $this->db->select("a.diklat_jenis, a.diklat_penyelenggara, a.diklat_tempat, a.diklat_jumlah_jam, a.status_event, b.diklat_jadwal_id, b.id_registrasi, b.status_registrant, b.nip, b.nama_lengkap");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id");
        $this->db->where("b.status_registrant >=",0);
        $this->db->where("b.nip",$nip);
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get();
    }

    function listriwayatpendidikan($limit, $offset, $nip)
    {
        $this->db->select("b.nip, b.belajar_pendidikan_nm, b.belajar_pend_lokasi, a.belajar_trans_riwayat_sttb_ijazah, a.belajar_trans_riwayat_pendidikan_tgl, a.belajar_trans_riwayat_pejabat, d.nama_lengkap");
        $this->db->from("belajar_trans_riwayat_pendidikan a");
        $this->db->join("registrasi_master_belajar b","a.registrasi_belajar_id = b.belajar_reg_id","left");
        $this->db->join("registrasi_master_reg_online c","a.registrasi_belajar_id = c.id_registrasi","left");
        $this->db->join("user_trans_detail_user d","b.nip = d.nip","left");
        $this->db->where("b.status_reg_belajar >=",0);
        $this->db->where("b.nip", $nip);
        $this->db->order_by("a.belajar_trans_riwayat_pendidikan_tgl DESC");
        $this->db->limit($limit, $offset);
        return $this->db->get();
    }

    function liststruk($limit,$offset){
        return $this->db->get("diklat_master_struktural_kategori",$limit,$offset);
    }

    function listfunc($limit,$offset){
        return $this->db->get("diklat_master_diklat_fungsional",$limit,$offset);
    }

    function listtek($limit,$offset){
        return $this->db->get("diklat_master_diklat_teknis",$limit,$offset);
    }

}