<?php
/**
 * Created by PhpStorm.
 * User: takahashidandy
 * Date: 18/02/19
 * Time: 10.41
 */

class Belajar_model extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table){
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table){
        return $this->db->get($table)->num_rows();
    }

    function listrekomendasi($limit=100,$offset=0){
        $this->db->select("f.rekomendasi_reg_id, f.nip,f.rekomendasi_pendidikan_nm, b.nm_tingpend,
        c.univ_nmpti, d.fakultas_nm, e.jurusan_nm, f.rekomendasi_pend_lokasi, f.status_rekomendasi");
        $this->db->join("belajar_master_sekolah a","f.rekomendasi_sekolah_id = a.sekolah_id", "left");
        $this->db->join("belajar_master_tingkat_pend b","f.rekomendasi_tingpend_id = b.tingpend_id", "left");
        $this->db->join("belajar_master_universitas c","f.rekomendasi_univ_id = c.univ_id", "left");
        $this->db->join("belajar_master_univ_fakultas d","f.rekomendasi_fakultas_id = d.fakultas_id", "left");
        $this->db->join("belajar_master_univ_jurusan e","f.rekomendasi_jurusan_id = e.jurusan_id", "left");
        $this->db->limit($limit,$offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get("belajar_trans_rekomendasi f");
    }

    function detailrekomendasi($id_rekomendasi){
        $this->db->select("f.rekomendasi_reg_id, f.nip,g.*,f.rekomendasi_pendidikan_nm, b.nm_tingpend,
        c.univ_nmpti, d.fakultas_nm, e.jurusan_nm, f.rekomendasi_pend_lokasi, f.status_rekomendasi");
        $this->db->join("belajar_master_sekolah a","f.rekomendasi_sekolah_id = a.sekolah_id", "left");
        $this->db->join("belajar_master_tingkat_pend b","f.rekomendasi_tingpend_id = b.tingpend_id", "left");
        $this->db->join("belajar_master_universitas c","f.rekomendasi_univ_id = c.univ_id", "left");
        $this->db->join("belajar_master_univ_fakultas d","f.rekomendasi_fakultas_id = d.fakultas_id", "left");
        $this->db->join("belajar_master_univ_jurusan e","f.rekomendasi_jurusan_id = e.jurusan_id", "left");
        $this->db->where("f.rekomendasi_reg_id",$id_rekomendasi);
        return $this->db->get("belajar_trans_rekomendasi f");
    }

}