<?php
/**
 * Created by PhpStorm.
 * User: Muhammad Iqbal Tawaqal
 * Date: 01/07/19
 * Time: 2.49
 */

class Tugasbelajar_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function notification($kode_skpd=null,$nip=null)
    {
        $this->db->select("a.*");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("integrasi_master_skpd f","b.instansi = f.nama_dinas");
        $this->db->where("a.status_reg_belajar <=",5);
        $this->db->where("a.jenis_belajar",1);
        if($kode_skpd !=null){
            $this->db->where("f.kode_skpd",$kode_skpd);
//            $this->db->where_in('jenis_belajar', array());
        }
        if($nip != null):
            $this->db->where("b.nip", $nip);
        endif;
        return $this->db->get()->num_rows();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table)
    {
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table)
    {
        $this->db->where("jenis_belajar", 1);
        return $this->db->get($table)->num_rows();
    }

    public function gettotalrowsbyNIP($table,$nip)
    {
        $this->db->where("jenis_belajar", 1);
        $this->db->where("nip", $nip);
        return $this->db->get($table)->num_rows();
    }

    function list_surat_rekomendasi($limit, $offset, $kode_skpd = null, $nip = null)
    {
        $this->db->select("a.*, b.*,c.*,d.univ_nmpti, e.print_tingpend");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("integrasi_master_skpd f","b.instansi = f.nama_dinas");
        $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id");
        $this->db->where("a.jenis_belajar", 1);
        $this->db->where("c.status_rekomendasi <=", 2);
        if($kode_skpd !=null){
            $this->db->where("f.kode_skpd",$kode_skpd);
        }
        if($nip != null):
            $this->db->where("b.nip", $nip);
        endif;
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.belajar_reg_id", "DESC");
        return $this->db->get();
    }

    function list_tugas_belajar($limit, $offset, $kode_skpd = null, $nip = null)
    {
        $this->db->select("a.*, b.*, c.*,d.univ_nmpti, e.print_tingpend");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("integrasi_master_skpd f","b.instansi = f.nama_dinas");
        $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id");
        $this->db->where("a.jenis_belajar", 1);
        //$this->db->where("c.status_rekomendasi", 1);
        if($kode_skpd !=null){
            $this->db->where("f.kode_skpd",$kode_skpd);
        }
        if($nip != null):
            $this->db->where("b.nip", $nip);
        endif;
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.belajar_reg_id", "DESC");
        // return $this->db->get_com)
        return $this->db->get();
        // return $this->db->get_compiled_select();
    }

    function list_aktifasi_tugas_belajar($limit, $offset, $kode_skpd = null, $nip = null)
    {
        $this->db->select("a.*, b.*, c.*,d.univ_nmpti, e.print_tingpend");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("integrasi_master_skpd f","b.instansi = f.nama_dinas");
        $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id");
        $this->db->where("a.jenis_belajar", 1);
        $this->db->where("c.status_rekomendasi", 1);
        $this->db->where("a.status_reg_belajar >=", 5);
        if($kode_skpd !=null){
            $this->db->where("f.kode_skpd",$kode_skpd);
        }
        if($nip != null):
            $this->db->where("b.nip", $nip);
        endif;
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.belajar_reg_id", "DESC");
        // return $this->db->get_com)
        return $this->db->get();
        // return $this->db->get_compiled_select();
    }

    function data_rekomendasi($belajar_reg_id){
        $this->db->select("a.*, b.*, c.*,d.*, e.*, f.*, g.*, h.*");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id","left");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id","left");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id","left");
        $this->db->join("belajar_master_univ_fakultas f","a.belajar_fakultas_id = f.fakultas_id","left");
        $this->db->join("belajar_master_univ_jurusan g","a.belajar_jurusan_id = g.jurusan_id","left");
        $this->db->join("belajar_trans_rekomendasi_detail h","c.rekomendasi_reg_id= h.rekomendasi_reg_id","left");
        $this->db->where("a.belajar_reg_id", $belajar_reg_id);
        $this->db->where("a.status_reg_belajar <=",3);
        $this->db->where("a.jenis_belajar", 1);
        return $this->db->get()->result();
    }

    function data_tugas_belajar($belajar_reg_id){
        $this->db->select("a.*, b.*, d.*, e.*, f.*, g.*, h.*");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("belajar_trans_rekomendasi c","a.rekomendasi_reg_id = c.rekomendasi_reg_id","left");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id","left");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id","left");
        $this->db->join("belajar_master_univ_fakultas f","a.belajar_fakultas_id = f.fakultas_id","left");
        $this->db->join("belajar_master_univ_jurusan g","a.belajar_jurusan_id = g.jurusan_id","left");
        $this->db->join("belajar_trans_rekomendasi_detail h","c.rekomendasi_reg_id= h.rekomendasi_reg_id","left");
        $this->db->where("a.belajar_reg_id", $belajar_reg_id);
        $this->db->where("a.jenis_belajar", 1);
        return $this->db->get()->result();
    }

    function detailverifikasi_bkpp($belajar_reg_id){
        $this->db->select("a.*, b.*,d.*, e.*, f.*, g.*");
        $this->db->from("registrasi_master_belajar a");
        $this->db->join("user_trans_detail_user b","a.nip = b.nip");
        $this->db->join("belajar_master_universitas d","a.belajar_univ_id = d.univ_id","left");
        $this->db->join("belajar_master_tingkat_pend e","a.belajar_tingpend_id = e.tingpend_id","left");
        $this->db->join("belajar_master_univ_fakultas f","a.belajar_fakultas_id = f.fakultas_id","left");
        $this->db->join("belajar_master_univ_jurusan g","a.belajar_jurusan_id = g.jurusan_id","left");
        $this->db->where("a.belajar_reg_id", $belajar_reg_id);
        $this->db->where("a.status_reg_belajar >=",4);
        return $this->db->get()->result();
    }

    function getattachmentbyposition($belajar_reg_id,$upload_proses=null){
        $this->db->where("belajar_reg_id",$belajar_reg_id);
        if($upload_proses !=null){
            $this->db->where("upload_proses",$upload_proses);
        }
        return $this->db->get("belajar_trans_upload")->result();
    }

    function getprogresslaporan($belajar_reg_id){
        $this->db->where("belajar_reg_id",$belajar_reg_id);
        return $this->db->get("belajar_trans_laporan")->result();
    }

    function getriwayatlulus($belajar_reg_id){
        $this->db->where("registrasi_belajar_id",$belajar_reg_id);
        return $this->db->get("belajar_trans_riwayat_pendidikan")->row();
    }

    function getsuratbyposition($belajar_reg_id,$posisi){
        $this->db->where("reg_belajar_id",$belajar_reg_id);
        return $this->db->get("belajar_trans_surat")->row($posisi);
    }

    public function insertSurat($table, $data,$belajar_reg_id){
        $this->db->where("reg_belajar_id",$belajar_reg_id);
        $count = $this->db->get("belajar_trans_surat")->num_rows();
        if($count == 0){
            return $this->db->insert_batch($table, $data);
        } else{
            return $this->db->update_batch($table, $data, "belajar_trans_posisi");
        }
    }

    public function insertOneSurat($table, $data,$belajar_reg_id,$posisi){
        $this->db->where("reg_belajar_id",$belajar_reg_id);
        $this->db->where("belajar_trans_posisi",$posisi);
        $count = $this->db->get("belajar_trans_surat")->num_rows();
        if($count == 0){
            return $this->db->insert($table, $data);
        } else{
            return $this->db->update($table, $data,array(
                "reg_belajar_id"=> $belajar_reg_id,
                "belajar_trans_posisi"=> $posisi
            ));
        }
    }

    function getKepalaSKPD($instansi){
        $this->db->select("kepala_dinas, jabatan_kepala");
        $this->db->where("nama_dinas",$instansi);
        return $this->db->get("integrasi_master_skpd")->row();
    }

    function getBKPP(){
        $this->db->select("kepala_dinas, jabatan_kepala");
        $this->db->where("kode_skpd","4.03.01");
        return $this->db->get("integrasi_master_skpd")->row();
    }

    public function save($table, $data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function update_tugasbelajar($belajar_reg_id,$data)
    {
        $this->db->where("belajar_reg_id",$belajar_reg_id);
        return $this->db->update("registrasi_master_belajar",$data);
    }

    public function update($table, $where, $data)
    {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }


    public function delete_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function updateDataLaporan($laporanid,$nama,$dokumen,$userid){
        $this->db->set('laporan_nama', $nama);
        if ($dokumen == null) {
        }else{
            $this->db->set('laporan_file', $dokumen);
        }
        $this->db->set('update_at', date("Y-m-d H:i:s"));
        $this->db->set('update_by', $userid);
        $this->db->where('laporan_id', $laporanid);
        return $this->db->update('belajar_trans_laporan');
    }
    
}

