<?php
/**
 * Created by PhpStorm.
 * User: takahashidandy
 * Date: 18/02/19
 * Time: 10.41
 */

class Event_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //=== SEMUA FUNGSI SELECT ALL
    public function listfield($table)
    {
        return $this->db->list_fields($table);
    }

    public function gettotalrows($table)
    {
        return $this->db->get($table)->num_rows();
    }

    function listjadwalAdmin($limit, $offset)
    {
        $this->db->distinct();
        $this->db->select("a.diklat_jadwal_id,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id","left");
        $this->db->where("a.status_event", 0);
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get();
    }

    function listjadwal($limit, $offset, $nip)
    {
        $this->db->select("a.diklat_jadwal_id,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->select("(SELECT count(nip) FROM registrasi_master_reg_online WHERE nip=".$nip." AND diklat_jadwal_id = a.diklat_jadwal_id) as is_registered");
        $this->db->from("diklat_trans_jadwal a");
        $this->db->join("registrasi_master_reg_online b","a.diklat_jadwal_id = b.diklat_jadwal_id","left");
        $this->db->where("a.status_event", 0);
        $this->db->limit($limit, $offset);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get();
    }

    function listevent($limit, $offset)
    {
        $this->db->where("status_event", 1);
        return $this->db->get("diklat_trans_jadwal", $limit, $offset);
    }

    //== SEMUA FUNGSI CRUD JADWAL
    function insertjadwal($data)
    {
        $this->db->insert("diklat_trans_jadwal", $data);
        return $this->db->insert_id();
    }

    function updatejadwal($id, $data)
    {
        $this->db->where("diklat_jadwal_id", $id);
        $this->db->update("diklat_trans_jadwal", $data);
    }

    function deletejadwal($id)
    {
        $this->db->where("diklat_jadwal_id", $id);
        $this->db->delete("diklat_trans_jadwal");
    }

    function getonejadwal($id, $status=0)
    {
        $this->db->select("a.*");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->where("a.status_event", $status);
        $this->db->where("a.diklat_jadwal_id", $id);
        return $this->db->get("diklat_trans_jadwal a");
    }

    function getattachment($id_jadwal)
    {
        $this->db->select("id_attachment,filepath");
        $this->db->where("id_jadwal", $id_jadwal);
        $this->db->where("id_option", 2);
        return $this->db->get("diklat_trans_upload");
    }

    function removeattachment($id_attachment){
        $this->db->select("filepath");
        $this->db->where("id_attachment", $id_attachment);
        $this->db->where("id_option", 2);
        $data["attachment"] =  $this->db->get("diklat_trans_upload")->result_array();
        $this->db->where("id_attachment",$id_attachment);
        $data["deleted"] = $this->db->delete("diklat_trans_upload");
        return $data;
    }

    function searchdiklatbyname($id, $string)
    {
        switch ($id) {
            case 1:
                $this->db->where("kategori_parent IS NOT NULL");
                $this->db->like("kategori_nama", $string);
                return $this->db->get("diklat_master_struktural_kategori", 10);
                break;
            case 2:
                $this->db->like("diklat_fungsional_nm", $string);
                return $this->db->get("diklat_master_diklat_fungsional", 10);
                break;
            case 3:
                $this->db->like("diklat_teknis_nm", $string);
                return $this->db->get("diklat_master_diklat_teknis", 10);
                break;
        }
    }

    private function autolivejadwal($date)
    {
        $this->db->select("diklat_jadwal_id, diklat_mulai,status_event");
        $this->db->from("diklat_trans_jadwal");
        $this->db->where("CAST(diklat_mulai as DATE) <",$date);
        $this->db->where("status_event",0);
        return $this->db->get();
    }

    private function autofinishjadwal($date)
    {
        $this->db->select("diklat_jadwal_id, diklat_mulai,status_event");
        $this->db->from("diklat_trans_jadwal");
        $this->db->where("CAST(diklat_selesai as DATE) <",$date);
        $this->db->where("status_event",1);
        return $this->db->get();
    }

    function autoupdatejadwal()
    {
        $date = date("Y-m-d");
        foreach ($this->autolivejadwal($date)->result() as $data){
            $updatedata = array(
                "diklat_jadwal_id"  => $data->diklat_jadwal_id,
                "status_event"  => 1,
            );
            $this->db->update("diklat_trans_jadwal",$updatedata,array("diklat_jadwal_id"=>$data->diklat_jadwal_id));
        }
        foreach ($this->autofinishjadwal($date)->result() as $data){
            $updatedata = array(
                "diklat_jadwal_id"  => $data->diklat_jadwal_id,
                "status_event"  => 2,
            );
            $this->db->update("diklat_trans_jadwal",$updatedata,array("diklat_jadwal_id"=>$data->diklat_jadwal_id));
        }
    }

    // FRONT PANEL

    function fplistjadwal($limit)
    {
        $this->db->select("a.diklat_jadwal_id,a.diklat_nama,a.diklat_deskripsi,a.diklat_mulai,a.diklat_selesai,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->where("status_event", 0);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get("diklat_trans_jadwal a", $limit);
    }

    function fplistevent($limit)
    {
        $this->db->select("a.diklat_jadwal_id,a.diklat_nama,a.diklat_deskripsi,a.diklat_mulai,a.diklat_selesai,a.diklat_jenis,a.diklat_penyelenggara,a.diklat_tempat,a.diklat_jumlah_jam,a.status_event");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->where("status_event", 1);
        $this->db->order_by("a.diklat_jadwal_id","DESC");
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        return $this->db->get("diklat_trans_jadwal a", $limit);
    }

    function fplistjadwalbyjenis($jenis,$limit,$offset)
    {
        $this->db->select("a.*");
        $this->db->select("(SELECT diklat_teknis_nm FROM diklat_master_diklat_teknis WHERE diklat_teknis_id = a.diklat_teknis_id) as teknis");
        $this->db->select("(SELECT diklat_fungsional_nm FROM diklat_master_diklat_fungsional WHERE diklat_fungsional_id = a.diklat_fungsional_id) as fungsional");
        $this->db->select("(SELECT kategori_nama FROM diklat_master_struktural_kategori WHERE kategori_id = a.kategori_id) as struktural");
        $this->db->where("status_event", 0);
        $this->db->order_by("a.diklat_jadwal_id", "DESC");
        if($jenis > 0){
            $this->db->where("diklat_jenis", $jenis);
        }
        if($limit > 0){
            return $this->db->get("diklat_trans_jadwal a", $limit,$offset);
        } else{
            return $this->db->get("diklat_trans_jadwal a");
        }
    }

    function save_diklat_trans_upload($data)
    {
        $this->db->insert("diklat_trans_upload", $data);
        return $this->db->insert_id();
    }
}
