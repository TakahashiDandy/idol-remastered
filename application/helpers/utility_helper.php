<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('asset_url()'))
{
    function asset_url()
    {
        return base_url().'assets/';
    }
}

if ( ! function_exists('vendor_url()'))
{
    function vendor_url()
    {
        return base_url().'vendor/';
    }
}

if ( ! function_exists('public_url()'))
{
    function public_url($path)
    {
        return base_url().'public/'.$path;
    }

}

if ( ! function_exists('admin_url()'))
{
    function admin_url($path)
    {
        return base_url().'admin/'.$path;
    }

}
