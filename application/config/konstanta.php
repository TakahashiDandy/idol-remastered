<?php
$config["dokumen_tb_1"] = array("Surat Rekomendasi Tugas Belajar",
    "Surat pernyataan bermaterai tidak pindah tugas",
    "Surat Pernyataan Bermaterai Pengembalian Biaya",
    "Surat keterangan dari lembaga pemberi beasiswa",
    "Pas Foto Berwarna Terbaru",
    "Ijazah Terakhir",
    "Transkrip Nilai Terakhir",
    "SK Pangkat Terakhir",
    "SK Jabatan Terakhir",
    "Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir",
    "Surat Permohonan Tugas Belajar",
    "Surat Keterangan OPD",
    "Surat Analisa Kebutuhan Pegawai"
);

$config["dokumen_tb_2"] = array(
    "Surat Permohonan Tugas Belajar",
    "Surat Keterangan OPD",
    "Surat Analisa Kebutuhan Pegawai"
);

$config["dokumen_ib_1"] = array("Surat Rekomendasi Izin Belajar",
    "Surat pernyataan bermaterai tidak pindah tugas",
    "Surat Pernyataan Bermaterai Pengembalian Biaya",
    "Surat keterangan dari lembaga pemberi beasiswa",
    "Pas Foto Berwarna Terbaru",
    "Ijazah Terakhir",
    "Transkrip Nilai Terakhir",
    "SK Pangkat Terakhir",
    "SK Jabatan Terakhir",
    "Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir",
    "Surat Keterangan Terdaftar Perguruan Tinggi / LOA",
    "Surat Permohonan Tugas Belajar",
    "Surat Keterangan OPD",
    "Surat Analisa Kebutuhan Pegawai"

);

$config["dokumen_ib_2"] = array("Surat Rekomendasi dari Kepala Perangkat Daerah",
    "Surat Keterangan dari Kepala Perangkat Daerah",
    "Surat Analisa Pegawai dari Kepala Perangkat Daerah",
    "Surat Permohonan yang bersangkutan ditujukan ke Wali Kota",
    "Surat Pernyataan bermaterai",
    "Surat Keterangan Terdaftar sebagai Mahasiswa dari Perguruan"
);

?>