<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html class="no-js" lang="en-US">


<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>
<link href=<?php echo asset_url() . 'fullcalendar/fullcalendar.min.css'?> rel='stylesheet' />
<link href=<?php echo asset_url() . 'fullcalendar/fullcalendar.print.min.css'?> rel='stylesheet' media='print' />

<body class="evant-page">


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>


<section class="page-name background-bg" data-image-src="https://demos.jeweltheme.com/courseware/images/breadcrumb.jpg">
    <div class="overlay">
        <div class="section">
            <div class="container">
                <h2 class="section-title">Events</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Events</li>
                    </ol>
                </nav>
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
</section><!-- /.page-name -->

<section class="events">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="calendar"></div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section>

<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>
<script src=<?php echo asset_url() . 'fullcalendar/lib/moment.min.js'?>></script>
<script src=<?php echo asset_url() . 'fullcalendar/lib/jquery.min.js'?>></script>
<script src=<?php echo asset_url() . 'fullcalendar/fullcalendar.min.js'?>></script>
<script>
    $(document).ready(function() {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next',
                center: 'title',
                right: ''
            },
            defaultDate: '<?php echo date("Y-m-d")?>',
            aspectRatio: 2,
            navLinks: true, // can click day/week names to navigate views
            editable: false,
            eventLimit: true, // allow "more" link when too many events
            events: <?php echo $calendar?>
        });

    });
</script>
</body>
</html>
