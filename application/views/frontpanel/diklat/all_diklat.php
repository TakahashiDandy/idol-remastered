<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html class="no-js" lang="en-US">


<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>

<body>


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>


<section class="page-name background-bg" data-image-src="https://demos.jeweltheme.com/courseware/images/breadcrumb.jpg">
    <div class="overlay">
        <div class="section">
            <div class="container">
                <form action="#" class="course-search-form">
                    <input type="text" name="search" id="search" class="search" placeholder="Find a course or tutorial ">
                    <input type="submit" name="submit" id="search-submit" class="sreach-submit">
                </form><!-- /.course-search-form -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
</section><!-- /.page-name -->




<section class="courses">
    <div class="section-padding">
        <div class="container">

            <div class="row">
                <div class="col-md-8">
                    <div class="filters">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="layout-switcher">
                                    <span class="grid"><i class="fa fa-th"></i></span>
                                    <span class="list"><i class="fa fa-list"></i></span>
                                </div><!-- /.layout-switcher -->
                                <p>
                                    Showing <?php //echo $page ." of " . $totalrows?>
                                </p>
                            </div>
                        </div>
                    </div><!-- /.filters -->

                    <div class="course-items">
                        <div class="row">
                            <?php foreach ($row->result() as $rows) :?>
                            <div class="col-lg-4 col-md-6">
                                <div class="item">
                                    <div class="item-thumb"><img src="https://demos.jeweltheme.com/courseware/images/popular/1.jpg" alt="Item Thumbnail"></div><!-- /.item-thumb -->
                                    <div class="item-details">
                                        <h3 class="item-title"><a href="<?php echo base_url("public/diklat/detail_diklat/").$rows->diklat_jadwal_id?>"><?php echo $rows->diklat_nama?></a></h3><!-- /.item-title -->
                                        <span class="instructor"><?php echo $rows->diklat_penyelenggara?></span><!-- /.instructor -->
                                        <div class="details-bottom">
                                            <div class="course-price float-left"><span><i class="icon-hourglass"></i></span><?php echo $rows->diklat_jumlah_jam?></div><!-- /.course-price -->
                                            <div class="rating float-right">
                                                <span>Maksimum Peserta :</span> <?php echo $rows->diklat_jumlah_peserta?>
                                            </div><!-- /.rating -->
                                        </div><!-- /.details-bottom -->
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->
                            </div>
                            <?php endforeach;?>
                        </div><!-- /.row -->

                        <nav aria-label="Page navigation example">
                            <?php echo $pagination; ?>
                        </nav>
                    </div><!-- /.course-items -->
                </div>

                <div class="col-md-4">
                    <aside class="sidebar">
                        <div class="category-list">
                            <ul>
                                <li class="<?php echo ($this->router->fetch_method() == "index" ? "active" : null)?>"><a href="<?php echo base_url("public/diklat")?>">All Courses</a></li>
                                <li class="<?php echo ($this->router->fetch_method() == "diklat_struktural" ? "active" : null)?>"><a href="<?php echo base_url("public/diklat/diklat_struktural")?>">Diklat Struktural</a></li>
                                <li class="<?php echo ($this->router->fetch_method() == "diklat_fungsional" ? "active" : null)?>"><a href="<?php echo base_url("public/diklat/diklat_fungsional")?>">Diklat Fungsional</a></li>
                                <li class="<?php echo ($this->router->fetch_method() == "diklat_teknis" ? "active" : null)?>"><a href="<?php echo base_url("public/diklat/diklat_teknis")?>">Diklat Teknis</a></li>
                            </ul>
                        </div>
                    </aside><!-- /.sidebar -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.courses -->


<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

</body>
</html>
