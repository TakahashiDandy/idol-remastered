
<!doctype html>

<html class="no-js" lang="en-US">

<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>
<body>


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>


<section class="page-name background-bg" data-image-src="https://demos.jeweltheme.com/courseware/images/breadcrumb.jpg">
    <div class="overlay">
        <div class="section">
            <div class="container">
                <form action="#" class="course-search-form">
                    <input type="text" name="search" id="search" class="search" placeholder="Cari Diklat Terbaru ">
                    <input type="submit" name="submit" id="search-submit" class="sreach-submit">
                </form><!-- /.course-search-form -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
</section><!-- /.page-name -->




<section class="courses">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <?php foreach ($jadwal->result() as $row):
                    $tglmulai = date("l, j F Y", strtotime($row->diklat_mulai));
                    $tglakhir = date("l, j F Y", strtotime($row->diklat_selesai));
                ?>
                <div class="col-md-8">
                    <h2 class="course-title"><?php echo $row->diklat_nama?></h2><!-- /.course-title -->

                    <img class="radius" src="https://demos.jeweltheme.com/courseware/images/single.jpg" alt="Course Image">

                    <div class="course-single-details">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-1" data-toggle="tab" href="#coursetype" role="tab" aria-controls="coursetype" aria-selected="false">Jenis Diklat</a>
                            <a class="nav-item nav-link" id="nav-2" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Deskripsi</a>
                            <a class="nav-item nav-link" id="nav-3" data-toggle="tab" href="#instructor" role="tab" aria-controls="instructor" aria-selected="false">Penyelenggara</a>

                        </div>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description">
                                <h4 class="title">Deskripsi Diklat</h4>
                                <p>
                                    <?php echo $row->diklat_deskripsi?>
                                </p>
                            </div>

                            <div class="tab-pane fade show active" id="coursetype" role="tabpanel" aria-labelledby="coursetype">
                                <h4 class="title">Jenis Diklat</h4>
                                <p>
                                    <?php switch ($row->diklat_jenis){
                                        case 1:
                                            echo "<p>Diklat Struktural - $row->struktural</p>";
                                            break;
                                        case 2:
                                            echo "<p>Diklat Fungsional - $row->fungsional</p>";
                                            break;
                                        case 3:
                                            echo "<p>Diklat Teknis - $row->teknis</p>";
                                            break;
                                    }?>
                                </p>
                            </div>

                            <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor">
                                <div class="author-bio">
                                    <h3 class="title">Penyelenggara</h3>
                                    <div class="author-contents media">
                                        <div class="author-details media-body">
                                            <p>
                                                <?php echo $row->diklat_penyelenggara?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- /.course-single-details -->

                </div>

                <div class="col-md-4">
                    <aside class="sidebar">
                        <a href="<?php echo base_url("public/registrasi/")?>" class="btn btn-lg enroll-btn">Daftar Sekarang</a>

                        <div class="info">
                            <ul class="info-list">
                                <li><i class="icon-clock"></i>  Jumlah Jam : <span class="text-dark"><?php echo $row->diklat_jumlah_jam?> Jam</span></li>
                                <li><i class="icon-location-pin"></i> Tempat : <span class="text-dark"><?php echo $row->diklat_tempat?></span></li>
                                <li><i class="icon-people"></i> Jumlah Peserta : <span class="text-dark"><?php echo $row->diklat_jumlah_peserta?> Orang</span></li>
                                <li><i class="icon-calendar"></i> Mulai : <span class="text-dark"><?php echo $tglakhir?></span></li>
                                <li><i class="icon-calendar"></i> Akhir : <span class="text-dark"><?php echo $tglmulai?></span></li>
                            </ul>
                        </div>
                    </aside><!-- /.sidebar -->
                </div>
                <?php endforeach;?>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.courses -->

<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

</body>
</html>
