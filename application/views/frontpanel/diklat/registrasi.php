<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html class="no-js" lang="en-US">


<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>

<body>


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>

    <section class="page-name">
        <div class="overlay">
            <div class="section">
                <div class="container">
                    <h2 class="section-title">Registrasi Online</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Registrasi Online</li>
                        </ol>
                    </nav>
                    <?php if($this->session->flashdata("success_message") != NULL) {
                        echo    "<div class=\"alert alert-success\">
                                <strong>Success!</strong> " . $this->session->flashdata("success_message") .
                                "</div>";
                    } else if($this->session->flashdata("error_message") != NULL){
                        echo    "<div class=\"alert alert-danger\">
                                <strong>Error!</strong> " . $this->session->flashdata("success_message") .
                                "</div>";
                    }
                    ?>
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </div><!-- /.overlay -->
    </section><!-- /.page-name -->


    <section class="contact">
        <!--Modal Loading-->
        <div class="section">
            <div class="container">
                <div class="respond">
                    <form action="<?php echo base_url("public/registrasi/")?>" method="post" class="comment-form">
                        <div class="row" id="modal-loading">
                            <div class="col align-self-center" style="min-height: 350px; text-align: center; font-size: 25px">
                                <i class="fa fa-spinner fa-spin align"></i>
                                <span>Loading Data Simpeg</span>
                            </div>
                        </div>
                    <!--Insert Phase 1-->
                        <div id="phase1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nip">NIP</label>
                                        <input type="text" class="form-control form-control-sm" name="nip" id="nip" placeholder="Masukan NIP" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="nip">Captcha</label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control form-control-sm" name="captcha" id="captcha" placeholder="Masukan Captcha" required="">
                                            </div>
                                            <div class="col-auto">
                                                <?php echo $captcha ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input class="btn " type="button" onclick="checkcaptcha()" value="Next">
                            </div>
                        </div>

                    <!--Insert Phase 2-->
                        <div id="phase2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Nama Lengkap" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Golongan</label>
                                        <input type="text" class="form-control form-control-sm" name="golongan" id="golongan" placeholder="Golongan" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Tempat Lahir</label>
                                        <input type="text" class="form-control form-control-sm" name="tempatlahir" id="tempatlahir" placeholder="Tempat Lahir" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Tanggal Lahir</label>
                                        <input type="text" class="form-control form-control-sm" name="tgllahir" id="tgllahir" placeholder="Tanggal Lahir" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Jenis Kelamin</label>
                                        <input type="text" class="form-control form-control-sm" name="jeniskelamin" id="jeniskelamin" placeholder="Jenis Kelamin" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nomor Kontak</label>
                                        <input type="text" class="form-control form-control-sm" name="nomorkontak" id="nomorkontak" placeholder="Nomor Kontak" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Email</label>
                                        <input type="text" class="form-control form-control-sm" name="email" id="email" placeholder="Email" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Alamat Rumah</label>
                                        <textarea class="form-control form-control-sm" rows="5" name="alamatrumah" id="alamatrumah" placeholder="Alamat Rumah" readonly></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Jabatan</label>
                                        <input type="text" class="form-control form-control-sm" name="jabatan" id="jabatan" placeholder="Jabatan" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Instansi</label>
                                        <input type="text" class="form-control form-control-sm" name="instansi" id="instansi" placeholder="Instansi" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Pendidikan Terakhir</label>
                                        <input type="text" class="form-control form-control-sm" name="pendidikanterakhir" id="pendidikanterakhir" placeholder="Pendidikan Terakhir" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Kab/Kota</label>
                                        <input type="text" class="form-control form-control-sm" name="kabkota" id="kabkota" placeholder="Kab/Kota" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Provinsi</label>
                                        <input type="text" class="form-control form-control-sm" name="provinsi" id="provinsi" placeholder="Provinsi" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Telepon/Fax</label>
                                        <input type="text" class="form-control form-control-sm" name="telpfax" id="telpfax" placeholder="Telp/Fax" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Alamat Kantor</label>
                                        <textarea class="form-control form-control-sm" rows="5" name="alamatkantor" id="alamatkantor" placeholder="Alamat Kantor" readonly></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="btn " type="button" onclick="phase1()" value="Prev">
                                <input class="btn " type="button" onclick="phase3()" value="Next">
                                <input class="btn btn-danger" type="reset" onclick="phase1()" value="Cancel">
                            </div>
                        </div>

                    <!--Insert Phase 3-->
                        <div id="phase3">
                            <div class="form-group">
                                <label for="nama">Pilih Diklat</label>
                                <div id="jadwaldiklat" class="form-group">

                                </div>
                            </div>
                            <div class="form-group">
                                <input class="btn " type="button" onclick="phase2()" value="Prev">
                                <input class="btn " type="button" id="submitregistrasi" onclick="checkregistrant()" value="Submit">
                                <input class="btn btn-danger" type="reset" onclick="phase1()" value="Cancel">
                            </div>
                        </div>

                    </form><!-- /.comment-form -->
                </div>
            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.contact-->

<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

<script>
    phase1();

    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

</script>

</body>
</html>
