<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IDOL - Sistem Diklat Online</title>
    <meta name="description" content="IDOL - Sistem Diklat Online Kota Bandung">
    <meta name="author" content="BKPP x Diskominfo Kota Bandung">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="icon" href="<?php echo asset_url()?>/courseware/img/favidol.png" sizes="32x32" type="image/png">
    <link rel="icon" href="<?php echo asset_url()?>/courseware/img/favidol.png" sizes="192x192" type="image/png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo asset_url()?>/courseware/img/favidol.png" type="image/png">

    <!-- Import Template Icons CSS Files -->

    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/all.css">
    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/simple-line-icons.css">

    <!-- Import Bootstrap CSS File -->

    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/bootstrap.css">

    <!-- Import External CSS Files -->

    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/magnific-popup.css">

    <!-- Import Template's CSS Files -->

    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/style.css">
    <link rel="stylesheet" href="<?php echo asset_url()?>/courseware/css/responsive.css">


</head>