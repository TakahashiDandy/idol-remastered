<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="site-footer">

    <div class="footer-bottom black-bg">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copy-right float-left">
                            <span> Copyright © 2018 <a href="https://demos.jeweltheme.com/courseware" target="_blank" rel="nofollow">Courseware</a>, All rights reservs  </span>
                        </div><!-- /.copy-right -->
                    </div>
                    <div class="col-md-4">
                        <ul class="menu float-right">
                            <li class="menu-item"><a href="https://demos.jeweltheme.com/courseware/#"> Privacy</a></li>
                            <li class="menu-item"><a href="https://demos.jeweltheme.com/courseware/#"> Terms</a></li>
                            <li class="menu-item"><a href="https://demos.jeweltheme.com/courseware/#"> Sitemap</a></li>
                        </ul>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.footer-bottom -->

</footer><!-- /.site-footer -->


<script src="<?php echo asset_url()?>/courseware/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo asset_url()?>/courseware/js/bootstrap.js"></script>
<script src="<?php echo asset_url()?>/courseware/js/plugins.js"></script>
<script src="<?php echo asset_url()?>/courseware/js/main.js"></script>
<script src="<?php echo asset_url()?>/courseware/js/diskominfo.js"></script>
