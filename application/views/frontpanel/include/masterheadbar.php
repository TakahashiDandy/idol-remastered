<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header class="masthead border-bottom">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10">
                <nav class="navbar navbar-expand-md">
                    <!-- LOGO IDOL -->
                    <a class="navbar-brand" href="<?php echo base_url("home")?>"><img src="<?php echo asset_url();?>courseware/img/logo-idol.png" alt="Logo"></a>

                    <!-- LOGO SITIMBEL -->
                    <!-- <a class="navbar-brand" href="<?php echo base_url("home")?>"><img src="<?php echo asset_url();?>courseware/img/logo-sitimbel.png" alt="Logo"></a> -->

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="navbar-nav">

                            <?php if($this->router->fetch_class() == "home") { ?>
                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "home"? "active" : NULL);?>">
                                <a class="nav-link dropdown-toggle" href="home" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Detail</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" onclick="showEvent()">Event</a>
                                    <a class="dropdown-item" onclick="showLive()">Live Event</a>
                                    <a class="dropdown-item" onclick="showClient()">Client</a>
                                </div>
                            </li>
                            <?php } else { ?>
                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "home"? "active" : NULL);?>" >
                                <a class="nav-link" href="<?php echo base_url()?>">Home</a>
                            </li>
                            <?php }; ?>

                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "registrasi"? "active" : NULL);?>" >
                                <a class="nav-link" href="<?php echo public_url("registrasi")?>">Registrasi Online</a>
                            </li>

                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "diklat"? "active" : NULL);?>">
                                <a class="nav-link" href="<?php echo public_url("diklat")?>">Diklat</a>
                            </li>

                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "jadwal"? "active" : NULL);?>" >
                                <a class="nav-link" href="<?php echo public_url("jadwal")?>">Jadwal</a>
                            </li>

                            <!-- <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "berita"? "active" : NULL);?>">
                                <a class="nav-link" href="<?php echo public_url("berita")?>">Berita</a>
                            </li> -->

                            <li class="nav-item menu-item-has-children dropdown <?php echo ($this->router->fetch_class() == "kontak"? "active" : NULL);?>">
                                <a class="nav-link" href="<?php echo public_url("kontak")?>">Kontak</a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div><!-- /.container -->
</header><!-- /.masthead -->