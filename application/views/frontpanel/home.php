<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html class="no-js" lang="en-US">

<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>

<body class="box-layout">

<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>

<section class="banner-section background-bg" data-image-src="<?php echo asset_url()?>courseware/img/cover-balaikota-bandung-1280x854-1.jpg">
    <div class="overlay">
        <div class="section-padding">
            <div class="container">
                <div class="banner-texts text-center">
                    <h4>Selamat Datang di IDOL</h4>
                    <p>Sistem Informasi Diklat Online</p>

                    <form action="#" class="course-search-form">
                        <input type="text" name="search" id="search" class="search" placeholder="Cari Diklat Terbaru">
                        <input type="submit" name="submit" id="search-submit" class="sreach-submit">
                    </form><!-- /.course-search-form -->
                </div><!-- /.banner-texts -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
</section><!-- /.banner-section -->


<section style="display: none" class="events events-02 mt-0">
    <div class="section-padding">
        <div class="container">
            <div class="top-content">
                <div class="left-content float-left">
                    <h2 class="section-title">Jadwal Diklat</h2>
                    <p>Jadwal Diklat yang akan dilaksanakan dalam waktu dekat.</p>
                </div><!-- /.left-content -->
                <div class="right-content float-right"><a href="#" class="btn">View all</a></div><!-- /.right-content -->
            </div><!-- /.top-content -->

            <div class="row">
                <?php if($jadwalnum > 0) :?>
                <div class="jadwal owl-carousel owl-theme owl-loaded">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                <?php foreach ($jadwal->result() as $row) :
                    $datexp = explode("-",$row->diklat_mulai);
                    $month = date("F",strtotime($row->diklat_mulai));
                    $day = $datexp[2];
                ?>
                            <div class="owl-item col-sm-4">
                                <div class="event media">
                                    <div class="event-time">
                                        <time datetime="<?php echo $row->diklat_mulai?>"><span class="date"><?php echo $day?></span><?php echo $month?></time>
                                    </div>
                                    <div class="event-details media-body">
                                        <div class="event-thumb radius"><img src="https://demos.jeweltheme.com/courseware/images/event/1.jpg" alt="Event Thumbnail"></div><!-- /.enent-thumb -->
                                        <h2 class="event-title"><a href="<?php echo base_url("public/diklat/detail_diklat/").$row->diklat_jadwal_id?>"><?php echo $row->diklat_nama?></a></h2><!-- /.event-title -->
                                        <div class="event-meta">
                                            <span class="time"><i class="icon-clock"></i> <?php echo $row->diklat_jumlah_jam?></span><!-- /.time -->
                                            <span class="place"><i class="icon-location-pin"></i> <?php echo $row->diklat_tempat?></span><!-- /.place -->
                                        </div><!-- /.event-meta -->
                                        <p>
                                            <?php echo $row->diklat_deskripsi?>
                                        </p>
                                    </div><!-- /.event-details -->
                                </div><!-- /.event -->
                            </div>
                <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php else :?>
                <div class="col align-content-md-center">
                    <span> Saat ini Tidak Ada Diklat dalam waktu dekat</span>
                </div>
                <?php endif;?>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section>


<section style="display: none" class="recent-posts recent-posts-02">
    <div class="section-padding">
        <div class="container">
            <div class="top-content">
                <div class="left-content float-left">
                    <h2 class="section-title">Live Diklat</h2>
                    <p>Diklat yang sedang berjalan.</p>
                </div><!-- /.left-content -->
                <div class="right-content float-right"><a href="#" class="btn">View all</a></div><!-- /.right-content -->
            </div><!-- /.top-content -->
            <div class="row"">
                <?php if($eventnum > 0) :?>
                    <div class="live owl-carousel owl-theme owl-loaded">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <?php foreach ($event->result() as $row) :?>
                                    <div class="owl-item col-sm-4">
                                        <div class="event media">
                                            <div class="event-details media-body">
                                                <div class="entry-thumbnail">
                                                    <iframe class="radius" width="560" height="315" src="https://www.youtube-nocookie.com/embed/tWYBiy5ee4I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </div>
                                                <h2 class="event-title"><a href="#"><?php echo $row->diklat_nama?></a></h2><!-- /.event-title -->
                                                <p>
                                                    <?php echo $row->diklat_deskripsi?>
                                                </p>
                                                <a href="https://www.youtube-nocookie.com/embed/tWYBiy5ee4I" class="load-more">Tonton Sekarang <i class="icon-control-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php else :?>
                    <div class="col align-content-md-center">
                        <span> Saat ini tidak ada Diklat yang sedang berjalan</span>
                    </div>
                <?php endif;?>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.recent-posts -->


<section style="display: none" class="popular-courses">

    <div class="section-padding">
        <div class="container">
            <div class="top-content">
                <div class="left-content pull-left">
                    <h2 class="section-title">Discover more</h2>
                    <p>Donec rutrum congue leo eget malesuada</p>
                </div><!-- /.left-content -->
            </div><!-- /.top-content -->

            <div class="course-items with-slider">
                <div id="course-slider" class="course-slider owl-carousel">
                    <div class="item">
                        <div class="item-thumb">
                            <img src="https://demos.jeweltheme.com/courseware/images/popular/1.jpg" alt="Item Thumbnail">
                        </div><!-- /.item-thumb -->
                        <div class="item-details">
                            <h3 class="item-title mt-0"><a href="https://demos.jeweltheme.com/courseware/#">Academics</a></h3><!-- /.item-title -->
                            <p>
                                He could see from the bed that it had been set for four o’clock as it should have been
                            </p>
                        </div><!-- /.item-details -->
                    </div><!-- /.item -->

                    <div class="item">
                        <div class="item-thumb">
                            <img src="https://demos.jeweltheme.com/courseware/images/popular/2.jpg" alt="Item Thumbnail">
                        </div><!-- /.item-thumb -->
                        <div class="item-details">
                            <h3 class="item-title mt-0"><a href="https://demos.jeweltheme.com/courseware/#">Faculty & stuff</a></h3><!-- /.item-title -->
                            <p>
                                True, he had not slept peacefully, but probably all the more deeply because of that
                            </p>
                        </div><!-- /.item-details -->
                    </div><!-- /.item -->

                    <div class="item">
                        <div class="item-thumb">
                            <img src="https://demos.jeweltheme.com/courseware/images/popular/3.jpg" alt="Item Thumbnail">
                        </div><!-- /.item-thumb -->
                        <div class="item-details">
                            <h3 class="item-title mt-0"><a href="https://demos.jeweltheme.com/courseware/#">Alumni</a></h3><!-- /.item-title -->
                            <p>
                                If he were to catch that he would have to rush like mad and samples was not packed
                            </p>
                        </div><!-- /.item-details -->
                    </div><!-- /.item -->

                    <div class="item">
                        <div class="item-thumb">
                            <img src="https://demos.jeweltheme.com/courseware/images/popular/4.jpg" alt="Item Thumbnail">
                        </div><!-- /.item-thumb -->
                        <div class="item-details">
                            <h3 class="item-title mt-0"><a href="https://demos.jeweltheme.com/courseware/#">Library</a></h3><!-- /.item-title -->
                            <p>
                                Even if he did catch the train he would not avoid his boss’s anger as the office assistant
                            </p>
                        </div><!-- /.item-details -->
                    </div><!-- /.item -->

                    <div class="item">
                        <div class="item-thumb">
                            <img src="https://demos.jeweltheme.com/courseware/images/popular/8.jpg" alt="Item Thumbnail">
                        </div><!-- /.item-thumb -->
                        <div class="item-details">
                            <h3 class="item-title mt-0"><a href="https://demos.jeweltheme.com/courseware/#">Academic</a></h3><!-- /.item-title -->
                            <p>
                                If he were to catch that he would have to rush like mad and samples was not packed
                            </p>
                        </div><!-- /.item-details -->
                    </div><!-- /.item -->
                </div><!-- /.course-slider -->
            </div><!-- /.course-items -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->

</section><!-- /.popular-courses -->



<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>
<script>
    $('.jadwal').owlCarousel({
        items:3,
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    });

    $('.live').owlCarousel({
        items:3,
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true
    });
</script>
</body>
</html>
