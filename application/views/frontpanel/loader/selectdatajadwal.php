<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<select name="jadwal_diklat" id="jadwal_diklat">
    <?php foreach ($jadwal->result() as $data) :?>
    <option value=<?php echo $data->diklat_jadwal_id;?>>
        <?php
        switch ($data->diklat_jenis){
            case 1:
                echo $data->struktural;
                break;
            case 2:
                echo $data->fungsional;
                break;
            case 3:
                echo $data->teknis;
                break;
        }
        ?>
    </option>
    <?php endforeach;?>
</select>

