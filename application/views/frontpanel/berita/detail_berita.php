<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>

<html class="no-js" lang="en-US">

<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>
<body>


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>


<section class="page-name background-bg" data-image-src="images/breadcrumb.jpg">
    <div class="overlay">
        <div class="section">
            <div class="container">
                <h2 class="section-title">Blog</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                </nav>
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </div><!-- /.overlay -->
</section><!-- /.page-name -->




<section class="blog-posts">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <article class="post type-post format-standard">
                        <div class="entry-thumbnail radius"><img src="images/posts/11.jpg" alt="Post Thumbnail"></div><!-- /.entry-thumbnail -->
                        <div class="entry-content media">
                            <div class="post-date">
                                <time datetime="2018-05-28"><span class="date">28</span> July</time>
                            </div><!-- /.post-date -->

                            <div class="content-details media-body">
                                <h3 class="entry-title"><a href="#">How To Choose Niche For Your Business Directory</a></h3><!-- /.entry-title -->
                                <div class="entry-meta">
                                    <span class="author"><i class="icon-user"></i> <a href="#">Anthony Doe</a></span><!-- /.author -->
                                    <span class="tag"><i class="icon-tag"></i> <a href="#">News</a></span><!-- /.tag -->
                                    <span class="comments"><i class="icon-bubbles"></i> <a href="#">13 comments</a></span><!-- /.tag -->
                                </div><!-- /.entry-meta -->

                                <p>
                                    <strong>The office assistant wasthe boss’s man,spineless, and with no understanding. What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never once yet been ill</strong>
                                </p>

                                <p>
                                    His boss would certainly come round with the doctor from the medical insurance company, accuse his parents of having a lazy son, and accept the doctor’s recommendation not to make any claim as the doctor believed that no-one was ever ill but that many were workshy. And what’s more, would he have been entirely wrong in this case? Gregor did in fact, apart from excessive sleepiness after sleeping for so long, feel completely well and even felt much hungrier than usual.
                                </p>

                                <p>
                                    He was still hurriedly thinking all this through, unable to decide to get out of the bed, when the clock struck quarter to seven. There was a cautious knock at the door near his head. Gregor, somebody called – it was his mother – it’s quarter to seven. Didn’t you want to go somewhere? That gentle voice, Gregor was shocked when he heard his own voice answering, it could hardly be recognised as the voice he had had before
                                </p>

                                <blockquote class="post-quote">
                                    That’ll be someone from work, he said to himself and froze very still, although his little legs only became all the more lively as they danced around. For a moment everything remained quiet
                                </blockquote>

                                <p>
                                    Gregor only needed to hear the visitor’s first words of greeting and he knew who it was the chief clerk himself. Why did Gregor have to be the only one condemned to work for a company where they immediately became highly suspicious at the slightest shortcoming? Were all employees, every one of them, louts, was there not one of them who was faithful and devoted who would go so mad with pangs of conscience that he couldn’t get out of bed if he didn’t spend at least a couple of hours
                                </p>

                                <p>
                                    Was it really not enough to let one of the trainees make enquiries assuming enquiries were even necessary did the chief clerk have to come himself, and did they have to show the whole, innocent family that this was so suspicious that only the chief clerk could be trusted to have the wisdom to investigate it? And more because these thoughts had made him upset than through any proper decision, he swang himself with all his force out of the bed.
                                </p>

                                <div class="content-bottom">
                                    <div class="tags float-left">
                                        <a href="#">HTML5</a>
                                        <a href="#">University</a>
                                        <a href="#">Courses</a>
                                    </div><!-- /.tags -->

                                    <div class="share dropdown float-right">
                                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            share <i class="fa fa-share-alt"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="twitter" href="#"><i class="icons icon-social-twitter"></i></a>
                                            <a class="facebook" href="#"><i class="icons icon-social-facebook"></i></a>
                                            <a class="pinterest" href="#"><i class="icons icon-social-pinterest"></i></a>
                                        </div><!-- /.dropdown-menu -->
                                    </div><!-- /.share -->
                                </div><!-- /.content-bottom -->

                            </div><!-- /.content-details -->
                        </div><!-- /.entry-content -->
                    </article><!-- /.post -->


                    <div class="post-navigation">
                        <div class="row">
                            <div class="col-md-6">
                                <article class="post type-post">
                                    <div class="entry-thumbnail"><img src="images/posts/12.jpg" alt="Thumbnail"></div><!-- /.entry-thumbnail -->
                                    <div class="entry-content">
                                        <h3 class="entry-title"><a href="single.html">WordPress Video Themes 2018 - Make Video Website Easily</a></h3>
                                        <div class="entry-meta">
                                            <span class="comment"><i class="icons icon-bubbles"></i> 13 comments</span>
                                        </div><!-- /.entry-meta -->
                                    </div><!-- /.entry-content -->
                                </article>
                            </div>
                            <div class="col-md-6">
                                <article class="post type-post">
                                    <div class="entry-thumbnail"><img src="images/posts/13.jpg" alt="Thumbnail"></div><!-- /.entry-thumbnail -->
                                    <div class="entry-content">
                                        <h3 class="entry-title"><a href="single.html">30+ Free WordPress Themes 2018 - Simple & Fast Loading</a></h3>
                                        <div class="entry-meta">
                                            <span class="comment"><i class="icons icon-bubbles"></i> 13 comments</span>
                                        </div><!-- /.entry-meta -->
                                    </div><!-- /.entry-content -->
                                </article>
                            </div>
                        </div>
                    </div><!-- /.post-navigation -->


                    <div class="author-bio">
                        <h3 class="title">About the author</h3>
                        <div class="author-contents media">
                            <div class="author-avatar float-left"><img class="radius" src="images/au.jpg" alt="Avatar"></div><!-- /.author-avatar -->
                            <div class="author-details media-body">
                                <h3 class="name"><a href="#">Julia Adams</a></h3>
                                <p>
                                    There was a painful and uncontrollable squeaking mixed in with it, the words could be made out at first but then there was a sort of echo which made them unclear, leaving the hearer unsure whether he had heard properly or not.
                                </p>
                                <div class="author-social">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-pinterest"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="comments">

                        <h2 class="title">25 Comments</h2>

                        <ol class="comment-list">
                            <li class="comment parent">
                                <div class="comment-body media">
                                    <img class="rounded-circle author-avatar" src="images/comments/1.jpg" alt="Comment Authors">

                                    <div class="comment-content media-body">
                                        <span class="time"><time datetime="2016-02-20 21:00">20-02-2016 at 21:37</time> </span>
                                        <span class="name"><a href="#">Anthony Doe</a></span>
                                        <p class="description">
                                            Gregor had wanted to give a full answer and explain everything but in the circumstances contented himself with saying- I’m getting up now.
                                        </p>
                                        <a href="#" class="btn reply-btn">Reply</a>
                                    </div><!-- /.comment-content -->
                                </div><!--/.comment-body-->

                                <ol class="children">
                                    <li class="comment">
                                        <div class="comment-body media">
                                            <img class="rounded-circle author-avatar" src="images/comments/2.jpg" alt="Comment Authors">

                                            <div class="comment-content media-body">
                                                <span class="time"><time datetime="2016-02-20 21:00">20-02-2016 at 21:37</time> </span>
                                                <span class="name"><a href="#">Anthony Doe</a></span>
                                                <p class="description">
                                                    The change in Gregor’s voice probably could not be noticed outside through the wooden door, as his mother was satisfied with this explanation and shuffled away
                                                </p>
                                                <a href="#" class="btn reply-btn">Reply</a>
                                            </div><!-- /.comment-content -->
                                        </div><!--/.comment-body-->
                                    </li>
                                </ol>
                            </li>

                            <li class="comment parent">
                                <div class="comment-body media">
                                    <img class="rounded-circle author-avatar" src="images/comments/4.jpg" alt="Comment Authors">

                                    <div class="comment-content media-body">
                                        <span class="time"><time datetime="2016-02-20 21:00">20-02-2016 at 21:37</time> </span>
                                        <span class="name"><a href="#">Anthony Doe</a></span>
                                        <p class="description">
                                            And yet, once in a while, he renders a head with such character, or a movement with such ease that we wonder whether he had not in him, after all, the making of a real artist.
                                        </p>
                                        <a href="#" class="btn reply-btn">Reply</a>
                                    </div><!-- /.comment-content -->
                                </div><!--/.comment-body-->
                            </li><!-- /.comment parent -->
                        </ol><!-- /.comment-list -->

                        <div class="respond">
                            <h2 class="title">Add Your Comment</h2>

                            <form action="#" method="post" class="comment-form">
                                <input class="form-control" name="author" type="text" placeholder="Name *" required>
                                <input class="form-control" name="email" type="email" placeholder="Email *" required>
                                <input class="form-control" name="url" type="url" placeholder="URL">
                                <textarea id="comment" class="form-control" name="comment" placeholder="Comment" rows="8" required></textarea>
                                <input class="btn" type="submit" value="Submit Comment">
                            </form><!-- /.comment-form -->
                        </div><!-- /.comment-respond -->
                    </div><!-- /.comment-area -->

                </div>

                <div class="col-md-4">
                    <aside class="sidebar">
                        <div class="widget widget_search">
                            <div class="widget-details">
                                <form method="get" class="search-form" action="#">
                                    <input type="text" class="form-control" placeholder="Search ..." name="s" title="Search here" required>
                                    <input type="submit" class="form-control">
                                </form>
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->

                        <div class="widget widget_categories">
                            <h2 class="widget-title">Categories</h2>
                            <div class="widget-details">
                                <ul>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">Photography</a></li>
                                    <li><a href="#">WordPress</a></li>
                                    <li><a href="#">Learning Press</a></li>
                                    <li><a href="#">HTML5</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->

                        <div class="widget widget_popular_post">
                            <h2 class="widget-title">Popular Posts</h2>
                            <div class="widget-details">
                                <article class="post type-post media">
                                    <div class="entry-thumbnail">
                                        <img src="images/widget/1.jpg" alt="post"/>
                                    </div><!--/.entry-thumbnail-->

                                    <div class="entry-content media-body">
                                        <h3 class="entry-title"><a href="#">WordPress Theme Development Resources</a></h3>
                                        <div class="entry-meta">
                                            <span class="time"><i class="icons icon-calendar"></i> <time datetime="2018-05-28">28 July, 2018</time></span>
                                        </div><!-- /.entry-meta -->
                                    </div><!--/.entry-content media-body-->
                                </article>
                                <article class="post type-post media">
                                    <div class="entry-thumbnail">
                                        <img src="images/widget/2.jpg" alt="post"/>
                                    </div><!--/.entry-thumbnail-->

                                    <div class="entry-content media-body">
                                        <h3 class="entry-title"><a href="#">How To Tell If A Site Is WordPress Or Not</a></h3>
                                        <div class="entry-meta">
                                            <span class="time"><i class="icons icon-calendar"></i> <time datetime="2018-05-28">28 July, 2018</time></span>
                                        </div><!-- /.entry-meta -->
                                    </div><!--/.entry-content media-body-->
                                </article>
                                <article class="post type-post media">
                                    <div class="entry-thumbnail">
                                        <img src="images/widget/3.jpg" alt="post"/>
                                    </div><!--/.entry-thumbnail-->

                                    <div class="entry-content media-body">
                                        <h3 class="entry-title"><a href="#">WordPress Themes 2018 : Responsive and Creative Design</a></h3>
                                        <div class="entry-meta">
                                            <span class="time"><i class="icons icon-calendar"></i> <time datetime="2018-05-28">28 July, 2018</time></span>
                                        </div><!-- /.entry-meta -->
                                    </div><!--/.entry-content media-body-->
                                </article>
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->

                        <div class="widget widget_tag_cloud">
                            <h2 class="widget-title">Tags Cloud</h2>
                            <div class="widget-details">
                                <div class="tagcloud">
                                    <a href="#">Theme</a>
                                    <a href="#">Template</a>
                                    <a href="#">Learning Press</a>
                                    <a href="#">WordPress</a>
                                    <a href="#">News</a>
                                    <a href="#">Development</a>
                                    <a href="#">HTML5</a>
                                    <a href="#">University</a>
                                    <a href="#">Courses</a>
                                </div><!--/.tagcloud-->
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->

                        <div class="widget widget_instagram">
                            <h2 class="widget-title">Instagram</h2>
                            <div class="widget-details">
                                <ul>
                                    <li><a href="#"><img src="images/insta/1.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/2.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/3.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/4.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/5.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/6.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/7.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/8.jpg" alt="flicker"></a></li>
                                    <li><a href="#"><img src="images/insta/9.jpg" alt="flicker"></a></li>
                                </ul>
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->

                        <div class="widget widget_archive">
                            <h2 class="widget-title">Archive</h2>
                            <div class="widget-details">
                                <ul>
                                    <li><a href="#">January 2018</a></li>
                                    <li><a href="#">February 2018</a></li>
                                    <li><a href="#">March 2018</a></li>
                                    <li><a href="#">April 2018</a></li>
                                    <li><a href="#">May 2018</a></li>
                                    <li><a href="#">June 2018</a></li>
                                </ul>
                            </div><!-- /.widget-details -->
                        </div><!--/.widget-->
                    </aside><!--/.sidebar-->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.section-padding -->
</section><!-- /.blog-posts -->


<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

</body>
</html>
