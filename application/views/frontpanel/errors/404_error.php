<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>

<html class="no-js" lang="en">

<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>

<body>


<div class="error-contents text-center">
    <a class="error-logo" href="index.html"><img src="images/logo.png" alt="Logo"></a>
    <div class="section-padding gray-bg">
        <h2 class="title">404</h2>
        <h3>
            Sorry, we can’t find the page you are looking for
        </h3>
        <a href="index.html" class="btn btn-lg mt-4">Back to homepage</a>
    </div>
</div>

<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

</body>
</html>
