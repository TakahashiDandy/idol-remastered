<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <?php if($this->session->flashdata("error_message")) {?>
                <div style="margin-top: 20px">
                    <div class="alert alert-danger" role="alert">
                        <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                    </div>
                </div>
                <?php }?>

                <!-- Card stats -->
                <?php if($this->session->flashdata("success_message")) {?>
                <div style="margin-top: 20px">
                    <div class="alert alert-success" role="alert">
                        <strong>Sukses!</strong> <?php echo $this->session->flashdata("success_message")?>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col-md-5">
                                <h3 class="text-white mb-0"><?php echo $tablename ?></h3>
                            </div>
                            <div class="col-md-7">
                                <div class="row text-right">
                                    <div class="col-md-7">
                                        <!-- Form -->
                                        <form class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                            <div class="form-group mb-0">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                    </div>
                                                    <input class="form-control" placeholder="Search" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-5">
                                        <!-- <i id="icon-sync" class="fas fa-sync fa-spin" style="font-size:25px"></i> -->
                                        <button id="btn-sync" type="button" class="btn btn-lg btn-primary" data-toggle="tooltip" data-placement="top" title="Integrasi dengan SIMPEG"> <i id="icon-sync" class="fas fa-sync">&nbsp;</i> Perbaharui Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark">
                            <thead class="thead-dark">
                            <tr>
                                <th>NIP</th>
                                <th>Nama Lengkap</th>
                                <th>Nama Pendidikan</th>
                                <th>Tanggal Lulus</th>
                                <th>No. Ijazah</th>
                                <th>Lokasi</th>
                                <th>Pejabat Penandatangan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($row->result() as $rows){
                                echo "<tr>";
                                if(!empty($rows->nip)){
                                    echo "<td scope=\"col\">$rows->nip</td>";
                                }else{
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->nama_lengkap)){
                                    echo "<td scope=\"col\">$rows->nama_lengkap</td>";
                                }else{
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->belajar_pendidikan_nm)){
                                    echo "<td scope=\"col\">$rows->belajar_pendidikan_nm</td>";
                                }else{
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->belajar_trans_riwayat_pendidikan_tgl)){
                                    echo "<td scope=\"col\">".tgl_indo($rows->belajar_trans_riwayat_pendidikan_tgl)."</td>";
                                }else{
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->belajar_trans_riwayat_sttb_ijazah)){
                                echo "<td scope=\"col\">$rows->belajar_trans_riwayat_sttb_ijazah</td>";
                                } else {
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->belajar_pend_lokasi)){
                                    echo "<td scope=\"col\">$rows->belajar_pend_lokasi</td>";
                                } else {
                                    echo "<td>-</td>";
                                }

                                if(!empty($rows->belajar_trans_riwayat_pejabat)){
                                    echo "<td scope=\"col\">$rows->belajar_trans_riwayat_pejabat</td>";
                                }else{
                                    echo "<td>-</td>";
                                }

                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer bg-gradient-default border-0">
                        <nav aria-label="Page navigation example">
                            <?php echo $pagination;?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
<script type="text/javascript">
    $(document).ready(function(){
        // $("#icon-sync").hide();
    });
    $("#btn-sync").click(function(){
        $.ajax({
            url: urlsyncsiriwayatpendidikan,
            beforeSend: function () {
                $("#icon-sync").addClass("fa-spin");
            },
            success: function (result) {
                try {
                    if(result == true){
                        alert("Data riwayat pendidikan berhasil disinkronisasikan.");
                    }else{
                        alert("Data gagal disinkronisasikan.");
                    }
                    location.reload();
                } catch (e) {
                    alert(e);
                }
            },
            complete: function () {
                $("#icon-sync").removeClass("fa-spin");
            }
        });
    });
</script>
</body>

</html>
