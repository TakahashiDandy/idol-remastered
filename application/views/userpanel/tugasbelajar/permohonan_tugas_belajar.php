<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Persyaratan Permohonan Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('user/tugasbelajar/unggah_persyaratan_permohonan/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">

                            <!--Kategori Diklat-->
                            <h6 class="heading-small text-muted mb-4">Unggah Persyaratan Permohonan Tugas Belajar</h6>
                            <hr class="my-4">

                            <!--Nama Diklat-->
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_1">
                                        Scan Surat Izin Rekomendasi dari Kepala Perangkat Daerah <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_1 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_1" id="upload_syarat_1" placeholder="Surat Rekomendasi dari Kepala Perangkat Daerah" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_2">
                                        Scan Surat Keterangan dari Lembaga Pemberi Beasiswa <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_2","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_2" id="upload_syarat_2" placeholder="Surat Keterangan dari Kepala Perangkat Daerah" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_3">
                                        Scan surat pernyataan bermaterai untuk tidak pindah tugas sekurang-kurangnya 2 (dua) kali masa pendidikan ditambah 1 (satu) tahun <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_3","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_3" id="upload_syarat_3" placeholder="Surat Analisa Pegawai dari Kepala Perangkat Daerah" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_4">
                                        Pas foto berwarna terbaru ukuran 3x4 sebanyak 2 (dua) buah <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_4","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_4" id="upload_syarat_4" placeholder="Surat Permohonan ditujukan ke Wali Kota melalui Sekretaris Daerah" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_5">
                                            Fotocopy ijazah terakhir dilegalisir <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_5","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_5" id="upload_syarat_5" placeholder="Surat Pernyataan bermaterai" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_6">
                                            Fotocopy transkrip nilai terakhir dilegalisir <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_6","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_6" id="upload_syarat_6" placeholder="Fotocopy transkrip nilai terakhir dilegalisir" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_7">
                                            Fotocopy SK terakhir <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_7","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_7" id="upload_syarat_7" placeholder="Fotocopy SK terakhir" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_8">
                                            Fotocopy SK Jabatan Terakhir (bagi yang menduduki Jabatan) <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_8","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_8" id="upload_syarat_8" placeholder="Fotocopy SK Jabatan Terakhir (bagi yang menduduki Jabatan)" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="upload_syarat_9">
                                            Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir <span style="color:red">(Maks. 1 Mb)</span>
                                        </label>
                                        <?php echo form_error("upload_syarat_6","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="upload_syarat_9" id="upload_syarat_9" placeholder="Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir" type="file" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="input_fields_wrap">
                            </div>

                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url("user/tugasbelajar")?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>