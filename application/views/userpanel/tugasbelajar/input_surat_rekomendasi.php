<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-2 pt-6">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Mengajukan Surat Rekomendasi Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('user/tugasbelajar/insertSuratRekomendasi/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">
                            <!--Kategori Diklat-->
                            <h5 class="heading-small text mb-4">Lengkapi Persyaratan</h5>
                            <hr class="my-4">
                            <h5 class="heading-small text mb-4">Data Peserta :</h5>

                            <input type="hidden" name="nip" id="nip" value="<?php if(!empty($detail_user->nip))echo $detail_user->nip; else echo "-" ?>">

                            <table class="table" border="0">
                                <tbody>
                                    <tr>
                                        <td width="17%">Nama</td>
                                        <td width="2%">:</td>
                                        <td><?php if(!empty($detail_user->nama_lengkap))echo $detail_user->nama_lengkap; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->nip))echo $detail_user->nip; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pangkat/Golongan</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->golongan))echo $detail_user->golongan; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->jabatan))echo $detail_user->jabatan; else echo "" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Unit Kerja</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->instansi))echo $detail_user->instansi; else echo "-" ?></td>
                                    </tr>
                                </tbody>
                            </table>

                            <hr class="my-4">
                            <h5 class="heading-small text mb-4">Data Universitas :</h5>

                            <!-- <div class="row"  style="margin-bottom: 20px">
                                <div class="col-md-3">
                                    <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariSekolah"><i class="fa fa-search"></i> Pilih Universitas</a>
                                </div>
                                <div class="col-md-3">
                                    <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariSekolah"><i class="fa fa-search"></i> Pilih Program Studi</a>
                                </div>
                            </div> -->

                            <table class="table" border="0">
                                <tbody>
                                    <tr>
                                        <td width="17%">Nama Universitas</td>
                                        <td width="2%">:</td>
                                        <td width="50%">
                                            <label style="white-space: normal" name="nama_universitas" id="nama_universitas"></label>
                                            <input type="hidden" name="id_universitas" id="id_univ">
                                            <input type="hidden" name="nama_universitas" id="nama_univ">
                                        </td>
                                        <td>
                                            <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariSekolah">Pilih</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tingkat Pendidikan</td>
                                        <td>:</td>
                                        <td>
                                            <label style="white-space: normal" name="tingpend_nm" id="tingpend_nm"></label>
                                            <input type="hidden" name="tingpend_id" id="tingpend_id">
                                        </td>
                                        <td>
                                            <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariTingpend">Pilih</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fakultas</td>
                                        <td>:</td>
                                        <td>
                                            <label style="white-space: normal" name="fakultas_nm" id="fakultas_nm"></label>
                                            <input type="hidden" name="fakultas_id" id="fakultas_id">
                                        </td>
                                        <td>
                                            <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariFakultas">Pilih</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Program Studi</td>
                                        <td>:</td>
                                        <td>
                                            <label style="white-space: normal" name="program_studi" id="program_studi"></label>
                                            <input type="hidden" name="program_studi_nama" id="program_studi_nama">
                                            <input type="hidden" name="id_program_studi" id="id_program_studi">
                                        </td>
                                        <td>
                                            <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#modalCariProdi">Pilih</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td>:</td>
                                        <td>
                                            <label style="white-space: normal" name="kota_universitas" id="kota_universitas"></label>
                                            <input type="hidden" name="kota_universitas" id="kota_univ">
                                        </td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="beasiswa">
                                            Beasiswa (Jika ada)
                                        </label>
                                        <?php echo form_error("beasiswa","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="beasiswa" id="beasiswa" placeholder="Beasiswa" type="text" value="">
                                    </div>
                                </div>
                            </div>

                            <label class="form-control-label" for="beasiswa">
                                Kirim ke
                            </label>
                            <br>

                            <div class="dropdown">
                                <select class="btn btn-secondary dropdown-toggle" name="jenis_pengiriman">
                                    <option class="dropdown-item" disabled="disabled" selected="true">Pilih Instansi</option>
                                    <option class="dropdown-item" value="0">OPD</option>
                                    <option class="dropdown-item" value="1">BKPP</option>
                                </select>
                            </div>
                            
                            <div class="input_fields_wrap">
                            </div>

                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url("user/tugasbelajar")?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalCariSekolah" tabindex="-1" role="dialog" aria-labelledby="modalCariSekolahLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Daftar Universitas/Perguruan Tinggi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="modal-title">Filter</h5>
                            <input class="form-control" name="search" id="textsearch" oninput="getdatarowuniversitas()" placeholder="Isikan Nama Universitas" type="text">
                            <hr class="my-4">
                            <h5 class="modal-title">Table Data</h5>
                            <div class="table-responsive">
                                <!-- Projects table -->
                                <table class="table align-items-center table-row-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <td>No. ID</td>
                                            <td>Nama Universitas</td>
                                            <td>Kota</td>
                                        </tr>
                                    </thead>
                                    <tbody id="modaldata">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalCariProdi" tabindex="-1" role="dialog" aria-labelledby="modalCariProdiLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalCariProdiLabel">Daftar Program Studi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="modal-title">Filter</h5>
                            <input class="form-control" name="search" id="textsearchProdi" oninput="getdatarowprodi()" placeholder="Isikan Nama Program Studi" type="text">
                            <hr class="my-4">
                            <h5 class="modal-title">Table Data</h5>
                            <div class="table-responsive">
                                <!-- Projects table -->
                                <table class="table align-items-center table-row-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <td>No. ID</td>
                                            <td>Nama Program Studi</td>
                                        </tr>
                                    </thead>
                                    <tbody id="modaldataprodi">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalCariTingpend" tabindex="-1" role="dialog" aria-labelledby="modalCariTingpendLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalCariProdiLabel">Daftar Tingkat Pendidikan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="modal-title">Filter</h5>
                            <input class="form-control" name="search" id="textsearchTingpend" oninput="getdatarowtingpend()" placeholder="Isikan Tingkat Pendidikan" type="text">
                            <hr class="my-4">
                            <h5 class="modal-title">Table Data</h5>
                            <div class="table-responsive">
                                <!-- Projects table -->
                                <table class="table align-items-center table-row-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <td>No.</td>
                                            <td>Tingkat Pendidikan</td>
                                        </tr>
                                    </thead>
                                    <tbody id="modaldatatingpend">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalCariFakultas" tabindex="-1" role="dialog" aria-labelledby="modalCariFakultasLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalCariFakultasLabel">Fakultas</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="modal-title">Filter</h5>
                            <input class="form-control" name="search" id="textsearchFakultas" oninput="getdatarowfakultas()" placeholder="Isikan Nama Fakultas" type="text">
                            <hr class="my-4">
                            <h5 class="modal-title">Table Data</h5>
                            <div class="table-responsive">
                                <!-- Projects table -->
                                <table class="table align-items-center table-row-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <td>No.</td>
                                            <td>Nama Fakultas</td>
                                        </tr>
                                    </thead>
                                    <tbody id="modaldatafakultas">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
        $(document).ready(function(){
            getdatarowuniversitas(1);
            getdatarowprodi(1);
            getdatarowtingpend(1);
            getdatarowfakultas(1);
        })
    </script>
</body>

</html>