<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-2 pt-6">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Laporan Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                        <div class="card-body">
                            <!--Kategori Diklat-->
                            <h5 class="heading-small text mb-4">Lengkapi Persyaratan</h5>
                            <hr class="my-4">
                            <h5 class="heading-small text mb-4">Data Peserta :</h5>

                            <input type="hidden" name="nip" id="nip"
                                value="<?php if(!empty($detail_user->nip))echo $detail_user->nip; else echo "-" ?>">

                            <table class="table" ;" border="0">
                                <tbody>
                                    <tr>
                                        <td width="17%">Nama</td>
                                        <td width="2%">:</td>
                                        <td><?php if(!empty($detail_user->nama_lengkap))echo $detail_user->nama_lengkap; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>NIP</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->nip))echo $detail_user->nip; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Pangkat/Golongan</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->golongan))echo $detail_user->golongan; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->jabatan))echo $detail_user->jabatan; else echo "" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Unit Kerja</td>
                                        <td>:</td>
                                        <td><?php if(!empty($detail_user->instansi))echo $detail_user->instansi; else echo "-" ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <hr class="my-4">
                            <h5 class="heading-small text mb-4">Data Tugas Belajar :</h5>

                            <table class="table" ;" border="0">
                                <tbody>
                                    <tr>
                                        <td width="17%">Nama Universitas / Kampus</td>
                                        <td width="2%">:</td>
                                        <td><?php if(!empty($data_master_belajar[0]->belajar_pendidikan_nm))echo $data_master_belajar[0]->belajar_pendidikan_nm; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tingkat</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_master_belajar[0]->print_tingpend))echo $data_master_belajar[0]->print_tingpend; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fakultas</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_master_belajar[0]->fakultas_nm))echo $data_master_belajar[0]->fakultas_nm; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jurusan</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_master_belajar[0]->jurusan_nm))echo $data_master_belajar[0]->jurusan_nm; else echo "-" ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Pendidikan Lokasi</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_master_belajar[0]->belajar_pend_lokasi))echo $data_master_belajar[0]->belajar_pend_lokasi; else echo "" ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr class="my-4">
                            <h5 class="heading-small text mb-4">Data Laporan :</h5>
                            <a href="<?php echo base_url("user/tugasbelajar/tambahLaporan/".$data_master_belajar[0]->belajar_reg_id)?>"
                                class="btn btn-icon btn-3 btn-primary btn-md">
                                <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                <span class="btn-inner--text">Tambah Laporan</span>
                            </a>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table align-items-center table-white">
                                    <thead class="thead-white">
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Laporan</th>
                                            <th>File Laporan</th>
                                            <th>Aksi (Shortcut)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                    $i = 0;
                                                    foreach ($data_laporan as $laporan) {
                                                        $i++;
                                                        echo '<tr>';
                                                        echo '<td style="white-space:normal">'.$i.'</td>';
                                                        echo '<td style="white-space:normal">'.$laporan->laporan_nama.'</td>';
                                                        echo '<td style="white-space:normal">
                                                             <a href="'.base_url($laporan->laporan_file).'" target="_blank"
                                                                 class="btn btn-icon btn-3 btn-primary btn-sm">
                                                                 <span class="btn-inner--icon"><i
                                                                         class="ni ni-bold-down"></i></span>
                                                                 <span class="btn-inner--text">Download Laporan</span>
                                                             </a>
                                                                </td>';
                                                        echo '<td style="white-space:normal">
                                                             <a href="'.base_url("user/tugasbelajar/ubahLaporan/".$laporan->laporan_id).'"
                                                                 class="btn btn-icon btn-3 btn-primary btn-sm tambahan-margin-button-table">
                                                                 <span class="btn-inner--icon"><i
                                                                         class="fa fa-edit"></i></span>
                                                                 <span class="btn-inner--text">Ubah</span>
                                                             </a>
                                                             <button type="button" class="btn btn-icon btn-3 btn-warning btn-sm tambahan-margin-button-table" 
                                                             data-toggle="modal" 
                                                             data-target="#modalHapusDataLaporan" 
                                                             data-laporanid="'.$laporan->laporan_id.'">
                                                                <span class="btn-inner--icon"><i class="fa fa-trash"></i></span>
                                                                <span class="btn-inner--text">Hapus</span></button>
                                                            </td>';

                                                    }
                                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr class="my-4">
                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <a href="<?php echo base_url("user/tugasbelajar")?>"
                                        class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalHapusDataLaporan" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#fb6340">
                            <h5 class="modal-title" id="exampleModalLabel" style="color:#fff">Hapus Data Laporan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda yakin ingin menghapus data laporan ?</p>
                            <form method="post" action="<?php echo base_url('user/tugasbelajar/prosesHapusLaporan');?>">
                                <input type="hidden" class="form-control" id="laporanid" name="laporanid">
                                <input type="hidden" class="form-control" id="regbelajarid" name="regbelajarid"
                                    value="<?php echo $data_master_belajar[0]->belajar_reg_id;?>">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Hapus Laporan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Argon Scripts -->
            <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
            <script>
                $('#modalHapusDataLaporan').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var laporanid = button.data('laporanid') // Extract info from data-* attributes
                    var modal = $(this)
                    modal.find('#laporanid').text('' + laporanid)
                    modal.find('#laporanid').val(laporanid)
                })
            </script>
</body>

</html>