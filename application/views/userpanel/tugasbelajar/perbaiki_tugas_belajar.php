<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Perbaikan. Unggah Ulang Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url("user/tugasbelajar/prosesPerbaikiTugasBelajar")?>" enctype="multipart/form-data" role="form">
                            <!--Kategori Diklat-->
                            <h6 class="heading-small text-muted mb-2">Unggah Berupa File berformat <strong> (PNG/JPG/PDF) </strong>dengan Maksimal Ukuran <strong>1MB</strong></h6>
                            <hr class="my-4">
                            <h6 class="heading-small text-muted mb-2">Unggah File Persyaratan</h6>
                            <input type="hidden" name="belajar_reg_id" value="<?php echo $belajar_reg_id;?>">
                            <!--Nama Diklat-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_surat_rekom">
                                        Scan Surat Rekomendasi Tugas Belajar
                                        </label>
                                        <?php echo form_error("inp_dokumen_1 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="0">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Surat_Rekomendasi">
                                        <input class="form-control" name="inp_dokumen_1" id="inp_dokumen_1" placeholder="Surat Rekomendasi Tugas Belajar" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_materai_1">
                                        Scan Surat pernyataan bermaterai tidak pindah tugas
                                        </label>
                                        <?php echo form_error("inp_dokumen_2 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="1">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Surat_Pernyataan1">
                                        <input class="form-control" name="inp_dokumen_2" id="inp_dokumen_2" placeholder="Surat pernyataan bermaterai tidak pindah tugas" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_materai_2">
                                        Scan Surat Pernyataan Bermaterai Pengembalian Biaya
                                        </label>
                                        <?php echo form_error("inp_dokumen_3 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="2">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Surat_Pernyataan2">
                                        <input class="form-control" name="inp_dokumen_3" id="inp_dokumen_3" placeholder="Surat Pernyataan Bermaterai Pengembalian Biaya" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_lembaga">
                                        Scan Surat keterangan dari lembaga pemberi beasiswa
                                        </label>
                                        <?php echo form_error("inp_dokumen_4 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="3">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Surat_Beasiswa">
                                        <input class="form-control" name="inp_dokumen_4" id="inp_dokumen_4" placeholder="Surat keterangan dari lembaga pemberi beasiswa" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>  
                            <hr class="my-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_pas_foto">
                                        Pas Foto Berwarna Terbaru
                                        </label>
                                        <?php echo form_error("inp_dokumen_5 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="4">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Pas_foto">
                                        <input class="form-control" name="inp_dokumen_5" id="inp_dokumen_5" placeholder="Pas Foto" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_ijazah">
                                        Scan Ijazah Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_6 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="5">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Ijazah">
                                        <input class="form-control" name="inp_dokumen_6" id="inp_dokumen_6" placeholder="Ijazah" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_transkrip">
                                        Scan Transkrip Nilai Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_7 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="6">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Transkrip_Nilai">
                                        <input class="form-control" name="inp_dokumen_7" id="inp_dokumen_7" placeholder="Transkrip Nilai" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_sk">
                                        Scan SK Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_8 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="7">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="SK">
                                        <input class="form-control" name="inp_dokumen_8" id="inp_dokumen_8" placeholder="SK" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_sk_jabatan">
                                        Scan SK Jabatan Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_9 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="8">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="SK_Jabatan">
                                        <input class="form-control" name="inp_dokumen_9" id="inp_dokumen_9" placeholder="SK Jabatan Terakhir" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_prestasi">
                                        Scan Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_10 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar[]" value="9">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar[]" value="Prestasi">
                                        <input class="form-control" name="inp_dokumen_10" id="inp_dokumen_10" placeholder="Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="input_fields_wrap">
                            </div>

                            <hr class="my-4">
                            <h6 class="heading-small text-muted mb-2">Unggah File Pengajuan</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_surat_permohonan">
                                        Scan Surat Permohonan Tugas Belajar
                                        </label>
                                        <?php echo form_error("inp_dokumen_2_1 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar_2[]" value="0">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar_2[]" value="Surat_Permohonan">
                                        <input class="form-control" name="inp_dokumen_2_1" id="inp_dokumen_2_1" placeholder="Scan Surat Permohonan Tugas Belajar" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_keterangan">
                                        Scan Surat Keterangan OPD
                                        </label>
                                        <?php echo form_error("inp_dokumen_2_2 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar_2[]" value="1">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar_2[]" value="Surat_Keterangan">
                                        <input class="form-control" name="inp_dokumen_2_2" id="inp_dokumen_2_2" placeholder="Surat Keterangan OPD" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_analisa">
                                        Scan Surat Analisa Kebutuhan Pegawai
                                        </label>
                                        <?php echo form_error("inp_dokumen_2_3 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_tugas_belajar_2[]" value="2">
                                        <input type="hidden" name="jenis_dokumen_syarat_tugas_belajar_2[]" value="Analisa_Kebutuhan">
                                        <input class="form-control" name="inp_dokumen_2_3" id="inp_dokumen_2_3" placeholder="Surat Analisa Kebutuhan Pegawai" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4">


                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url("user/tugasbelajar/detailprosestugasbelajar/".$belajar_reg_id)?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>