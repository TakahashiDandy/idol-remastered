<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Laporan Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                        <div class="card-body">
                            <form method="post" action="<?php echo base_url("user/tugasbelajar/prosesTambahLaporan/") . $this->uri->segment(4)?>" enctype="multipart/form-data" role="form">
                                <h6 class="heading-small text-muted mb-4">Laporan Tugas Belajar</h6>

                                <input type="hidden" name="nip" id="nip"
                                    value="<?php if(!empty($detail_user->nip))echo $detail_user->nip; else echo "-" ?>">
                                <input type="hidden" name="belajarid" id="belajarid"
                                    value="<?php echo $belajar_reg_id; ?>">

                                <hr class="my-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="inp_namalap">
                                                Nama Laporan Tugas Belajar
                                            </label>
                                            <?php echo form_error("inp_notelp","<h5 class='text-danger'>","</h6>") ?>
                                            <input class="form-control" name="inp_namalap" id="inp_namalap"
                                                placeholder="Nama Laporan" type="text" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="inp_dok_lap">
                                                Laporan Tugas Belajar
                                            </label>
                                            <?php echo form_error("inp_dok_lap","<h5 class='text-danger'>","</h6>") ?>
                                            <input class="form-control" name="inp_dok_lap" id="inp_dok_lap"
                                                placeholder="Laporan Tugas Belajar" type="file"
                                                value="">
                                        </div>
                                    </div>
                                </div>


                                <div class="input_fields_wrap">
                                </div>

                                <hr class="my-4">

                                <!--Action Button-->
                                <div class="row">
                                    <div class="col-lg-auto align-self-center">
                                        <button type="submit" class="btn btn-success">Save</button>
                                        <a href="<?php echo base_url("user/tugasbelajar/laporan/".$belajar_reg_id)?>"
                                            class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
        $(document).ready(function(){
            getdatarowuniversitas(1);
            getdatarowprodi(1);
            getdatarowtingpend(1);
            getdatarowfakultas(1);
        })
    </script>
</body>

</html>