<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Pengajuan Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                        <div class="card-body">
                            <div class="nav-wrapper">
                                <ul class="nav nav-pills nav-pills-circle" id="tabs_2" role="tablist">
                                    <?php $i = 1; ?>
                                    <?php foreach ($data_rows as $value) {
                                        if($value['index'] % 2 == 0){ 
                                            echo '<li class="nav-item">
                                                <a class="nav-link rounded-circle" style="background-color:#F4F7FA;-webkit-box-shadow: none;
                                                        -moz-box-shadow: none;box-shadow: none;"" aria-controls="home" aria-selected="true">
                                                    <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                                </a>
                                            </li>'; 
                                        } 
                                        else{ 
                                            echo '<li class="nav-item">
                                                <a class="nav-link rounded-circle '.$value['class_active'].' '.$value['status_nav'].' " id="proses'.$i.'-tab" data-toggle="tab" href="#'.$value['navigation'].'" role="tab" aria-controls="home" aria-selected="'.$value['aria_selected'].'">
                                                    <span class="nav-link-icon d-block">'.$i++.'</span>
                                                </a>
                                            </li>';
                                            
                                        }
                                    } ?>
                                </ul>
                            </div>
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="tab-content" id="myTabContent">
                                        <?php foreach ($data_rows as $value) { ?>
                                            <?php switch ($value['index']) {
                                                
                                                case 1: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 1. Membuat/Mengajukan Surat Rekomendasi Tugas Belajar</h4>
                                                    <p></p>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="description"> Persiapkan Data Diri :<br>
                                                        1. Nama Peserta/ASN yang bersangkutan;<br>
                                                        2. NIP Peserta/ASN yang bersangkutan;<br>
                                                        3. Pangkat/Golongan Peserta/ASN yang bersangkutan;<br>
                                                        4. Jabatan Peserta/ASN yang bersangkutan;<br>
                                                        5. Unit Kerja Peserta/ASN yang bersangkutan;<br>
                                                    </p>
                                                    <p class="description"> Data Sekolah/Universitas yang dituju : <br>
                                                        1. Tingkat Pendidikan;<br>
                                                        2. Nama Sekolah/Universitas;<br>
                                                        3. Fakultas;<br>
                                                        4. Program Studi;<br>
                                                    </p>
                                                    <a href="<?php echo base_url("user/tugasbelajar/inputTugasBelajar")?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Isi Persyaratan</span>
                                                    </a>
                                                </div> <?php
                                                break;
                                                
                                                case 3: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 2. Proses Persetujuan BKPP</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="message">Silahkan unduh dan klik tombol lanjutkan untuk mengunggah kembali surat rekomendasi yang sudah ditandatangani oleh BKPP.</p>
                                                    <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="<?php echo base_url('user/tugasbelajar/cetak/'.$belajar_reg_id)?>" target="_blank"
                                                            class="btn btn-icon btn-3 btn-primary btn-md">
                                                            <span class="btn-inner--icon"><i class="fa fa-print"></i></span>
                                                            <span class="btn-inner--text">Print</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <ul class="nav nav-pills" id="tabs_2" role="tablist">
                                                            <li class='nav-item'>
                                                            <button onclick="submit_proses(3)" class="btn btn-icon btn-3 btn-success btn-md">
                                                                <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                                <span class="btn-inner--text">Lanjutkan</span>
                                                            </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    </div>
                                                </div> <?php
                                                break;

                                                case 4: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 3. Permohonan Tugas Belajar kepada BKPP</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>">Persyaratan Permohonan Tugas Belajar Sedang Diperiksa</span>
                                                    </p>
                                                    <p class="description"> Silahkan tunggu hasil verifikasi berkas persyaratan permohonan tugas belajar anda.<br>
                                                    </p>
                                                    <a href="<?php echo base_url('user/tugasbelajar')?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>
                                                        <span class="btn-inner--text">Kembali</span>
                                                    </a>
                                                </div> <?php
                                                break;
                                                
                                                case 5: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 3. Permohonan Tugas Belajar kepada BKPP</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="description"> Persiapkan Data untuk di unggah:<br>
                                                        1. Scan Surat Rekomendasi dari Kepala Perangkat Daerah;<br>
                                                        2. Scan Surat Keterangan dari Kepala Perangkat Daerah;<br>
                                                        3. Scan Surat Analisa Pegawai dari Kepala Perangkat Daerah <br>
                                                        4. Scan Surat Permohonan yang bersangkutan ditujukan ke Wali Kota
                                                        melalui Sekretaris Daerah;<br>
                                                        5. Scan Surat Pernyataan bermaterai;<br>
                                                        6. Scan Surat Keterangan Terdaftar sebagai Mahasiswa dari Perguruan
                                                        Tinggi;<br>
                                                    </p>
                                                    <a href="<?php echo base_url('user/tugasbelajar/ajukanPermohonanTugasBelajar/'.$belajar_reg_id)?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Kerjakan</span>
                                                    </a>
                                                </div> <?php
                                                break;
                                                
                                                case 7: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 4. Proses Persetujuan DJPP</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="message">Silahkan segera diperbaiki</p>
                                                    <p class="description"> Catatan Perbaikan :<br>
                                                        1. Scan Surat Rekomendasi dari Kepala Perangkat Daerah;<br>
                                                        2. Scan Surat Keterangan dari Kepala Perangkat Daerah;<br>
                                                    </p>
                                                    <a href="<?php echo base_url("user/tugasbelajar/editTugasBelajarBKPP")?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Perbaiki</span>
                                                    </a>
                                                </div>
                                                 <?php
                                                break;
                                                
                                                case 9: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 5. Progress dan Laporan Tugas Belajar</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="message">Silahkan untuk mengupload laporan Tugas belajar</p>
                                                    <p class="description"> Catatan :<br>
                                                        1. Laporan berupa file;<br>
                                                    </p>
                                                    <a href="<?php echo base_url("user/tugasbelajar/tambahLaporanTugasBelajar")?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Tambah Laporan</span>
                                                    </a>
                                                    <a href="<?php echo base_url("user/tugasbelajar/tambahLaporanTugasBelajar")?>"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                                        <span class="btn-inner--text">Selesai Belajar</span>
                                                    </a>
                                                    <hr>
                                                    <div class="table-responsive">
                                                        <table class="table align-items-center table-dark"
                                                            ">
                                                            <thead class="thead-dark">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Nama Laporan</th>
                                                                    <th>File Laporan</th>
                                                                    <th>Aksi (Shortcut)</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="white-space:normal">1
                                                                    </td>
                                                                    <td style="white-space:normal">Laporan Nilai UTS Semester 1
                                                                    </td>
                                                                    <td style="white-space:normal">
                                                                        <a href="tugasbelajarlaporan/apa.pdf"
                                                                            class="btn btn-icon btn-3 btn-primary btn-sm">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="ni ni-bold-down"></i></span>
                                                                            <span class="btn-inner--text">Download Laporan</span>
                                                                        </a>
                                                                    </td>
                                                                    <td style="white-space:normal">
                                                                        <a href=""
                                                                            class="btn btn-icon btn-3 btn-primary btn-sm tambahan-margin-button-table">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="fa fa-edit"></i></span>
                                                                            <span class="btn-inner--text">Ubah</span>
                                                                        </a>
                                                                        <a href=""
                                                                            class="btn btn-icon btn-3 btn-warning btn-sm tambahan-margin-button-table">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="fa fa-trash"></i></span>
                                                                            <span class="btn-inner--text">Hapuss</span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="white-space:normal">2
                                                                    </td>
                                                                    <td style="white-space:normal">Laporan Nilai UAS Semester 1
                                                                    </td>
                                                                    <td style="white-space:normal">
                                                                        <a href="tugasbelajarlaporan/apa.pdf"
                                                                            class="btn btn-icon btn-3 btn-primary btn-sm">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="ni ni-bold-down"></i></span>
                                                                            <span class="btn-inner--text">Download Laporan</span>
                                                                        </a>
                                                                    </td>
                                                                    <td style="white-space:normal">
                                                                        <a href=""
                                                                            class="btn btn-icon btn-3 btn-primary btn-sm tambahan-margin-button-table">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="fa fa-edit"></i></span>
                                                                            <span class="btn-inner--text">Ubah</span>
                                                                        </a>
                                                                        <a href=""
                                                                            class="btn btn-icon btn-3 btn-warning btn-sm tambahan-margin-button-table">
                                                                            <span class="btn-inner--icon"><i
                                                                                    class="fa fa-trash"></i></span>
                                                                            <span class="btn-inner--text">Hapuss</span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                 <?php
                                                break;
                                                
                                                case 11: ?> 
                                                <div class="tab-pane fade <?php echo $value['status_nav']; ?>" id="tabs-icons-text-<?php echo $value['index']?>" role="tabpanel"
                                                    aria-labelledby="tabs-icons-text-<?php echo $value['index']?>-tab">
                                                    <h4>Tahap 6. Tugas Belajar Selesai</h4>
                                                    <p>
                                                        <br>Status : <span class="badge <?php echo $value['badge_status'] ?>"><?php echo $value['status_registrasi']; ?></span>
                                                    </p>
                                                    <p class="description">Selamat anda telah menyelesaikan Tugas belajar. Silahkan cek data Tugas belajarmu di link berikut : <a href="">Klik Disini</a> </p>
                                                </div>
                                                 <?php
                                                break;
                                                
                                                default:
                                                break;
                                            } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
        function submit_proses(e){
            $('#proses'+e+'-tab').click();
            $('#proses'+(e-1)+'-tab').addClass("nav-disabled-success");
            if((e+2) == 5){
                $('#tabs-icons-text-5 > p > span').text("Pengumpulan Berkas Persyaratan Permohonan Tugas Belajar");
                $('#tabs-icons-text-5 > p > span').addClass("badge-default");
            }
            if((e+2) == 7){
                $('#tabs-icons-text-7 > p > span').text("Upload Berkas Persyaratan");
                $('#tabs-icons-text-7 > p > span').addClass("badge-default");
            }
            if((e+2) == 9){
                $('#tabs-icons-text-9 > p > span').text("Upload Berkas Persyaratan");
                $('#tabs-icons-text-9 > p > span').addClass("badge-default");
            }
            if((e+2) == 11){
                $('#tabs-icons-text-11 > p > span').text("Upload Berkas Persyaratan");
                $('#tabs-icons-text-11 > p > span').addClass("badge-default");
            }
        }
    </script>
</body>

</html>