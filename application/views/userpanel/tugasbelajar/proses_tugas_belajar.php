<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <?php
        $now_data_master_belajar = $data_master_belajar->result();
        $now_data_master_belajar_trans_upload = $data_master_belajar_trans_upload->result();
        $now_data_master_belajar_trans_tolak = $data_master_belajar_trans_tolak->result();
        $now_data_master_belajar_trans_upload_2 = $data_master_belajar_trans_upload_2->result();
        $now_data_master_belajar_trans_laporan = $data_master_belajar_trans_laporan->result();
        $now_data_master_rekomendasi = $data_master_rekomendasi->result();
        $nav_default = "active";
        $nav_disable = "disabled nav-disabled-default";
        $nav_success = "nav-disabled-success";
        $nav_final = "active nav-disabled-success";
        $nav_failed = "active nav-disabled-failed";
        $nav_forbidden = "disabled nav-disabled-failed";
        $array_dokumen_upload = array("Surat Rekomendasi Tugas Belajar", 
        "Surat pernyataan bermaterai tidak pindah tugas", 
        "Surat Pernyataan Bermaterai Pengembalian Biaya", 
        "Surat keterangan dari lembaga pemberi beasiswa",
        "Pas Foto Berwarna Terbaru",
        "Ijazah Terakhir",
        "Transkrip Nilai Terakhir",
        "SK Terakhir",
        "SK Jabatan Terakhir",
        "Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir", "Surat Keterangan Terdaftar Perguruan Tinggi / LOA");
        $array_dokumen_bkpp = array("Surat Permohonan Tugas Belajar", 
        "Surat Keterangan OPD", 
        "Surat Analisa Kebutuhan Pegawai");
    ?>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                    <?php if($this->session->flashdata("success_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Selamat!</strong> <?php echo $this->session->flashdata("success_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Pengajuan Tugas Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                        <div class="card-body">
                            <!-- PILL -->
                            <div class="nav-wrapper centered">
                                <ul class="nav nav-pills nav-pills-circle" id="tabs_2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 0){
                                                echo $nav_default;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                echo $nav_forbidden;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 1){
                                                echo $nav_default;
                                            }else{
                                                echo $nav_success;
                                            }
                                        ?>" id="proses1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar < 1 ){
                                                echo $nav_disable;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                echo $nav_default;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                echo $nav_forbidden;
                                            }else{
                                                echo $nav_success;
                                            }
                                        ?>" id="proses2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">2</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar < 2){
                                                echo $nav_disable;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 2){
                                                echo $nav_default;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                                echo $nav_default;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 4){
                                                echo $nav_failed;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                echo $nav_forbidden;
                                            }else{
                                                echo $nav_success;
                                            }
                                        ?>" id="proses3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">3</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar < 5 ){
                                                echo $nav_disable;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 5){
                                                echo $nav_success;
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                echo $nav_failed;
                                            }else{
                                                echo $nav_success;
                                            }
                                        ?>" id="proses4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">4</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <!-- PILL -->
                            <!-- CONTENT PILL -->
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar == 0){
                                                echo 'active';
                                            }
                                        ?>" id="tabs-icons-text-1" role="tabpanel"
                                            aria-labelledby="tabs-icons-text-1-tab">
                                            <h4>Tahap 1. Pengajuan Rekomendasi Tugas Belajar</h4>
                                            <p class="description">
                                                <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 0){
                                                    echo '<br>Status : <span class="badge badge-default">Menunggu Konfirmasi OPD</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 1){
                                                    echo '<br>Status : <span class="badge badge-default">Rekomendasi Tugas Belajar Diterima OPD</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                    echo '<br>Status : <span class="badge badge-success">Sudah dikerjakan, menunggu konfirmasi OPD</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar < 6 && $now_data_master_belajar[0]->status_reg_belajar != 0){
                                                    echo '<br>Status : <span class="badge badge-success">Sudah DiKonfirmasi OPD</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                    echo '<br>Status : <span class="badge badge-warning">Tugas Belajar Ditolak</span>';
                                                }else{
                                                    echo '<br>Status : <span class="badge badge-success">Selesai</span>';
                                                }
                                             ?>
                                            </p>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 0){
                                                    echo '<p class="description"> Menunggu Konfirmasi OPD untuk Menerbitkan Rekomendasi Tugas Belajar</p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 1){
                                                    echo '<p class="description"> Unduh Dokumen untuk melakukan tahap selanjutnya.<br>
                                                    Jika sudah diterima perguruan tinggi, maka lanjutkan dengan klik tombol lanjutkan.
                                                    </p>';
                                                    echo '<p class="description"> <strong>Persiapkan Data : </strong><br><br>
                                                        1. Scan & Hardcopy Surat Rekomendasi Tugas Belajar;<br><br>
                                                        2. Scan & Hardcopy Surat pernyataan bermaterai Rp.6000,- untuk<br>
                                                        tidak pindah tugas paling kurang 2 (dua) kali<br>
                                                        masa pendidikan ditambah 1 (satu) tahun;<br><br>
                                                        3. Scan & Hardcopy surat pernyataan bermaterai Rp.6000,- untuk<br>
                                                        mengembalikan seluruh biaya yang telah<br>
                                                        dikeluarkan apabila tidak dapat menyelesaikan<br>
                                                        pendidikannya atau menarik diri dari<br>
                                                        pendidikannya, kecuali sakit atau meninggal dunia;<br><br>
                                                        4. Scan & Hardcopy Surat keterangan dari lembaga pemberi beasiswa<br>
                                                        yang menerangkan bahwa yang bersangkutan diberikan beasiswa oleh lembaga tersebut;<br><br>
                                                        5. Scan Pas Foto Berwarna Terbaru & Pas photo Berwarna terbaru ukuran 3x4 sebanyak 2 (dua) buah;<br><br>
                                                        6. Scan & Hardcopy Photo Copy ijazah terakhir di legalisir;<br><br>
                                                        7. Scan & Hardcopy Photo Copy Transkrip nilai terakhir dilegalisir;<br><br>
                                                        8. Scan & Hardcopy Photo Copy SK Terakhir;<br><br>
                                                        9. Scan & Hardcopy Photo Copy SK Jabatan Terakhir (bagi yang menduduki Jabatan);<br><br>
                                                        10. Scan & Hardcopy Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir.<br><br>
                                                        11. Scan & Hardcopy Surat Keterangan Terdaftar Perguruan Tinggi / LOA;<br><br>
                                                    </p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                    echo '<p class="description"> Menunggu konfirmasi OPD untuk tahap selanjutnya, tahap mengajukan tugas belajar ke BKPP<br></p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar < 6 && $now_data_master_belajar[0]->status_reg_belajar != 0){
                                                    echo '<p class="description"> Sudah dikonfirmasi OPD<br></p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){

                                                }else{
                                                    echo '<p class="description"> Selesai Tugas Belajar. <br></p>';
                                                }
                                             ?>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 0){

                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_rekomendasi[0]->status_rekomendasi == 1){
                                                    echo '<a target="_blank" href="'.base_url('functions/reports/sunyatugas/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-sm">
                                                    <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                    <span class="btn-inner--text">Unduh Surat Pernyataan Bermaterai Untuk Tidak Pindah Tugas</span>
                                                    </a><br><br>';
                                                    echo '<a target="_blank" href="'.base_url('functions/reports/sunyatigi/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-sm">
                                                    <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                    <span class="btn-inner--text">Unduh Surat Pernyataan Bermaterai Ganti Rugi</span>
                                                    </a><br><br>';
                                                    echo '<hr class="my-4">';
                                                    echo '<a href="'.base_url('user/tugasbelajar/unggahDokumenTugasBelajar/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Lanjutkan Lengkapi Dokumen Syarat Tugas Belajar</span>
                                                    </a>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){

                                                }else{
                                                    echo '<a target="_blank" href="'.base_url('functions/reports/sunyatugas/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-sm">
                                                    <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                    <span class="btn-inner--text">Unduh Surat Pernyataan Bermaterai Untuk Tidak Pindah Tugas</span>
                                                    </a><br><br>';
                                                    echo '<a target="_blank" href="'.base_url('functions/reports/sunyatigi/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-sm">
                                                    <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                    <span class="btn-inner--text">Unduh Surat Pernyataan Bermaterai Ganti Rugi</span>
                                                    </a><br><br>';
                                                }
                                             ?>
                                        </div>
                                        <div class="tab-pane fade show <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                echo 'active';
                                            }
                                        ?>" id="tabs-icons-text-2" role="tabpanel"
                                            aria-labelledby="tabs-icons-text-2-tab">
                                            <h4>Tahap 2. Proses Persetujuan OPD</h4>
                                            <p class="description">
                                                <?php 
                                                    if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                        echo '<br>Status : <span class="badge badge-primary">Menunggu Konfirmasi OPD</span>';
                                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                        echo '<br>Status : <span class="badge badge-danger">Permintaan ditolak.</span>';
                                                    }else{
                                                        echo '<br>Status : <span class="badge badge-success">Sudah Dikonfirmasi OPD</span>';
                                                    }
                                                ?>
                                            </p>
                                            <hr>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                    echo '<p class="description"> Pesan : Menunggu Konfirmasi</p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                    echo '<p class="description">Mohon maaf, permohonan anda ditolak, segera hubungi OPD terkait.</p>';
                                                }else{
                                                    echo '<p class="description">Sudah Dikonfirmasi OPD</p>';
                                                }
                                            ?>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                    
                                                }else{
                                                    echo '<hr class="my-4">';
                                                    echo '<p class="description">Dokumen Persyaratan Tugas Belajar Yang Telah Diunggah</p>';
                                                    $i = 0;
                                                    foreach($now_data_master_belajar_trans_upload as $upload){
                                                        echo '<a target="_blank" href="'.base_url($upload->upload_file).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-sm">
                                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                        <span class="btn-inner--text">Unduh '.$array_dokumen_upload[$i].'</span>
                                                        </a><br></br>';
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="tab-pane fade show <?php 
                                            if($now_data_master_belajar[0]->status_reg_belajar == 2 || $now_data_master_belajar[0]->status_reg_belajar == 3 || $now_data_master_belajar[0]->status_reg_belajar == 4){
                                                echo 'active';
                                            }
                                        ?>" id="tabs-icons-text-3" role="tabpanel"
                                            aria-labelledby="tabs-icons-text-3-tab">
                                            <h4>Tahap 3. Permohonan Tugas Belajar kepada BKPP</h4>
                                            <p class="description">
                                                <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 2){
                                                    echo '<br>Status : <span class="badge badge-default">Belum dikerjakan</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                                    echo '<br>Status : <span class="badge badge-success">Sudah dikerjakan, menunggu konfirmasi BKPP</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 4){
                                                    echo '<br>Status : <span class="badge badge-warning">Diterima, Segera Perbaiki Syarat-syarat</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar > 4 && $now_data_master_belajar[0]->status_reg_belajar < 6){
                                                    echo '<br>Status : <span class="badge badge-success">Tugas Belajar Dimulai, Sudah disetujui BKPP</span>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                    echo '<br>Status : <span class="badge badge-danger">Permintaan ditolak.</span>';
                                                }else{
                                                    echo '<br>Status : <span class="badge badge-success">Selesai Tugas Belajar</span>';
                                                }
                                             ?>
                                            </p>
                                            <hr>
                                            <p class="description">Pesan :</p>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 2 ){
                                                    echo '<a target="_blank" href="'.base_url('functions/reports/sunyatugas/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-sm">
                                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                        <span class="btn-inner--text">Unduh Surat Permohonan Tugas Belajar</span>
                                                        </a><br></br>';
                                                    echo '<p class="description"> Persiapkan Data untuk di unggah:<br>
                                                    1. Scan & Hardcopy Surat Permohonan Tugas Belajar;<br>
                                                    2. Scan & Hardcopy Surat Keterangan;<br>
                                                    3. Scan & Hardcopy Analisa Kebutuhan Pegawai;<br>
                                                    </p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                                    echo '<p class="description">Sudah dikerjakan, menunggu konfirmasi BKPP<br></p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 4){
                                                    echo '<p class="description">Perbaiki Syarat-syarat<br></p>';
                                                    echo '<p class="description">Pesan Perbaikan :';
                                                    foreach ($now_data_master_belajar_trans_tolak as $data_tolak) {
                                                        echo $data_tolak->tolak_comment.'<br>';
                                                    }
                                                    echo '</p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar > 4 && $now_data_master_belajar[0]->status_reg_belajar < 6){
                                                    echo '<p class="description">Tugas Belajar Dimulai, Sudah disetujui BKPP.<br></p>';
                                                }else{
                                                    echo '<p class="description"> Tugas Belajar telah selesai. <br></p>';
                                                }
                                            ?>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 2 ){
                                                    echo '<hr class="my-4">';
                                                    echo '<a href="'.base_url("user/tugasbelajar/ajukanTugasBelajar/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Ajukan Tugas Belajar</span>
                                                    </a>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 3 ){
                                                    echo '<p class="description">Sudah dikerjakan, menunggu konfirmasi BKPP<br></p>';
                                                    $i = 0;
                                                    foreach($now_data_master_belajar_trans_upload_2 as $upload){
                                                        echo '<a target="_blank" href="'.base_url($upload->upload_file).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-sm">
                                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                        <span class="btn-inner--text">Unduh '.$array_dokumen_bkpp[$i].'</span>
                                                        </a><br></br>';
                                                        $i++;
                                                    }
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 4 ){
                                                    echo '<hr class="my-4">';
                                                    echo '<a href="'.base_url("user/tugasbelajar/perbaikiTugasBelajar/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Perbaikan Tugas Belajar</span>
                                                    </a>';
                                                }else{
                                                    $i = 0;
                                                    foreach($now_data_master_belajar_trans_upload_2 as $upload){
                                                        echo '<a target="_blank" href="'.base_url($upload->upload_file).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-sm">
                                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                        <span class="btn-inner--text">Unduh '.$array_dokumen_bkpp[$i].'</span>
                                                        </a><br></br>';
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="tab-pane fade show <?php 
                                                        if($now_data_master_belajar[0]->status_reg_belajar == 5){
                                                            echo 'active';
                                                        }
                                                    ?>" id="tabs-icons-text-4" role="tabpanel"
                                            aria-labelledby="tabs-icons-text-4-tab">
                                            <h4>Tahap 4. Tugas Belajar Dimulai</h4>
                                            <p>
                                                <?php 
                                                    if($now_data_master_belajar[0]->status_reg_belajar == 5 ){
                                                        echo '<br>Status : <span class="badge badge-success">Tugas Belajar Diterima BKPP</span>';
                                                    }else{
                                                        echo '<br>Status : <span class="badge badge-success">Tugas Belajar telah selesai.</span>';
                                                    }
                                                ?>
                                            </p>
                                            <?php 
                                                if($now_data_master_belajar[0]->status_reg_belajar == 5 ){
                                                    echo '<p class="description">Tugas Belajar Dimulai, Silahkan laporkan laporan semester secara berkala</p>';
                                                    echo '<a target="_blank" href="'.base_url("user/tugasbelajar/laporan/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Laporan Tugas Belajar</span>
                                                        </a>';
                                                }else{
                                                    echo '<p class="description">Tugas Belajar telah selesai.</p>';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CONTENT PILL -->
                        </div>
                        <!-- END INPUT AREA-->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>