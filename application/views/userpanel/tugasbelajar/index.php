<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-2 pt-6">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>

                    <!-- Card stats -->
                    <?php if($this->session->flashdata("success")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Sukses!</strong> <?php echo $this->session->flashdata("success")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-gradient-default shadow">
                        <div class="card-header bg-transparent border-0">
                            <div class="row align-items-center">
                                <!-- <div class="col-md-12">
                                    <h2 class="text-white mb-0"></h2>
                                </div>
                                <hr> -->
                                <div class="col">
                                    <h3 class="text-white mb-0">Tugas Belajar</h3>
                                </div>
                                <div class="col-md-auto text-right">
                                    <div class="row">
                                        <div class="col-md-auto">
                                            <form
                                                class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                                <div class="form-group mb-0">
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-search"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="Search" type="text">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-auto">
                                            <a href="<?php echo base_url("user/tugasbelajar/addSuratRekomendasi")?>"
                                                class="btn btn-md btn-primary">
                                                <i class="fas fa-plus"></i> Ajukan Tugas Belajar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-dark" ">
                                <thead class=" thead-dark">
                                <tr>
                                    <th>Nama Program</th>
                                    <th width="30%">Status</th>
                                    <th width="30%">Lihat Proses</th>
                                    <th>Aksi (Shortcut)</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    <?php foreach ($row->result() as $rows){
                                        $i++;
                                        echo "<tr>";
                                        echo "<td style='white-space:normal'>Program $rows->print_tingpend $rows->belajar_pendidikan_nm</td>";
                                        switch ($rows->status_reg_belajar){
                                            case 0:
                                                if($rows->status_rekomendasi == 0){
                                                    echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-secondary'></i>Menunggu Konfirmasi Rekomendasi". ($rows->jenis_pengiriman == 0 ? " OPD " : " BKPP ") ."
                                                        </span></td>";
                                                }else if ($rows->status_rekomendasi == 1){
                                                    echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-primary'></i>Rekomendasi Tugas Belajar
                                                        </span></td>";
                                                }
                                                echo "<td style='white-space:normal'>";
                                                        if($rows->status_rekomendasi == 0){
                                                            echo " <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-secondary btn-sm'>
                                                                    <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                                    <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 1</span>
                                                                    </a>";
                                                        }else if ($rows->status_rekomendasi == 1){
                                                            echo " <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-primary btn-sm'>
                                                                    <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                                    <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 1</span>
                                                                    </a>";
                                                        }
                                                echo "</td>";
                                                echo "<td scope=\"col\">";
                                                    if($rows->status_rekomendasi == 0){
                                                       
                                                    }else if ($rows->status_rekomendasi == 1){
                                                        echo "<center>
                                                        <div class='dropdown'>
                                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                            <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                                <a class='dropdown-item' href='".base_url('user/tugasbelajar/unggahDokumenTugasBelajar/'.$rows->belajar_reg_id)."'><i class='fa fa-arrow-right'></i>Unggah Dokumen Tugas Belajar</a>
                                                            </div>
                                                        </div>
                                                        </center>";
                                                    }
                                                    echo "</td>";
                                                break;
                                            case 1:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-primary'></i>Menunggu Konfirmasi OPD
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-primary btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 1</span>
                                                        </a>
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        <center>
                                                        <div class='dropdown'>
                                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                            <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                            
                                                            </div>
                                                        </div>
                                                        </center>
                                                    </td>";
                                                break;
                                            case 2:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-secondary'></i>Ajukan Tugas Belajar
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-secondary btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 3</span>
                                                        </a>
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        <center>
                                                            <div class='dropdown'>
                                                                <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                                <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                                </a>
                                                                <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                                <a class='dropdown-item' href='".base_url('user/tugasbelajar/ajukanTugasBelajar/'.$rows->belajar_reg_id)."'><i class='fa fa-arrow-right'></i>Ajukan Tugas Belajar</a>
                                                                </div>
                                                            </div>
                                                        </center>
                                                    </td>";
                                                break;
                                            case 3:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-primary'></i>Menunggu Konfirmasi BKPP
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-secondary btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 3</span>
                                                        </a>
                                                    </td>";
                                                echo "<td></td>";
                                                break;
                                            case 4:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4' style='text-align:left'>
                                                        <i class='bg-danger' style='vertical-align:text-top'></i>
                                                        <label style='white-space:normal;line-height:initial'>Tugas Belajar Diterima, segera perbaiki</label>
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-warning btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar 3 <br>Perbaikan Berkas Tugas Belajar</span>
                                                        </a>
                                                    </td>";
                                                    echo "<td scope=\"col\">
                                                    <center>
                                                    <div class='dropdown'>
                                                        <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                        <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                        </a>
                                                        <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                        <a class='dropdown-item' href='".base_url('user/tugasbelajar/perbaikiTugasBelajar/'.$rows->belajar_reg_id)."'><i class='fa fa-arrow-right'></i>Perbaikan Berkas</a>
                                                        </div>
                                                    </div>
                                                    </center>
                                                </td>";
                                                break;
                                            case 5:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-success'></i>Permohonan Tugas Belajar Disetujui BKPP
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-success btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : <br>Tugas Belajar Dimulai</span>
                                                        </a>
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        <center>
                                                            <div class='dropdown'>
                                                                <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                                <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                                </a>
                                                                <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                                <a class='dropdown-item' href='".base_url('user/tugasbelajar/laporan/'.$rows->belajar_reg_id)."'><i class='fa fa-arrow-right'></i>Laporan Tugas Belajar</a>
                                                                </div>
                                                            </div>
                                                        </center>
                                                    </td>";
                                                               // <a class='dropdown-item' href='".base_url('user/tugasbelajar/dokumenGenerator/'.$rows->belajar_reg_id)."'><i class='fa fa-print'></i>Cetak Surat Terbit Tugas Belajar</a>
                                                break;
                                            case 6:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-success'></i>Tugas Belajar Selesai
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='".base_url('user/tugasbelajar/detailprosestugasbelajar/'.$rows->belajar_reg_id)."' class='btn btn-icon btn-3 btn-success btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 6</span>
                                                        </a>
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        -
                                                    </td>";
                                                break;
                                            case 7:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-success'></i>Tugas Belajar Telah Selesai, Selamat!
                                                        </span></td>";
                                                echo "<td style='white-space:normal'>
                                                        <a href='tugasbelajar/detailprosestugasbelajar/$rows->belajar_reg_id' class='btn btn-icon btn-3 btn-success btn-sm'>
                                                            <span class='btn-inner--icon'><i class='ni ni-bold-right'></i></span>
                                                            <span class='btn-inner--text'>Proses Tugas Belajar : Tahap 6</span>
                                                        </a>
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        
                                                    </td>";
                                                break;
                                            case 9:
                                                echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                                        <i class='bg-warning'></i>Permohonan Ditolak
                                                        </span></td>";
                                                echo "<td style='text-align:center'>
                                                        -
                                                    </td>";
                                                echo "<td scope=\"col\">
                                                        <center>
                                                        <div class='dropdown'>
                                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                              <i class='fa fa-lg fa-chevron-circle-down' ></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                              <a class='dropdown-item' disabled='disabled'><i class='fa fa-file'></i>Lihat Data</a>
                                                            </div>
                                                        </div>
                                                        </center>
                                                    </td>";
                                                break;
                                        }
                                        echo "</tr>";
                                    }?>
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer bg-gradient-default border-0">
                            <nav aria-label="Page navigation example">
                                <?php echo $pagination;?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>