<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body class="bg-gray-dark">
    <div class="main-content">
        <!-- Header -->
        <div class="header py-4 py-lg-5">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-7">
                            <h1 class="text-white">Selamat Datang!</h1>
                            <!-- LOGO IDOL -->
                            <!-- <img style="max-width: 200px; max-height: 100px; margin-left: auto; margin-right: auto; display: block"
                                src="<?php echo asset_url()?>/courseware/img/logo-idol.png"> -->
                            <!-- LOGO SITIMBEL -->
                            <img style="max-width: 200px; max-height: 100px; margin-left: auto; margin-right: auto; display: block"
                                src="<?php echo asset_url()?>/courseware/img/logo-sitimbel.png">
                            <?php if($this->session->flashdata("error_message")) {?>
                            <div style="margin-top: 20px">
                                <div class="alert alert-danger" role="alert">
                                    <strong>Error!</strong> <?php echo $this->session->flashdata("error_message")?>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <strong>Buat Akun.</strong><br>
                                <small>Masukan data anda pada kolom dibawah ini.</small>
                            </div>
                            <form role="form" method="post" action="<?php echo base_url("user/auth/registeraccount");?>">
                                <div class="form-group">
                                    <div class="form_error">
                                        <?php echo form_error('username'); ?>
                                    </div>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Username" id="username" name="username" type="text">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form_error">
                                            <?php echo form_error('firstname'); ?>
                                        </div>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Nama Depan" id="firstname" name="firstname" type="text">
                                    </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form_error">
                                            <?php echo form_error('lastname'); ?>
                                        </div>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Nama Akhir" id="lastname" name="lastname" type="text">
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form_error">
                                        <?php echo form_error('nip'); ?>
                                    </div>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-badge"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="NIP" id="nip" name="nip" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form_error">
                                        <?php echo form_error('email'); ?>
                                    </div>
                                    <div class="input-group input-group-alternative mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Email" type="email" id="email" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form_error">
                                        <?php echo form_error('password'); ?>
                                    </div>
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Password" type="password" id="password" name="password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form_error">
                                        <?php echo form_error('passwordcheck'); ?>
                                    </div>
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Validasi Password" type="password" id="passwordcheck" name="passwordcheck">
                                    </div>
                                </div>
                                <div class="row my-4">
                                    <div class="col-12">
                                        <div class="form_error">
                                        <?php echo form_error('checkbox'); ?>
                                        </div>
                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                            <input class="custom-control-input" id="checkbox" name="checkbox"
                                                type="checkbox">
                                            <label class="custom-control-label" for="checkbox">
                                                <span class="text-muted">Saya setuju dengan <a href="#!">Syarat dan Ketentuan
                                                    </a></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary mt-4" onclick="if(!this.form.checkbox.checked){alert('You must agree to the terms first!');return false}">Buat Akun</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>