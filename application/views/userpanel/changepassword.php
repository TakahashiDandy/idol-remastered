<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body class="bg-gray-dark">
<div class="main-content">
    <!-- Header -->
    <div class="header py-4 py-lg-5">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-7">
                        <h1 class="text-white">Selamat Datang!</h1>
                        <!-- LOGO IDOL -->
                        <!-- <img style="max-width: 200px; max-height: 100px; margin-left: auto; margin-right: auto; display: block" src="<?php echo asset_url()?>/courseware/img/logo-idol.png"> -->
                        <!-- LOGO SITIMBEL -->
                        <img style="max-width: 200px; max-height: 100px; margin-left: auto; margin-right: auto; display: block" src="<?php echo asset_url()?>/courseware/img/logo-sitimbel.png">
                        <?php if($this->session->flashdata("error_message")) {?>
                            <div style="margin-top: 20px">
                                <div class="alert alert-danger" role="alert">
                                    <strong>Gagal!</strong> <?php echo $this->session->flashdata("error_message")?>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-gradient-lighter shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Ganti Password Anda</small>
                        </div>
                        <form method="post" action="<?php echo base_url("user/auth/changepassword/");?>">
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Password" type="text" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Verify Password" type="text" id="check" name="check">
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="hidden" name="token" id="token" value="<?php echo $ids; ?>">
                                <button type="submit" class="btn btn-primary my-4">Ganti Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>