<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->

                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl-12 order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Akun Saya</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <input type="hidden" id="nip" name="nip" value=<?php if(!empty($nip))echo $nip ?> />
                                    <i id="icon-sync" class="fas fa-sync fa-spin" style="margin-right:10px"></i>
                                    <button id="btn-sync" type="button" class="btn btn-lg btn-primary" data-toggle="tooltip" data-placement="top" title="Sinkronisasi dengan SIMPEG">Perbaharui Data</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6 class="heading-small text mb-4">Informasi Data Diri</h6>
                                    <table class="table" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="30%">Nama Lengkap</td>
                                                <td width="1%" style="padding-left:0;">:</td>
                                                <td style="white-space: normal;"><label
                                                        id="input-nama-lengkap"><?php if(!empty($detail_user->nama_lengkap)) echo $detail_user->nama_lengkap ?></label>
                                                </td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Lahir</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;"><label
                                                        id="input-tempat-lahir"><?php if(!empty($detail_user->tempat_lahir)) echo $detail_user->tempat_lahir ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;"><label
                                                        id="input-tanggal-lahir"><?php if(!empty($detail_user->tanggal_lahir)) echo $detail_user->tanggal_lahir ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-jenis-kelamin"><?php if(!empty($detail_user->jenis_kelamin))echo $detail_user->jenis_kelamin ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jabatan</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-jabatan"><?php if(!empty($detail_user->jabatan))echo $detail_user->jabatan ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Golongan</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-golongan"><?php if(!empty($detail_user->golongan))echo $detail_user->golongan ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Instansi/Unit Kerja</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-instansi"><?php if(!empty($detail_user->instansi)) echo $detail_user->instansi ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan Terakhir</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-pendidikan-terakhir"><?php if(!empty($detail_user->pendidikan_terakhir))echo $detail_user->pendidikan_terakhir ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td style="padding-left:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-email"><?php if(!empty($detail_user->email))echo $detail_user->email ?></label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h6 class="heading-small text mb-4">Informasi Kontak</h6>
                                    <table class="table" style="border:0">
                                        <tbody>
                                            <tr>
                                                <td>Alamat Rumah</td>
                                                <td style="padding-right:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-alamat-rumah"><?php if(!empty($detail_user->alamat_rumah))echo $detail_user->alamat_rumah ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Alamat Kantor</td>
                                                <td style="padding-right:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-alamat-kantor"><?php if(!empty($detail_user->alamat_kantor))echo $detail_user->alamat_kantor ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Provinsi</td>
                                                <td style="padding-right:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-provinsi"><?php if(!empty($detail_user->provinsi))echo $detail_user->provinsi ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kota/Kabupaten</td>
                                                <td style="padding-right:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-city"><?php if(!empty($detail_user->kotakab))echo $detail_user->kotakab ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Telepon/Fax</td>
                                                <td style="padding-right:0">:</td>
                                                <td style="white-space: normal;">
                                                    <label
                                                        id="input-telpfax"><?php if(!empty($detail_user->telpfax))echo $detail_user->telpfax ?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Kontak</td>
                                                <td style="padding-right:0">:</td>
                                                <td><label
                                                        id="input-no-kontak"><?php if(!empty($detail_user->no_kontak)) echo $detail_user->no_kontak ?></label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
        $(document).ready(function () {
            $("#icon-sync").hide();
        });
        $("#btn-sync").click(function () {
            var nip = $("#nip").val();
            $.ajax({
                url: urlregistrasi + nip,
                beforeSend: function () {
                    $("#icon-sync").show();
                },
                success: function (result) {
                    try {
                        //memeriksa apakah data simpegnya ada
                        var data = JSON.parse(result);
                        $("#input-nama-lengkap").val(data.nama);
                        $("#input-golongan").val(data.golongan_akhir);
                        $("#input-tempat-lahir").val(data.tempat_lahir);
                        $("#input-tanggal-lahir").val(data.tanggal_lahir);
                        $("#input-jenis-kelamin").val(data.jenis_kelamin);
                        $("#input-jabatan").val(data.jabatan_nama);
                        $("#input-no-kontak").val(data.no_hp);
                        $("#input-email").val((data.email == "" ? "-@gmail.com" : data.email));
                        $("#input-instansi").val(data.instansi);
                        $("#input-pendidikan-terakhir").val(data.pendidikan_akhir);
                        $("#input-kotakab").val("Bandung");
                        $("#input-provinsi").val("Jawa Barat");
                        $("#input-telpfax").val(data.no_hp);
                        $("#input-alamat-kantor").val(data.alamat);
                        $("#input-alamat-rumah").val(data.alamat);

                        location.reload();
                    } catch (e) {
                        alert(e);
                    }
                },
                complete: function () {
                    $("#icon-sync").hide();
                }
            });
        });
    </script>
</body>

</html>