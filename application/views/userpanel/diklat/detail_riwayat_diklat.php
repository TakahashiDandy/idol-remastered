<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $pagename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form id="form-perbaikan-berkas-diklat" method="post" action="<?php echo base_url('user/diklat/update_riwayat_diklat/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">
                            <input type="hidden" id="nip" name="nip" value=<?php echo $detail_user->nip ?>/>
                            <input type="hidden" id="id_registrasi" name="id_registrasi" value=<?php echo $detail_user->id_registrasi ?>/>
                            <input type="hidden" id="status_registrant" name="status_registrant" value=<?php echo $detail_user->status_registrant ?>/>

                            <?php if($detail_user->status_registrant == 2){ ?>
                                <h6 class="heading-small mb-4">Unggah Semua Dokumen/Berkas Persyaratan Diklat</h6>
                                <table class="table table-non-bordered">
                                    <tbody>
                                    <tr>
                                        <td width="10%">Keterangan</td>
                                        <td width="1%">:</td>
                                        <td width="auto"><?php echo $detail_user->keterangan ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="btn-add-more-perbaikan" type="button"  class="btn btn-primary">Unggah Dokumen/Berkas Persyaratan Diklat</button>
                                    </div>
                                </div>

                                <!-- <div class="input_fields_wrap">
                                </div> -->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input_fields_wrap">
                                            <?php
                                            if(isset($attachment)){
                                                $x = 0;
                                                 foreach ($attachment as $data){
                                                    $x++;
                                                    echo "<div class='row'><div class='col-md-10'>".
                                                        "<label class='form-control-label' for='search'>Berkas/Dokumen Persyaratan ke-".$x.":</label> <br>".
                                                        "<a target='_blank' href='".base_url().$data["filepath"]."' class='form-control-label'>Klik untuk lihat berkas persyaratan.</a>".
                                                        "</div>".
                                                        "<div class='col-md-2'>".
                                                        "<button type='button' onclick='removefilesyaratdiklat(".$data["id_attachment"].")' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>".
                                                        "</div></div><br>";
                                                }
                                            } else{
                                                echo '<div class="col-lg-auto">';
                                                echo '<a>Tidak Ada Syarat yang Diupload</a>';
                                                echo '</div>';
                                            }?>
                                        </div>
                                    </div>
                                </div>

                                <hr class="my-4">

                                <div class="form-group">
                                    <input class="btn btn-success" type="button" id="submitregistrasi" value="Simpan">
                                    <a href="<?php echo base_url("user/diklat/index_user") ?>" class="btn btn-danger">Batal</a>
                                </div>

                            <?php } ?>

                            <?php if($detail_user->status_registrant == 3){ ?>
                                <h6 class="heading-small text-muted mb-4">Unggah Laporan Hasil Diklat</h6>
                                <table class="table table-non-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="10%">Keterangan</td>
                                            <td width="1%">:</td>
                                            <td width="auto"><?php echo $detail_user->keterangan ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- <br> -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button id="btn-add-more" type="button"  class="btn btn-primary">Tambah Lampiran Hasil Diklat</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="input_fields_wrap">
                                </div> -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input_fields_wrap">
                                            <?php
                                            if(isset($attachment)){
                                                $x = 0;
                                                 foreach ($attachment as $data){
                                                    $x++;
                                                    echo "<div class='row'><div class='col-md-10'>".
                                                        "<label class='form-control-label' for='search'>Laporan Hasil Diklat ".$x."</label> <br>".
                                                        "<a target='_blank' href='".base_url().$data["filepath"]."' class='form-control-label'>Klik untuk lihat berkas hasil diklat.</a>".
                                                        "</div>".
                                                        "<div class='col-md-2'>".
                                                        "<button type='button' onclick='removefile(".$data["id_attachment"].")' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>".
                                                        "</div></div>";
                                                }
                                            } else{
                                                echo '<div class="col-lg-auto">';
                                                echo '<a>Tidak Ada Syarat yang Diupload</a>';
                                                echo '</div>';
                                            }?>
                                        </div>
                                    </div>
                                </div>

                                <hr class="my-4">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="btn btn-success" type="submit" id="simpanregistrasi" value="Simpan">
                                            <a href="<?php echo base_url("user/diklat/index_user") ?>" class="btn btn-danger">Batal</a>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        
                            <?php if($detail_user->status_registrant == 4){ ?>
                                <h6 class="heading-small text-muted mb-4">Detail Hasil Riwayat Diklat</h6>
                                <table class="table table-non-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="10%">Keterangan</td>
                                            <td width="1%">:</td>
                                            <td width="auto"><?php echo $detail_user->keterangan ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <a href="<?php echo base_url("user/diklat/index_user") ?>" class="btn btn-danger">Batal</a>
                                </div>

                            <?php } ?>

                            <?php if($detail_user->status_registrant == 5){ ?>
                                <h6 class="heading-small text-muted mb-4">Upload Laporan Hasil Diklat</h6>
                                <table class="table table-non-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="10%">Keterangan</td>
                                            <td width="1%">:</td>
                                            <td width="auto">Kelulusan anda tertunda hingga proses verifikasi laporan hasil diklat selesai. Mohon </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button id="btn-add-more" type="button"  class="btn btn-primary">Tambah Lampiran Hasil Diklat</button>
                                    </div>
                                </div>

                                <!-- <div class="input_fields_wrap">
                                </div> -->
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input_fields_wrap">
                                            <?php
                                            if(isset($attachment)){
                                                $x = 0;
                                                 foreach ($attachment as $data){
                                                    $x++;
                                                    echo "<div class='row'><div class='col-md-10'>".
                                                        "<label class='form-control-label' for='search'>Laporan Hasil Diklat ".$x."</label> <br>".
                                                        "<a target='_blank' href='".base_url().$data["filepath"]."' class='form-control-label'>Klik untuk lihat berkas hasil diklat.</a>".
                                                        "</div>".
                                                        "<div class='col-md-2'>".
                                                        "<button type='button' onclick='removefile(".$data["id_attachment"].")' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>".
                                                        "</div></div>";
                                                }
                                            } else{
                                                echo '<div class="col-lg-auto">';
                                                echo '<a>Tidak Ada Syarat yang Diupload</a>';
                                                echo '</div>';
                                            }?>
                                        </div>
                                    </div>
                                </div>

                                <hr class="my-4">

                                <div class="form-group">
                                    <input class="btn btn-success" type="button" id="submitregistrasi" value="Simpan">
                                    <a href="<?php echo base_url("user/diklat/index_user") ?>" class="btn btn-danger">Batal</a>
                                </div>

                            <?php } ?> 

                            <?php if($detail_user->status_registrant == 6){ ?>
                                <h6 class="heading-small text-muted mb-4">Detail Hasil Riwayat Diklat</h6>
                                <table class="table table-non-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="10%">Nomor STTP</td>
                                            <td width="1%">:</td>
                                            <td width="auto"><?php foreach ($rows1->result() as $data){ echo $data->diklat_sttp_no; } ?></td>
                                        </tr>
                                        <tr>
                                            <td width="10%">Tanggal STTP</td>
                                            <td width="1%">:</td>
                                            <td width="auto"><?php foreach ($rows1->result() as $data){ 
                                                echo $data->diklat_sttp_tgl; } ?></td>
                                        </tr>
                                        <tr>
                                            <td width="10%">STTP Pejabat</td>
                                            <td width="1%">:</td>
                                            <td width="auto"><?php foreach ($rows1->result() as $data){ echo $data->diklat_sttp_pej; } ?></td>
                                        </tr>
                                        <tr>
                                            <td width="10%">Keterangan</td>
                                            <td width="1%">:</td>
                                            <td width="auto">
                                            <?php foreach ($rows1->result() as $data){
                                                if(!empty($data->keterangan)){
                                                    echo $data->keterangan;
                                                } else { ?>
                                                    Selamat anda telah lulus mengikuti diklat.</td>
                                            <?php }
                                            } ?>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="form-group">
                                    <a href="<?php echo base_url("user/riwayat/index_riwayat") ?>" class="btn btn-danger"> < Kembali</a>
                                </div>

                            <?php } ?>

                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>

        <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="modal-confirm-submit" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form method="post" action="<?php echo base_url('admin/registrasi/updatealumni')?>" style="width:800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="nip" id="nip">
                            <h2 class="modal-title">Perhatian!</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4>Berkas yang sudah anda upload akan dihapus otomatis oleh sistem. Silahkan cek kembali berkas anda! Mohon unggah kembali semua berkas yang diperlukan.</h4>
                            <h4>Jika sudah maka lanjutkan dengan menekan tombol Kirim.</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="kirim-perbaikan-berkas">Kirim</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Footer -->
        <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
<script>
    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $("#btn-add-more"); //Add button ID
	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
            element =   "<div class='row'><div class='col-md-8'>"+
                            "<label for='search'>Upload Laporan Hasil Diklat "+x+"</label>"+
                            "<input class='form-control' name='upload-hasil"+x+"' id='upload-hasil"+x+"' placeholder='File upload-"+x+"' type='text'>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button onclick='browse_file("+x+")' id='btn-upload-hasil"+x+"' type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Pilih File</button>"+
                            "<input id='file-upload"+x+"' name='laporanhasildiklat[]' type='file' multiple hidden/>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>"+
                        "</div></div>";
			$(wrapper).append(element); //add input box
		}
    });
    
    $("#btn-add-more-perbaikan").click(function(e){
        e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
            element =   "<div class='row'><div class='col-md-8'>"+
                            "<label for='search'>Upload Berkas/Dokumen Perbaikan Diklat "+x+"</label>"+
                            "<input class='form-control' name='upload-hasil"+x+"' id='upload-hasil"+x+"' placeholder='File upload-"+x+"' type='text'>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button onclick='browse_file("+x+")' id='btn-upload-hasil"+x+"' type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Pilih File</button>"+
                            "<input id='file-upload"+x+"' name='laporanhasildiklat[]' type='file' multiple hidden/>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>"+
                        "</div></div>";
			$(wrapper).append(element); //add input box
		}
    });
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent().parent().remove(); x--;
	});

    function removefile(prm){
        $.ajax({
            url : "<?php echo base_url("user/Diklat/deleteattachment/")?>" + prm
        });
    }

    function removefilesyaratdiklat(prm){
        $.ajax({
            url : "<?php echo base_url("user/Diklat/deleteattachmentsyaratdiklat/")?>" + prm
        });
    }

    function browse_file(e){
        i = e;
        $("#file-upload"+i).click();

        $("#file-upload"+i).change(function(e){
            $("#upload-hasil"+i).html($(this).val());
            $("#upload-hasil"+i).val($(this).val());
        });
    }

    $('#submitregistrasi').click(function() {
        $("#confirm-submit").modal("show");
    });

    $('#kirim-perbaikan-berkas').click(function(){
        // console.log('Sedang mengirim perbaikan berkas');
        $('#form-perbaikan-berkas-diklat').submit();
    });
</script>
</body>

</html>
