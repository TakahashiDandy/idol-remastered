<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Core -->
<script src="<?php echo asset_url();?>argon/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo asset_url();?>argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Optional JS -->
<script src="<?php echo asset_url();?>argon/vendor/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo asset_url();?>argon/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="<?php echo asset_url();?>argon/js/argon.js?v=1.0.0"></script>
<script> var base_url = "<?php echo base_url();?>";</script>
<!-- Diskominfo JS -->
<!-- <script src="<?php echo asset_url();?>courseware/js/diskominfo.js"></script> -->
<script>
// var base_url = window.location.origin;
// var path = window.location.pathname;
var urlregistrasi   = base_url + "public/integrasi/load_data/";
var urlmodaldiklat  = base_url + "/admin/diklat/modalsearchdiklatbyname/";
var urlsearchdata   = base_url + "/admin/diklat/searchdiklatbyname/";
var urluniversitas  = base_url + "user/tugasbelajar/modalsearchuniversitasbyname/";
var urlprodi        = base_url + "user/tugasbelajar/modalsearchprogramstudibyname/";
var urlfakultas     = base_url + "user/tugasbelajar/modalsearchfakultasbyname/";
var urltingpend     = base_url + "user/tugasbelajar/modalSearchTingkatPendidikanByName/";
var urlsyncsiriwayatdiklat      = base_url + "user/riwayat/getriwayatdiklat/";
var urlsyncsiriwayatpendidikan  = base_url + "user/riwayat/getriwayatpendidikan/";

function phase1() {
    $("#modal-loading").hide();
    $("#phase1").show();
    $("#phase2").hide();
    $("#phase3").hide();
}

function phase2() {
    $("#phase1").hide();
    $("#phase2").show();
    $("#phase3").hide();
}

function phase3() {
    $("#phase1").hide();
    $("#phase2").hide();
    $("#phase3").show();
}

function modal_diklat_data(prm){
    var diklat_id = document.getElementById("diklat_id_"+prm).innerText;
    var diklat_nama = document.getElementById("diklat_nama_"+prm).innerText;
    $("#id_diklat").val(diklat_id);
    $("#diklat_nama").val(diklat_nama);
    $("#exampleModal").modal('toggle');
    $("#textsearch").val("");
}

function getdatarow() {
    if($("#textsearch").val() == ""){
        $.ajax({
                url: urlmodaldiklat
            + $("input[type='radio'][name='diklat_jenis']:checked").val() + "/all",
            success: function (result) {
            try {
                $("#modaldata").html(result);
            } catch (e) {
                alert(result);
            }
        }
    });
    } else{
        $.ajax({
                url: urlmodaldiklat +
            $("input[type='radio'][name='diklat_jenis']:checked").val() +"/"+$("#textsearch").val(),
            success: function (result) {
            try {
                $("#modaldata").html(result);
            } catch (e) {
                alert(result);
            }
        }
    });
    }
}
function searchdata(prm){
    if($("#textsearch").val() == ""){
        inputan = "all";
    } else {
        inputan = $("#textsearch").val();
    }
    $.ajax({
        url: urlsearchdata + prm +"/" +inputan,
        success: function (result) {
            try {
                switch (prm){
                    case 1:
                        $("#datastruktural").html(result);
                        break;
                    case 2:
                        $("#datafungsional").html(result);
                        break;
                    case 3:
                        $("#datateknis").html(result);
                        break;
                }
                $("#pagination").hide();
            } catch (e) {
                alert(result);
            }
        }
    });
}

function getdatarowuniversitas() {
    if($("#textsearch").val() == ""){
        $.ajax({
            url: urluniversitas + "/all",
            success: function (result) {
                try {
                    $("#modaldata").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    } else{
        $.ajax({
            url: urluniversitas + $("#textsearch").val(),
            success: function (result) {
                try {
                    $("#modaldata").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    }
}

function modal_universitas_data(prm){
    var univ_nama = document.getElementById("univ_nama_"+prm).innerText;
    var univ_kota = document.getElementById("univ_kota_"+prm).innerText;
    var univ_id = document.getElementById("univ_id_"+prm).innerText;

    //label
    $("#kota_universitas").text(univ_kota);
    $("#nama_universitas").text(univ_nama);

    //input value
    $("#kota_univ").val(univ_kota);
    $("#nama_univ").val(univ_nama);
    $("#id_univ").val(univ_id);

    $("#modalCariSekolah").modal('toggle');
    $("#textsearch").val("");
}

function getdatarowprodi() {
    if($("#textsearchProdi").val() == ""){
        $.ajax({
            url: urlprodi + "/all",
            success: function (result) {
                try {
                    $("#modaldataprodi").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    } else{
        $.ajax({
            url: urlprodi + $("#textsearchProdi").val(),
            success: function (result) {
                try {
                    $("#modaldataprodi").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    }
}

function modal_jurusan_data(prm){
    var id_jurusan = document.getElementById("jurusan_id_"+prm).innerText;
    var nama_jurusan = document.getElementById("jurusan_nama_"+prm).innerText;

    //label
    $("#program_studi").text(nama_jurusan);

    //input value
    $("#program_studi_nama").val(nama_jurusan);
    $("#id_program_studi").val(id_jurusan);

    $("#modalCariProdi").modal('toggle');
    $("#textsearchProdi").val("");
}

function getdatarowfakultas() {
    if($("#textsearchFakultas").val() == ""){
        $.ajax({
            url: urlfakultas + "/all",
            success: function (result) {
                try {
                    $("#modaldatafakultas").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    } else{
        $.ajax({
            url: urlfakultas + $("#textsearchFakultas").val(),
            success: function (result) {
                try {
                    $("#modaldatafakultas").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    }
}

function modal_fakultas_data(prm){
    var id_fakultas = document.getElementById("fakultas_id_"+prm).innerText;
    var nama_fakultas = document.getElementById("fakultas_nama_"+prm).innerText;

    //label
    $("#fakultas_nm").text(nama_fakultas);

    //input value
    $("#fakultas_nm").val(nama_fakultas);
    $("#fakultas_id").val(id_fakultas);

    $("#modalCariFakultas").modal('toggle');
    $("#textsearchFakultas").val("");
}

function getdatarowtingpend() {
    if($("#textsearchTingpend").val() == ""){
        $.ajax({
            url: urltingpend + "/all",
            success: function (result) {
                try {
                    $("#modaldatatingpend").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    } else{
        $.ajax({
            url: urltingpend + $("#textsearchTingpend").val(),
            success: function (result) {
                try {
                    $("#modaldatatingpend").html(result);
                } catch (e) {
                    alert(result);
                }
            }
        });
    }
}

function modal_tingkat_pendidikan_data(prm){
    var id_tingpend = document.getElementById("tingpend_id_"+prm).value;
    var nama_tingpend = document.getElementById("tingpend_nama_"+prm).innerText;

    //label
    $("#tingpend_nm").text(nama_tingpend);

    //input value
    $("#tingpend_nm").val(nama_tingpend);
    $("#tingpend_id").val(id_tingpend);

    $("#modalCariTingpend").modal('toggle');
    $("#textsearchTingpend").val("");
}
</script>
