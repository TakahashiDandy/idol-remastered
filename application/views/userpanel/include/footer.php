<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; <?php echo date('Y') ?> <a href="http://bkpp.bandung.go.id" class="font-weight-bold ml-1" target="_blank">BKPP</a> X <a href="https://diskominfo.bandung.go.id" class="font-weight-bold ml-1" target="_blank">DISKOMINFO KOTA BANDUNG</a>
            </div>
        </div>
    </div>
</footer>
