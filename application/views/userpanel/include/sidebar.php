<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand pt-0" href="<?php echo base_url("user/home")?>">
            <!-- LOGO IDOL -->
            <!-- <img src="<?php echo asset_url();?>courseware/img/logo-idol.png" class="navbar-brand-img" alt="..."> -->
            <!-- LOGO SITIMBEL -->
            <img src="<?php echo asset_url();?>courseware/img/logo-sitimbel.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="<?php echo asset_url();?>argon/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Settings</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url("user/auth/logout");?>" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="<?php echo base_url("user/home");?>">
                            <img src="<?php echo asset_url();?>courseware/img/logo-idol.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav" style="cursor:pointer">
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "home"? "active" : NULL);?>" href="<?php echo base_url("user/home")?>">
                        <i class="ni ni-tv-2 text-success"></i> Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "jadwal"? "active" : NULL);?>" href="<?php echo base_url("user/jadwal")?>">
                        <i class="ni ni-badge text-pink"></i> Registrasi Diklat
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "registrasi"? "active" : NULL);?>" href="<?php echo base_url("user/registrasi")?>">
                        <i class="ni ni-badge text-pink"></i> Registrasi
                    </a>
                </li> -->
                <!-- <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "jadwal"? "active" : NULL);?>" href="<?php echo base_url("user/jadwal")?>">
                        <i class="ni ni-calendar-grid-58 text-blue"></i> Jadwal Diklat
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "izinbelajar"? "active" : NULL);?>" href="<?php echo base_url("user/izinbelajar")?>">
                        <i class="ni ni-badge text-orange"></i> Izin Belajar
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "tugasbelajar"? "active" : NULL);?>" href="<?php echo base_url("user/tugasbelajar")?>">
                        <i class="ni ni-hat-3 text-orange"></i> Tugas Belajar
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "riwayat"? "active" : NULL);?>" href="<?php echo base_url("user/riwayat/index_riwayat")?>">
                        <i class="ni ni-paper-diploma text-orange"></i> Riwayat Diklat
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "diklat"? "active" : NULL);?>" data-toggle="collapse" data-target="#submenudiklat" aria-controls="nav-link" aria-expanded="false" aria-label="Toggle submenu">
                        <i class="ni ni-paper-diploma text-orange"></i> Riwayat
                    </a>
                    <div class="collapse" id="submenudiklat">
                        <ul class="navbar-sub">
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($this->router->fetch_class() == "diklat"? "active" : NULL);?>" href="<?php echo base_url("user/riwayat/index_riwayat")?>">
                                    <i class="fa fa-bars text-default"></i> Riwayat Diklat
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo ($this->router->fetch_class() == "pendidikan"? "active" : NULL);?>" href="<?php echo base_url("user/riwayat/index_pendidikan")?>">
                                    <i class="fa fa-bars text-default"></i> Riwayat Pendidikan
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "bimteksosialisasi"? "active" : NULL);?>" href="<?php echo base_url("user/bimteksosialisasi")?>">
                        <i class="ni ni-paper-diploma text-orange"></i> Bimtek/Sosialisasi
                    </a>
                </li>
            </ul>
            <!-- Divider -->
        </div>
    </div>
</nav>