<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- META DESCRIPTION APLIKASI IDOL -->
    <!-- <meta name="description" content="Area User untuk Aplikasi IDOL"> -->
    <!-- META DESCRIPTION APLIKASI SITIMBEL -->
    <meta name="description" content="Area User untuk Aplikasi SITIMBEL">
    <meta name="author" content="BKPP x Diskominfo Kota Bandung">
    <!-- Title IDOL -->
    <!-- <title><?php echo strtoupper($this->session->userdata("username"))?> - IDOL User Dashboard</title> -->
    <!-- Title SITIMBEL -->
    <title><?php echo strtoupper($this->session->userdata("username"))?> - SITIMBEL User Dashboard</title>
    <!-- Favicon IDOL -->
    <!-- <link href="<?php echo asset_url();?>courseware/img/favidol.png" rel="icon" type="image/png"> -->
    <!-- Favicon SITIMBEL -->
    <link href="<?php echo asset_url();?>courseware/img/fav-icon-sitimbel.ico" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo asset_url();?>argon/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="<?php echo asset_url();?>argon/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="<?php echo asset_url();?>argon/css/argon.css?v=1.0.0" rel="stylesheet">
    <style>
        .nav-disabled{
            background-color: #5e72e4 !important;
            color : #fff !important;
        }
        .nav-disabled-default{
            background-color: #fff !important;
            color : #5e72e4 !important;
        }
        .nav-disabled-success{
            background-color: #1aae6f !important;
            color : #fff !important;
        }
        .nav-disabled-failed{
            background-color: #fb6340 !important;
            color : #fff !important;
        }
        .tambahan-margin-button-table{
            margin-bottom: 5px !important;
        }
        .table-row-hover > tbody > tr:hover{
            background: antiquewhite;
            cursor: pointer;
        }
    </style>
</head>