<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <?php if($this->session->flashdata("error_message")) {?>
                <div style="margin-top: 20px">
                    <div class="alert alert-danger" role="alert">
                        <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                    </div>
                </div>
                <?php }?>

                <!-- Card stats -->
                <?php if($this->session->flashdata("success_message")) {?>
                <div style="margin-top: 20px">
                    <div class="alert alert-success" role="alert">
                        <strong>Sukses!</strong> <?php echo $this->session->flashdata("success_message")?>
                        <a href="<?php echo base_url("user/riwayat/index_riwayat")?>" style="color:black">Klik untuk melihat riwayat diklat.</a>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="text-white mb-0"><?php echo $tablename ?></h3>
                            </div>
                            <div class="col text-right">
                                <div class="row">
                                    <div class="col-md-auto">
                                        <!-- Form -->
                                        <form class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                            <div class="form-group mb-0">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                    </div>
                                                    <input class="form-control" placeholder="Search" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- <div class="col-md-auto">
                                        <a href="<?php echo base_url("user/jadwal/tambah_jadwal/")?>" class="btn btn-md btn-primary">
                                            <i class="fas fa-plus"></i> Tambah Baru
                                        </a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark">
                            <thead class="thead-dark">
                            <tr>
                                <th>Jadwal ID</th>
                                <th>Jenis Diklat</th>
                                <th>Judul Diklat</th>
                                <th>Penyelenggara</th>
                                <th>Tempat</th>
                                <th>Jumlah Jam</th>
                                <th>Status Event</th>
                                <th colspan="2">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($row->result() as $rows){
                                if(!$rows->is_registered > 0){
                                echo "<tr>";
                                echo "<td scope=\"col\">$rows->diklat_jadwal_id</td>";
                                switch ($rows->diklat_jenis){
                                    case 1:
                                        echo "<td scope=\"col\">Struktural</td>";
                                        echo "<td scope=\"col\">$rows->struktural</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Fungsional</td>";
                                        echo "<td scope=\"col\">$rows->fungsional</td>";
                                        break;
                                    case 3:
                                        echo "<td scope=\"col\">Teknis</td>";
                                        echo "<td scope=\"col\">$rows->teknis</td>";
                                        break;
                                }
                                echo "<td scope=\"col\">$rows->diklat_penyelenggara</td>";
                                echo "<td scope=\"col\">$rows->diklat_tempat</td>";
                                echo "<td scope=\"col\">$rows->diklat_jumlah_jam</td>";
                                switch ($rows->status_event){
                                    case 0:
                                        echo "<td scope=\"col\">Hold</td>";
                                        break;
                                    case 1:
                                        echo "<td scope=\"col\">Ongoing</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Finished</td>";
                                        break;
                                }
                                echo "<td scope=\"col\">
                                    <a class='text-success' href='".base_url("user/jadwal/jadwal_registrasi_diklat/").$rows->diklat_jadwal_id."' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Data Diklat\"><i class='fa fa-edit'></i></a>
                                    </td>";
                                // echo "<td scope=\"col\">
                                //     <a class='text-danger' href='".base_url("user/jadwal/delete_jadwal/").$rows->diklat_jadwal_id."' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Data\"><i class='fa fa-trash'></i></a>
                                //     </td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer bg-gradient-default border-0">
                        <nav aria-label="Page navigation example">
                            <?php echo $pagination;?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>
