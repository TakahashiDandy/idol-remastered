<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <?php if(validation_errors() != null) {?>
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> <?php echo validation_errors();?>
                    </div>
                <?php }?>
                <?php if($this->session->flashdata("error_message") != null) {?>
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> <?php echo $this->session->flashdata("error_message");?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $pagename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('user/jadwal/jadwal_registrasi_diklat/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">
                        <input type="hidden" id="nip" name="nip" value=<?php echo $detail_user->nip ?>/>
                        <!--Insert Phase 1-->
                            <div id="phase1">
                                <h6 class="heading-small text-muted mb-4">Data Pribadi</h6>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama">Nama Lengkap</label>
                                            <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Nama Lengkap" required readonly value="<?php echo $detail_user->nama_lengkap ?>" >
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Golongan</label>
                                            <input type="text" class="form-control form-control-sm" name="golongan" id="golongan" placeholder="Golongan" required readonly value="<?php echo $detail_user->golongan ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Tempat Lahir</label>
                                            <input type="text" class="form-control form-control-sm" name="tempatlahir" id="tempatlahir" placeholder="Tempat Lahir" required readonly value="<?php echo $detail_user->tempat_lahir ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Tanggal Lahir</label>
                                            <input type="text" class="form-control form-control-sm" name="tgllahir" id="tgllahir" placeholder="Tanggal Lahir" required readonly value="<?php echo $detail_user->tanggal_lahir ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Jenis Kelamin</label>
                                            <input type="text" class="form-control form-control-sm" name="jeniskelamin" id="jeniskelamin" placeholder="Jenis Kelamin" required readonly value="<?php echo $detail_user->jenis_kelamin ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Nomor Kontak</label>
                                            <input type="text" class="form-control form-control-sm" name="nomorkontak" id="nomorkontak" placeholder="Nomor Kontak" required readonly value="<?php echo $detail_user->no_kontak ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Email</label>
                                            <input type="text" class="form-control form-control-sm" name="email" id="email" placeholder="Email" required readonly value="<?php echo $detail_user->email ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Alamat Rumah</label>
                                            <textarea class="form-control form-control-sm" rows="5" name="alamatrumah" id="alamatrumah" placeholder="Alamat Rumah" required readonly ><?php echo $detail_user->alamat_rumah ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama">Jabatan</label>
                                            <input type="text" class="form-control form-control-sm" name="jabatan" id="jabatan" placeholder="Jabatan" required readonly value="<?php echo $detail_user->jabatan ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Instansi</label>
                                            <input type="text" class="form-control form-control-sm" name="instansi" id="instansi" placeholder="Instansi" required readonly value="<?php echo $detail_user->instansi ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Pendidikan Terakhir</label>
                                            <input type="text" class="form-control form-control-sm" name="pendidikanterakhir" id="pendidikanterakhir" placeholder="Pendidikan Terakhir" required readonly value="<?php echo $detail_user->pendidikan_terakhir ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Kab/Kota</label>
                                            <input type="text" class="form-control form-control-sm" name="kabkota" id="kabkota" placeholder="Kab/Kota" required readonly value="<?php echo $detail_user->kotakab ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Provinsi</label>
                                            <input type="text" class="form-control form-control-sm" name="provinsi" id="provinsi" placeholder="Provinsi" required readonly value="<?php echo $detail_user->provinsi ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Telepon/Fax</label>
                                            <input type="text" class="form-control form-control-sm" name="telpfax" id="telpfax" placeholder="Telp/Fax" required readonly value="<?php echo $detail_user->telpfax ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama">Alamat Kantor</label>
                                            <textarea class="form-control form-control-sm" rows="5" name="alamatkantor" id="alamatkantor" placeholder="Alamat Kantor" required readonly ><?php echo $detail_user->alamat_rumah ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="btn btn-success" type="button" onclick="phase2()" value="Selanjutnya >">
                                </div>
                            </div>

                        <!--Insert Phase 2-->
                            <div id="phase2">
                                <!-- <div class="row"> -->
                                <h6 class="heading-small text-muted mb-4">Jenis Diklat</h6>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mb-4">
                                            <input type="hidden" name="diklat_jenis" value=<?php echo $this->session->userdata('diklat_jenis') ?>>
                                            <div class="form-check">
                                                <input class="custom-radio" type="radio" name="rb_diklat_jenis" id="rb_struk" value="1" readonly="true" disabled
                                                    <?php if($this->session->userdata('diklat_jenis') == NULL){
                                                        echo "checked";
                                                    } else if($this->session->userdata('diklat_jenis') == 1){
                                                        echo "checked";
                                                    }?>>
                                                <label class="form-control-label" for="rb_struk">
                                                    Struktural
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="custom-radio" type="radio" name="rb_diklat_jenis" id="rb_func" value="2" readonly="true" disabled
                                                    <?php echo ($this->session->userdata("diklat_jenis")==2 ? "checked" : NULL)?>>
                                                <label class="form-control-label" for="rb_func">
                                                    Fungsional
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="custom-radio" type="radio" name="rb_diklat_jenis" id="rb_tek" value="3" readonly="true" disabled
                                                    <?php echo ($this->session->userdata("diklat_jenis")==3 ?  "checked" : NULL)?>>
                                                <label class="form-control-label" for="rb_tek">
                                                    Teknis
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                    <h6 class="heading-small text-muted mb-4">Detail Diklat</h6>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="nama_diklat">
                                                    Nama Diklat
                                                </label>
                                                <?php echo form_error("id_diklat","<h5 class='text-danger'>","</h6>") ?>
                                                <input type="hidden" name="id_diklat" id="id_diklat"
                                                    value="<?php echo ($this->session->userdata("id_diklat")? $this->session->userdata("id_diklat") : NULL)?>">

                                                <input class="form-control" name="diklat_nama" id="diklat_nama" placeholder="Nama Diklat" type="text" readonly
                                                    value="<?php echo ($this->session->userdata("diklat_nama")? $this->session->userdata("diklat_nama") : NULL)?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_mulai">
                                                    Tanggal Mulai
                                                </label>
                                                <?php echo form_error("diklat_mulai","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_mulai" id="diklat_mulai" placeholder="Tanggal Mulai" type="date" readonly
                                                value="<?php echo ($this->session->userdata("diklat_mulai") ? $this->session->userdata("diklat_mulai") : NULL)?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_selesai">
                                                    Tanggal Akhir
                                                </label>
                                                <?php echo form_error("diklat_selesai","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_selesai" id="diklat_selesai" placeholder="Tanggal Selesai" type="date" readonly
                                                value="<?php echo ($this->session->userdata("diklat_selesai") ? $this->session->userdata("diklat_selesai") : NULL)?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_jumlah_jam">
                                                    Jumlah Jam
                                                </label>
                                                <?php echo form_error("diklat_jumlah_jam","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_jumlah_jam" id="diklat_jumlah_jam" placeholder="Jumlah Jam" type="text" readonly
                                                    value="<?php echo ($this->session->userdata("diklat_jumlah_jam") ? $this->session->userdata("diklat_jumlah_jam") : NULL)?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_jumlah_peserta">
                                                    Jumlah Peserta
                                                </label>
                                                <?php echo form_error("diklat_jumlah_peserta","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_jumlah_peserta" id="diklat_jumlah_peserta" placeholder="Jumlah Peserta" type="text" readonly
                                                    value="<?php echo ($this->session->userdata("diklat_jumlah_peserta") ? $this->session->userdata("diklat_jumlah_peserta") : NULL)?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_tempat">
                                                    Tempat
                                                </label>
                                                <?php echo form_error("diklat_tempat","<h5 class='text-danger'>","</h6>") ?>
                                                <textarea class="form-control" id="diklat_tempat" name="diklat_tempat" placeholder="Tempat" rows="3" readonly
                                                ><?php echo ($this->session->userdata("diklat_tempat") ? $this->session->userdata("diklat_tempat") : "")?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_deskripsi">
                                                    Deskripsi Diklat
                                                </label>
                                                <?php echo form_error("diklat_deskripsi","<h5 class='text-danger'>","</h6>") ?>
                                                <textarea class="form-control" id="diklat_deskripsi" name="diklat_deskripsi" placeholder="Deskripsi Diklat" rows="5" readonly
                                                ><?php echo ($this->session->userdata("diklat_deskripsi") ? $this->session->userdata("diklat_deskripsi") : "")?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <!-- </div> -->
                                <div class="form-group">
                                    <input class="btn btn-primary" type="button" onclick="phase1()" value="< Sebelumnya">
                                    <input class="btn btn-success" type="button" onclick="phase3()" value="Selanjutnya >">
                                    <input class="btn btn-danger" type="reset" onclick="phase1()" value="Cancel">
                                </div>
                            </div>

                        <!--Insert Phase 3-->
                            <div id="phase3">
                                <h6 class="heading-small text-muted mb-4">Penyelenggara Diklat</h6>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_usul_no">
                                                    No. Diklat Usul
                                                </label>
                                                <?php echo form_error("diklat_usul_no","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_usul_no" id="diklat_usul_no" placeholder="No. Diklat Usul" type="text" readonly
                                                value="<?php echo ($this->session->userdata("diklat_usul_no") ? $this->session->userdata("diklat_usul_no") : NULL)?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_usul_tgl">
                                                    Diklat Usul Tanggal
                                                </label>
                                                <?php echo form_error("diklat_usul_tgl","<h5 class='text-danger'>","</h6>") ?>
                                                <input class="form-control" name="diklat_usul_tgl" id="diklat_usul_tgl" placeholder="Diklat Usul Tanggal" type="text" readonly
                                                value="<?php echo ($this->session->userdata("diklat_usul_tgl") ? $this->session->userdata("diklat_usul_tgl") : NULL)?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_penyelenggara">
                                                    Penyelenggara
                                                </label>
                                                <?php echo form_error("diklat_penyelenggara","<h5 class='text-danger'>","</h6>") ?>
                                                <textarea class="form-control" id="diklat_penyelenggara" name="diklat_penyelenggara" placeholder="Penyelenggara Diklat" rows="3" readonly
                                                ><?php echo ($this->session->userdata("diklat_penyelenggara") ? $this->session->userdata("diklat_penyelenggara") : "")?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="diklat_penyelenggara">
                                                    Dokumen Syarat Diklat
                                                </label>
                                                <div class="row">
                                                    <?php $i = 1; if(isset($attachment)){
                                                        foreach ($attachment as $data){
                                                            echo '<div class="col-lg-12"> Syarat ke-'.$i++.' : &nbsp;&nbsp;&nbsp;';
                                                            echo '<a href="'.base_url().$data["filepath"].'" target="_blank">Klik untuk lihat persyaratan diklat ini.</a>';
                                                            echo '</div>';
                                                        }
                                                    } else{
                                                        echo '<div class="col-lg-auto">';
                                                        echo '<a>Tidak Ada Syarat yang Diupload</a>';
                                                        echo '</div>';
                                                    }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="btn-add-more" type="button"  class="btn btn-primary">Unggah Semua Persyaratan Diklat</button>
                                        </div>
                                    </div>
                                    <label>(Tipe file: gif|jpg|png|pdf|docx|doc | Maksimal ukuran file 1 Mb)</label>
                                    <br>

                                    <div class="input_fields_wrap">
                                    </div>

                                    <hr class="my-4">

                                <div class="form-group">
                                    <input class="btn btn-primary" type="button" onclick="phase2()" value="< Sebelumnya">
                                    <!-- <input class="btn " type="button" id="submitregistrasi" onclick="checkregistrant()" value="Submit"> -->
                                    <input class="btn btn-success" type="submit" id="submitregistrasi" value="Kirim">
                                    <input class="btn btn-danger" type="reset" onclick="phase1()" value="Batal">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
<script>
    phase1();
    $(document).ready(function() {
        getdatarow(1);
    });

    var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $("#btn-add-more"); //Add button ID
	
	var x = 0; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
            element =   "<div class='row'><div class='col-md-8'>"+
                            "<label for='search'>Persyaratan Diklat "+x+" </label>"+
                            "<input class='form-control' name='syarat"+x+"' id='syarat"+x+"' placeholder='File syarat ke-"+x+"' type='text'>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button onclick='browse_file("+x+")' id='btn-syarat"+x+"' type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Pilih File</button>"+
                            "<input id='file-syarat"+x+"' name='lampiran[]' type='file' multiple hidden/>"+
                        "</div>"+
                        "<div class='col-md-2'>"+
                            "<button type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Hapus</button>"+
                        "</div></div>";
			$(wrapper).append(element); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent().parent().remove(); x--;
	})

    // i = 0;
    function browse_file(e){
        i = e;
        $("#file-syarat"+i).click();

        $("#file-syarat"+i).change(function(e){
            $("#syarat"+i).html($(this).val());
            $("#syarat"+i).val($(this).val());
        });
    }
</script>
</body>

</html>
