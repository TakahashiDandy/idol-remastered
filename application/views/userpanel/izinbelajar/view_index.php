<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                    <?php if($this->session->flashdata("success_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Selamat!</strong> <?php echo $this->session->flashdata("success_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-gradient-default shadow">
                        <div class="card-header bg-transparent border-0">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="text-white mb-0"></h2>
                                </div>
                                <hr>
                                <div class="col">
                                    <h3 class="text-white mb-0">Izin Belajar</h3>
                                </div>
                                <div class="col-md-auto text-right">
                                    <div class="row">
                                        <div class="col-md-auto">
                                            <form
                                                class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                                <div class="form-group mb-0">
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-search"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="Search" type="text">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-auto">
                                            <a href="<?php echo base_url("user/izinbelajar/pengajuanizinbelajar")?>"
                                                class="btn btn-md btn-primary">
                                                <i class="fas fa-plus"></i> Mengajukan Izin Belajar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-dark">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Lihat Proses</th>
                                        <th>Aksi (Shortcut)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($row->result() as $rows){
                                            echo '<tr>';
                                            echo '<td style="white-space:normal">'.$rows->belajar_pendidikan_nm.'
                                            </td>';
                                            $tahap = "";
                                            $btn_type = "";
                                            $status = "";
                                            $bg_type = "";
                                            if($rows->status_reg_belajar == 0){
                                                if($rows->status_rekomendasi == 0){
                                                    $tahap = "Proses: Menunggu Surat Rekomendasi";
                                                    $btn_type = "btn-secondary";
                                                    $status = "Menunggu Konfirmasi Rekomendasi Oleh OPD";
                                                    $bg_type = "bg-secondary";
                                                }else if($rows->status_rekomendasi == 1){
                                                    $tahap = "Proses: Pengajuan Berkas Izin Belajar";
                                                    $btn_type = "btn-primary";
                                                    $status = "Surat Rekomendasi Disetujui OPD";
                                                    $bg_type = "bg-success";
                                                }else{
                                                    $tahap = "Oops terjadi kesalahan, segera muat ulang.";
                                                    $btn_type = "btn-warning";
                                                    $status = "terjadi kesalahan";
                                                    $bg_type = "bg-warning";
                                                }
                                            }else if($rows->status_reg_belajar == 1){
                                                $tahap = "Proses: Menunggu Verifikasi OPD";
                                                $btn_type = "btn-secondary";
                                                $status = "Menunggu Verifikasi OPD";
                                                $bg_type = "bg-secondary";
                                            }else if($rows->status_reg_belajar == 2){
                                                $tahap = "Proses: Ajukan Berkas ke BKPP";
                                                $btn_type = "btn-primary";
                                                $status = "Berkas Izin Belajar Disetujui OPD";
                                                $bg_type = "bg-success";
                                            }else if($rows->status_reg_belajar == 3){
                                                $tahap = "Proses: Permohonan Ditangguhkan OPD";
                                                $btn_type = "btn-warning";
                                                $status = "Berkas masih ada yang belum sesuai";
                                                $bg_type = "bg-warning";
                                            }else if($rows->status_reg_belajar == 4){
                                                $tahap = "Proses: Menunggu Verifikasi BKPP";
                                                $btn_type = "btn-secondary";
                                                $status = "Menunggu Verifikasi BKPP";
                                                $bg_type = "bg-secondary";
                                            }else if($rows->status_reg_belajar == 5){
                                                $tahap = "Proses: Penerbitan Surat Izin Belajar";
                                                $btn_type = "btn-success";
                                                $status = "Izin belajar diterima, menunggu penerbitan surat";
                                                $bg_type = "bg-success";
                                            }else if($rows->status_reg_belajar == 6){
                                                $tahap = "Proses: Permohonan Ditangguhkan BKPP";
                                                $btn_type = "btn-warning";
                                                $status = "Berkas masih ada yang belum sesuai";
                                                $bg_type = "bg-warning";
                                            }else if($rows->status_reg_belajar == 7){
                                                $tahap = "Proses: Izin Belajar Dimulai";
                                                $btn_type = "btn-success";
                                                $status = "Izin belajar telah disetujui dan terbit";
                                                $bg_type = "bg-success";
                                            }else if($rows->status_reg_belajar == 9){
                                                $tahap = "Proses: Permohonan Ditolak";
                                                $btn_type = "btn-danger";
                                                $status = "Permohonan izin belajar ditolak";
                                                $bg_type = "bg-danger";
                                            }else{
                                                $tahap = "Oops terjadi kesalahan, segera muat ulang.";
                                                $btn_type = "btn-warning";
                                                $status = "terjadi kesalahan";
                                                $bg_type = "bg-warning";
                                            }
                                            echo '<td class="status" style="white-space:normal"><span class="badge badge-dot mr-4">
                                            <i class="'.$bg_type.'"></i> '.$status.'
                                            </span>
                                            </td>';
                                            echo '<td style="white-space:normal">
                                            <a href="'.base_url("user/izinbelajar/prosesizinbelajar/".$rows->belajar_reg_id).'"
                                                class="btn btn-icon btn-3 '.$btn_type.' btn-sm">
                                                <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                <span class="btn-inner--text">'.$tahap.'</span>
                                            </a>
                                            </td>';
                                            if($rows->status_reg_belajar == 0){
                                                if($rows->status_rekomendasi == 0){
                                                    echo '<td class="text-right" style="white-space:normal">Menunggu Konfirmasi OPD</td>';
                                                }else if($rows->status_rekomendasi == 1){
                                                    echo '<td class="text-right" style="white-space:normal">
                                                    <div class="dropdown">
                                                        <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            <a class="dropdown-item" href="'.base_url("user/izinbelajar/pengajuanberkas/".$rows->belajar_reg_id).'">Ajukan Berkas Izin Belajar ke OPD</a>
                                                        </div>
                                                    </div>
                                                    </td>';
                                                }else{
                                                    echo '<td class="text-right" style="white-space:normal">-</td>';
                                                }
                                            }else if($rows->status_reg_belajar == 1){
                                                echo '<td class="text-right" style="white-space:normal">Menunggu Verifikasi OPD</td>';
                                            }else if($rows->status_reg_belajar == 2){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/pengajuanbkpp").'">Ajukan Izin Belajar ke BKPP</a>
                                                        <a target="_blank" class="dropdown-item" href="'.base_url('functions/reports/sumoizin/'.$rows->belajar_reg_id).'">Unduh/Download Berkas Surat Permohonan Izin Belajar</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($rows->status_reg_belajar == 3){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/perbaikiBerkasOPD").'">Perbaiki Berkas</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($rows->status_reg_belajar == 4){
                                                echo '<td class="text-right" style="white-space:normal">Menunggu Verifikasi BKPP</td>';
                                            }else if($rows->status_reg_belajar == 5){
                                                echo '<td class="text-right" style="white-space:normal">Menunggu Terbit Surat</td>';
                                            }else if($rows->status_reg_belajar == 6){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/perbaikiBerkasBKPP").'">Perbaiki Berkas</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($rows->status_reg_belajar == 7){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("reportGenerator").'">Unduh Scan Surat Terbit Izin Belajar</a>
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/perpanjangan/".$rows->belajar_reg_id).'">Mengajukan Perpanjangan</a>
                                                    </div> 
                                                </div>
                                                </td>';
                                            }else if($rows->status_reg_belajar == 9){
                                                echo '<td class="text-right" style="white-space:normal">Izin Belajar ditolak</td>';
                                            }else{
                                                echo '<td class="text-right" style="white-space:normal">Oops terjadi kesalahan, kontak Admin@idol.bandung.go.id</td>';
                                            }
                                            echo '</tr>';
                                        } ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer bg-gradient-default border-0">
                            <nav aria-label="Page navigation example">
                                <?php echo $pagination;?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>