<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <?php
$now_data_master_belajar = $data_master_belajar->result();
$now_data_master_belajar_trans_upload = $data_master_belajar_trans_upload->result();
$now_data_master_belajar_trans_tolak = $data_master_belajar_trans_tolak->result();
$now_data_master_belajar_trans_tolak_2 = $data_master_belajar_trans_tolak_2->result();
$now_data_master_belajar_trans_upload_2 = $data_master_belajar_trans_upload_2->result();
$now_data_master_belajar_trans_laporan = $data_master_belajar_trans_laporan->result();
$now_data_master_belajar_trans_perpanjangan = $data_master_belajar_trans_perpanjangan->result();
$nav_default = "active";
$nav_disable = "disabled nav-disabled-default";
$nav_success = "nav-disabled-success";
$nav_final = "active nav-disabled-success";
$nav_failed = "active nav-disabled-failed";
$nav_forbidden = "disabled nav-disabled-failed";
//,"Surat Keterangan Terdaftar sebagai Mahasiswa dari Perguruan"
$array_dokumen_upload = array("Surat Rekomendasi Tugas Belajar", 
        "Surat pernyataan bermaterai tidak pindah tugas", 
        "Surat Pernyataan Bermaterai Pengembalian Biaya", 
        "Surat keterangan dari lembaga pemberi beasiswa",
        "Pas Foto Berwarna Terbaru",
        "Ijazah Terakhir",
        "Transkrip Nilai Terakhir",
        "SK Terakhir",
        "SK Jabatan Terakhir",
        "Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir", 
        "Surat Keterangan Terdaftar Perguruan Tinggi / LOA");
$array_dokumen_upload_2 = array("Surat Permohonan Tugas Belajar", 
        "Surat Keterangan OPD", 
        "Surat Analisa Kebutuhan Pegawai");
?>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                    <?php if($this->session->flashdata("success_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Selamat!</strong> <?php echo $this->session->flashdata("success_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Proses Pengajuan Izin Belajar</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                        <div class="card-body">

                            <!-- PILL -->
                            <div class="nav-wrapper">
                                <ul class="nav nav-pills nav-pills-circle" id="tabs_2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 0){
                                        if($now_data_master_belajar[0]->status_rekomendasi == 0){
                                            echo $nav_default;
                                        }else if($now_data_master_belajar[0]->status_rekomendasi == 1){
                                            echo $nav_default;
                                        }else{
                                            echo $nav_forbidden;
                                        }
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_forbidden;
                                    }else{
                                        echo $nav_success;
                                    }
                                    ?>" id="proses1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">1</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 0 ){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                        echo $nav_default;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                        echo $nav_failed;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_forbidden;
                                    }else{
                                        echo $nav_success;
                                    }
                                    ?>" id="proses2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">2</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar < 2 ){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 2){
                                        echo $nav_default;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_forbidden;
                                    }else{
                                        echo $nav_success;
                                    }
                                    ?>" id="proses3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">3</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar < 4 ){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 4){
                                        echo $nav_default;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 6){
                                        echo $nav_failed;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_forbidden;
                                    }else{
                                        echo $nav_success;
                                    }
                                    ?>" id="proses4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">4</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar < 5 ){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 6){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 5){
                                        echo $nav_default;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_forbidden;
                                    }else{
                                        echo $nav_success;
                                    }
                                    ?>" id="proses5-tab" data-toggle="tab" href="#tabs-icons-text-5" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">5</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle" id="proses2-tab" style="background-color:#F4F7FA;-webkit-box-shadow: none;
	                                            -moz-box-shadow: none;
	                                            box-shadow: none;" aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block"><i class="ni ni-bold-right"></i></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link rounded-circle <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar < 7 ){
                                        echo $nav_disable;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 7){
                                        echo $nav_final;
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                        echo $nav_failed;
                                    }else{
                                        echo $nav_final;
                                    }
                                    ?>" id="proses6-tab" data-toggle="tab" href="#tabs-icons-text-6" role="tab"
                                            aria-controls="home" aria-selected="true">
                                            <span class="nav-link-icon d-block">6</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- PILL -->

                            <!-- CONTENT PILL -->
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 0){
                                        echo 'active';
                                    }
                                    ?>" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                            <!-- TAHAP 1 -->
                                            <h4>Tahap Rekomendasi Izin Belajar</h4>
                                            <p>
                                                <?php
                                            if($now_data_master_belajar[0]->status_reg_belajar == 0){
                                                if($now_data_master_belajar[0]->status_rekomendasi == 0){
                                                    echo '<br>Status : <span class="badge badge-default">Menunggu Konfirmasi Surat Rekomendasi</span>';
                                                }else if($now_data_master_belajar[0]->status_rekomendasi == 1){
                                                    echo '<br>Status : <span class="badge badge-success">Surat Rekomendasi Disetujui</span>';
                                                }else{
                                                    echo '<br>Status : <span class="badge badge-danger">Oops, terjadi kesalahan</span>';
                                                }
                                            }else if($now_data_master_belajar[0]->status_reg_belajar < 6 && $now_data_master_belajar[0]->status_reg_belajar > 0){
                                                echo '<br>Status : <span class="badge badge-success">Surat Rekomendasi Disetujui</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 7 ){
                                                echo '<br>Status : <span class="badge badge-success">Izin Belajar Diterima</span>';
                                            }else{
                                                echo '<br>Status : <span class="badge badge-success">Selesai</span>';
                                            }
                                            ?>
                                            </p>
                                            <?php
                                            if($now_data_master_belajar[0]->status_rekomendasi == 1){
                                                echo '<hr>';
                                                echo '<a href="'.base_url('functions/reports/sunyaizin/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                target="_blank" class="btn btn-icon btn-3 btn-primary btn-sm"> 
                                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span> Unduh/Download Berkas Surat Pernyataan Bermaterai</a><br>';
                                            }
                                        if($now_data_master_belajar[0]->status_reg_belajar == 0){
                                            if($now_data_master_belajar[0]->status_rekomendasi == 0){
                                                echo '<hr>';
                                                echo '<p class="description"> Menunggu Konfirmasi Surat Rekomendasi</p>';
                                            }else if($now_data_master_belajar[0]->status_rekomendasi == 1){
                                                echo '<hr>';
                                                echo '<p class="description"> <strong>Persiapkan Data Berkas: </strong><br><br>
                                                        <strong>Untuk Scan diharuskan dokumen/berkas asli bukan photo copy</strong><br><br>
                                                        1. Scan & Hardcopy Surat Rekomendasi Izin Belajar;<br><br>
                                                        2. Scan & Hardcopy Surat pernyataan bermaterai Rp.6000,- untuk<br>
                                                        tidak pindah tugas paling kurang 2 (dua) kali<br>
                                                        masa pendidikan ditambah 1 (satu) tahun;<br><br>
                                                        3. *Untuk Penerima Beasiswa | Scan & Hardcopy surat pernyataan bermaterai Rp.6000,-<br>
                                                        untuk mengembalikan seluruh biaya yang telah<br>
                                                        dikeluarkan apabila tidak dapat menyelesaikan<br>
                                                        pendidikannya atau menarik diri dari<br>
                                                        pendidikannya, kecuali sakit atau meninggal dunia;<br><br>
                                                        4. *Untuk Penerima Beasiswa | Scan & Hardcopy Surat keterangan dari lembaga pemberi beasiswa<br>
                                                        yang menerangkan bahwa yang bersangkutan diberikan beasiswa oleh lembaga tersebut;<br><br>
                                                        5. Scan Pas Foto Berwarna Terbaru & Pas photo Berwarna terbaru ukuran 3x4 sebanyak 2 (dua) buah;<br><br>
                                                        6. Scan & Hardcopy Photo Copy ijazah terakhir di legalisir;<br><br>
                                                        7. Scan & Hardcopy Photo Copy Transkrip nilai terakhir dilegalisir;<br><br>
                                                        8. Scan & Hardcopy Photo Copy SK Terakhir;<br><br>
                                                        9. Scan & Hardcopy Photo Copy SK Jabatan Terakhir (bagi yang menduduki Jabatan);<br><br>
                                                        10. Scan & Hardcopy Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir.<br><br>
                                                        11. Scan & Hardcopy Surat Keterangan Terdaftar Perguruan Tinggi / LOA;<br><br>
                                                    </p>';
                                            }else{
                                                echo '<hr>';
                                                echo '<p class="description">Oops, terjadi kesalahan</p>';
                                            }
                                        }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                            echo '<hr>';
                                            echo '<p class="description">Sudah mengajukan berkas izin belajar, menunggu verifikasi OPD. Silahkan untuk menunggu.</p>';
                                        }
                                        if($now_data_master_belajar[0]->status_reg_belajar == 0 && $now_data_master_belajar[0]->status_rekomendasi == 1){
                                            echo '<hr>';
                                            echo '<a href="'.base_url('user/izinbelajar/pengajuanberkas/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Pengajuan Berkas Izin Belajar</span>
                                                    </a>';
                                        }
                                        ?>
                                        </div>
                                        <div class="tab-pane fade show <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 1 || $now_data_master_belajar[0]->status_reg_belajar == 3){
                                        echo 'active';
                                    }
                                    ?>" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                            <h4>Tahap Proses Verifikasi Berkas Oleh OPD</h4>
                                            <p>
                                                <?php
                                            if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                echo '<br>Status : <span class="badge badge-primary">Menunggu Verifikasi OPD</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                                echo '<br>Status : <span class="badge badge-warning">Ditangguhkan OPD, Segera Perbaiki</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar > 3){
                                                echo '<br>Status : <span class="badge badge-success">Izin Belajar Disetujui</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 2){
                                                echo '<br>Status : <span class="badge badge-success">Berkas Izin Belajar Disetujui</span>';
                                            }else{
                                                echo '<br>Status : <span class="badge badge-danger">Oops, terjadi kesalahan</span>';
                                            }
                                            ?>
                                            </p>
                                            <hr>
                                            <p class="description">Dokumen :</p>
                                            <?php
                                                $count_num = 0;
                                                foreach($now_data_master_belajar_trans_upload as $data_upload_tahap_satu){
                                                    if($data_upload_tahap_satu->upload_proses == 0){
                                                        echo '<a href="'.base_url($data_upload_tahap_satu->upload_file).'"
                                                                target="_blank" class="btn btn-icon btn-3 btn-primary btn-sm"> 
                                                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span> Download : '.$array_dokumen_upload[$data_upload_tahap_satu->upload_posisi].'</a><br><br>';
                                                        $count_num++;
                                                    }
                                                }
                                                if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                                    echo '<p class="description">Permohonan berkas izin belajar ditangguhkan, silahkan segera diperbaiki</p>
                                                    <p class="description"> Catatan Perbaikan :<br><strong>';
                                                        foreach($now_data_master_belajar_trans_tolak as $tolak){
                                                            echo $tolak->tolak_comment;
                                                        }
                                                    echo '</strong></p>';
                                                    echo '<hr>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 1){
                                                    echo '<p class="description">Menunggu Konfirmasi OPD</p>';
                                                }else if($now_data_master_belajar[0]->status_reg_belajar == 9){
                                                    echo '<p class="description">Mohon maaf, permohonan anda ditolak, segera hubungi OPD terkait.</p>';
                                                }else{
                                                }
                                            ?>
                                            <?php
                                        if($now_data_master_belajar[0]->status_reg_belajar == 3){
                                            echo '<a href="'.base_url("user/izinbelajar/perbaikanberkas/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-warning btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Perbaikan Berkas Izin Belajar</span>
                                                    </a>';
                                        }
                                        ?>
                                        </div>
                                        <div class="tab-pane fade show <?php
                                        if($now_data_master_belajar[0]->status_reg_belajar == 2 ){
                                            echo 'active';
                                        }
                                    ?>" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                            <h4>Tahap Permohonan Izin Belajar kepada BKPP</h4>
                                            <p>
                                                <?php
                                            if($now_data_master_belajar[0]->status_reg_belajar == 2){
                                                echo '<br>Status : <span class="badge badge-default">Pengajuan Izin Belajar ke BKPP</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar > 3 ){
                                                echo '<br>Status : <span class="badge badge-success">Sudah dikerjakan, menunggu konfirmasi BKPP</span>';
                                            }else{
                                                echo '<br>Status : <span class="badge badge-success">Selesai</span>';
                                            }
                                            ?>
                                            </p>
                                            <hr>
                                            <?php
                                        //Disini Gaes
                                        if($now_data_master_belajar[0]->status_reg_belajar == 2 ){
                                            echo '<a href="'.base_url('functions/reports/sumoizin/'.$now_data_master_belajar[0]->belajar_reg_id).'"
                                                target="_blank" class="btn btn-icon btn-3 btn-primary btn-sm"> 
                                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span> Unduh/Download Berkas Surat Permohonan Izin Belajar</a><br>';
                                            echo '<hr class="my-4">';
                                            echo '<p class="description"><strong> Persiapkan Data untuk di unggah:<br>
                                                    Untuk Scan diharuskan dokumen/berkas asli bukan photo copy</strong><br><br>
                                                    1. Scan Surat Keterangan dari Kepala Perangkat Daerah;<br>
                                                    2. Scan Surat Rekomendasi Kebutuhan Pegawai dari Kepala Perangkat Daerah <br>
                                                    3. Scan Surat Permohonan Izin Belajar yang bersangkutan ditujukan ke Wali Kota
                                                    melalui Sekretaris Daerah;<br>
                                                    </p>';
                                            echo '<hr class="my-4">';
                                            echo '<a href="'.base_url("user/izinbelajar/pengajuanbkpp/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                    class="btn btn-icon btn-3 btn-primary btn-md">
                                                    <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                    <span class="btn-inner--text">Pengajuan Izin Belajar BKPP</span>
                                                    </a>';
                                        }else if($now_data_master_belajar[0]->status_reg_belajar > 3){
                                            echo '<p class="description"> Segera kumpulkan berkas fisik dalam map Menunggu Konfirmasi BKPP <br></p>';
                                        }else{
                                            echo '<p class="description"> Selesai, telah dikonfirmasi BKPP <br></p>';
                                        }
                                        ?>
                                        </div>

                                        <div class="tab-pane fade show <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 4 ||  $now_data_master_belajar[0]->status_reg_belajar == 6){
                                        echo 'active';
                                    }
                                    ?>" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                                            <h4>Tahap Proses Verifikasi dan Persetujuan BKPP</h4>
                                            <p>
                                                <?php
                                            if($now_data_master_belajar[0]->status_reg_belajar == 4 ){
                                                echo '<br>Status : <span class="badge badge-primary">Menunggu Konfirmasi BKPP</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 5 ){
                                                echo '<br>Status : <span class="badge badge-success">Disetujui BKPP, Menunggu Terbit Surat Kepwal Izin Belajar</span>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 6 ){
                                                echo '<br>Status : <span class="badge badge-warning">Ditangguhkan BKPP</span>';
                                            }else{
                                                echo '<br>Status : <span class="badge badge-success">Disetujui BKPP, Surat Kepwal Izin Belajar Telah Terbit</span>';
                                            }
                                            ?>
                                            </p>
                                            <?php
                                        echo '<h6 class="description">Dokumen : </h6>';
                                        $count_num_2 = 0;
                                        foreach($now_data_master_belajar_trans_upload_2 as $data_upload_tahap_dua){
                                            if($data_upload_tahap_dua->upload_proses == 2){
                                                echo '<a href="'.base_url($data_upload_tahap_dua->upload_file).'"
                                                            target="_blank" class="btn btn-icon btn-3 btn-primary btn-sm"> 
                                                            <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span> Download : '.$array_dokumen_upload_2[$data_upload_tahap_dua->upload_posisi].'</a><br><br>';
                                                $count_num_2++;
                                            }
                                        }
                                        if($now_data_master_belajar[0]->status_reg_belajar == 6 ){
                                            echo '<p class="message">Silahkan segera diperbaiki</p>';
                                            echo '<p class="description"> Catatan Perbaikan :<br>';
                                            foreach($now_data_master_belajar_trans_tolak_2 as $tolak){
                                                echo $tolak->tolak_comment;
                                            }
                                            echo '</p>';
                                            echo '<a href="'.base_url("user/izinbelajar/perbaikanberkasbkpp/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                        class="btn btn-icon btn-3 btn-warning btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Perbaikan Persyaratan</span>
                                                        </a>';
                                        }else if($now_data_master_belajar[0]->status_reg_belajar == 4 ){
                                            echo '<p class="description">Menunggu persetujuan BKPP</p>';
                                        }else{
                                            echo '<p class="description">Sudah Disetujui BKPP</p>';
                                        }
                                        ?>
                                        </div>
                                        <div class="tab-pane fade show <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 5){
                                        echo 'active';
                                    }
                                    ?>" id="tabs-icons-text-5" role="tabpanel" aria-labelledby="tabs-icons-text-5-tab">
                                            <?php if($now_data_master_belajar[0]->status_reg_belajar == 5){ ?>
                                            <h4>Tahap Penerbitan Kepwal Izin Belajar</h4>
                                            <p class="description">Izin Belajar anda sudah disetujui BKPP, Silahkan
                                                menunggu penerbitan Kepwal Izin Belajar</p>
                                            <?php }else{ ?>
                                            <h4>Tahap Penerbitan Kepwal Izin Belajar</h4>
                                            <p class="description">Izin Belajar anda sudah disetujui BKPP, Kepwal Izin
                                                Belajar telah terbit.</p>
                                            <?php } ?>
                                        </div>

                                        <div class="tab-pane fade show <?php
                                    if($now_data_master_belajar[0]->status_reg_belajar == 7 ){
                                        echo 'active';
                                    }else if($now_data_master_belajar[0]->status_reg_belajar == 9 ){
                                        echo 'active';
                                    }
                                    ?>" id="tabs-icons-text-6" role="tabpanel" aria-labelledby="tabs-icons-text-6-tab">
                                            <h4>Tahap Izin Belajar Terbit <?php
                                            if($now_data_master_belajar[0]->status_reg_belajar == 7 ){
                                                echo '<p class="description">Izin Belajar telah terbit, silahkan untuk mengunduh / mendownload scan kepwal terbit izin belajar.<br>
                                                Atau bisa dengan mendapatkan salinannya di BKPP</p>';
                                                echo '<hr class="my-4">';
                                                echo '<p class="description">Selamat Izin Belajar anda telah terbit, Silahkan klik berikut jika ingin melakukan perpanjangan</p>';
                                                echo '<a href="'.base_url("user/izinbelajar/perpanjangan/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                        <span class="btn-inner--text">Perpanjangan Izin Belajar</span>
                                                        </a>';
                                                echo '<a href="'.base_url("dokumenGenerator/".$now_data_master_belajar[0]->belajar_reg_id).'"
                                                        class="btn btn-icon btn-3 btn-primary btn-md">
                                                        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                                                        <span class="btn-inner--text">Unduh Scan Kepwal Terbit Izin Belajar</span>
                                                        </a>';
                                            }else if($now_data_master_belajar[0]->status_reg_belajar == 9 ){
                                                echo '<p class="description">Izin Belajar Ditolak</p>';
                                            }
                                            ?></h4>
                                            <?php
                                        echo '<hr class="my-4">';
                                        ?>
                                        <div class="table-responsive">
                                        <table class="table align-items-center table-white">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th>#</th>
                                                <th>Alasan Perpanjangan</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i = 0;
                                            foreach ($now_data_master_belajar_trans_perpanjangan as $perpanjangan) {
                                                $i++;
                                                echo '<tr>';
                                                echo '<td style="white-space:normal">'.$i.'</td>';
                                                echo '<td style="white-space:normal">'.$perpanjangan->belajar_alasan.'</td>';
                                                if($perpanjangan->status_perpanjangan == 0){
                                                    echo '<td style="white-space:normal">
                                                        <span class="badge badge-primary">Menunggu Konfirmasi BKPP</span>
                                                    </td>';
                                                }else if($perpanjangan->status_perpanjangan == 1){
                                                    echo '<td style="white-space:normal">
                                                        <span class="badge badge-success">Disetujui BKPP</span>
                                                    </td>';
                                                }else{
                                                    echo '<td style="white-space:normal">
                                                        <span class="badge badge-primary">Ditolak BKPP</span>
                                                    </td>';
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        </div>
                                        <?php
                                        echo '<hr class="my-4">';
                                        if($now_data_master_belajar[0]->status_reg_belajar == 7 ){
                                        }else if($now_data_master_belajar[0]->status_reg_belajar == 9 ){
                                            echo '<p class="description">Mohon maaf, izin belajar anda ditolak. Hubungi pihak terkait</p>';
                                        }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CONTENT PILL -->

                        </div>
                        <!-- END INPUT AREA-->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>
    <!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>