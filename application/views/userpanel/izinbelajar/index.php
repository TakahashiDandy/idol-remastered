<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<!---- MUST DELETED ---->

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                    <?php if($this->session->flashdata("success_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Selamat!</strong> <?php echo $this->session->flashdata("success_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-gradient-default shadow">
                        <div class="card-header bg-transparent border-0">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <h2 class="text-white mb-0"></h2>
                                </div>
                                <hr>
                                <div class="col">
                                    <h3 class="text-white mb-0">Izin Belajar</h3>
                                </div>
                                <div class="col-md-auto text-right">
                                    <div class="row">
                                        <div class="col-md-auto">
                                            <form
                                                class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                                <div class="form-group mb-0">
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-search"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="Search" type="text">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-auto">
                                            <a href="<?php echo base_url("user/izinbelajar/inputIzinBelajar")?>"
                                                class="btn btn-md btn-primary">
                                                <i class="fas fa-plus"></i> Ajukan Izin Belajar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-dark">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Nama</th>
                                        <th>Status</th>
                                        <th>Lihat Proses</th>
                                        <th>Aksi (Shortcut)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data_master_belajar->result() as $master_belajar){
                                            echo '<tr>';
                                            echo '<td style="white-space:normal">'.$master_belajar->belajar_pendidikan_nm.'
                                            </td>';
                                            $tahap = "";
                                            $btn_type = "";
                                            $status = "";
                                            $bg_type = "";
                                            if($master_belajar->status_reg_belajar == 0){
                                                $tahap = "Proses Izin Belajar : 1. Tahap Pengumpulan Dokumen";
                                                $btn_type = "btn-secondary";
                                                $status = "Pengumpulan Dokumen";
                                                $bg_type = "bg-info";
                                            }else if($master_belajar->status_reg_belajar == 1){
                                                $tahap = "Proses Izin Belajar : 1. Tahap Perbaikan Dokumen";
                                                $btn_type = "btn-danger";
                                                $status = "Ditangguhkan Kepala OPD";
                                                $bg_type = "bg-danger";
                                            }else if($master_belajar->status_reg_belajar == 2){
                                                $tahap = "Proses Izin Belajar : 1. Tahap Pengumpulan Dokumen";
                                                $btn_type = "btn-primary";
                                                $status = "Menunggu Konfirmasi Kepala OPD";
                                                $bg_type = "bg-info";
                                            }else if($master_belajar->status_reg_belajar == 3){
                                                $tahap = "Proses Izin Belajar : 2. Tahap Pengajuan Izin Belajar";
                                                $btn_type = "btn-secondary";
                                                $status = "Diterima Kepala OPD";
                                                $bg_type = "bg-success";
                                            }else if($master_belajar->status_reg_belajar == 4){
                                                $tahap = "Proses Izin Belajar : 2. Tahap Perbaikan Dokumen";
                                                $btn_type = "btn-danger";
                                                $status = "Ditangguhkan BKPP";
                                                $bg_type = "bg-danger";
                                            }else if($master_belajar->status_reg_belajar == 5){
                                                $tahap = "Proses Izin Belajar : 2. Tahap Pengajuan Izin Belajar";
                                                $btn_type = "btn-primary";
                                                $status = "Menunggu Konfirmasi BKPP";
                                                $bg_type = "bg-info";
                                            }else if($master_belajar->status_reg_belajar == 6){
                                                $tahap = "Proses Izin Belajar : 3. Tahap Laporan Izin Belajar";
                                                $btn_type = "btn-success";
                                                $status = "Diterima BKPP";
                                                $bg_type = "bg-success";
                                            }else if($master_belajar->status_reg_belajar == 7){
                                                $tahap = "Proses Izin Belajar : 4. Izin Belajar Selesai";
                                                $btn_type = "btn-success";
                                                $status = "Izin belajar telah selesai";
                                                $bg_type = "bg-success";
                                            }else if($master_belajar->status_reg_belajar == 9){
                                                $tahap = "Proses Izin Belajar : Ditolak";
                                                $btn_type = "btn-warning";
                                                $status = "Izin belajar ditolak";
                                                $bg_type = "bg-warning";
                                            }else{
                                                $tahap = "Something wrong, Kontak Diskominfo kota Bandung.";
                                                $btn_type = "btn-secondary";
                                                $status = "Something wrong, Kontak Diskominfo kota Bandung.";
                                                $bg_type = "bg-info";
                                            }
                                            echo '<td class="status" style="white-space:normal"><span class="badge badge-dot mr-4">
                                            <i class="'.$bg_type.'"></i> '.$status.'
                                            </span>
                                            </td>';
                                            echo '<td style="white-space:normal">
                                            <a href="izinbelajar/detailProsesIzinBelajar/'.$master_belajar->belajar_reg_id.'"
                                                class="btn btn-icon btn-3 '.$btn_type.' btn-sm">
                                                <span class="btn-inner--icon"><i class="ni ni-bold-right"></i></span>
                                                <span class="btn-inner--text">'.$tahap.'</span>
                                            </a>
                                            </td>';
                                            if($master_belajar->status_reg_belajar == 0){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/unggahSyaratIzinBelajar").'">Isi Dokumen</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($master_belajar->status_reg_belajar == 1){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/editIzinBelajar").'">Perbaiki</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($master_belajar->status_reg_belajar == 2){
                                                echo '<td class="text-right" style="white-space:normal">Menunggu Konfirmasi Kepala OPD</td>';
                                            }else if($master_belajar->status_reg_belajar == 3){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/ajukanIzinBelajar").'">Ajukan Izin Belajar</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($master_belajar->status_reg_belajar == 4){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/editDokumenIzinBelajar").'">Perbaiki</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($master_belajar->status_reg_belajar == 5){
                                                echo '<td class="text-right" style="white-space:normal">Menunggu Konfirmasi BKPP</td>';
                                            }else if($master_belajar->status_reg_belajar == 6){
                                                echo '<td class="text-right" style="white-space:normal">
                                                <div class="dropdown">
                                                    <a class="btn btn-lg btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  <i class="fa fa-lg fa-chevron-circle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/tambahLaporanIzinBelajar").'">Tambah Laporan</a>
                                                        <a class="dropdown-item" href="'.base_url("user/izinbelajar/selesaiIzinBelajar/".$master_belajar->belajar_reg_id).'">Selesai Belajar</a>
                                                    </div>
                                                </div>
                                                </td>';
                                            }else if($master_belajar->status_reg_belajar == 7){
                                                /*
                                                if($master_belajar->status_disposisi == 0){
                                                    echo '<td class="text-right" style="white-space:normal">Menunggu Konfirmasi BKPP</td>';
                                                }else{
                                                    echo '<td class="text-right" style="white-space:normal">Lulus</td>';
                                                }*/
                                                echo '<td class="text-right" style="white-space:normal">Lulus</td>';
                                            }else if($master_belajar->status_reg_belajar == 9){
                                                echo '<td class="text-right" style="white-space:normal">Izin Belajar ditolak</td>';
                                            }else{
                                                echo '<td class="text-right" style="white-space:normal">Something wrong, kontak Diskominfo</td>';
                                            }
                                            echo '</tr>';
                                        } ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer bg-gradient-default border-0">
                            <nav aria-label="Page navigation example">
                                <?php //echo $pagination;?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>