<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Unggah/Upload Berkas Persyaratan, Pengajuan Izin Belajar ke OPD</h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url("user/izinbelajar/prosesopd")?>" enctype="multipart/form-data" role="form">
                            <!--Kategori Diklat-->
                            <h6 class="heading-small text-muted mb-2" style="color:red !important">Unggah/Upload Berupa File berformat <strong> (PNG/JPG/PDF/RAR/ZIP) </strong>dengan Maksimal Ukuran <strong>2MB</ (2 MegaByte) </strong></h6>
                            <hr class="my-4">
                            <input type="hidden" name="belajar_reg_id" value="<?php echo $belajar_reg_id;?>">
                            <input type="hidden" name="proses_izin_belajar" value="0">
                            <!--Nama Diklat-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_surat_rekom">
                                        Scan Surat Rekomendasi Izin Belajar
                                        </label>
                                        <?php echo form_error("inp_dokumen_1 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="0">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Surat_Rekomendasi">
                                        <input onchange="checkValidation('inp_dokumen_1')" class="form-control" name="inp_dokumen_1" id="inp_dokumen_1" placeholder="Surat Rekomendasi Izin Belajar" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_materai_1">
                                        Scan Surat pernyataan bermaterai tidak pindah tugas
                                        </label>
                                        <?php echo form_error("inp_dokumen_2 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="1">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Surat_Pernyataan1">
                                        <input onchange="checkValidation('inp_dokumen_2')" class="form-control" name="inp_dokumen_2" id="inp_dokumen_2" placeholder="Surat pernyataan bermaterai tidak pindah izin" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_materai_2">
                                         *Beasiswa | Scan Surat Pernyataan Bermaterai Pengembalian Biaya
                                        </label>
                                        <?php echo form_error("inp_dokumen_3 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="2">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Surat_Pernyataan2">
                                        <input onchange="checkValidation('inp_dokumen_3')" class="form-control" name="inp_dokumen_3" id="inp_dokumen_3" placeholder="Surat Pernyataan Bermaterai Pengembalian Biaya" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_lembaga">
                                        *Beasiswa | Scan Surat keterangan dari lembaga pemberi beasiswa
                                        </label>
                                        <?php echo form_error("inp_dokumen_4 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="3">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Surat_Beasiswa">
                                        <input onchange="checkValidation('inp_dokumen_4')" class="form-control" name="inp_dokumen_4" id="inp_dokumen_4" placeholder="Surat keterangan dari lembaga pemberi beasiswa" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>  
                            <hr class="my-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_pas_foto">
                                        Pas Foto Berwarna Terbaru
                                        </label>
                                        <?php echo form_error("inp_dokumen_5 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="4">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Pas_foto">
                                        <input onchange="checkValidation('inp_dokumen_5')" class="form-control" name="inp_dokumen_5" id="inp_dokumen_5" placeholder="Pas Foto" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_ijazah">
                                        Scan Ijazah Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_6 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="5">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Ijazah">
                                        <input onchange="checkValidation('inp_dokumen_6')" class="form-control" name="inp_dokumen_6" id="inp_dokumen_6" placeholder="Ijazah" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_transkrip">
                                        Scan Transkrip Nilai Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_7 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="6">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Transkrip_Nilai">
                                        <input onchange="checkValidation('inp_dokumen_7')" class="form-control" name="inp_dokumen_7" id="inp_dokumen_7" placeholder="Transkrip Nilai" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_sk">
                                        Scan SK Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_8 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="7">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="SK">
                                        <input onchange="checkValidation('inp_dokumen_8')" class="form-control" name="inp_dokumen_8" id="inp_dokumen_8" placeholder="SK" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_sk_jabatan">
                                        Scan SK Jabatan Terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_9 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="8">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="SK_Jabatan">
                                        <input onchange="checkValidation('inp_dokumen_9')" class="form-control" name="inp_dokumen_9" id="inp_dokumen_9" placeholder="SK Jabatan Terakhir" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_prestasi">
                                        Scan Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir
                                        </label>
                                        <?php echo form_error("inp_dokumen_10 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="9">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="Prestasi">
                                        <input onchange="checkValidation('inp_dokumen_10')" class="form-control" name="inp_dokumen_10" id="inp_dokumen_10" placeholder="Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="inp_dokumen_11">
                                        Scan Surat Keterangan Terdaftar Perguruan Tinggi / LOA
                                        </label>
                                        <?php echo form_error("inp_dokumen_11 ","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="posisi_proses_izin_belajar[]" value="10">
                                        <input type="hidden" name="jenis_dokumen_syarat_izin_belajar[]" value="LOA">
                                        <input onchange="checkValidation('inp_dokumen_11')" class="form-control" name="inp_dokumen_11" id="inp_dokumen_11" placeholder="Surat Keterangan Terdaftar Perguruan Tinggi / LOA" type="file"
                                            value="">
                                    </div>
                                </div>
                            </div>
                            <div class="input_fields_wrap">
                            </div>

                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url("user/izinbelajar/prosesizinbelajar/".$belajar_reg_id)?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
        </div>
    </div>

    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
        function checkValidation(id){
            var uploadField = document.getElementById(id);
            var fileSize = uploadField.files[0].size;
            var fileName = uploadField.files[0].name;
            var splitFileName = fileName.split('.');
            if (fileSize >= 2048000) {
                alert("Berkas yang anda unggah terlalu besar, tidak sesuai syarat 2MB");
                uploadField.value = '';
            };
            if(splitFileName[1] == 'pdf' || splitFileName[1] == 'png' || splitFileName[1] == 'rar' || splitFileName[1] == 'zip' || splitFileName[1] == 'jpg'){
                
            }else{
                alert("Ekstensi berkas yang anda unggah salah, diharuskan (PNG|PDF|JPG|RAR|ZIP)");
                uploadField.value = '';
            }
        }
    </script>
</body>

</html>