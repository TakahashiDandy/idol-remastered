<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body class="bg-gray-dark">
<div class="main-content">
    <!-- Header -->
    <div class="header py-4 py-lg-5">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-7">
                        <h1 class="text-white">Selamat Datang!</h1>
                        <!-- LOGO IDOL -->
                        <!-- <img style="max-width: 200px; max-height: 100px" src="<?php echo asset_url()?>/courseware/img/logo-idol.png"> -->
                        <!-- LOGO SITIMBEL -->
                        <img style="max-width: 200px; max-height: 100px" src="<?php echo asset_url()?>/courseware/img/logo-sitimbel.png">
                        <?php if($this->session->flashdata("error_message")) {?>
                            <div style="margin-top: 20px">
                                <div class="alert alert-danger" role="alert">
                                    <strong>Gagal!</strong> <?php echo $this->session->flashdata("error_message")?>
                                </div>
                            </div>
                        <?php }?>
                        <?php if($this->session->flashdata("success_message")) {?>
                            <div style="margin-top: 20px">
                                <div class="alert alert-success" role="alert">
                                    <strong>Sukses!</strong> <?php echo $this->session->flashdata("success_message")?>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-gradient-lighter shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Masuk dengan akun anda</small>
                        </div>
                        <form method="post" action="<?php echo base_url("user/auth/login");?>">
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Username" type="text" name="username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Password" type="password" name="password">
                                </div>
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox">
                                <input class="custom-control-input" name="remember" id="remember" type="checkbox">
                                <label class="custom-control-label" for="remember">
                                    <span class="text-muted">Ingat saya</span>
                                </label>
                            </div>
                            <div class="text-left text-muted mb-lg--10">
                                <br>
                                <small><a href="<?php echo base_url("user/auth/resetpassword")?>">Lupa Password ?</a></small>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">Sign in</button>
                                <a href="<?php echo base_url('user/auth/registeraccount'); ?>" type="btn" class="btn btn-success my-4">Buat akun</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/userpanel/include/js.php');?>
</body>

</html>