<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $tablename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/registrasi/updateregistrasi')?>">
                            <!--Tabel Registrasi Diklat-->
                            <h6 class="heading-small text-muted mb-4">Daftar Registrasi Diklat</h6>
                            <div class="row">
                                <div class="col-md-5">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <td><i class="fa fa-check"></td>
                                                <td>NIP</td>
                                                <td>Nama</td>
                                            </tr>
                                        </thead>
                                        <tbody id="datareject">
                                            <?php foreach ($rows1->result() as $data){
                                                echo "<tr id='rrow-$data->id_registrasi'>";
                                                echo "<td><input type='checkbox' name='chkreject[]' value='$data->id_registrasi'></td>";
                                                echo "<td id='rnip-$data->id_registrasi'>$data->nip</td>";
                                                echo "<td id='rnama-$data->id_registrasi'>$data->nama_lengkap</td>";
                                                echo "</tr>";
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <div class="col-md-2">
                                        <button type="button" onclick="acceptbutton()" class="btn btn-success"><i class="fa fa-arrow-right"></i> Accept</button>
                                    </div>
                                    <br>
                                    <div class="col-md-2">
                                        <button type="button" onclick="rejectbutton()" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Reject</button>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <td><i class="fa fa-check"></td>
                                                <td>NIP</td>
                                                <td>Nama</td>
                                            </tr>
                                        </thead>
                                        <tbody id="dataaccept">
                                        <?php foreach ($rows2->result() as $data){
                                            echo "<tr id='arow-$data->id_registrasi'>";
                                            echo "<td><input type='checkbox' name='chkaccept[]' value='$data->id_registrasi'></td>";
                                            echo "<td id='anip-$data->id_registrasi'>$data->nip</td>";
                                            echo "<td id='anama-$data->id_registrasi'>$data->nama_lengkap</td>";
                                            echo "</tr>";
                                        }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="<?php echo base_url("admin/registrasi/")?>" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>
    function acceptbutton() {
        accept = [];
        $.each($("input[name='chkreject[]']:checked"), function(){
            accept.push($(this).val());
        });
        for(var i = 0; i< accept.length; i++){
            nip = $("#rnip-"+accept[i]).text();
            nama = $("#rnama-"+accept[i]).text();
            rowdata = "<tr id='arow-"+accept[i]+"'>" +
                "<td><input type='checkbox' name='chkaccept[]' value='"+accept[i]+"'></td>"+
                "<td id='anip-"+accept[i]+"'>"+nip+"</td>"+
                "<td id='anama-"+accept[i]+"'>"+nama+"</td>"+
                "</tr>";
            $("#dataaccept").append(rowdata);
            $("#rrow-"+accept[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }
    function rejectbutton() {
        reject = [];
        $.each($("input[name='chkaccept[]']:checked"), function(){
            reject.push($(this).val());
        });
        for(var i = 0; i< reject.length; i++){
            nip = $("#anip-"+reject[i]).text();
            nama = $("#anama-"+reject[i]).text();
            rowdata = "<tr id='rrow-"+reject[i]+"'>" +
                "<td><input type='checkbox' name='chkreject[]' value='"+reject[i]+"'></td>"+
                "<td id='rnip-"+reject[i]+"'>"+nip+"</td>"+
                "<td id='rnama-"+reject[i]+"'>"+nama+"</td>"+
                "</tr>";
            $("#datareject").append(rowdata);
            $("#arow-"+reject[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }
</script>
</body>

</html>
