<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/userpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php require_once(APPPATH.'views/userpanel/include/sidebar.php');?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/userpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                        <div style="margin-top: 20px">
                            <div class="alert alert-danger" role="alert">
                                <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <h2 class="text-white mb-0"><?php echo $tablename ?></h2>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <h5 class="text-white mb-0">Total JPL yang telah diikuti:&nbsp;&nbsp; <?php echo $data_jpl->total_jpl ?>&nbsp; JPL</h5>
                            </div>
                            <!-- <div class="col-md-3 text-right">
                                <div class="row">
                                    <div class="col-md-auto">
                                        <form class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                            <div class="form-group mb-0">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                    </div>
                                                    <input class="form-control" placeholder="Search" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-auto">
                                        <a href="<?php echo base_url("user/registrasi/tambah_user/")?>" class="btn btn-md btn-primary">
                                            <i class="fas fa-plus"></i> Tambah Baru
                                        </a>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark" ">
                            <thead class="thead-dark">
                            <tr>
                                <th>Tanggal</th>
                                <th>Aktivitas</th>
                                <th>Keterangan</th>
                                <th>Volume (JPL)</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rows->result() as $row){
                                    echo '<tr>';
                                        echo '<td style="white-space:normal">'.tgl_indo($row->tanggal).'</td>';
                                        echo '<td style="white-space:normal">'.$row->aktifitas.'</td>';
                                        echo '<td style="white-space:normal">'.$row->keterangan.'</td>';
                                        echo '<td>'.$row->volume.'</td>';
                                    echo '</tr>';
                                } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer bg-gradient-default border-0">
                        <nav aria-label="Page navigation example">
                            <?php echo $pagination;?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/userpanel/include/footer.php');?>
    </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/userpanel/include/js.php');?>
    <script>
    </script>
</body>

</html>