<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url() . "courseware/img/logo-pemkot-bdg-sm.png"?>" width="100" height="70"></td>
            <td align="center" width="80%">
                <h2 align="center" class="nomargin">PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:20px"><?php echo strtoupper($nama_skpd)?></span>
                </h2>JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG
            </td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px;">
        <tr>
            <td>Nomor</td>
            <td>:</td>
            <td><?php echo $data_surat->no_surat;?></td>
            <td colspan="2" width="30%">
                Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?>
            </td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td>-</td>
            <td colspan="2" width="30%">
                Kepada :
            </td>
        </tr>
        <tr>
            <td rowspan="3" valign="top">Perihal</td>
            <td valign="top">:</td>
            <td valign="top">Permohonan Tugas Belajar</td>
            <td width="1%" valign="top">Yth. </td>
            <td valign="top">
                Bapak Wali Kota Bandung<br>
                dengan melalui:
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td valign="top">
                Yth. <br> di-</td>
            <td valign="top">
                Bapak Sekretaris Daerah
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>B A N D U N G</td>
        </tr>
    </table>
    <br><br>
    <p>
        Bersama ini kami sampaikan permohonan Tugas belajar dari :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan terakhir</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <p style="padding-left: 40px;">
        Untuk dapat melanjutkan pendidikan pada <?php echo $rows->belajar_pendidikan_nm ?>
    </p>
    <p align="justify"><span style="padding-left: 40px;"></span>
        Pada prinsipnya kami tidak menaruh keberatan atas permohonan pegawai tersebut, 
        mengingat menurut pengamatan kami yang bersangkutan mempunyai potensi untuk dikembangkan,
        serta sifat dan tugas pekerjaan yang kami serahkan kepadanya benar-benar menuntut 
        peningkatan kemampuan melalui pendidikan yang tinggi yaitu pendidikan yang diikutinya.
    </p>
    <p align="justify"><span style="padding-left: 40px;"></span>
        Untuk penyelesaian lebih lanjut sesuai dengan kewenangannya, kami serahkan kepada Bapak.
    </p>
    <p style="padding-left: 40px;">
        Demikian kiranya menjadi maklum.
    </p>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Bandung,<?php echo date("d F Y",strtotime($rows->updated_at))?></div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Kepala <?php echo $nama_skpd?>,</div>
                <br><br><br><br><br>
                <div align="center"><?php echo $kepala_dinas?></div>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</body>
</html>