<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }
    </style>
</head>
<body style="margin-top:0;" onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url() . "courseware/img/logo-pemkot-bdg-sm.png"?>" width="100" height="50"></td>
            <td align="center" width="80%" style="font-size: 25px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:30px">SEKRETARIAT DAERAH</span></strong>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="10%">&nbsp;</td>
            <td align="center" width="90%">
                Jalan Wastukancana Nomor 2 Tlp. 4232338, 4232339, 4232369, 4232370 - BANDUNG
            </td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center"><center>SURAT IZIN WALI KOTA BANDUNG</center></td>
        </tr>
        <tr>
            <td align="center">Nomor : <font color="#ffffff"><?php echo "(DI ISI DENGAN TANGGAL)"?></font></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td align="center">TENTANG</td>
        </tr>
        <tr>
            <td align="center">PEMBERIAN IZIN BELAJAR</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table" valign="top">Dasar</td>
            <td valign="top">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">a.&nbsp;&nbsp;</td>
            <td align="justify">Peraturan Wali Kota Bandung Nomor 755 Tahun 2015
                tentang petunjuk Pelaksanaan Tugas Belajar dan Izin Belajar bagi Pegawai Negeri Sipil
                di Lingkungan Pemerintah Kota Bandung sebagaimana telah diubah dengan Peraturan Wali Kota Bandung
                Nomor 836 Tahun 2017;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">b.&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" align="justify">Surat Keterangan <?php echo $rows->instansi?> Nomor <?php echo $data_surat_opd->no_surat?>
                Tanggal <?php echo date("d F Y",strtotime($data_surat_opd->created_at))?>
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">c.&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" align="justify">Surat Keterangan Sebagai Mahasiswa dari <?php echo $rows->belajar_pendidikan_nm ?> Nomor <?php echo $data_surat_mahasiswa->no_surat?>
                Tanggal <?php echo date("d F Y",strtotime($data_surat_mahasiswa->created_at))?>
            </td>
        </tr>
    </table>
    <p align="center"><strong>M E N G I Z I N K A N</strong></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Kepada</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat/golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">SKPD/Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0px" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table" valign="top">Untuk</td>
            <td valign="top">:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="justify">Mengikuti Pendidikan <?php echo $rows->print_tingpend?> (<?php echo $rows->nm_tingpend?>), <?php echo $rows->jurusan_nm?>, Tahun Ajaran <?php echo date("Y",strtotime($rows->updated_at))?>, dengan ketentuan:
                <ol style="padding-left: 15px;margin-top: 0;">
                    <li>Izin Belajar diberikan diluar jam kerja;</li>
                    <li>Tidak Mengganggu tugas-tugas kedinasan;</li>
                    <li>Biaya sepenuhnya ditanggung oleh yang bersangkutan;</li>
                    <li>Tidak menuntut penyesuaian ijazah;</li>
                    <li>Melaporkan hasil kelulusan setelah menyeselaikan Pendidikan.</li>
                </ol>
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="70%" align="center">&nbsp;</td>
            <td width="30%">
                <div>Ditetapkan di Bandung,<br>
                    pada tanggal <font color="#ffffff">(DI ISI DENGAN TANGGAL)</font></div>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">a.n WALI KOTA BANDUNG</div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">SEKRETARIS DAERAH</div>
            </td>
        </tr>
    </table>
    <br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%" align="center"><?php echo $kepala_dinas?></td>
        </tr>
    </table>
    <p class="pleft-table">Tembusan :</p>
    <ol style="padding-left:55px">
        <li>Yth. Bapak Wali Kota Bandung (sebagai laporan);</li>
        <li>Yth. Bapak Wakil Wali Kota Bandung (sebagai laporan);</li>
    </ol>
<?php endforeach; ?>
</body>
</html>
 