<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }

        li {
            margin-left: 10px;
        }
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) :?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url() . "courseware/img/logo-pemkot-bdg-sm.png"?>" width="100" height="70"></td>
            <td align="center" width="80%" style="font-size: 20px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:19px"><?php echo strtoupper($nama_skpd)?></span></strong>
                <br>
                <span style="font-size: 12px;">JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG</span>
            </td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <br>
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><center>REKOMENDASI</center></td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 10px;">Nomor : <?php echo $rows->nomor_rekomendasi?></td>
        </tr>
    </table>
    <br>
    <p><span style="margin-left: 60px;"></span>
        Kami yang bertanda tangan di bawah ini :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $kepala_dinas?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $jabatan_kepala?></td>
        </tr>
    </table>
    <br>
    <p><span style="margin-left: 60px;"></span>
        Merekomendasikan bahwa nama yang tercantum di bawah ini :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat/Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <br>
    <p align="justify" style="padding-left: 40px;"><span style="padding-left: 20px;"></span>
        Telah kami setujui untuk mengikuti proses seleksi akademis penerimaan Mahasiswa Baru 
        Tahun Akademik <?php echo date("Y",strtotime($rows->updated_at))?> / <?php echo date("Y",strtotime($rows->updated_at))+1?> Program Studi <?php echo $rows->print_tingpend?> (<?php echo $rows->nm_tingpend?>) pada <?php echo $rows->belajar_pendidikan_nm?>
        melalui Program Tugas Belajar <?php echo (!empty($rows->rekomendasi_beasiswa) ? "Beasiswa ". $rows->rekomendasi_beasiswa : ".")?>
    </p>
    <p><span style="padding-left: 60px;"></span>
        Demikian rekomendasi ini kami buat untuk dipergunakan sebagaimana mestinya.
    </p>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center">
                <div>Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?></div>
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center" style="padding-top:20px;">
                <div><?php echo $jabatan_kepala?></div>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center">
                    <div>(<?php echo $kepala_dinas?>)</div>
            </td>
        </tr>
    </table>
    <br>
    <p><span style="padding-left: 40px;"></span>
        Tembusan :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <ol>
            <li>Bapak Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Wakil Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Sekretaris Daerah Kota Bandung (sebagai laporan);</li>
        </ol>
    </table>
</body>
<?php endforeach;?>
</html>