<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <br>
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><u><center>SURAT PERNYATAAN SIAP GANTI RUGI</center></u></td>
        </tr>
    </table>
    <br><br><br>
    <table width="36%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Yang bertanda tangan dibawah ini</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Tempat & tanggal lahir</td>
            <td>:</td>
            <td><?php echo $rows->tempat_lahir .", ". $rows->tanggal_lahir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan & Tahun Lulus</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat/Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <p>
        <br>Menyatakan dengan ini sesungguhnya bahwa saya siap untuk mengganti rugi biaya pendidikan jika tidak menyelesaikan pendidikan sesuai dengan waktu yang ditentukan.
    </p>
    <!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top" style="padding-top:10px;">Pertama</td>
            <td valign="top" style="padding-top:10px;">&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" style="padding-top:10px;">Siap untuk mengganti rugi biaya pendidikan jika tidak menyelesaikan pendidikan sesuai dengan waktu yang ditentukan.</td>
        </tr>
        <tr>
            <td valign="top" style="padding-top:20px;">Kedua</td>
            <td valign="top" style="padding-top:20px;">&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" style="padding-top:20px;">Tidak menuntut penyesuaian Ijazah/Jabatan sepanjang formasi tidak memungkinkan bila telah lulus.</td>
        </tr>
        <tr>
            <td valign="top" style="padding-top:20px;">Ketiga</td>
            <td valign="top" style="padding-top:20px;">&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" style="padding-top:20px;">Selama pendidikan biaya ditanggung sepenuhnya oleh
                    lembaga pemberi beasiswa/cost sharing/sendiri (tugas belajar mandiri).</td>
        </tr>
    </table> -->
    <p>
        <br>Demikian surat pernyataan ini saya buat dengan sesungguhnya dan bersedia menerima segala tindakan yang diambil Pemerintah bila di kemudian hari terbukti pernyataan ini tidak benar.
    </p>
    <br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Bandung, <?php echo date("d F Y")?></div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center" style="padding-top:10px;">&nbsp;</td>
            <td width="40%" style="padding-top:10px;">
                <div align="center">Yang membuat pernyataan</div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center" style="padding-top:20px;">&nbsp;</td>
            <td width="40%" style="padding-top:20px;">
                <div align="center">Materai <br> (Rp. 6000)</div>
                <br><br><br>
                <div align="center"><?php echo $rows->nama_lengkap?></div>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</body>
</html>