<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }

        li {
            margin-left: 21px;
        }
    
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <!--<table align="right" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td rowspan="4" valign="top" align="right">LAMPIRAN &nbsp; : &nbsp;</td>
            <td colspan="2">SALINAN PERATURAN WALI KOTA BANDUNG</td>
        </tr>
        <tr>
            <td>NOMOR</td>
            <td>: 836 TAHUN 2017</td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td>: 22 AGUSTUS 2017</td>
        </tr>
        <tr>
            <td colspan="2" valign="top" style="margin:0; padding:0;"><hr></td>
        </tr>
    </table>-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px;">
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" width="30%">
                Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?> <br>
                Kepada: 
            </td>
        </tr>
        <tr>
            <td rowspan="3" valign="top">Perihal: Permohonan Izin Belajar</td>
            <td width="1%" valign="top">Yth. </td>
            <td valign="top">
                Bapak Wali Kota Bandung<br>
                dengan melalui:
            </td>
        </tr>
        <tr>
            <td valign="top">
                Yth. <br> di</td>
            <td valign="top">
                Bapak Sekretaris Daerah
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>B A N D U N G</td>
        </tr>
    </table>
    <table width="36%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Yang bertanda tangan dibawah ini</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">No. Tlp/HP</td>
            <td>:</td>
            <td><?php echo $rows->no_kontak?></td>
        </tr>
        <tr>
            <td class="pleft-table">Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td><?php echo $rows->tempat_lahir .", ". $rows->tanggal_lahir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan dan Tahun Lulus</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <p align="justify"><span style="padding-left: 40px;"></span>
        Dengan ini menyampaikan kepada Bapak agar saya diberi Izin untuk melanjutkan pendidikan pada <?php echo $rows->belajar_pendidikan_nm ?>.
    </p>
    <p align="justify">Sebagai bahan pertimbangan kepada Bapak, bersama ini saya lampirkan :</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <ol>
                <li>Surat Rekomendasi dari Kepala Perangkat Daerah yang bersangkutan;</li>
                <li>Surat Keterangan dari Kepala Perangkat Daerah yang bersangkutan;</li>
                <li>Surat Analisa Pegawai dari Kepala Perangkat Daerah yang bersangkutan;</li>
                <li>Surat Permohonan yang bersangkutan ditujukan ke Wali Kota melalui Sekertaris Daerah;</li>
                <li>Surat Pernyataan bermaterai;</li>
                <li>Surat Keterangan Terdaftar sebagai Mahasiswa dari Perguruan Tinggi;</li>
                <li>Jadwal Kuliah dan atau Rencana Studi yang bersangkutan;</li>
                <li>Jadwal Mengajar / Jadwal shift Kerja;</li>
                <li>Pas photo Berwarna terbaru ukuran 3x4 sebanyak 2 buah;</li>
                <li>Photo copy ijazah terakhir di legalisir;</li>
                <li>Photo copy Transkrip nilai terakhir di legalisir;</li>
                <li>Photo copy SK Pangkat terakhir/SK Jabatan;</li>
                <li>Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir;</li>
                <li>Surat Keterangan dari Perguruan Tinggi tempat pendidikan yang menyatakan Akreditasi dan bukan kelas jarak jauh.</li>
                <li>Melampirkan Surat Keterangan konversi nilai bagi PNS yang pindah kampus.</li>
            </ol>
    </table>
    <p>Demikian permohonan ini saya sampaikan dengan harapan Bapak sudi memberikan Izin.</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="60%" align="center">&nbsp;</td>
                <td width="40%">
                    <div align="center">Pemohon,</div>
                    <br><br><br><br><br>
                    <div align="center"><?php echo $rows->nama_lengkap?></div>
                </td>
            </tr>
    </table>
<?php endforeach; ?>
</body>
</html>