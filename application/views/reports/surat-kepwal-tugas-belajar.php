<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 30px;
            width: 25%;
        }
        .p-cpacing {
        letter-spacing: 1px;
}
    </style>
</head>
<body style="margin-top:0;" onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center"><center><strong>KEPUTUSAN WALI KOTA BANDUNG</strong></center></td>
        </tr>
        <tr>
            <td align="center"><strong>NOMOR : <font color="white">(DIISI DENGAN DENGAN NOMOR)</font></strong></td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 15px;"><strong>TENTANG</strong></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center"><strong>PENUNJUKAN <?php echo strtoupper($rows->nama_lengkap)?> <br>
            NIP. <?php echo $rows->nip?><br>
            SEBAGAI MAHASISWA TUGAS BELAJAR<br>
            <?php echo strtoupper($rows->jurusan_nm)?> <?php echo strtoupper($rows->print_tingpend)?> (<?php echo strtoupper($rows->nm_tingpend)?>)<br>
            <?php echo strtoupper($rows->belajar_pendidikan_nm)?><br>
            TAHUN <?php echo date("Y",strtotime($rows->updated_at))?><br><br>
            WALI KOTA BANDUNG,</strong></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table" valign="top">Menimbang</td>
            <td valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="p-cpacing" align="justify">bahwa untuk meningkatkan pengetahuan dan kemampuan Pegawai Negeri Sipil Daerah Kota Bandung perlu diberikan kesempatan
                mengikuti pendidikan pada <?php echo $rows->belajar_pendidikan_nm?><br> sesuai dengan kebutuhan;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">bahwa Pegawai Negeri Sipil Kota Bandung yang telah mengikuti seleksi dan dinyatakan lulus dalam program <?php echo $rows->jurusan_nm?>
                <?php echo $rows->print_tingpend?> (<?php echo $rows->nm_tingpend?>)
                <?php echo $rows->belajar_pendidikan_nm?>
                beasiswa <?php echo $rows->rekomendasi_beasiswa ?> <?php echo date("Y",strtotime($rows->updated_at))?> berdasarkan, perlu ditugaskan sebagai mahasiswa tugas belajar yang ditetapkan
                dengan Keputusan Wali Kota Bandung.
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">Mengingat</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Pasal 18 ayat (6) Undang-Undang Dasar Negara Republik Indonesia Tahun 1945
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Undang-Undang Nomor 20 Tahun 2003 tentang Sistem Pendidikan Nasional;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Undang-Undang Nomor 12 Tahun 2011 tentang Pembentukan Peraturan Undang-Undang;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah sebagaimana telah diubah
                beberapakali terakhir dengan Undang-Undang Nomor 9 Tahun 2015 tentang Perubahan Kedua Atas Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Peraturan Pemerintah Nomor 101 Tahun 2000 Tentang Pendidikan dan Pelatihan Jabatan Pegawai Negeri Sipil;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Peraturan Pemerintah Nomor 38 Tahun 2007 tentang Pembagian Urusan Pemerintahan antara Pemerintah, Pemerintahan
                Daerah Provinsi, dan Pemerintahan Daerah Kabupaten/ Kota;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Peraturan Pemerintah Nomor 11 Tahun 2017 tentang manajemen Pegawai Negeri Sipil;
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Peraturan Walikota Bandung Nomor 755 Tahun 2015 tentang Peunjuk Pelaksanaan Tugas Belajar dan Izin Belajar
                bagi Pegawai Negeri Sipil di Ligkungan Pemerintah Kota Bandung sebaimana telah diubah dengan Peraturan Walikota Bandung Nomor 836 Tahun 2017.
            </td> 
        </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p align="center"><strong>2</strong></p>
    <br>
    <br>
    <br>
    <br>
    <p align="center"><strong>M E M U T U S K A N</strong></p>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Menetapkan</td>
            <td>:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KESATU</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Penunjukan <?php echo $rows->nama_lengkap?> NIP <?php echo $rows->nip?> sebagai mahasiswa tugas belajar <?php echo $rows->jurusan_nm?>
                <?php echo $rows->print_tingpend?> (<?php echo $rows->nm_tingpend?>)
                <?php echo $rows->belajar_pendidikan_nm?>
                Beasiswa <?php echo $rows->rekomendasi_beasiswa ?> <?php echo date("Y",strtotime($rows->updated_at))?>;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KEDUA</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Membebastugaskan yang bersangkutan dari tugas-tugas rutin selaku Pegawai Negeri Sipil Daerah Kota Bandung,
                yang bekerja pada <?php echo $rows->instansi?>, selama mengikuti pendidikan sebagaimana dimaksud pada Diktum kesatu kecuali bila tenaganya sangat dibutuhkan
                untuk kepentingan Dinas;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KETIGA</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Semua biaya untuk kelangsungan proses belajar dibebankan kepada <?php echo $rows->rekomendasi_beasiswa?>;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KEEMPAT</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Melaporkan perkembangan akademis secara berkala paling sedikit 1 (satu) kali setiap semester;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KELIMA</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Setelah selesai mengikuti pendidikan, segera melapor untuk bekerja kembali sesuai dengan kewajiban
                sebagai Pegawai Negeri Sipil Daerah pada Pemerintah Kota Bandung dan yang bersangkutan berhak menerima kembali Hak dan Kewajibannya sebagai
                Pegawai Negeri Sipil Daerah Kota Bandung serta bersedia untuk tidak pindah kerja selama tenaganya masih dibutuhkan oleh Pemerintah Kota Bandung;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KEENAM</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" class="p-cpacing" align="justify">Keputusan ini berlaku sejak ditetapkan, dan berakhir pada tahun <?php echo date("Y",strtotime($rows->updated_at)) +  $rows->masa_tempuh?> dengan ketentuan
                akan diubah dan atau diperbaiki kembali sebagaimana mestinya apabila terdapat kekeliruan dalam penetapannya.
            </td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 6rem;">
        <tr>
            <td width="65%" align="center">&nbsp;</td>
            <td width="35%">
                <div>Ditetapkan di Bandung,<br>
                    pada tanggal <font color="white">(DIISI DENGAN DENGAN TANGGAL)</font></div>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">WALI KOTA BANDUNG</div>
            </td>
        </tr>
    </table>
    <br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%" align="center">ODED MOHAMAD DANIAL</td>
        </tr>
    </table>
    <p class="pleft-table">Tembusan :</p>
        <ol style="padding-left:55px">
            <li>Kepala BKN Regional III Bandung;</li>
            <li>Sekretaris Daerah Kota Bandung;</li>
            <li>Kepala Badan Kepegawaian, Pendidikan dan Pelatihan Kota Bandung;</li>
            <li>Kepala <?php echo $rows->instansi?> Kota Bandung.</li>
        </ol>
<?php endforeach; ?>
</body>
</html>