<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 36%;
        }
        li {
            margin-left: 2px;
        }
    </style>
</head>
<body onload="window.print()">
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center"><center>SURAT PERNYATAAN</center></td>
        </tr>
        <tr>
            <td style="padding-top: 10px;">NOMOR : [diisi dengan nomor]</td>
        </tr>
    </table>
    <br><br>
    <p>Yang bertanda tangan di bawah ini &nbsp;:</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat/golongan ruang</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Organisasi</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <p>
        dengan ini menyatakan dengan sesungguhnya, bahwa :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
        </tr>
            <td class="pleft-table">Pangkat/golongan ruang</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Organisasi</td>
            <td>:</td>
        </tr>
    </table>
    <br><br>
    <p align="justify"> terhitung mulai bulan [diisi dengan bulan] tidak menerima tunjangan struktural,
            fungsional, atau tunjangan yang dipersamakan dengan tunjangan Jabatan.
    </p>
    <p align="justify">Demikian surat pernyataan ini saya buat dengan sesungguhnya dengan
            mengingat sumpah jabatan/Pegawai Negeri Sipil. Apabila dikemudian hari isi
            surat pernyataan ini ternyata tidak benar, yang mengakibatkan kerugian
            terhadap negara, maka saya bersedia menanggung kerugian tersebut.
    </p>
    <p align="justify">Asli surat pernyataan ini disampaikan kepada Kepala Badan Pengelolaan
            Keuangan dan Aset Kota Bandung.
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Bandung,[diisi dengan tanggal]</div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Pejabat yang membuat pernyataan,</div>
                <br><br><br><br><br>
                <div align="center">[diisi dengan nama Kepala OPD]</div>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br><br><br><br><br>
    <p>Tembusan Yth &nbsp;:</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <ol>
            <li>Bapak Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Wakil Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Sekretaris Daerah Kota Bandung (sebgai laporan);</li>
            <li>Kepala Badan Kepegawaian, Pendidikan dan Pelatihan Kota Bandung;</li>
            <li>Kepala Badan Pengelolaan Keuangan dan Aset Kota Bandung;</li>
            <li>Bendaharawan Gaji;</li>
            <li>Pegawai Negeri Sipil yang bersangkutan.</li>
        </ol>
    </table>
</body>
</html>