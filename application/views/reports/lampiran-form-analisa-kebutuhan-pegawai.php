<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 36%;
        }
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url() . "courseware/img/logo-pemkot-bdg-sm.png"?>" width="100" height="70"></td>
            <td align="center" width="80%">
                <h2 align="center" class="nomargin">PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:20px"><?php echo strtoupper($nama_skpd)?></span>
                </h2>JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG
            </td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <br><br>
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><u><center>ANALISA KEBUTUHAN PEGAWAI</center></u></td>
        </tr>
        <tr>
            <td style="padding-top: 10px;">Nomor : <?php echo $data_surat->no_surat;?></td>
        </tr>
    </table>
    <br>
    <p align="justify">
        Berdasarkan Peraturan Wali Kota Nomor:[diisi dengan nomor] Tahun [diisi dengan tahun] tentang Tugas Belajar dan Izin Belajar Pegawai Negeri Sipil di Lingkungan Pemerintah Kota Bandung, bahwa Pegawai Negeri Sipil di bawah ini :
    </p>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td><?php echo $rows->tempat_lahir .", ". $rows->tanggal_lahir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan / Tahun Lulus</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <br>
    <p align="justify">Dengan Pertimbangan bahwa tugas pokok sehari-hari 
            sebagai <?php echo $rows->jabatan?> pada <?php echo $rows->instansi?>, dengan uraian tugas (terlampir),
            perlu meningkatkan kompetensinya dalam bidang pendidikan formal,
            dipandang perlu peningkatan mutu pendidikan ke jenjang yang lebih tinggi
            untuk menunjang pelaksanaan tugas pokok dan fungsinya secara profesional.
    </p>
    <p align="justify">Sehingga diharapkan dengan disiplin ilmu yang diperoleh, dapat lebih
            mengembangkan wawasan dan kemampuan dalam melaksanakan tugas.
    </p>
    <p align="justify">Demikian analisa kebutuhan pegawai ini kami buat, untuk dapat diproses
            lebih lanjut sesuai dengan ketentuan yang berlaku.
    </p>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?></div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center"><?php echo $jabatan_kepala?></div>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br>
    <table width="100%" border="0" callspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">(<?php echo $kepala_dinas?>)</div>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</body>
</html>