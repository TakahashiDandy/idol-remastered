<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 30px;
            width: 25%;
        }
        .p-cpacing {
        letter-spacing: 1px;
}
    </style>
</head>
 <body style="margin-top:0;" onload="window.print()">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <br><br><br><br><br>
    </table>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center"><strong>KEPUTUSAN WALI KOTA BANDUNG</strong></td>
        </tr>
        <tr>
            <td align="center">NOMOR :[DIISI NOMOR PENGAKTIFAN KEMBALI]</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 15px;">TENTANG</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">PENGAKTIFAN DAN PENETAPAN KEMBALI PEGAWAI NEGERI SIPIL DAERAH <br>
            YANG TELAH SELESAI MENGIKUTI TUGAS BELAJAR<br>
            <br>
            <strong>WALI KOTA BANDUNG</strong></td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table" valign="top">Menimbang</td>
            <td width=1% valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">a.&nbsp;</td>
            <td valign="top" align="justify">bahwa dalam rangka pembinaan dan Pengembangan Karier Pegawai Negeri Sipil Daerah dipandang perlu menaktifkan kembali Pegawai Negeri Sipil Daerah yang telah selesai melaksanakan tugas belajar dan menempatkan sesuai dengan pendidikan yang dimilikinya;
            </td>
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td width=1% valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">b.&nbsp;</td>
            <td valign="top" align="justify">bahwa untuk maksud tersebut pada huruf a di atas perlu ditetapkan dengan Keputusan Wali Kota Bandung.
            </td> 
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">Mengingat</td>
            <td style="padding-top:10px;" width=1% valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">1.</td>
            <td style="padding-top:10px;" valign="top" align="justify">Undang-undang Nomor 33 Tahun 2004 tentang Perimbangan Keuangan Antara Pemerintah Pusat dan Pemerintah Daerah;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">2.&nbsp;</td>
            <td valign="top" align="justify">Undang-Undang Nomor 5 Tahun 2014 tentang Aparatur Sipil Negara;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">3.&nbsp;</td>
            <td valign="top" align="justify">Undang-Undang Nomor 30 Tahun 2014 tentang Administrasi Pemerintahan;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">4.&nbsp;</td>
            <td valign="top" align="justify">Undang-Undang Nomor 9 Tahun 2015 tentang Perubahan Kedua Atas Undang-undang Nomor 23 Tahun 2014 tentang Pemerintahan Daerah;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">5.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Pemerintah Nomor 18 Tahun 2016 tentang Perangkat Daerah;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">6.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">7.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Menteri Pendayagunaan Aparatur Negara Nomor 41 Tahun 2018 tenteng Nomenklatur Jabatan Pelaksana bagi Pegawain negeri Sipil di Lingkungan Instansi Pemerintah;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">8.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Daerah Kota Bandung Nomor 8 Tahun 2016 tentang Pembentukan dan Perangkat Daerah Kota Bandung;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">9.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Wali Kota Bandung Nomor 1407 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi serta Tata Kerja Kecamatan dan Kelurahan Kota Bandung;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">10.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Wali Kota Bandung Nomor 1481 Tahun 2016 tentang Nama, Kelas dan Nilai Jabatan di Lingkungan Pemerintah Bandung;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">11.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Wali Kota Bandung Nomor 836 Tahun 2017 tentang Perubahan Atas Peraturan Wali Kota Bandung Nomor 755 Tahun 2015 Tentang Petunjuk Pelaksanaan Tugas Belajar dan Izin Belajar Pegawai Negeri Sipil di Lingkungan Pemerintah Bandung;
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">12.&nbsp;</td>
            <td valign="top" align="justify">Peraturan Wali Kota Bandung Nomor 1 Tahun 2019 tentang Perubahan Atas Peraturan Wali Kota Bandung Nomor 126 Tahun 2018 Tentang Penilaian Kinerja Pegawai di Lingkungan Pemerintah Bandung;
            </td> 
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">Memperhatikan</td>
            <td style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top">1.&nbsp;</td>
            <td style="padding-top:10px;" valign="top" align="justify">[Keputusan Wali Kota Bandung Nomor 826/Kep.1079-BKD/2016 tanggal 20 September 2016 tentang Penunjukan sdr. BACHTIAR ACHMAD, S.STP, NIP.198503302004121001 sebagai Mahasiswa Tugas Belajar Mandiri S2 Manajemen Pertahanan Univarsitas Pertahanan Tahun 2016;]
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">2.&nbsp;</td>
            <td valign="top" align="justify">[Surat Keterangan Karo Akademik dan Kemahasiswaan Universitas Pertahanan Nomor SKET/21/V/2019 tanggal Mei 2019;]
            </td> 
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">3.&nbsp;</td>
            <td valign="top" align="justify">[Surat Camat Sumur Bandung Kota Bandung Nomor 800/197-Kec.Surban tanggal 17 juni 2019 perihal Permohonan Pengaktifan Kembali ASN;]
            </td> 
        </tr>
    </tr>
</table>
<br>
    <p align="center"><strong>M E M U T U S K A N</strong></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top" >Menetapkan</td>
            <td style="padding-top:10px;" valign="top">:</td>
            <td style="padding-top:10px;" valign="top" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KESATU</td>
            <td style="padding-top:10px;" width=1% valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" align="justify" colspan="3">Pengaktifan dan Penempatan Kembali Pegawai Negeri Sipil Daerah Yang Telah Selesai Mengikuti Tugas Belajar;
            </td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KEDUA</td>
            <td td width=1% style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" align="justify" colspan="3">Mengaktifkan dan Menempatkan Kembali Pegawai Negeri Sipil Daerah :
            </td>	
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">&nbsp;</td>
            <td style="padding-top:10px;" valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" valign="top" width=25%>Nama</td> 
            <td style="padding-top:10px;" valign="top" width=1.5%>:</td>
            <td style="padding-top:10px;">&nbsp;</td>
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" width=25%>NIP</td> 
            <td valign="top" width=1.5%>:</td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" width=25%>Pangkat/Gol.Ruang</td> 
            <td valign="top" width=1.5%>:</td>
            <td >&nbsp;</td>
            
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" width=25%>Jabatan</td> 
            <td valign="top" width=1.5%>:</td>
            <td </td>
        </tr>
        <tr>
            <td class="pleft-table" valign="top">&nbsp;</td>
            <td valign="top">&nbsp;&nbsp;&nbsp;</td>
            <td valign="top" width=25%>Unit Kerja</td> 
            <td valign="top" width=1.5%>:</td>
            <td >&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-top:10px;" class="pleft-table" valign="top">KETIGA</td>
            <td width=1% style="padding-top:10px;" valign="top">:&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-top:10px;" align="justify" colspan="3">Keputusan ini mulai berlaku sejak tanggal <variabel> dengan ketentuan akan diadakan perubahan dan perbaikan kembali apabila dikemudian hari terdapat kekeliruan dalam penetapannya.
            </td>
        </tr>
    </table>
    <!-- style="margin-top: 6rem;" -->
 	<br>  
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="65%" align="center">&nbsp;</td>
            <td width="35%">
                <div>Ditetapkan di Bandung,<br>
                    pada tanggal [diisi dengan tanggal]</div>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">WALI KOTA BANDUNG</div>
            </td>
        </tr>
    </table>
    <br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%" align="center">ODED MOHAMAD DANIAL</td>
        </tr>
    </table>
    
</body>
<footer>

</footer>
</html>