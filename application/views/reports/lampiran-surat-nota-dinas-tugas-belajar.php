<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 20px;
            width: 35%;
        }

        li {
            margin-left: 10px;
        }
    </style>
</head>
<body onload="window.print()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" width="10%"><img src="" width="100" height="70"></td>
        <td align="center" width="80%" style="font-size: 20px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:19px">BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN</span></strong>
            <span style="font-size: 12px;">JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG</span>
        </td>
    </tr>
</table>
<hr style="border-top: 3px solid black;">
<br>
<table align="center" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><b>N O T A &nbsp; D I N A S</b></td>
    </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>Kepada</td>
        <td>:&nbsp;&nbsp;&nbsp;Yth. Bapak Walikota Bandung</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Melalui :</td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Yth. Bapak Sekretaris Daerah Kota Bandung</td>
    </tr>
    <tr>
        <td>Dari</td>
        <td>: &nbsp;&nbsp;Kepala Badan Kepegawaian Pendidikan dan Pelatihan Kota Bandung</td>
    </tr>
    <tr>
        <td>Nomor</td>
        <td>:</td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>: &nbsp;&nbsp;1 (Satu) Berkas</td>
    </tr>
    <tr>
        <td>Perihal</td>
        <td>: &nbsp;&nbsp;Permohonan Penandatanganan Surat Keputusan Walikota Bandung</td>
    </tr>
    <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;Tentang PNS untuk Tugas Belajar</td>
    </tr>
</table>
<hr style="border-top: 2px solid black;">
<p align="justify"><span style="margin-left: 60px;"></span>
    Bersama ini disampaikan dengan hormat, rancangan Keputusan Walikota Bandung tentang Penunjukan Pegawai Negeri Sipil di lingkungan Pemerintah Kota Bandung sebagai Mahasiswa Tugas Belajar [diisi dengan program pendidikan][diisi dengan fakultas dan universitas][diisi dengan beasiswa].
</p>
<p align="justify"><span style="margin-left: 60px;"></span>
    Adapun Pegawai Negeri Sipil termaksud adalah :
</p>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
    <colgroup></colgroup>
    <tr>
        <td align="center">No.</td>
        <td align="center">Nama / Pangkat / NIP</td>
        <td align="center">Jabatan</td>
        <td align="center">Program Pendidikan</td>
        <td align="center">Sumber Pendanaan</td>
    </tr>
    <tr>
        <td align="center">1.</td>
        <td align="justify">[diisi dengan Nama / Pangkat / NIP]</td>
        <td align="center">[diisi dengan Jabatan]</td>
        <td align="center">[diisi dengan Program Pendidikan]</td>
        <td align="center">[diisi dengan Sumber Pendanaan]</td>
    </tr>
</table>
<p align="justify"><span style="margin-left: 60px;"></span>
    Apabila Bapak berkenan kiranya draft Keputusan Tugas Belajarnya sebagaimana terlampir dapat ditandatangani.
</p>
<p align="justify"><span style="margin-left: 60px;"></span>
    Demikian kiranya Bapak maklum dan atas perkenannya diucapkan terima kasih.
</p>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50%" align="center">&nbsp;</td>
        <td width="50%" align="center" style="padding-top:20px;">
            <div>KEPALA BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA BANDUNG</div>
        </td>
    </tr>
</table>
<br><br><br><br><br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50%" align="center">&nbsp;</td>
        <td width="50%" align="center">
            <div><u>[diisi nama kepala perangkat daerah]</u></div>
            <div>[diisi nama pangkat kepala perangkat daerah]</div>
            <div>[diisi nip kepala perangkat daerah]</div>
        </td>
    </tr>
</table>
</body>
</html>