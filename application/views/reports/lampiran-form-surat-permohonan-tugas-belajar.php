<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }

        li {
            margin-left: 21px;
        }
    
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px;">
        <tr>
            <td width="50%">&nbsp;</td>
            <td colspan="2" width="30%">
                    Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?><br>
                    Kepada:</td>
        </tr>
        <tr>
            <td width="50%">&nbsp;</td>
            <td width="1%" valign="top">Yth. </td>
            <td valign="top">
                Bapak Wali Kota Bandung<br>
                dengan melalui:
            </td>
        </tr>
        <tr>
            <td width="50%">&nbsp;</td>
            <td valign="top">
                Yth. <br> di</td>
            <td valign="top">
                Bapak Sekretaris Daerah
            </td>
        </tr>
        <tr>
            <td width="50%">&nbsp;</td>
            <td>&nbsp;</td>
            <td>B A N D U N G</td>
        </tr>
    </table>
    <table width="50%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px;">
        <tr>
            <td>Perihal</td>
            <td>:</td>
            <td>Permohonan Tugas Belajar</td>
        </tr>
    </table>
    <br><br><br>
    <table width="36%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Yang bertanda tangan dibawah ini</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">No. Tlp/HP</td>
            <td>:</td>
            <td><?php echo $rows->no_kontak?></td>
        </tr>
        <tr>
            <td class="pleft-table">Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td><?php echo $rows->tempat_lahir .", ". $rows->tanggal_lahir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan dan Tahun Lulus</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <p align="justify"><span style="padding-left: 40px;"></span>
            Dengan ini menyampaikan kepada Bapak agar saya diberi Izin untuk
            melanjutkan pendidikan pada <?php echo $rows->belajar_pendidikan_nm ?><br>
            Sebagai bahan pertimbangan kepada Bapak, bersama ini saya lampirkan:
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <ol>
            <li>Surat izin dari Kepala Perangkat Daerah mengikuti seleksi;</li>
            <li>Surat keterangan dari lembaga pemberi beasiswa yang menerangkan bahwa yang bersangkutan diberikan beasiswa oleh lembaga tersebut;</li>
            <li>Surat Pernyataan bermaterai untuk tidak pindah tugas sekurangkurangnya 2 (dua) kali masa pendidikan ditambah 1 (satu) tahun;</li>
            <li>Pas photo Berwarna terbaru ukuran 3x4 sebanyak 2 (dua) buah;</li>
            <li>Photo Copy ijazah terakhir di legalisir;</li>
            <li>Photo Copy Transkrip nilai terakhir dilegalisir;</li>
            <li>Photo Copy SK Terakhir</li>
            <li>Photo Copy SK Jabatan Terakhir (bagi yang menduduki Jabatan);</li>
            <li>Penilaian Prestasi Kerja Pegawai 1 (satu) tahun terakhir;</li>
           </ol>
    </table>
    <p align="justify">
            Demikian permohonan ini saya sampaikan dengan harapan Bapak sudi
            memberikan Izin.
    </p>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div align="center">Pemohon,</div>
                <br><br><br><br><br>
                <div align="center"><?php echo $rows->nama_lengkap?></div>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</body>
</html>