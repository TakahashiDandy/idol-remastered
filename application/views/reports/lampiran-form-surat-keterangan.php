<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 36%;
        }
    </style>
</head>
<body onload="window.print()">
<?php foreach ($data_rows as $rows) : ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url() . "courseware/img/logo-pemkot-bdg-sm.png"?>" width="100" height="70"></td>
            <td align="center" width="80%" style="font-size:25px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:30px"><?php echo strtoupper($nama_skpd)?></span></strong>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="center">JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG</td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <br><br>
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><u><center>SURAT KETERANGAN</center></u></td>
        </tr>
        <tr>
            <td style="padding-top: 10px;">No : <?php echo $data_surat->no_surat;?></td>
        </tr>
    </table>
    <br>
    <p>Yang bertanda tangan di bawah ini &nbsp;:</p>
    <br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $kepala_dinas?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $nip_kepala?></td>
        </tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $pangkat_kepala ." / ". $golongan_kepala?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $jabatan_kepala?></td>
        </tr>
    </table>
    <br><br>
    <p> Menerangkan bahwa sifat tugas/pekerjaan yang diberikan kepada &nbsp;:</p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
            <td><?php echo $rows->nama_lengkap?></td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
            <td><?php echo $rows->nip?></td>
        </tr>
        <tr>
            <td class="pleft-table">Tempat, Tanggal Lahir</td>
            <td>:</td>
            <td><?php echo $rows->tempat_lahir .", ". $rows->tanggal_lahir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pendidikan dan Tahun Lulus</td>
            <td>:</td>
            <td><?php echo $rows->pendidikan_terakhir?></td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat / Golongan</td>
            <td>:</td>
            <td><?php echo $rows->golongan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
            <td><?php echo $rows->jabatan?></td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
            <td><?php echo $rows->instansi?></td>
        </tr>
    </table>
    <br><br>
    <p>Benar-benar menuntut peningkatan kemampuan melalui pendidikan yang lebih tinggi yaitu pendidikan pada <?php echo $rows->belajar_pendidikan_nm ?></p>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                <div>Bandung, <?php echo date("d F Y",strtotime($rows->updated_at))?> </div>
            </td>
        </tr>
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%" style="padding-top:20px;">
                <div>Yang Menerangkan,</div>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%" align="center">&nbsp;</td>
            <td width="40%">
                 <div>(<?php echo $kepala_dinas?>)</div>
            </td>
        </tr>
    </table>
<?php endforeach; ?>
</body>
</html>