<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h3 class="text-white mb-0"><?php echo $tablename ?></h3>
                            </div>
                            <div class="col-md-3 text-right">
                                <!-- Form -->
                                <form class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                    <div class="form-group mb-0">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Cari" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark">
                            <thead class="thead-dark">
                            <tr>
                                <th>Jadwal ID</th>
                                <th>Jenis Diklat</th>
                                <th>Nama Diklat</th>
                                <th>Penyelenggara</th>
                                <th>Jumlah Pendaftar</th>
                                <th>Status Event</th>
                                <th>Aksi</th>
                                <th>Pilih Status Diklat</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($row->result() as $rows){
                                echo "<tr>";
                                echo "<td scope=\"col\">$rows->diklat_jadwal_id</td>";
                                switch ($rows->diklat_jenis){
                                    case 1:
                                        echo "<td scope=\"col\">Struktural</td>";
                                        echo "<td scope=\"col\">$rows->struktural</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Fungsional</td>";
                                        echo "<td scope=\"col\">$rows->fungsional</td>";
                                        break;
                                    case 3:
                                        echo "<td scope=\"col\">Teknis</td>";
                                        echo "<td scope=\"col\">$rows->teknis</td>";
                                        break;
                                }
                                echo "<td scope=\"col\">$rows->diklat_penyelenggara</td>";
                                echo "<td scope=\"col\">$rows->jumlah_pendaftar</td>";
                                switch ($rows->status_event){
                                    case 0:
                                        echo "<td scope=\"col\">Hold</td>";
                                        break;
                                    case 1:
                                        echo "<td scope=\"col\">Ongoing</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Finished</td>";
                                        break;
                                }
                                if($rows->status_event == 0){
                                    echo "<td scope=\"col\">
                                        <a class='text-success' href='".base_url("admin/registrasi/listregistrasi/").$rows->diklat_jadwal_id."' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Detail Data\"><i class='fa fa-info-circle fa-lg'></i></a>
                                        </td>";
                                }else if ($rows->status_event == 1){
                                    echo "<td scope=\"col\">
                                        <a class='text-success' href='".base_url("admin/registrasi/listprosesdiklat/").$rows->diklat_jadwal_id."' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Detail Data\"><i class='fa fa-info-circle fa-lg'></i></a>
                                        </td>";
                                }else{
                                    echo "<td scope=\"col\"><center>-</center></td>";
                                }
                                if($rows->status_event == 0){
                                echo "<td scope=\"col\">
                                        <center>
                                        <div class='dropdown'>
                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                              <i class='fa fa-lg fa-chevron-circle-down'></i>
                                            </a>
                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                              <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",1)'>Ongoing</a>
                                              <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",2)'>Finished</a>
                                            </div>
                                        </div>
                                        </center>
                                    </td>";
                                }else if($rows->status_event == 1){
                                    echo "<td scope=\"col\">
                                        <center>
                                        <div class='dropdown'>
                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                              <i class='fa fa-lg fa-chevron-circle-down'></i>
                                            </a>
                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                              <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",0)'>Hold</a>
                                              <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",2)'>Finished</a>
                                            </div>
                                        </div>
                                        </center>
                                    </td>";
                                }else{
                                    echo "<td scope=\"col\">
                                        <center>
                                        <div class='dropdown'>
                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                              <i class='fa fa-lg fa-chevron-circle-down'></i>
                                            </a>
                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",0)'>Hold</a>
                                                <a class='dropdown-item' onclick='setStatusJadwalDiklat(".$rows->diklat_jadwal_id.",1)'>Ongoing</a>
                                            </div>
                                        </div>
                                        </center>
                                    </td>";
                                }
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer bg-gradient-default border-0">
                        <nav aria-label="Page navigation example">
                            <?php echo $pagination;?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-ubah-status-registrasi" tabindex="-1" role="dialog" aria-labelledby="modal-ubah-status-registrasi-Label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form method="post" action="<?php echo base_url('admin/registrasi/updateregistrasi')?>" style="width:800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- <p class="modal-title">Anda akan meluluskan proses registrasi peserta.</p>
                        <br/>
                        <br/>
                        <p class="modal-title">Pastikan bahwa syarat diklat dipenuhi.</p> -->
                        <input type="hidden" name="nip" id="nip">
                        <h2 class="modal-title" id="modal-ubah-status-registrasi-Label"></h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- <h2 class="modal-title" id="modalLulusLabel">Pastikan bahwa syarat diklat dipenuhi.</h2> -->
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btn-save-modal"></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script type="text/javascript">
    var urlupdatestatusjadwaldiklat  = base_url + "/admin/registrasi/updatestatusjadwaldiklat/";
    function setStatusJadwalDiklat(e,status_event){
        $.ajax({
            url: urlupdatestatusjadwaldiklat,
            type:'POST',
            data: {
                id: e,
                status_event: status_event
            },
            async: false,
            success: function (result) {
                try {
                    if(result.length > 0){
                        location.reload();
                    }
                } catch (e) {
                    alert(e);
                }
            }
        });
    }
</script>
</body>

</html>
