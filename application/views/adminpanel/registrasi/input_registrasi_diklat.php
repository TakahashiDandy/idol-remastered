<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $tablename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/registrasi/updateregistrasi')?>" onsubmit="return checkStatus()">
                            <!--Tabel Registrasi Diklat-->
                            <h6 class="heading-small text-muted mb-4">Daftar Registrasi Diklat</h6>
                            <input type="hidden" name="id_jadwal_diklat" id="id_jadwal_diklat"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <td><i class="fa fa-check"></td>
                                                <td>NIP</td>
                                                <td>Nama</td>
                                                <td>Status</td>
                                                <td>Aksi</td>
                                            </tr>
                                        </thead>
                                        <tbody id="datareject">
                                            <?php $index = 0; ?>
                                            <?php foreach ($rows as $data){
                                                $i = $index++;
                                                echo "<input type='hidden' name='nip[$i]' value=".$data['nip'].">";
                                                echo "<input type='hidden' name='nama[$i]' value=".$data['nama_lengkap'].">";
                                                echo "<input type='hidden' name='keterangan[$i]' value=>";
                                                echo "<tr id='rrow-$i'>";
                                                echo "<td><input type='hidden' name='id_registrasi[$i]' id='id_registrasi$i' value=".$data['id_registrasi']."><input type='checkbox' name='chkreject[]' value=".$data['id_registrasi']."></td>";
                                                echo "<td id='rnip-".$data['id_registrasi']."' >".$data['nip']."</td>";
                                                echo "<td id='rnama-".$data['id_registrasi']."' >".$data['nama_lengkap']."</td>";
                                                echo "<td id='rstatus".$data['id_registrasi']."'> <input type='hidden' name='status[$i]' value='".$data['status_registrant']."'>".$data['status_registrant']."</td>";
                                                echo "<td><div>
                                                            <a id='btn-cek-lampiran".$i."' onclick='checkBerkasLampiran(this)' href='#' role='button' aria-haspopup='true' aria-expanded='false' data-toggle='tooltip' data-placement='top' data-original-title='Cek Berkas Lampiran'><i class='fas fa-user-edit ni-2x'></i></a>
                                                            
                                                            <div class='dropdown' data-toggle='tooltip' data-placement='top' data-original-title='Ubah Status Registrasi Diklat'>
                                                                <a style='width:auto' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-file-signature ni-2x'></i></a>
                                                                <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' x-placement='top-end' style='position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -120px, 0px);'>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Gagal Registrasi</a>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Tunda Registrasi</a>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Registrasi Berhasil</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>";
                                                echo "</tr>";
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?php echo base_url("admin/registrasi/")?>" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<div class="modal fade" id="modal-ubah-status-registrasi" tabindex="-1" role="dialog" aria-labelledby="modal-ubah-status-registrasi-Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form method="post" action="<?php echo base_url('admin/registrasi/updateregistrasi')?>" style="width:800px">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <p class="modal-title">Anda akan meluluskan proses registrasi peserta.</p>
                    <br/>
                    <br/>
                    <p class="modal-title">Pastikan bahwa syarat diklat dipenuhi.</p> -->
                    <input type="hidden" name="nip" id="nip">
                    <h2 class="modal-title" id="modal-ubah-status-registrasi-Label"></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- <h2 class="modal-title" id="modalLulusLabel">Pastikan bahwa syarat diklat dipenuhi.</h2> -->
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn-save-modal" data-dismiss="modal"></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>
    $(document).ready(function(){
        last_url_segments = window.location.pathname.split("/").pop();
        $("#id_jadwal_diklat").val(last_url_segments);
        table = $("#datareject").find("tr").length;
        for (var i = 0; i < table; i++) {
            td = $("#rrow-"+i)[0].children[3].id;
            value = $("#"+td)[0].children[0].value;

            btn = $("#rrow-"+i)[0].children[4].children[0].children[1].id;
            if(value == "Registrasi Berhasil"){
                $("#"+btn).attr("style","visibility:visible");
            }else{
                $("#"+btn).attr("style","visibility:visible");
            }
        }
    });

    function acceptbutton() {
        accept = [];
        $.each($("input[name='chkreject[]']:checked"), function(){
            accept.push($(this).val());
        });
        for(var i = 0; i< accept.length; i++){
            nip = $("#rnip-"+accept[i]).text();
            nama = $("#rnama-"+accept[i]).text();
            rowdata = "<tr id='arow-"+accept[i]+"'>" +
                "<td><input type='checkbox' name='chkaccept[]' value='"+accept[i]+"'></td>"+
                "<td id='anip-"+accept[i]+"'>"+nip+"</td>"+
                "<td id='anama-"+accept[i]+"'>"+nama+"</td>"+
                "</tr>";
            $("#dataaccept").append(rowdata);
            $("#rrow-"+accept[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }
    function rejectbutton() {
        reject = [];
        $.each($("input[name='chkaccept[]']:checked"), function(){
            reject.push($(this).val());
        });
        for(var i = 0; i< reject.length; i++){
            nip = $("#anip-"+reject[i]).text();
            nama = $("#anama-"+reject[i]).text();
            rowdata = "<tr id='rrow-"+reject[i]+"'>" +
                "<td><input type='checkbox' name='chkreject[]' value='"+reject[i]+"'></td>"+
                "<td id='rnip-"+reject[i]+"'>"+nip+"</td>"+
                "<td id='rnama-"+reject[i]+"'>"+nama+"</td>"+
                "</tr>";
            $("#datareject").append(rowdata);
            $("#arow-"+reject[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }

    var urlbindingdataupload  = base_url + "/admin/registrasi/listdetailupload";
    var _element = null;
    var status_diklat = null;
    function setStatus(e){
        _element = e;
        status_diklat = e.text;
        $("div.modal-body").empty();
        $("#btn-save-modal").show();

        if(e.text === "Registrasi Berhasil"){
            $("#btn-save-modal").html("Simpan");
            $("#modal-ubah-status-registrasi-Label").html("Anda akan meluluskan proses registrasi peserta. <br> Pastikan bahwa syarat diklat dipenuhi.");
            var element = "<div class='row'>"+
                            "<div class='col-md-12'>"+
                                "<label for='keterangan-registrasi'>Keterangan</label>"+
                                "<textarea class='form-control' name='keterangan-registrasi' id='keterangan-registrasi' placeholder='Keterangan' type='text'></textarea>"+
                            "</div>"+
                        "</div>";
            $("div.modal-body").append(element);

        }else if(e.text == "Gagal Registrasi" || e.text == "Tunda Registrasi"){
            $("#btn-save-modal").html("Simpan");
            
           if(e.text == "Gagal Registrasi") x = "menggagalkan"
           else if(e.text == "Tunda Registrasi") x = "menunda";
           
            $("#modal-ubah-status-registrasi-Label").html("Anda akan "+x+" proses registrasi peserta. <br> Berikan keterangan pada kolom di bawah untuk memberi informasi kepada peserta.");

            var element = "<div class='row'>"+
                            "<div class='col-md-12'>"+
                                "<label for='keterangan-registrasi'>Keterangan</label>"+
                                "<textarea class='form-control' name='keterangan-registrasi' id='keterangan-registrasi' placeholder='Keterangan' type='text'></textarea>"+
                            "</div>"+
                        "</div>";
            $("div.modal-body").append(element);
        }
        $("#modal-ubah-status-registrasi").modal('show');
    }

    function checkBerkasLampiran(e){
        row = e.closest("tr");
        id = parseInt(row.id.replace(/[^\d.]/g,''));
        td = $("#"+row.id)[0].children[3].id;
        id_reg = parseInt($("#id_registrasi"+id).val());
        status_diklat = $("#"+row.id)[0].children[3].innerText;
        $("div.modal-body").empty();
        bindDataUploads(id_reg,status_diklat);
    }

    function checkStatus(){
        tbody = $("#datareject > tr");
        state = true;
        for (let i = 0; i < tbody.length; i++) {
            if(tbody[i].children[3].textContent === " "){
                state = false;
            }
        }
        if(state){
            return true;
        }else{
            return false;
        }
    }

    function bindDataUploads(id_reg, status_diklat){
        $.ajax({
            url: urlbindingdataupload,
            type: 'POST',
            dataType: 'JSON',
            data: {
                "id_reg" : id_reg,
                "id_jadwal_diklat" : $("#id_jadwal_diklat").val()
            },
            async: false,
            success: function (result) {
                try {
                    if(result.detail_uploads.length > 0){
                        for (let i = 0; i < result.detail_uploads.length; i++) {
                            var base_url = window.location.origin;
                            var pathArray = window.location.pathname.split( '/' );
                            var file_name = result.detail_uploads[i].filepath.split( '/' );
                            if(pathArray[1] == "idol-remastered"){
                                var url_profile = base_url + '/' + pathArray[1] + '/' + result.detail_uploads[i].filepath;
                            } else{
                                var url_profile = base_url + '/' + result.detail_uploads[i].filepath;
                            }
                            if(result.status.status_registrant == '3'){
                                $("#modal-ubah-status-registrasi-Label").html("Berkas Laporan Hasil Diklat.");
                                $("#btn-save-modal").hide();
                                text_label = 'Hasil Diklat ke-';
                                name_label = 'hasil-diklat';
                            }else{
                                $("#modal-ubah-status-registrasi-Label").html("Berkas Lampiran Persyaratan Registrasi Diklat.");
                                $("#btn-save-modal").show();
                                text_label = 'Syarat ke-';
                                name_label = 'syarat';
                            }
                            var element = "<div class='row'>"+
                                                "<div class='col-md-10'>"+
                                                    "<label for='search'>"+text_label+(i+1)+"</label>"+
                                                    "<input class='form-control' name='"+name_label+(i+1)+"' id='"+name_label+(i+1)+"' placeholder='File syarat ke-"+(i+1)+" type='text' value='"+file_name[2]+"' readonly>"+
                                                "</div>"+
                                                "<div class='col-md-2'>"+
                                                    "<a href='"+url_profile+"' target='_blank' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Cek File</a>"
                                                "</div>"+
                                            "</div>";
                            $("div.modal-body").append(element);
                            $("#btn-save-modal").html("OK");
                            $("#modal-ubah-status-registrasi").modal('show');
                        }
                    }
                } catch (e) {
                    alert(e);
                }
            }
        });
    }

    $("#btn-save-modal").click(function(e){
        row = _element.closest("tr");
        id = parseInt(row.id.replace(/[^\d.]/g,''));
        td = $("#"+row.id)[0].children[3].id;
        
        //set status di list registrasi diklat
        $("#"+td).empty();
        $("#"+td).append("<input type='hidden' name='status["+id+"]' value='"+status_diklat+"' />");
        $("#"+row.id)[0].children[3].append(status_diklat);
        
        var keterangan = $("#keterangan-registrasi").val();
        $("input[name='keterangan["+id+"]']").val(keterangan);

        if(status_diklat == "Gagal Registrasi" || status_diklat == "Tunda Registrasi"){
            $("#btn-cek-lampiran"+id).attr("style","visibility:visible");
        }else{
            $("#btn-cek-lampiran"+id).attr("style","visibility:visible");
        }
        $("#modal-ubah-status-registrasi").modal('hide');
    });
</script>
</body>

</html>
