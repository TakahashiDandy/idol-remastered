<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $tablename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/registrasi/updatealumni')?>">
                            <!--Tabel Registrasi Diklat-->
                            <h6 class="heading-small text-muted mb-4">Daftar Peserta Diklat</h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-reject" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <td><i class="fa fa-check"></td>
                                                <td>NIP</td>
                                                <td>Nama</td>
                                                <td>Aksi</td>
                                                <td>Keterangan</td>
                                            </tr>
                                        </thead>
                                        <tbody id="datareject">
                                            <!-- <a id='btn-set-sttp' onclick='setSTTP(this)' role='button' aria-haspopup='true' aria-expanded='false' data-toggle='tooltip' data-placement='top' data-original-title='Ubah STTP Diklat'><i class='fas fa-user-edit ni-2x'></i></a> -->
                                            <?php $index = 0; ?>
                                            <?php foreach ($rows1->result() as $data){
                                                $i = $index++;
                                                echo "<input type='hidden' name='nip[$i]' value='".$data->nip."'>";
                                                echo "<input type='hidden' name='nama[$i]' value='".$data->nama_lengkap."'>";
                                                echo "<input type='hidden' name='keterangan[$i]' value=>";
                                                echo "<input type='hidden' name='arr_sttp_no[$i]' >";
                                                echo "<input type='hidden' name='arr_sttp_tgl[$i]' >";
                                                echo "<input type='hidden' name='arr_sttp_pejabat[$i]' >";

                                                echo "<tr id='rrow-$i'>";
                                                    echo "<td><input type='hidden' name='id_registrasi[$i]' id='id_registrasi$i' value='".$data->id_registrasi."'><input type='checkbox' name='chkreject[]' value='$data->id_registrasi'></td>";
                                                    echo "<td id='rnip-$data->id_registrasi'>$data->nip</td>";
                                                    echo "<td id='rnama-$data->id_registrasi'>$data->nama_lengkap</td>";
                                                    echo "<td>
                                                            <div>
                                                                <a id='btn-cek-lampiran".$i."' onclick='checkBerkasLampiran(this)' href='#' role='button' aria-haspopup='true' aria-expanded='false' data-toggle='tooltip' data-placement='top' data-original-title='Cek Berkas Lampiran'><i class='fas fa-user-edit ni-2x'></i></a>

                                                                <div class='dropdown' data-toggle='tooltip' data-placement='top' data-original-title='Ubah Status Diklat'>
                                                                    <a style='width:auto' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-file-signature ni-2x'></i></a>
                                                                    <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' x-placement='top-end' style='position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -120px, 0px);'>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Gagal Diklat</a>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Tunda Kelulusan Diklat</a>
                                                                        <a class='dropdown-item' onclick='setStatus(this)'>Lulus Diklat</a>
                                                                    </div>
                                                                </div>
                                                            
                                                            </div>
                                                        </td>";
                                                    switch ($data->status_registrant) {
                                                        case 1:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Registrasi ditolak</td>";
                                                            break;
                                                        case 2:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Registrasi ditunda</td>";
                                                            break;
                                                        case 3:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Registrasi berhasil</td>";
                                                            break;
                                                        case 4:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Gagal Diklat</td>";
                                                            break;
                                                        case 5:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Kelulusan Diklat Tertunda</td>";
                                                            break;
                                                        case 6:
                                                            echo "<td id='rstatus".$data->id_registrasi."'> <input type='hidden' name='status' value=>Lulus Diklat</td>";
                                                            break;
                                                    }
                                                echo "</tr>";
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?php echo base_url("admin/registrasi")?>" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-ubah-status-registrasi" tabindex="-1" role="dialog" aria-labelledby="modal-ubah-status-registrasi-Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form method="post" action="<?php echo base_url('admin/registrasi/updatealumni')?>" style="width:800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <!-- <p class="modal-title">Anda akan meluluskan proses registrasi peserta.</p>
                            <br/>
                            <br/>
                            <p class="modal-title">Pastikan bahwa syarat diklat dipenuhi.</p> -->
                            <input type="hidden" name="nip" id="nip">
                            <h2 class="modal-title" id="modal-ubah-status-registrasi-Label"></h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- <h2 class="modal-title" id="modalLulusLabel">Pastikan bahwa syarat diklat dipenuhi.</h2> -->
                        <div class="modal-body" id="modal-body-ubah-status-diklat">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn-save-modal"></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="modal-sttp" tabindex="-1" role="dialog" aria-labelledby="modal-sttp-Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form method="post" action="<?php echo base_url('admin/registrasi/updatealumnisttp')?>" style="width:800px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <input type="hidden" name="nip" id="nip">
                            <h2 class="modal-title" id="modal-sttp-Label">Silahkan masukkan STTP Diklat.</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <!-- <h2 class="modal-title" id="modalLulusLabel">Pastikan bahwa syarat diklat dipenuhi.</h2> -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="sttp_tgl">No. STTP</label>
                                        <input type="text" class="form-control" id="sttp_no" name="sttp_no" placeholder="Nomor STTP" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="sttp_tgl">
                                            STTP Tgl.
                                        </label>
                                        <input class="form-control" name="sttp_tgl" id="sttp_tgl" placeholder="Tanggal STTP" type="date" >
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="sttp_pej">
                                            STTP Pejabat
                                        </label>
                                        <input class="form-control" name="sttp_pejabat" id="sttp_pejabat" placeholder="STTP Pejabat" type="text" >
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-md-12'>
                                    <label for='keterangan-registrasi'>Keterangan</label>
                                    <textarea class='form-control' name='keterangan-registrasi' id='keterangan-registrasi' placeholder='Keterangan' type='text'></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn-save-sttp-modal">Simpan</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>
    var urlbindingdataupload  = base_url + "admin/registrasi/listdetailuploadalumni/";
    var _element = status_diklat = row_id = null;

    $(document).ready(function(){
        $("#btn-set-sttp").attr("style","visibility:hidden");
        $("#btn-set-sttp").attr("onclick","javascript:void(0)");
    });

    function setSTTP(e){
        row = e.closest("tr");
        row_id = parseInt(row.id.replace(/[^\d.]/g,''));
        $("#modal-sttp").modal("show");
    }

    function setStatus(e){
        _element = e;
        status_diklat = e.text;
        row = e.closest("tr");
        id = parseInt(row.id.replace(/[^\d.]/g,''));
        td = $("#"+row.id)[0].children[4].id;
        $("#"+td).empty();
        $("#"+td).append("<input type='hidden' name='status["+id+"]' value='"+e.text+"' />");
        $("#"+row.id)[0].children[4].append(e.text);
        id_reg = parseInt($("#id_registrasi"+id).val());
        $("#modal-body-ubah-status-diklat").empty();

        if(e.text === "Lulus Diklat"){
            row = e.closest("tr");
            row_id = parseInt(row.id.replace(/[^\d.]/g,''));
            $("#sttp_no").val(null);
            $("#sttp_tgl").val(null);
            $("#sttp_pejabat").val(null);
            $("#keterangan-registrasi").val(null);
            $("#modal-sttp").modal("show");
            
            // $("#btn-set-sttp").attr("style","visibility:visible");
            // $("#btn-set-sttp").attr("onclick","setSTTP(this)");
            // $("#btn-save-modal").html("Simpan");
            // $("#modal-ubah-status-registrasi-Label").html("Anda akan meluluskan proses registrasi peserta. <br> Pastikan bahwa syarat diklat dipenuhi.");
            // bindDataUploads();
        }else if(e.text == "Gagal Diklat" || e.text == "Tunda Kelulusan Diklat"){
            $("#btn-set-sttp").attr("style","visibility:hidden");
            $("#btn-set-sttp").attr("onclick","javascript:void(0)");

            $("#btn-save-modal").html("Simpan");
            
           if(e.text == "Gagal Diklat") x = "menggagalkan"; else if(e.text == "Tunda Kelulusan Diklat") x = "menunda";
           
            $("#modal-ubah-status-registrasi-Label").html("Anda akan "+x+" proses registrasi peserta. <br> Berikan keterangan pada kolom di bawah untuk memberi informasi kepada peserta.");

            var element = "<div class='row'>"+
                            "<div class='col-md-12'>"+
                                "<label for='keterangan-registrasi'>Keterangan</label>"+
                                "<textarea class='form-control' name='keterangan-registrasi' id='keterangan-registrasi' placeholder='Keterangan' type='text'></textarea>"+
                            "</div>"+
                        "</div>";
            $("#modal-body-ubah-status-diklat").append(element);
            $("#modal-ubah-status-registrasi").modal('show');
        }
    }

    function bindDataUploads(){
        $.ajax({
            url: urlbindingdataupload + id_reg,
            async: false,
            success: function (result) {
                try {
                    if(result.length > 0){
                        var data = JSON.parse(result);
                        for (var k = 0; k < data.detail_upload.length; k++) {
                            var base_url = window.location.origin;
                            var pathArray = window.location.pathname.split( '/' );
                            var file_name = data.detail_upload[k].filepath.split( '/' );
                            var file_extension = data.detail_upload[k].extension.split('/');
                            if(pathArray[1] == "idol-remastered"){
                                var url_profile = base_url + '/' + pathArray[1] + "/" + data.detail_upload[k].filepath
                            } else{
                                var url_profile = base_url + "/" + data.detail_upload[k].filepath
                            }
                            var element = "<div class='row'>"+
                                            "<div class='col-md-10'>"+
                                                "<label for='search'>Laporan Hasil Diklat "+(k+1)+"</label>"+
                                                "<input class='form-control' name='syarat"+(k+1)+"' id='syarat"+(k+1)+"' placeholder='File syarat ke-"+(k+1)+" type='text' value='"+file_name[2]+"' readonly>"+
                                            "</div>"+
                                            "<div class='col-md-2'>"+
                                                "<a href='"+url_profile+"' target='_blank' class='btn btn-primary' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Cek File</a>"
                                            "</div>"+
                                        "</div>";
                            $("#modal-body-ubah-status-diklat").append(element);
                        }
                    }
                } catch (e) {
                    alert(e);
                }
            }
        });
    }

    $("#btn-save-modal").click(function(e){
        if(status_diklat == "Gagal Diklat" || status_diklat == "Tunda Kelulusan Diklat"){
            row = _element.closest("tr");
            id = parseInt(row.id.replace(/[^\d.]/g,''));
            var keterangan = $("#keterangan-registrasi").val();
            $("input[name='keterangan["+id+"]']").val(keterangan);
            $("#modal-ubah-status-registrasi").modal('hide');
        }else{
            $("#modal-ubah-status-registrasi").modal('hide');
        }
    });

    $("#btn-save-sttp-modal").click(function(e){
        $("input[name='arr_sttp_no["+row_id+"]']").val($("#sttp_no").val());
        $("input[name='arr_sttp_tgl["+row_id+"]']").val($("#sttp_tgl").val());
        $("input[name='arr_sttp_pejabat["+row_id+"]']").val($("#sttp_pejabat").val());
        
        var keterangan = $("#keterangan-registrasi").val();
        $("input[name='keterangan["+id+"]']").val(keterangan);
        
        $("#modal-sttp").modal("hide");
    });

    function acceptbutton() {
        accept = [];
        $.each($("input[name='chkreject[]']:checked"), function(){
            accept.push($(this).val());
        });
        for(var i = 0; i< accept.length; i++){
            nip = $("#rnip-"+accept[i]).text();
            nama = $("#rnama-"+accept[i]).text();
            rowdata = "<tr id='arow-"+accept[i]+"'>" +
                "<td><input type='checkbox' name='chkaccept[]' value='"+accept[i]+"'></td>"+
                "<td id='anip-"+accept[i]+"'>"+nip+"</td>"+
                "<td id='anama-"+accept[i]+"'>"+nama+"</td>"+
                "<td><input type='text' name='asttpno-"+accept[i]+"'></td>"+
                "</tr>";
            $("#dataaccept").append(rowdata);
            $("#rrow-"+accept[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }
    function rejectbutton() {
        reject = [];
        $.each($("input[name='chkaccept[]']:checked"), function(){
            reject.push($(this).val());
        });
        for(var i = 0; i< reject.length; i++){
            nip = $("#anip-"+reject[i]).text();
            nama = $("#anama-"+reject[i]).text();
            rowdata = "<tr id='rrow-"+reject[i]+"'>" +
                "<td><input type='checkbox' name='chkreject[]' value='"+reject[i]+"'></td>"+
                "<td id='rnip-"+reject[i]+"'>"+nip+"</td>"+
                "<td id='rnama-"+reject[i]+"'>"+nama+"</td>"+
                "</tr>";
            $("#datareject").append(rowdata);
            $("#arow-"+reject[i]).remove();
        }
        $.each($("input[name='chkreject[]']:not(:checked)"), function(){
            $("input[name='chkreject[]']").prop('checked',true);
        });
        $.each($("input[name='chkaccept[]']:not(:checked)"), function(){
            $("input[name='chkaccept[]']").prop('checked',true);
        });
    }

    function checkBerkasLampiran(e){
        _element = e;
        status_diklat = e.text;
        row = e.closest("tr");
        id = parseInt(row.id.replace(/[^\d.]/g,''));
        td = $("#"+row.id)[0].children[4].id;
        $("#"+td).empty();
        $("#"+td).append("<input type='hidden' name='status["+id+"]' value='"+e.text+"' />");
        $("#"+row.id)[0].children[4].append(e.text);
        id_reg = parseInt($("#id_registrasi"+id).val());
        $("#modal-body-ubah-status-diklat").empty();
        
        // $("#btn-set-sttp").attr("style","visibility:visible");
        // $("#btn-set-sttp").attr("onclick","setSTTP(this)");

        $("#btn-save-modal").html("Simpan");
        $("#modal-ubah-status-registrasi-Label").html("Cek Berkas Hasil Diklat.");

        bindDataUploads();
        $("#modal-ubah-status-registrasi").modal('show');
    }
</script>
</body>

</html>
