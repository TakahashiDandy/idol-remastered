<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="modal-title">Filter</h5>
                    <input class="form-control" name="search" id="textsearch" oninput="getdatarow()" placeholder="Isikan Nama Diklat" type="text">
                    <hr class="my-4">
                    <h5 class="modal-title">Table Data</h5>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-striped">
                            <thead class="thead-light">
                                <tr>
                                    <td>No. ID</td>
                                    <td>Nama Diklat</td>
                                </tr>
                            </thead>
                            <tbody id="modaldata">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $tablename ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post"  action="<?php echo base_url('admin/jadwal/edit_jadwal/').$this->detektif->tutup($this->session->userdata("diklat_jadwal_id"))?>" enctype="multipart/form-data">
                            <!--Kategori Diklat-->
                            <h6 class="heading-small text-muted mb-4">Jenis Diklat</h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group mb-4">
                                        <div class="form-check">
                                            <input class="custom-radio" type="radio" onclick="getdatarow()" name="diklat_jenis" id="rb_struk" value="1"
                                                <?php if($this->session->userdata('diklat_jenis') == NULL){
                                                    echo "checked";
                                                } else if($this->session->userdata('diklat_jenis') == 1){
                                                    echo "checked";
                                                }?>>
                                            <label class="form-control-label" for="rb_struk">
                                                Struktural
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="custom-radio" type="radio" onclick="getdatarow()" name="diklat_jenis" id="rb_func" value="2"
                                                <?php echo ($this->session->userdata("diklat_jenis")==2 ? "checked" : NULL)?>>
                                            <label class="form-control-label" for="rb_func">
                                                Fungsional
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="custom-radio" type="radio" onclick="getdatarow()" name="diklat_jenis" id="rb_tek" value="3"
                                                <?php echo ($this->session->userdata("diklat_jenis")==3 ?  "checked" : NULL)?>>
                                            <label class="form-control-label" for="rb_tek">
                                                Teknis
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4">

                            <!--Nama Diklat-->
                            <h6 class="heading-small text-muted mb-4">Detail Diklat</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="nama_diklat">
                                            Nama Diklat
                                        </label>
                                        <?php echo form_error("id_diklat","<h5 class='text-danger'>","</h6>") ?>
                                        <input type="hidden" name="id_diklat" id="id_diklat"
                                            value="<?php echo ($this->session->userdata("id_diklat")? $this->session->userdata("id_diklat") : NULL)?>">

                                        <input class="form-control" name="diklat_nama" id="diklat_nama" placeholder="Nama Diklat" type="text"
                                            value="<?php echo ($this->session->userdata("diklat_nama")? $this->session->userdata("diklat_nama") : NULL)?>">
                                    </div>
                                </div>
                                <div class="col-md-3 align-self-center">
                                    <a class="btn btn-md btn-primary" href="" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search"></i> Cari Diklat</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_mulai">
                                            Tanggal Mulai
                                        </label>
                                        <?php echo form_error("diklat_mulai","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_mulai" id="diklat_mulai" placeholder="Tanggal Mulai" type="date"
                                           value="<?php echo ($this->session->userdata("diklat_mulai") ? $this->session->userdata("diklat_mulai") : NULL)?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_selesai">
                                            Tanggal Akhir
                                        </label>
                                        <?php echo form_error("diklat_selesai","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_selesai" id="diklat_selesai" placeholder="Tanggal Selesai" type="date"
                                           value="<?php echo ($this->session->userdata("diklat_selesai") ? $this->session->userdata("diklat_selesai") : NULL)?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_jumlah_jam">
                                            Jumlah Jam
                                        </label>
                                        <?php echo form_error("diklat_jumlah_jam","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_jumlah_jam" id="diklat_jumlah_jam" placeholder="Jumlah Jam" type="text"
                                               value="<?php echo ($this->session->userdata("diklat_jumlah_jam") ? $this->session->userdata("diklat_jumlah_jam") : NULL)?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_jumlah_peserta">
                                            Jumlah Peserta
                                        </label>
                                        <?php echo form_error("diklat_jumlah_peserta","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_jumlah_peserta" id="diklat_jumlah_peserta" placeholder="Jumlah Peserta" type="text"
                                               value="<?php echo ($this->session->userdata("diklat_jumlah_peserta") ? $this->session->userdata("diklat_jumlah_peserta") : NULL)?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_tempat">
                                            Tempat
                                        </label>
                                        <?php echo form_error("diklat_tempat","<h5 class='text-danger'>","</h6>") ?>
                                        <textarea class="form-control" id="diklat_tempat" name="diklat_tempat" placeholder="Tempat" rows="3"
                                        ><?php echo ($this->session->userdata("diklat_tempat") ? $this->session->userdata("diklat_tempat") : "")?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_tempat">
                                            Deskripsi Diklat
                                        </label>
                                        <?php echo form_error("diklat_deskripsi","<h5 class='text-danger'>","</h6>") ?>
                                        <textarea class="form-control" id="diklat_deskripsi" name="diklat_deskripsi" placeholder="Deskripsi Diklat" rows="5"
                                        ><?php echo ($this->session->userdata("diklat_deskripsi") ? $this->session->userdata("diklat_deskripsi") : "")?></textarea>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4">

                            <!--Penyelenggara Diklat-->
                            <h6 class="heading-small text-muted mb-4">Penyelenggara Diklat</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_usul_no">
                                            No. Diklat Usul
                                        </label>
                                        <?php echo form_error("diklat_usul_no","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_usul_no" id="diklat_usul_no" placeholder="No. Diklat Usul" type="text"
                                                value="<?php echo ($this->session->userdata("diklat_usul_no") ? $this->session->userdata("diklat_usul_no") : NULL)?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_usul_tgl">
                                            Diklat Usul Tanggal
                                        </label>
                                        <?php echo form_error("diklat_usul_tgl","<h5 class='text-danger'>","</h6>") ?>
                                        <input class="form-control" name="diklat_usul_tgl" id="diklat_usul_tgl" placeholder="Diklat Usul Tanggal" type="date"
                                                value="<?php echo ($this->session->userdata("diklat_usul_tgl") ? $this->session->userdata("diklat_usul_tgl") : NULL)?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="diklat_penyelenggara">
                                            Penyelenggara
                                        </label>
                                        <?php echo form_error("diklat_penyelenggara","<h5 class='text-danger'>","</h6>") ?>
                                        <textarea class="form-control" name="diklat_penyelenggara" id="diklat_penyelenggara" rows="2" placeholder="Penyelenggara"
                                        ><?php echo ($this->session->userdata("diklat_penyelenggara") ? $this->session->userdata("diklat_penyelenggara") : "")?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button id="btn-add-more" type="button"  class="btn btn-primary">Tambah Data Lampiran</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input_fields_wrap">
                                        <?php
                                        if(isset($attachment)){
                                            $x = 0;
                                            foreach ($attachment as $data){
                                                $x++;
                                                echo "<div class='row'><div class='col-md-8'>".
                                                    "<label class='form-control-label' for='search'>Dokumen ".$x."</label> <br>".
                                                    "<a target='_blank' href='".base_url().$data["filepath"]."' class='form-control-label'>".$data["filepath"]."</a>".
                                                    "</div>".
                                                    "<div class='col-md-2'>".
                                                    "<button type='button' onclick='removefile(".$data["id_attachment"].")' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Remove</button>".
                                                    "</div></div>";
                                            }
                                        } else{
                                            echo '<div class="col-lg-auto">';
                                            echo '<a>Tidak Ada Syarat yang Diupload</a>';
                                            echo '</div>';
                                        }?>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <a href="<?php echo base_url("admin/jadwal")?>" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>
    $( document ).ready(function() {
        getdatarow(1);
    });

    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $("#btn-add-more"); //Add button ID

    var x = <?php echo $x-1?>;
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            element =   "<div class='row'><div class='col-md-8'>"+
                "<label class='form-control-label' or='search'>Dokumen "+x+"</label>"+
                "<input class='form-control' name='syarat"+x+"' id='syarat"+x+"' placeholder='Dokumen ke-"+x+"' type='text'>"+
                "</div>"+
                "<div class='col-md-2'>"+
                "<button onclick='browse_file("+x+")' id='btn-syarat"+x+"' type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-primary'>Pilih File</button>"+
                "<input id='file-syarat"+x+"' name='lampiran[]' type='file' multiple hidden/>"+
                "</div>"+
                "<div class='col-md-2'>"+
                "<button type='button' style='position:absolute;bottom:0;left:0;width:90%' class='btn btn-danger remove_field'>Remove</button>"+
                "</div></div>";
            $(wrapper).append(element); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();$(this).parent().parent().remove(); x--;
    });

    function removefile(prm){
        $.ajax({
            url : "<?php echo base_url("admin/jadwal/deleteattachment/")?>" + prm
        });
    }

    // i = 0;
    function browse_file(e){
        i = e;
        $("#file-syarat"+i).click();

        $("#file-syarat"+i).change(function(e){
            $("#syarat"+i).html($(this).val());
            $("#syarat"+i).val($(this).val());
        });
    }
</script>
</body>

</html>
