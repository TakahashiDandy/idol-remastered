<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php
if($this->ion_auth->is_admin()){
    require_once(APPPATH . 'views/adminpanel/include/sidebar.php');
} else {
    require_once(APPPATH . 'views/adminpanel/include/sidebar_skpd.php');
}
?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-2 pt-6">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                <?php }?>
                <?php if(validation_errors() != NULL) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo validation_errors()?>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"><?php echo $tablename; ?></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/izinbelajar/detailverifikasibkpp/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">
                            <!--Kategori Diklat-->
                            <!-- <h5 class="heading-small text mb-4">Lengkapi Persyaratan</h5>
                            <hr class="my-4"> -->

                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="heading-small text mb-4">Data Peserta :</h5>
                                    <table class="table" ;" border="0">
                                    <?php foreach ($data_rows as $data_izin_belajar){ ?>
                                        <input type="hidden" name="rekomendasi_reg_id" value="<?php echo $data_izin_belajar->rekomendasi_reg_id; ?>">
                                        <tbody>
                                        <tr>
                                            <td width="30%">Nama</td>
                                            <td width="2%">:</td>
                                            <td style="white-space: normal;"><?php if(!empty($data_izin_belajar->nama_lengkap))echo $data_izin_belajar->nama_lengkap; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>NIP</td>
                                            <td>:</td>
                                            <td><?php if(!empty($data_izin_belajar->nip))echo $data_izin_belajar->nip; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pangkat/Golongan</td>
                                            <td>:</td>
                                            <td><?php if(!empty($data_izin_belajar->golongan))echo $data_izin_belajar->golongan; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jabatan</td>
                                            <td>:</td>
                                            <td style="white-space: normal;"><?php if(!empty($data_izin_belajar->jabatan))echo $data_izin_belajar->jabatan; else echo "" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Unit Kerja</td>
                                            <td>:</td>
                                            <td style="white-space: normal;"><?php if(!empty($data_izin_belajar->instansi))echo $data_izin_belajar->instansi; else echo "-" ?></td>
                                        </tr>
                                        </tbody>
                                    <?php } ?>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="heading-small text mb-4">Data Universitas :</h5>
                                    <table class="table" ;" border="0">
                                    <?php foreach ($data_rows as $data_izin_belajar){ ?>
                                        <tbody>
                                        <tr>
                                            <td width="30%">Nama Universitas</td>
                                            <td width="2%">:</td>
                                            <td style="white-space: normal;"><?php if(!empty($data_izin_belajar->univ_nmpti))echo $data_izin_belajar->univ_nmpti; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tingkat Pendidikan</td>
                                            <td>:</td>
                                            <td style="white-space: normal;"><?php echo "Program ".$data_izin_belajar->print_tingpend.' '.$data_izin_belajar->univ_nmpti; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Fakultas</td>
                                            <td>:</td>
                                            <td><?php if(!empty($data_izin_belajar->fakultas_nm))echo $data_izin_belajar->fakultas_nm; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Program Studi</td>
                                            <td>:</td>
                                            <td><?php if(!empty($data_izin_belajar->jurusan_nm))echo $data_izin_belajar->jurusan_nm; else echo "-" ?></td>
                                        </tr>
                                        <tr>
                                            <td>Kota</td>
                                            <td>:</td>
                                            <td><?php if(!empty($data_izin_belajar->univ_kota))echo $data_izin_belajar->univ_kota; else echo "-" ?></td>
                                        </tr>
                                        </tbody>
                                    <?php } ?>
                                    </table>
                                </div>
                            </div>

                            <hr class="my-4">
                            <br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Cek Kelengkapan Dokumen
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table align-items-center table-white">
                                    <thead class="thead-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Laporan</th>
                                        <th>File Laporan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($data_attachment_1 as $laporan) {
                                        $i++;
                                        echo '<tr>';
                                        echo '<td style="white-space:normal">' . $i . '</td>';
                                        echo '<td style="white-space:normal">' . $array_dokumen_upload_1[$laporan->upload_posisi] . '</td>';
                                        echo '<td style="white-space:normal">
                                                         <a href="' . base_url($laporan->upload_file) . '" target="_blank"
                                                             class="btn btn-icon btn-3 btn-primary btn-sm">
                                                             <span class="btn-inner--icon"><i
                                                                     class="fa fa-eye"></i></span>
                                                             <span class="btn-inner--text">Lihat Berkas</span>
                                                         </a>
                                                            </td>';

                                    }
                                    foreach ($data_attachment_2 as $laporan) {
                                        $i++;
                                        echo '<tr>';
                                        echo '<td style="white-space:normal">' . $i . '</td>';
                                        echo '<td style="white-space:normal">' . $array_dokumen_upload_2[$laporan->upload_posisi] . '</td>';
                                        echo '<td style="white-space:normal">
                                                         <a href="' . base_url($laporan->upload_file) . '" target="_blank"
                                                             class="btn btn-icon btn-3 btn-primary btn-sm">
                                                             <span class="btn-inner--icon"><i
                                                                     class="fa fa-eye"></i></span>
                                                             <span class="btn-inner--text">Lihat Berkas</span>
                                                         </a>
                                                            </td>';

                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                            <hr class="my-4">
                            <br>

                            <?php if($this->ion_auth->in_group(1)){ ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="beasiswa">Status Permohonan Izin Belajar</label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="dropdown">
                                        <select class="btn btn-secondary dropdown-toggle" name="jenis_pengiriman" id="jenis_pengiriman_ib">
                                            <option class="dropdown-item" disabled="disabled" <?php echo ($data_izin_belajar->status_reg_belajar < 5 ? "selected='true'" : "")?>>Pilih Status</option>
                                            <option class="dropdown-item" value="5" <?php echo ($data_izin_belajar->status_reg_belajar == 5 ? "selected='true'" : "")?>>Disetujui</option>
                                            <option class="dropdown-item" value="6" <?php echo ($data_izin_belajar->status_reg_belajar == 6 ? "selected='true'" : "")?>>Ditangguhkan</option>
                                            <option class="dropdown-item" value="9" <?php echo ($data_izin_belajar->status_reg_belajar == 9 ? "selected='true'" : "")?>>Ditolak</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">
                            <br>

                            <div id="keterangan" <?php echo ($data_izin_belajar->status_reg_belajar < 6 ? "style='display:none'":"")?>>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="keterangan">
                                                Keterangan
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="keterangan" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">
                            <br>
                            <?php } ?>

                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <?php if($this->ion_auth->in_group(1)){ ?>
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                    <?php } ?>
                                    <a href="<?php echo base_url("admin/izinbelajar")?>" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>

        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>
    $(document).ready(function(){
    })
</script>
</body>

</html>