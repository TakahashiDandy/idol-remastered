<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php
if($this->ion_auth->is_admin()){
    require_once(APPPATH . 'views/adminpanel/include/sidebar.php');
} else {
    require_once(APPPATH . 'views/adminpanel/include/sidebar_skpd.php');
}
?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-2 pt-6">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>

                    <!-- Card stats -->
                    <?php if($this->session->flashdata("success")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-success" role="alert">
                            <strong>Sukses!</strong> <?php echo $this->session->flashdata("success")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-gradient-default shadow">
                        <div class="card-header bg-transparent border-0">
                            <div class="row align-items-center">
                                <!-- <div class="col-md-12">
                                    <h2 class="text-white mb-0"></h2>
                                </div>
                                <hr> -->
                                <div class="col">
                                    <h3 class="text-white mb-0"><?php echo $tablename; ?></h3>
                                </div>
                                <div class="col-md-auto text-right">
                                    <div class="row">
                                        <div class="col-md-auto">
                                            <form
                                                class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                                <div class="form-group mb-0">
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="fas fa-search"></i></span>
                                                        </div>
                                                        <input class="form-control" placeholder="Cari" type="text">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- <div class="col-md-auto">
                                            <a href="<?php echo base_url("user/izinbelajar/addSuratRekomendasi")?>"
                                                class="btn btn-md btn-primary">
                                                <i class="fas fa-plus"></i> Ajukan Izin Belajar
                                            </a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-dark table-flush">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Nama Peserta</th>
                                        <th>Instansi</th>
                                        <th>Nama Program</th>
                                        <th>Status</th>
                                        <!-- <th width="30%">Lihat Proses</th> -->
                                        <th>Aksi (Shortcut)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    <?php foreach ($row->result() as $rows){
                                        $i++;
                                        echo "<tr>";
                                        echo "<td style='white-space:normal'>$rows->nama_lengkap</td>";
                                        echo "<td style='white-space:normal'>$rows->instansi</td>";
                                        echo "<td style='white-space:normal'>Program $rows->print_tingpend $rows->univ_nmpti</td>";
                                        if($rows->status_reg_belajar == 7 && $rows->status_validasi == 1 ){
                                            echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                            <i class='bg-success'></i>Aktifasi Diterima
                                            </span></td>";
                                            echo "<td scope=\"col\">
                                                        <center>
                                                        <div class='dropdown'>
                                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                              <i class='fa fa-lg fa-chevron-circle-down'></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                              <a class='dropdown-item' target='_blank' href='".base_url("functions/reports/sumoizin/".$rows->belajar_reg_id)."'><i class='fa fa-eye'></i> Cetak Surat Permohonan Aktifasi</a>
                                                            </div>
                                                        </div>
                                                        </center>
                                                    </td>";
                                        } else{
                                            echo "<td scope=\"col\"><span class='badge badge-dot mr-4'>
                                            <i class='bg-success'></i>Permohonan Aktifasi
                                            </span></td>";
                                            echo "<td scope=\"col\">
                                                        <center>
                                                        <div class='dropdown'>
                                                            <a class='btn btn-lg btn-icon-only text-light' role='button' aria-expanded='false' aria-haspopup='true' href='#' data-toggle='dropdown'>
                                                              <i class='fa fa-lg fa-chevron-circle-down'></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right dropdown-menu-arrow' style='left: 0px; top: 0px; position: absolute; transform: translate3d(32px, -3px, 0px);'' x-placement='top-end'>
                                                              <a class='dropdown-item' href='".base_url("admin/izinbelajar/detailverifikasiaktifasi/".$rows->belajar_reg_id)."'><i class='fa fa-eye'></i> Lihat Progress Pengajuan Aktifasi Izin Belajar</a>
                                                            </div>
                                                        </div>
                                                        </center>
                                                    </td>";
                                        }
                                        echo "</tr>";
                                    }?>
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer bg-gradient-default border-0">
                            <nav aria-label="Page navigation example">
                                <?php //echo $pagination;?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
    <script>
        function createTarget(t){
            // var width = 1050;
            // var height = 600;
            // var left = (screen.width/2)-(width/2); 
            // var top = (screen.height/2)-(height/2); 
            // window.open ("", t, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+width+', height='+height+', top='+top+', left='+left);
            // return true;

            var params = [
                'height='+screen.height,
                'width='+screen.width,
                'fullscreen=yes' // only works in IE, but here for completeness
            ].join(',');
                 // and any other options from
                 // https://developer.mozilla.org/en/DOM/window.open

            var popup = window.open("", t, params); 
            popup.moveTo(0,0);
            return true;
        }
    </script>
</body>

</html>