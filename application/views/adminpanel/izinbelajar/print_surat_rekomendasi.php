
	<script src="<?php echo asset_url();?>argon/vendor/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>
	<script src="<?php echo asset_url();?>courseware/js/FileSaver.js"></script>
	<script src="<?php echo asset_url();?>courseware/js/html-docx.js"></script>

  	<link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/dist/css/bootstrap.min.css">
  	<link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">
  	<style type="text/css">
  	.float{
		position:fixed; 
		top:40px;
		right:40px; 
		color:#FFF;
		border-radius:50px;
		text-align:center;
		box-shadow: 2px 2px 3px #999;
	}

	.my-float{
		margin-top:22px;
	}
	</style>

	<?php $row = $data_print;?>
  	<div class="content-wrapper">
    	<div class="col-xs-12"> 

    		<br/>
			<textarea id="content" cols="60" rows="24"> 
			</textarea> 
 
			<button class="btn btn-success float" id="convert"><i class="fa fa-save"></i>  Download DOC</button> 

		</div>
	</div>

  	<script> 
		$(document).ready( function() {  
	        var firstDivContent = document.getElementById('template');
			var secondDivContent = document.getElementById('content'); 

			secondDivContent.innerHTML = firstDivContent.innerHTML;
		});

	    tinymce.init({
	      selector: '#content',
	      plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen fullpage",
	        "insertdatetime media table contextmenu paste"
	      ],
	      toolbar: "insertfile undo redo | styleselect | bold italic | " +
	        "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | " +
	        "link image",
            readonly: 1,

	    });

	    document.getElementById('convert').addEventListener('click', function(e) {
	      e.preventDefault();
	      convertImagesToBase64();
          
	      var contentDocument = tinymce.get('content').getDoc();
          getEl = contentDocument.getElementsByClassName("mce-item-table");
          while (getEl[0]) {
            getEl[0].classList.remove('mce-item-table')
          }
            // debugger;
	      var content = '<!DOCTYPE html>' + contentDocument.documentElement.outerHTML;
	      var orientation = 'portrait';
	      var converted = htmlDocx.asBlob(content, {orientation: orientation});

          saveAs(converted, 'Surat Rekomendasi Tugas Belajar <?php echo $row[0]->nomor_rekomendasi; ?>.docx');

	      var link = document.createElement('a');
	      link.href = URL.createObjectURL(converted);
	      link.download = 'document.docx';
	    });

	    function convertImagesToBase64 () {
            var img = document.querySelector('img')
            img.addEventListener('load', function (event) {
              var dataUrl = getDataUrl(event.currentTarget)
              console.log(dataUrl)
            })

            var getDataUrl = function (img) {
                var canvas = document.createElement('canvas')
                var ctx = canvas.getContext('2d')

                canvas.width = img.width
                canvas.height = img.height
                ctx.drawImage(img, 0, 0)

                // If the image is not png, the format
                // must be specified here
                var dataURL = canvas.toDataURL();
                img.setAttribute('src', dataURL);

                canvas.remove();
            }
	    }
  	</script>

<div id="template" style="display:none;">
	<table border="0" cellspacing="3" cellpadding="0" width="100%">
    <tbody>
        <tr>
            <td align="center" width="10%"><img src="<?php echo asset_url(); ?>courseware/img/logo-pemkot-bdg-sm.png"></td>
            <td align="center" width="80%" style="font-size: 20px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:19px"><?php echo $instansi; ?></span></strong><br>
                <span style="font-size: 12px;">JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG</span>
            </td>
        </tr>
    </tbody>
</table>
<div align="center">
    <hr size="3" width="100%" align="center" />
</div>
<div align="center" style="margin-top:20px">
    <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td width="403" valign="top">
                    <p align="center">
                        REKOMENDASI
                    </p>
                </td>
            </tr>
            <tr>
                <td width="403" valign="top">
                    <p>
                        Nomor : <?php echo $row[0]->nomor_rekomendasi; ?>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p style="margin-top:20px">
    Kami yang bertanda tangan di bawah ini :
</p>
<table border="0" cellspacing="3" cellpadding="0" width="100%">
    <tbody>
        <tr>
            <td>
                <p>
                    Nama
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->nama_verifikator; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    Jabatan
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->jabatan_verifikator; ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    Merekomendasikan bahwa nama yang tercantum di bawah ini :
</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tbody>
        <tr>
            <td>
                <p>
                    Nama
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->nama_lengkap; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    NIP
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->nip; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    Pangkat/Golongan
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->golongan; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    Jabatan
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->jabatan; ?>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    Unit Kerja
                </p>
            </td>
            <td>
                <p>
                    : <?php echo $row[0]->instansi; ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    Telah kami setujui untuk mengikuti proses seleksi akademis penerimaan
    Mahasiswa Baru Tahun Akademik 2019/2020 Program Studi <?php echo $row[0]->jurusan_nm; ?> pada Universitas <?php echo $row[0]->univ_nmpti; ?> melalui Program
    Tugas Belajar Beasiswa [diisi dengan beasiswa]
</p>
<p>
    Demikian rekomendasi ini kami buat untuk dipergunakan sebagaimana mestinya.
</p>
<table border="0" cellspacing="3" cellpadding="0" width="100%">
    <tbody>
        <tr>
            <td width="50%">
            </td>
            <td width="50%">
                <p align="center">
                    Bandung, <?php echo tgl_indo(date("Y-m-d")); ?>
                </p>
            </td>
        </tr>
        <tr>
            <td width="40%">
            </td>
            <td width="60%">
                <p align="center">
                    KEPALA <?php echo $instansi; ?> KOTA
                    BANDUNG
                </p>
            </td>
        </tr>
    </tbody>
</table>
<br>
<br>
<table border="0" cellspacing="3" cellpadding="0" width="100%">
    <tbody>
        <tr>
            <td width="50%">
            </td>
            <td width="50%">
                <p align="center">
                    <?php echo $row[0]->nama_verifikator; ?>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    Tembusan :
</p>
<ol start="1" type="1">
    <li>
        Bapak Wali Kota Bandung (sebagai laporan);
    </li>
    <li>
        Bapak Wakil Wali Kota Bandung (sebagai laporan);
    </li>
    <li>
        Bapak Sekretaris Daerah Kota Bandung (sebagai laporan);
    </li>
</ol>
</div>