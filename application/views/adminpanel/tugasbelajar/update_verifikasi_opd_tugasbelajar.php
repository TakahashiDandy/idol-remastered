<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

    <!-- Sidenav -->
    <?php
    if($this->ion_auth->is_admin()){
        require_once(APPPATH . 'views/adminpanel/include/sidebar.php');
    } else {
        require_once(APPPATH . 'views/adminpanel/include/sidebar_skpd.php');
    }
    ?>

    <!-- Main content -->
    <div class="main-content">
        <!-- Top navbar -->
        <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
        <!-- Header -->
        <div class="header bg-gradient-primary pb-2 pt-6">
            <div class="container-fluid">
                <div class="header-body">
                    <!-- Card stats -->
                    <?php if($this->session->flashdata("error_message")) {?>
                    <div style="margin-top: 20px">
                        <div class="alert alert-danger" role="alert">
                            <strong>Perhatian!</strong> <?php echo $this->session->flashdata("error_message")?>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-xl mb-5 mb-xl-0">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0"><?php echo $tablename; ?></h3>
                                </div>
                            </div>
                        </div>

                        <!--INPUT AREA-->
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url('admin/tugasbelajar/detailverifikasiopd/'.$this->uri->segment(4))?>" enctype="multipart/form-data" role="form">
                            <!--Kategori Diklat-->
                            <!-- <h5 class="heading-small text mb-4">Lengkapi Persyaratan</h5>
                            <hr class="my-4"> -->

                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="heading-small text mb-4">Data Peserta :</h5>
                                    <table class="table" ;" border="0">
                                        <?php foreach ($data_rows as $data_tugas_belajar){ ?>
                                        <input type="hidden" name="rekomendasi_reg_id" value="<?php echo $data_tugas_belajar->rekomendasi_reg_id; ?>">
                                        <tbody>
                                            <tr>
                                                <td width="30%">Nama</td>
                                                <td width="2%">:</td>
                                                <td style="white-space: normal;"><?php if(!empty($data_tugas_belajar->nama_lengkap))echo $data_tugas_belajar->nama_lengkap; else echo "-" ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIP</td>
                                                <td>:</td>
                                                <td><?php if(!empty($data_tugas_belajar->nip))echo $data_tugas_belajar->nip; else echo "-" ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pangkat/Golongan</td>
                                                <td>:</td>
                                                <td><?php if(!empty($data_tugas_belajar->golongan))echo $data_tugas_belajar->golongan; else echo "-" ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jabatan</td>
                                                <td>:</td>
                                                <td style="white-space: normal;"><?php if(!empty($data_tugas_belajar->jabatan))echo $data_tugas_belajar->jabatan; else echo "" ?></td>
                                            </tr>
                                            <tr>
                                                <td>Unit Kerja</td>
                                                <td>:</td>
                                                <td style="white-space: normal;"><?php if(!empty($data_tugas_belajar->instansi))echo $data_tugas_belajar->instansi; else echo "-" ?></td>
                                            </tr>
                                        </tbody>
                                        <?php } ?>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="heading-small text mb-4">Data Universitas :</h5>
                            <table class="table" ;" border="0">
                                <?php foreach ($data_rows as $data_tugas_belajar){ ?>
                                <tbody>
                                    <tr>
                                        <td width="30%">Nama Universitas</td>
                                        <td width="2%">:</td>
                                        <td style="white-space: normal;"><?php if(!empty($data_tugas_belajar->univ_nmpti))echo $data_tugas_belajar->univ_nmpti; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tingkat Pendidikan</td>
                                        <td>:</td>
                                        <td style="white-space: normal;"><?php echo "Program ".$data_tugas_belajar->print_tingpend.' '.$data_tugas_belajar->univ_nmpti; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Fakultas</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_tugas_belajar->fakultas_nm))echo $data_tugas_belajar->fakultas_nm; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Program Studi</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_tugas_belajar->jurusan_nm))echo $data_tugas_belajar->jurusan_nm; else echo "-" ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kota</td>
                                        <td>:</td>
                                        <td><?php if(!empty($data_tugas_belajar->univ_kota))echo $data_tugas_belajar->univ_kota; else echo "-" ?></td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                                </div>
                            </div>

                            <hr class="my-4">
                            <br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">
                                            Attachment :
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table align-items-center table-white">
                                    <thead class="thead-white">
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Berkas</th>
                                        <th>File Berkas</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($data_attachment as $laporan) {
                                        $i++;
                                        echo '<tr>';
                                        echo '<td style="white-space:normal">' . $i . '</td>';
                                        echo '<td style="white-space:normal">' . $array_dokumen_upload[$i-1] . '</td>';
                                        echo '<td style="white-space:normal">
                                                         <a href="' . base_url($laporan->upload_file) . '" target="_blank"
                                                             class="btn btn-icon btn-3 btn-primary btn-sm">
                                                             <span class="btn-inner--icon"><i
                                                                     class="fa fa-eye"></i></span>
                                                             <span class="btn-inner--text">Lihat Berkas</span>
                                                         </a>
                                                            </td>';

                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                            <hr class="my-4">
                            <br>

                            <?php foreach ($data_rows as $data_tugas_belajar) : ?>


                            <?php if($this->ion_auth->in_group(3)) :?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="beasiswa">Status Pengajuan Tugas Belajar</label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="dropdown">
                                        <select class="btn btn-secondary dropdown-toggle" name="jenis_pengiriman" id="jenis_pengiriman_tb">
                                            <option class="dropdown-item" disabled="disabled" <?php echo ($data_tugas_belajar->status_reg_belajar == 1 ? "selected='true'" : "")?>>Pilih Status</option>
                                            <option class="dropdown-item" value="2"  <?php echo ($data_tugas_belajar->status_reg_belajar == 2 ? "selected='true'" : "")?>>Disetujui</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?>


                            <div id="collector" <?php echo ($data_tugas_belajar->status_reg_belajar < 2 ? "style='display:none'":"")?>>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label">
                                                Nomor Surat :
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="no_permohonan">
                                                Permohonan Tugas Belajar
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="no_permohonan" id="no_permohonan"
                                            <?php echo ($this->ion_auth->is_admin() ? "readonly" : "")?>
                                               value="<?php echo (isset($data_surat_p) ? ($data_surat_p->belajar_trans_posisi == 0 ? $data_surat_p->no_surat : "-") : "")?>">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="no_abk">
                                                Analisa Kebutuhan Pegawai
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="no_abk" id="no_abk"
                                            <?php echo ($this->ion_auth->is_admin() ? "readonly" : "")?>
                                               value="<?php echo (isset($data_surat_abk) ? ($data_surat_abk->belajar_trans_posisi == 1 ? $data_surat_abk->no_surat : "-") : "")?>">
                                    </div>
                                </div>

                                <hr class="my-4">
                                <br>

                            </div>

                            <div id="keterangan" <?php echo ($data_tugas_belajar->status_reg_belajar < 3 ? "style='display:none'":"")?>>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="keterangan">
                                                    Keterangan
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <textarea name="keterangan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>


                            <!--Action Button-->
                            <div class="row">
                                <div class="col-lg-auto align-self-center">
                                    <?php if($this->ion_auth->in_group(3)) :?>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                    <?php endif;?>
                                    <a href="<?php echo base_url("admin/tugasbelajar/")?>" class="btn btn-danger">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
        </div>
    </div>

    <!-- Argon Scripts -->
    <?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
    <script>
        $(document).ready(function(){
        })
    </script>
</body>

</html>