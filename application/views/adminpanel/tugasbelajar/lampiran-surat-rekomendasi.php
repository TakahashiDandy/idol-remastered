<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            padding-left: 40px;
            padding-right: 30px;
        }

        .pleft-table {
            padding-left: 40px;
            width: 35%;
        }

        li {
            margin-left: 10px;
        }
    </style>
</head>
<body onload="window.print()">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" width="10%"><img src="" width="100" height="70"></td>
            <td align="center" width="80%" style="font-size: 20px;"><strong>PEMERINTAH KOTA BANDUNG<br>
                <span style="font-size:19px">BADAN KEPEGAWAIAN, PENDIDIKAN DAN PELATIHAN</span></strong>
                <span style="font-size: 12px;">JALAN WASTUKANCANA NO.2 Telp./Fax. 4206190 BANDUNG</span>
            </td>
        </tr>
    </table>
    <hr style="border-top: 1px solid black;">
    <br>
    <table align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>REKOMENDASI</td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 10px;">Nomor :</td>
        </tr>
    </table>
    <br>
    <p><span style="margin-left: 60px;"></span>
        Kami yang bertanda tangan di bawah ini :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <p><span style="margin-left: 60px;"></span>
        Merekomendasikan bahwa nama yang tercantum di bawah ini :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pleft-table">Nama</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">NIP</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Pangkat/Golongan</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Jabatan</td>
            <td>:</td>
        </tr>
        <tr>
            <td class="pleft-table">Unit Kerja</td>
            <td>:</td>
        </tr>
    </table>
    <br>
    <p align="justify" style="padding-left: 40px;"><span style="padding-left: 20px;"></span>
        Telah kami setujui untuk mengikuti proses seleksi akademis penerimaan Mahasiswa Baru 
        Tahun Akademik 2019/2020 Program Studi [diisi dengan jurusan kuliah] pada Universitas [diisi nama universitas] 
        melalui Program Tugas Belajar Beasiswa [diisi dengan beasiswa]
    </p>
    <p><span style="padding-left: 60px;"></span>
        Demikian rekomendasi ini kami buat untuk dipergunakan sebagaimana mestinya.
    </p>
    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center">
                <div>Bandung,[diisi dengan tanggal]</div>
            </td>
        </tr>
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center" style="padding-top:20px;">
                <div>KEPALA BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN KOTA BANDUNG</div>
            </td>
        </tr>
    </table>
    <br><br><br><br><br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="50%" align="center">&nbsp;</td>
            <td width="50%" align="center">
                    <div>([diisi nama kepala perangkat daerah])</div>
            </td>
        </tr>
    </table>
    <br>
    <p><span style="padding-left: 40px;"></span>
        Tembusan :
    </p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <ol>
            <li>Bapak Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Wakil Wali Kota Bandung (sebagai laporan);</li>
            <li>Bapak Sekretaris Daerah Kota Bandung (sebagai laporan);</li>
        </ol>
    </table>
</body>
</html>