<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php
if($this->ion_auth->is_admin()){
    require_once(APPPATH . 'views/adminpanel/include/sidebar.php');
} else {
    require_once(APPPATH . 'views/adminpanel/include/sidebar_skpd.php');
}
?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="modal-title">Filter</h5>
                    <input class="form-control" name="search" id="textsearch" oninput="getdatarow()" placeholder="Isikan Nama Diklat" type="text">
                    <hr class="my-4">
                    <h5 class="modal-title">Table Data</h5>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-striped">
                            <thead class="thead-light">
                            <tr>
                                <td>No. ID</td>
                                <td>Nama Diklat</td>
                            </tr>
                            </thead>
                            <tbody id="modaldata">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0"></h3>
                            </div>
                        </div>
                    </div>

                    <!--INPUT AREA-->
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Integrasi Master Diklat</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button class="btn btn-icon btn-3 btn-primary" id="btna" type="button" onclick="integrasidata('master-diklat')">
                                            <span class="btn-inner--icon"><i id="inta" class="fa fa-sync"></i></span>
                                            <span class="btn-inner--text" id="spa">Integrasi Diklat</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Integrasi Master Belajar</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button class="btn btn-icon btn-3 btn-primary" id="btnb" type="button" onclick="integrasidata('master-belajar')">
                                            <span class="btn-inner--icon"><i id="intb" class="fa fa-sync"></i></span>
                                            <span class="btn-inner--text" id="spb">Integrasi Belajar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END INPUT AREA-->

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
<script>

</script>
</body>

</html>
