<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html class="no-js" lang="en-US">


<!--Insert Header-->
<?php require_once(APPPATH.'views/frontpanel/include/header.php');?>

<body>


<!--Insert Mastbar-->
<?php require_once(APPPATH.'views/frontpanel/include/masterheadbar.php');?>

    <section class="page-name">
        <div class="overlay">
            <div class="section">
                <div class="container">
                    <h2 class="section-title">Registrasi Online</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Registrasi Online</li>
                        </ol>
                    </nav>
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </div><!-- /.overlay -->
    </section><!-- /.page-name -->




    <section class="contact">
        <div class="section">
            <div class="container">
                <div class="respond">
                    <form action="#" method="post" class="comment-form">
                    <!--Insert Phase 1-->
                        <div id="phase1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nip">NIP</label>
                                        <input type="text" class="form-control form-control-sm" name="nip" id="nip" placeholder="Masukan NIP" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="nip">Captcha</label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control form-control-sm" name="captcha" id="captcha" placeholder="Masukan Captcha" required="">
                                            </div>
                                            <div class="col-auto">
                                                <img style="max-width: 100px; max-height: 75px;" src="<?php echo asset_url()?>courseware/img/logo-idol.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input class="btn " type="button" onclick="phase2()" value="Next">
                            </div>
                        </div>

                    <!--Insert Phase 2-->
                        <div id="phase2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control form-control-sm" name="nama" id="nama" placeholder="Nama Lengkap">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Golongan</label>
                                        <input type="text" class="form-control form-control-sm" name="golongan" id="golongan" placeholder="Golongan">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Tempat Lahir</label>
                                        <input type="text" class="form-control form-control-sm" name="tempatlahir" id="tempatlahir" placeholder="Tempat Lahir">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Tanggal Lahir</label>
                                        <input type="text" class="form-control form-control-sm" name="tgllahir" id="tgllahir" placeholder="Tanggal Lahir">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Jenis Kelamin</label>
                                        <input type="text" class="form-control form-control-sm" name="jeniskelamin" id="jeniskelamin" placeholder="Jenis Kelamin">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nomor Kontak</label>
                                        <input type="text" class="form-control form-control-sm" name="nomorkontak" id="nomorkontak" placeholder="Nomor Kontak">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Email</label>
                                        <input type="text" class="form-control form-control-sm" name="email" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Alamat Rumah</label>
                                        <textarea class="form-control form-control-sm" rows="5" name="alamatrumah" id="alamatrumah" placeholder="Alamat Rumah"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Jabatan</label>
                                        <input type="text" class="form-control form-control-sm" name="jabatan" id="jabatan" placeholder="Jabatan">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Instansi</label>
                                        <input type="text" class="form-control form-control-sm" name="instansi" id="instansi" placeholder="Instansi">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Pendidikan Terakhir</label>
                                        <input type="text" class="form-control form-control-sm" name="pendidikanterakhir" id="pendidikanterakhir" placeholder="Pendidikan Terakhir">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Kab/Kota</label>
                                        <input type="text" class="form-control form-control-sm" name="kabkota" id="kabkota" placeholder="Kab/Kota">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Provinsi</label>
                                        <input type="text" class="form-control form-control-sm" name="provinsi" id="provinsi" placeholder="Provinsi">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Telepon/Fax</label>
                                        <input type="text" class="form-control form-control-sm" name="telpfax" id="telpfax" placeholder="Telp/Fax">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Alamat Kantor</label>
                                        <textarea class="form-control form-control-sm" rows="5" name="alamatkantor" id="alamatkantor" placeholder="Alamat Kantor"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="btn " type="button" onclick="phase1()" value="Prev">
                                <input class="btn " type="button" onclick="phase3()" value="Next">
                            </div>
                        </div>

                    <!--Insert Phase 3-->
                        <div id="phase3">
                            <div class="form-group">
                                <input class="btn " type="button" onclick="phase2()" value="Prev">
                                <input class="btn " type="submit" value="Simpan">
                                <input class="btn " type="reset" onclick="phase1()" value="Cancel">
                            </div>
                        </div>

                    </form><!-- /.comment-form -->
                </div>
            </div><!--/.container-->
        </div><!-- /.section-padding -->
    </section><!--/.contact-->

<!--Insert Footer-->
<?php require_once(APPPATH.'views/frontpanel/include/footer.php');?>

<script>
    phase1();

    function phase1() {
        $("#phase1").show();
        $("#phase2").hide();
        $("#phase3").hide();
    }

    function phase2() {
        var nip = $("#nip").val();
        var norobot = $("input[name='captcha']").val();
        if(nip  &&  norobot && !isNaN(nip)) {
            $.ajax({
                url: "<?php echo base_url("public/integrasi/load_data")?>" + "/" + nip,
                success: function (result) {
                    try {
                        //memeriksa apakah data simpegnya ada
                        var data = JSON.parse(result);
                        $("#phase1").hide();
                        $("#phase2").show();
                        $("#phase3").hide();

                        $("#nama").val(data.nama);
                        $("#golongan").val(data.golongan_akhir);
                        $("#tempatlahir").val(data.tempat_lahir);
                        $("#tgllahir").val(data.tanggal_lahir);
                        $("#jeniskelamin").val(data.jenis_kelamin);
                        $("#jabatan").val(data.jabatan_nama);
                        $("#nomorkontak").val(data.no_hp);
                        $("#email").val((data.email = "" ? data.email : "-@gmail.com"));
                        $("#instansi").val(data.instansi);
                        $("#pendidikanterakhir").val(data.pendidikan_akhir);
                        $("#kabkota").val("Bandung");
                        $("#provinsi").val("Jawa Barat");
                        $("#telpfax").val(data.no_hp);
                        $("#alamatkantor").val(data.alamat);
                        $("#alamatrumah").val(data.alamat);
                    } catch (e) {
                        alert(result);
                    }
                }
            });
        } else {
            alert("Periksa Kembali Data NIP dan Captcha Anda.");
        }
    }

    function phase3() {
        $("#phase1").hide();
        $("#phase2").hide();
        $("#phase3").show();
    }
</script>

</body>
</html>
