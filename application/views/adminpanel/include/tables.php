<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once(APPPATH . 'views/adminpanel/include/header.php'); ?>

<div class="card bg-gradient-default shadow">
    <div class="card-header bg-transparent border-0">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="text-white mb-0">List Diklat Struktural</h3>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <!-- Projects table -->
        <table id="integrasi_master" class="table align-items-center table-dark table-flush">
            <thead class="thead-dark">
            <tr>
                <th>Kode SKPD</th>
                <th>Nama SKPD</th>
                <th>Tahun</th>
                <th>Kepala Dinas</th>
            </tr>
            </thead>
            <tfoot class="thead-dark">
            <tr>
                <th>Kode SKPD</th>
                <th>Nama SKPD</th>
                <th>Tahun</th>
                <th>Kepala Dinas</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>


<?php require_once(APPPATH . 'views/adminpanel/include/js.php'); ?>
