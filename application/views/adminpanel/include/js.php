<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Core -->
<script src="<?php echo asset_url();?>argon/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo asset_url();?>argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Optional JS -->
<script src="<?php echo asset_url();?>argon/vendor/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo asset_url();?>argon/vendor/chart.js/dist/Chart.extension.js"></script>
<script> var base_url = "<?php echo base_url();?>";</script>
<!-- Argon JS -->
<script src="<?php echo asset_url();?>argon/js/argon.js"></script>
<!--<script src="--><?php //echo asset_url();?><!--argon/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<!-- Data tables -->
<script type="text/javascript" src="<?php echo vendor_url()?>datatables/datatables/media/js/jquery.dataTables.min.js"></script>
<!-- Diskominfo JS -->
<script src="<?php echo asset_url();?>courseware/js/diskominfo.js"></script>
