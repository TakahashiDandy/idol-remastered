<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html"><?php echo $pagename;?></a>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
            <li class="nav-item dropdown icon icon-shape bg-danger text-white rounded-circle">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-lg ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a href="<?php echo base_url("admin/izinbelajar/");?>" class="dropdown-item">
                        <span class="badge badge-pill badge-lg badge-danger" ><?php echo $this->session->userdata("notif_ib") ?></span>
                        <span class="text-danger"> Permohonan Izin Belajar Belum Selesai</span>
                    </a>
                    <a href="<?php echo base_url("admin/tugasbelajar/");?>" class="dropdown-item">
                        <span class="badge badge-pill badge-lg badge-danger" ><?php echo $this->session->userdata("notif_tb") ?></span>
                        <span class="text-danger"> Permohonan Tugas Belajar Belum Selesai</span>
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                    <?php 
                        $url_foto = '';
                        if(!empty($this->session->userdata("datauser")->foto)){
                            $url_foto = $this->session->userdata("datauser")->foto;
                        }else{
                            $url_foto = asset_url()."argon/img/theme/team-1-800x800.jpg";
                        }
                    ?>
                    <img alt="Image placeholder" src="<?php echo $url_foto ?>" style="width: 100%;height: 100%;object-fit: cover;border-radius: 50%;">
                </span>
                        <div class="media-body ml-2 d-none d-lg-block">
                            <span class="mb-0 text-sm  font-weight-bold"><?php echo $this->session->userdata("username")?></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a href="<?php echo base_url("admin/auth/logout");?>" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>