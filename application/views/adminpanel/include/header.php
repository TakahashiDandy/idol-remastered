<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- META DESCRIPTION APLIKASI IDOL -->
    <!-- <meta name="description" content="Area Admin untuk Aplikasi IDOL"> -->
    <!-- META DESCRIPTION APLIKASI SITIMBEL -->
    <meta name="description" content="Area Admin untuk Aplikasi SITIMBEL">
    <meta name="author" content="BKPP x Diskominfo Kota Bandung">
    <meta name="base_url" content="<?php echo base_url()?>">
    <!-- Title IDOL -->
    <!-- <title><?php echo strtoupper($this->session->userdata("username"))?> - IDOL Admin Beranda</title> -->
    <!-- Title SITIMBEL -->
    <title><?php echo strtoupper($this->session->userdata("username"))?> - SITIMBEL Admin Beranda</title>
    <!-- Favicon IDOL -->
    <!-- <link href="<?php echo asset_url();?>courseware/img/favidol.png" rel="icon" type="image/png"> -->
    <!-- Favicon SITIMBEL -->
    <link href="<?php echo asset_url();?>courseware/img/fav-icon-sitimbel.ico" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="<?php echo asset_url();?>argon/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="<?php echo asset_url();?>argon/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="<?php echo asset_url();?>argon/css/argon.css" rel="stylesheet">
<!--    <link type="text/css" href="--><?php //echo asset_url();?><!--argon/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">-->
    <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link type="text/css" href="<?php echo asset_url();?>argon/css/jquery.dataTables.css" rel="stylesheet">

</head>