<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand pt-0" href="<?php echo base_url("admin/home")?>">
            <!-- LOGO IDOL -->
            <!-- <img src="<?php echo asset_url();?>courseware/img/logo-idol.png" class="navbar-brand-img" alt="..."> -->
            <!-- LOGO SITIMBEL -->
            <img src="<?php echo asset_url();?>courseware/img/logo-sitimbel.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="<?php echo asset_url();?>argon/img/theme/team-1-800x800.jpg">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>My profile</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>Pengaturan</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>Activity</span>
                    </a>
                    <a href="./examples/profile.html" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>Support</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url("admin/auth/logout");?>" class="dropdown-item">
                        <i class="ni ni-user-run"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="<?php echo base_url("admin/home");?>">
                            <img src="<?php echo asset_url();?>courseware/img/logo-idol.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Navigation -->
            <ul class="navbar-nav" style="cursor:pointer">
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "home"? "active" : NULL);?>" href="<?php echo base_url("admin/home")?>">
                        <i class="ni ni-tv-2 text-success"></i> Beranda
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "izinbelajar"? "active" : NULL);?>" data-toggle="collapse" data-target="#submenuijinbelajar" aria-controls="nav-link" aria-expanded="false" aria-label="Toggle submenu">
                        <i class="ni ni-badge text-orange"></i> Izin Belajar
                    </a>
                    <div class="collapse" id="submenuijinbelajar">
                        <ul class="navbar-sub">
                            <li class="nav-item">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url("admin/izinbelajar/rekomendasi")?>">
                                    <i class="fa fa-bars text-default"></i> Pengajuan Surat Rekomendasi Izin Belajar
                                </a>
                            </li>
                                <a class="nav-link" href="<?php echo base_url("admin/izinbelajar/")?>">
                                    <i class="fa fa-bars text-default"></i> Pengajuan Surat Izin Belajar
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->router->fetch_class() == "tugasbelajar"? "active" : NULL);?>" data-toggle="collapse" data-target="#submenutugasbelajar" aria-controls="nav-link" aria-expanded="false" aria-label="Toggle submenu">
                        <i class="ni ni-paper-diploma text-orange"></i> Tugas Belajar
                    </a>
                    <div class="collapse" id="submenutugasbelajar">
                        <ul class="navbar-sub">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url("admin/tugasbelajar/rekomendasi")?>">
                                    <i class="fa fa-bars text-default"></i> Pengajuan Surat Rekomendasi Tugas Belajar
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url("admin/tugasbelajar/")?>">
                                    <i class="fa fa-bars text-default"></i> Pengajuan Surat Tugas Belajar
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <!-- Divider -->
             <hr class="my-3">
            <!-- Heading -->
             <h6 class="navbar-heading text-muted">Pengaturan</h6>
            <!-- Navigation -->
             <ul class="navbar-nav mb-md-3">
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">-->
<!--                        <i class="ni ni-spaceship"></i> Getting started-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">-->
<!--                        <i class="ni ni-palette"></i> Foundation-->
<!--                    </a>-->
<!--                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url("admin/integrasicontroller")?>">
                        <i class="ni ni-ui-04"></i> Pengaturan Lanjut
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>