<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<!--Header-->
<?php require_once(APPPATH.'views/adminpanel/include/header.php');?>

<body>

<!-- Sidenav -->
<?php require_once(APPPATH.'views/adminpanel/include/sidebar.php');?>

<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php require_once(APPPATH.'views/adminpanel/include/topbar.php');?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl mb-5 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <h3 class="text-white mb-0"><?php echo $tablename ?></h3>
                            </div>
                            <div class="col-md-3 text-right">
                                <!-- Form -->
                                <form class="navbar-search navbar-search-dark form-inline mr-1 d-none d-md-flex ml-lg-auto">
                                    <div class="form-group mb-0">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Cari" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark">
                            <thead class="thead-dark">
                            <tr>
                                <th>Jadwal ID</th>
                                <th>Jenis Diklat</th>
                                <th>Nama Diklat</th>
                                <th>Penyelenggara</th>
                                <th>Jumlah Pendaftar</th>
                                <th>Status Event</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($row->result() as $rows){
                                echo "<tr>";
                                echo "<td scope=\"col\">$rows->diklat_jadwal_id</td>";
                                switch ($rows->diklat_jenis){
                                    case 1:
                                        echo "<td scope=\"col\">Struktural</td>";
                                        echo "<td scope=\"col\">$rows->struktural</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Fungsional</td>";
                                        echo "<td scope=\"col\">$rows->fungsional</td>";
                                        break;
                                    case 3:
                                        echo "<td scope=\"col\">Teknis</td>";
                                        echo "<td scope=\"col\">$rows->teknis</td>";
                                        break;
                                }
                                echo "<td scope=\"col\">$rows->diklat_penyelenggara</td>";
                                echo "<td scope=\"col\">$rows->jumlah_pendaftar</td>";
                                switch ($rows->status_event){
                                    case 0:
                                        echo "<td scope=\"col\">Hold</td>";
                                        break;
                                    case 1:
                                        echo "<td scope=\"col\">Ongoing</td>";
                                        break;
                                    case 2:
                                        echo "<td scope=\"col\">Finished</td>";
                                        break;
                                }
                                echo "<td scope=\"col\">
                                    <a class='text-success' href='".base_url("admin/report/getReportPassed/").$rows->diklat_jadwal_id."' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Download File\"><i class='fa fa-download fa-lg'></i></a>
                                    </td>";
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer bg-gradient-default border-0">
                        <nav aria-label="Page navigation example">
                            <?php echo $pagination;?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php require_once(APPPATH.'views/adminpanel/include/footer.php');?>
    </div>
</div>

<!-- Argon Scripts -->
<?php require_once(APPPATH.'views/adminpanel/include/js.php');?>
</body>

</html>
