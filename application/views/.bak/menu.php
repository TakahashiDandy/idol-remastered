<!--Menu Single-->

<li class="nav-item menu-item-has-children dropdown active">
    <a class="nav-link" href="https://demos.jeweltheme.com/courseware/#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Registrasi Online</a>
</li>

<!--Menu Multiple-->
<li class="nav-item menu-item-has-children dropdown">
    <a class="nav-link dropdown-toggle" href="https://demos.jeweltheme.com/courseware/#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Courses</a>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="https://demos.jeweltheme.com/courseware/courses.html">All Courses</a>
        <a class="dropdown-item" href="https://demos.jeweltheme.com/courseware/course-single-01.html">Course single 01</a>
        <a class="dropdown-item" href="https://demos.jeweltheme.com/courseware/course-single-02.html">Course single 02</a>
    </div>
</li>

<!--Sidebar collapse submenu admin-->
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" data-target="#submenu" aria-controls="nav-link" aria-expanded="false" aria-label="Toggle submenu">
            <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
        <div class="collapse" id="submenu">
            <ul class="navbar-sub">
                <li class="nav-item">
                    <a class="nav-link-sub" href="./index.html">
                        <i class="ni ni-bold-right text-primary"></i> Dashboard
                    </a>
                </li>
            </ul>
        </div>
    </li>
</ul>

<!--Sidebar single menu admin-->
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="./examples/icons.html">
            <i class="ni ni-planet text-blue"></i> Icons
        </a>
    </li>
</ul>

<!--Divider Menu-->
<!-- Divider -->
<hr class="my-3">
<!-- Heading -->
<h6 class="navbar-heading text-muted">Documentation</h6>
<!-- Navigation -->
<ul class="navbar-nav mb-md-3">
    <li class="nav-item">
        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
            <i class="ni ni-spaceship"></i> Getting started
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">
            <i class="ni ni-palette"></i> Foundation
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">
            <i class="ni ni-ui-04"></i> Components
        </a>
    </li>
</ul>