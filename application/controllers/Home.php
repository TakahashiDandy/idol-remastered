<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("event_model");
    }

    public function index()
    {
        $data["jadwal"] = $this->event_model->fplistjadwal(3);
        $data["jadwalnum"] = $data["jadwal"]->num_rows();
        $data["event"] = $this->event_model->fplistevent(3);
        $data["eventnum"] = $data["event"]->num_rows();
        $this->load->view('frontpanel/home',  $data);
    }
}
