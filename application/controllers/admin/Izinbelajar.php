<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izinbelajar extends CI_Controller {

    // private static $total_jpl = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->helper('dateformat_helper');
        $this->load->model("integrasi_api_model");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model("Universitas_model","universitas");
        $this->load->model("Izinbelajar_model","izinbelajar");
        $this->load->library(array('session','pagination','ion_auth','form_validation','upload'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Izin Belajar";
            $data["tablename"] = "Daftar Pengajuan Izin Belajar";
            $data["field"] = $this->izinbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/izinbelajar/index/");
            $config["total_rows"] = $this->izinbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->izinbelajar->list_izin_belajar($config["per_page"],$page);
            } else{
                $data["row"] = $this->izinbelajar->list_izin_belajar($config["per_page"],$page,$this->session->userdata("username"));
            }

            $this->load->view('adminpanel/izinbelajar/list_izin_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function rekomendasi()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Surat Rekomendasi Izin Belajar";
            $data["tablename"] = "Daftar Pengajuan Surat Rekomendasi Izin Belajar";
            $data["field"] = $this->izinbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/jadwal/index/");
            $config["total_rows"] = $this->izinbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->izinbelajar->list_surat_rekomendasi($config["per_page"],$page);
            } else{
                $data["row"] = $this->izinbelajar->list_surat_rekomendasi($config["per_page"],$page,$this->session->userdata("username"));
            }
            $this->load->view('adminpanel/izinbelajar/list_surat_rekomendasi',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function perpanjangan(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Aktifasi Pegawai Izin Belajar";
            $data["tablename"] = "Daftar Pengajuan Aktifasi Izin Belajar";
            $data["field"] = $this->izinbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/izinbelajar/aktifasi/");
            $config["total_rows"] = $this->izinbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->izinbelajar->list_perpanjangan($config["per_page"],$page);
            } else{
                $data["row"] = $this->izinbelajar->list_perpanjangan($config["per_page"],$page,$this->session->userdata("username"));
            }

            $this->load->view('adminpanel/izinbelajar/list_perpanjangan_izin_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function aktifasi(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Aktifasi Pegawai Izin Belajar";
            $data["tablename"] = "Daftar Pengajuan Aktifasi Izin Belajar";
            $data["field"] = $this->izinbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/izinbelajar/aktifasi/");
            $config["total_rows"] = $this->izinbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->izinbelajar->list_aktifasi($config["per_page"],$page);
            } else{
                $data["row"] = $this->izinbelajar->list_aktifasi($config["per_page"],$page,$this->session->userdata("username"));
            }

            $this->load->view('adminpanel/izinbelajar/list_aktifasi_izin_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function detailVerifikasi($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Detail Verifikasi";
            $data["content"] = 'ini Content 1';
            $data["tablename"] = "Verifikasi Persetujuan Surat Rekomendasi";

            //data registrasi belajar
            $data["data_rows"] = $this->izinbelajar->data_rekomendasi($belajar_reg_id);
            foreach ($data["data_rows"] as $detail){
                if($detail->jenis_pengiriman == 0){
                    $skpd = $this->izinbelajar->getKepalaSKPD($detail->instansi);
                    $data["kepala_dinas"] = $skpd->kepala_dinas;
                    $data["jabatan_kepala"] = $skpd->jabatan_kepala;
                } else{
                    $skpd = $this->izinbelajar->getBKPP();
                    $data["kepala_dinas"] = $skpd->kepala_dinas;
                    $data["jabatan_kepala"] = $skpd->jabatan_kepala;
                }
            }
            // print_r($data["data_rows"][0]);die();
            $this->load->view('adminpanel/izinbelajar/update_status_registrasi',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailverifikasiopd($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $this->form_validation->set_rules('no_rekomendasi', 'Nomor Surat Rekomendasi SKPD', 'required');
            $this->form_validation->set_rules('tgl_rekomendasi', 'Tanggal Surat Rekomendasi SKPD', 'required');
            $this->form_validation->set_rules('no_keterangan', 'Nomor Surat Keterangan', 'required');
            $this->form_validation->set_rules('tgl_keterangan', 'Tanggal Surat Keterangan', 'required');
            $this->form_validation->set_rules('no_abk', 'Nomor Surat Analisa Kebutuhan Pegawai', 'required');
            $this->form_validation->set_rules('tgl_abk', 'Tanggal Surat Analisa Kebutuhan Pegawai', 'required');
            $this->form_validation->set_rules('no_permohonan', 'Nomor Surat Permohonan', 'required');
            $this->form_validation->set_rules('tgl_permohonan', 'Tanggal Surat Permohonan', 'required');
            $this->form_validation->set_rules('no_suket_mahasiswa', 'Nomor Surat Keterangan Mahasiswa', 'required');
            $this->form_validation->set_rules('tgl_suket_mahasiswa', 'Tanggal Keterangan Mahasiswa', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi OPD";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Persetujuan OPD";

                //data registrasi belajar
                $data["data_rows"] = $this->izinbelajar->detailverifikasi_opd($belajar_reg_id);
                $data["data_attachment"] = $this->izinbelajar->getattachmentbyposition($belajar_reg_id,0);
                for ($i = 0; $i < 5; $i++){
                    $data["data_surat"][$i] = $this->izinbelajar->getsuratbyposition($belajar_reg_id,$i);
                }
                // print_r($data["data_rows"][0]);die();
                $data["array_dokumen_upload"] = $this->config->item("dokumen_ib_1");
                $this->load->view('adminpanel/izinbelajar/update_verifikasi_opd',$data);
            } else {
                $status_belajar = $this->input->post("jenis_pengiriman");
                $this->izinbelajar->updateProsesRegistrasiMasterBelajar($status_belajar,$belajar_reg_id);

                if($status_belajar == 3 || $status_belajar == 9){
                    $tolak_jenis = ($status_belajar == 3 ? 1 : 0 );
                    $insertTolak = array(
                        "belajar_reg_id" => $belajar_reg_id,
                        "tolak_jenis_belajar" => 0,
                        "tolak_comment" => $this->input->post("keterangan"),
                        "tolak_jenis" => $tolak_jenis,
                        "tolak_proses" => 1,
                        "created_by" => $this->ion_auth->get_user_id(),
                        "created_at" => date("y-m-d h:i:s")
                    );
                    $this->izinbelajar->save("belajar_trans_tolak",$insertTolak);
                } else {
                    $datainsert = array();
                    for($i = 0; $i < 5; $i++){
                        switch ($i){
                            case 1:
                                $no_surat = $this->input->post("no_keterangan");
                                $tgl_surat = $this->input->post("tgl_keterangan");
                                break;
                            case 2:
                                $no_surat = $this->input->post("no_abk");
                                $tgl_surat = $this->input->post("tgl_abk");
                                break;
                            case 3:
                                $no_surat = $this->input->post("no_permohonan");
                                $tgl_surat = $this->input->post("tgl_permohonan");
                                break;
                            case 4:
                                $no_surat = $this->input->post("no_suket_mahasiswa");
                                $tgl_surat = $this->input->post("tgl_suket_mahasiswa");
                                break;
                            case 0:
                                $no_surat = $this->input->post("no_rekomendasi");
                                $tgl_surat = $this->input->post("tgl_rekomendasi");
                                break;
                        }
                        $insertData = array(
                            "belajar_trans_posisi" => $i,
                            "no_surat" => $no_surat,
                            "reg_belajar_id" => $belajar_reg_id,
                            "created_by" => $this->ion_auth->get_user_id(),
                            "created_at" => $tgl_surat
                        );
                        array_push($datainsert,$insertData);
                    }
                    $this->izinbelajar->insertSurat("belajar_trans_surat",$datainsert,$belajar_reg_id);
                }
                if($status_belajar == 3){
                    $this->session->set_flashdata("error_message","Verifikasi izin belajar oleh OPD ditangguhkan.");
                } else if($status_belajar == 9){
                    $this->session->set_flashdata("error_message","Verifikasi izin belajar oleh OPD ditolak.");
                }else {
                    $this->session->set_flashdata("success","Verifikasi Izin Belajar oleh OPD berhasil disetujui.");
                }
                redirect("admin/izinbelajar");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailverifikasibkpp($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi Dokumen Persyaratan BKPP";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Persetujuan BKPP";

                //data registrasi belajar
                $data["data_rows"] = $this->izinbelajar->detailverifikasi_bkpp($belajar_reg_id);
                $data["data_attachment_1"] = $this->izinbelajar->getattachmentbyposition($belajar_reg_id,0);
                $data["data_attachment_2"] = $this->izinbelajar->getattachmentbyposition($belajar_reg_id,2);
                $data["array_dokumen_upload_1"] = $this->config->item("dokumen_ib_1");
                $data["array_dokumen_upload_2"] = $this->config->item("dokumen_ib_2");
                $this->load->view('adminpanel/izinbelajar/update_verifikasi_bkpp',$data);
            } else {
                $status_belajar = $this->input->post("jenis_pengiriman");
                $this->izinbelajar->updateProsesRegistrasiMasterBelajar($status_belajar,$belajar_reg_id);

                if($status_belajar == 6 || $status_belajar == 9){
                    $tolak_jenis = ($status_belajar == 6 ? 1 : 0 );
                    $insertTolak = array(
                        "belajar_reg_id" => $belajar_reg_id,
                        "tolak_jenis_belajar" => 0,
                        "tolak_comment" => $this->input->post("keterangan"),
                        "tolak_jenis" => $tolak_jenis,
                        "tolak_proses" => 2,
                        "created_by" => $this->ion_auth->get_user_id(),
                        "created_at" => date("y-m-d h:i:s")
                    );
                    $this->izinbelajar->save("belajar_trans_tolak",$insertTolak);
                }
                if($status_belajar == 6){
                    $this->session->set_flashdata("error_message","Surat Keputusan Walikota Izin Belajar ditangguhkan.");
                } else if($status_belajar == 9){
                    $this->session->set_flashdata("error_message","Surat Keputusan Walikota Izin Belajar ditolak.");
                }else {
                    $this->session->set_flashdata("success","Surat Keputusan Walikota Izin Belajar disetujui.");
                }
                redirect("admin/izinbelajar");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailkepwalbkpp($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            $this->form_validation->set_rules('keterangan', 'Keterangan','required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi Keputusan Walikota Izin Belajar BKPP";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Keputusan Walikota Izin Belajar BKPP";

                //data registrasi belajar
                $data["data_rows"] = $this->izinbelajar->detailverifikasi_bkpp($belajar_reg_id);
                $data["data_attachment_1"] = $this->izinbelajar->getattachmentbyposition($belajar_reg_id,0);
                $data["data_attachment_2"] = $this->izinbelajar->getattachmentbyposition($belajar_reg_id,2);
                $data["array_dokumen_upload_1"] = $this->config->item("dokumen_ib_1");
                $data["array_dokumen_upload_2"] = $this->config->item("dokumen_ib_2");
                $data["data_wali"] = $this->izinbelajar->getsuratbyposition($belajar_reg_id,5);
                $this->load->view('adminpanel/izinbelajar/update_kepwal_bkpp',$data);
            } else {
                $status_belajar = $this->input->post("jenis_pengiriman");
                if($status_belajar == 6 || $status_belajar == 9){
                    $tolak_jenis = ($status_belajar == 6 ? 1 : 0 );
                    $insertTolak = array(
                        "belajar_reg_id" => $belajar_reg_id,
                        "tolak_jenis_belajar" => 0,
                        "tolak_comment" => $this->input->post("keterangan"),
                        "tolak_jenis" => $tolak_jenis,
                        "tolak_proses" => 2,
                        "created_by" => $this->ion_auth->get_user_id(),
                        "created_at" => date("y-m-d h:i:s")
                    );
                    $this->izinbelajar->save("belajar_trans_tolak",$insertTolak);
                    $this->izinbelajar->updateProsesRegistrasiMasterBelajar($status_belajar,$belajar_reg_id);
                } else{
                    for($i = 1; $i <=1; $i++){
                        switch ($i){
                            case 1:
                                $no_surat = $this->input->post("no_wal");
                                $tgl_surat = $this->input->post("tgl_wal");
                                break;
                        }
                        $insertData = array(
                            "belajar_trans_posisi" => 5,
                            "no_surat" => $no_surat,
                            "reg_belajar_id" => $belajar_reg_id,
                            "created_by" => $this->ion_auth->get_user_id(),
                            "created_at" => $tgl_surat
                        );
                        $this->izinbelajar->insertOneSurat("belajar_trans_surat",$insertData,$belajar_reg_id, 5);
                    }
                    if($this->upload_files($belajar_reg_id,7,0)){
                        $this->izinbelajar->updateProsesRegistrasiMasterBelajar($status_belajar,$belajar_reg_id);
                    } else{
                        $this->session->set_flashdata("error_message","Upload Gagal, Cek Kembali Ukuran dan Tipe Berkas");
                        redirect("admin/detailkepwal/".$belajar_reg_id);
                    }
                }
                if($status_belajar == 6){
                    $this->session->set_flashdata("error_message","Surat Keputusan Walikota Izin Belajar ditangguhkan.");
                } else if($status_belajar == 9){
                    $this->session->set_flashdata("error_message","Surat Keputusan Walikota Izin Belajar ditolak.");
                }else {
                    $this->session->set_flashdata("success","Surat Keputusan Walikota Izin Belajar disetujui.");
                }
                redirect("admin/izinbelajar");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailprogressbelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Detail Progress Belajar";
            $data["content"] = 'ini Content 1';
            $data["tablename"] = "Detail Pelaporan Per Semester";

            //data registrasi belajar
            $data["data_rows"] = $this->izinbelajar->detailverifikasi_bkpp($belajar_reg_id);
            $data["data_attachment"] = $this->izinbelajar->getprogresslaporan($belajar_reg_id);
            $data["data_riwayat"] = $this->izinbelajar->getriwayatlulus($belajar_reg_id);

            // print_r($data["data_rows"][0]);die();
            $this->load->view('adminpanel/izinbelajar/detail_progress_izin_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailverifikasiaktifasi($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            $this->form_validation->set_rules('no_permohonan_aktifasi', 'Nomor Surat Permohonan Aktifasi', 'required');
            $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi Permohonan Aktifasi BKPP";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Permohonan Aktifasi BKPP";

                //data registrasi belajar
                $data["data_rows"] = $this->izinbelajar->detailverifikasi_bkpp($belajar_reg_id);
                $data["data_aktifasi"] = $this->izinbelajar->getsuratbyposition($belajar_reg_id,7);
                $data["data_attachment"] = $this->izinbelajar->getprogresslaporan($belajar_reg_id);
                $data["data_riwayat"] = $this->izinbelajar->getriwayatlulus($belajar_reg_id);
                $this->load->view('adminpanel/izinbelajar/aktifasi_izin_belajar',$data);
            } else {
                $status_belajar = $this->input->post("jenis_pengiriman");
                $this->izinbelajar->updateValidasi($status_belajar,$belajar_reg_id);
                $insertData = array(
                    "belajar_trans_posisi" => 7,
                    "no_surat" => $this->input->post("no_permohonan_aktifasi"),
                    "reg_belajar_id" => $belajar_reg_id,
                    "created_by" => $this->ion_auth->get_user_id(),
                    "created_at" => date("y-m-d h:i:s")
                );
                $this->izinbelajar->insertOneSurat("belajar_trans_surat",$insertData,$belajar_reg_id, 7);
                redirect("admin/izinbelajar/aktifasi");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailverifikasiperpanjangan($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi Permohonan Perpanjangan BKPP";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Permohonan Perpanjangan BKPP";

                //data registrasi belajar
                $data["data_rows"] = $this->izinbelajar->detailperpanjangan_bkpp($belajar_reg_id);
                $this->load->view('adminpanel/izinbelajar/perpanjangan_izin_belajar',$data);
            } else {
                $status_belajar = $this->input->post("jenis_pengiriman");
                $this->izinbelajar->updatePerpanjangan($status_belajar,$belajar_reg_id);
                if($status_belajar == 0){
                    $this->session->set_flashdata("error_message","Perpanjangan Izin Belajar ditolak.");
                }else {
                    $this->session->set_flashdata("success","Perpanjangan Izin Belajar disetujui.");
                }
                redirect("admin/izinbelajar/perpanjangan");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function updateSuratRekomendasi($belajar_reg_id){
        //ADMIN BKPP atau ADMIN SKPD
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            // $id_user = $this->ion_auth->get_user_id();
            // $data_detail = $this->detail_user->get_by_id($id_user);
            $nama = $this->input->post('nama_verifikator');
            $nomor = $this->input->post('no_rekomendasi');
            $jabatan = $this->input->post('jabatan_verifikator');
            $status_surat = $this->input->post('jenis_pengiriman');
            $rekomendasi_reg_id = $this->input->post('rekomendasi_reg_id');
            $keterangan = $this->input->post('keterangan');

            if(!empty($rekomendasi_reg_id) && !empty($status_surat)):
                if($status_surat == 2){
                    $status_belajar = 9;
                } else if($status_surat == 1){
                    $status_belajar = 0;
                } else {
                    $status_belajar = 0;
                }
                //update status surat rekomendasi tugas belajar
                $this->izinbelajar->update("belajar_trans_rekomendasi", array("rekomendasi_reg_id" => $rekomendasi_reg_id),
                    array("status_rekomendasi" => $status_surat));

                //update status registrasi tugas belajar
                $this->izinbelajar->update("registrasi_master_belajar", array("belajar_reg_id" => $belajar_reg_id),
                    array("status_reg_belajar" => $status_belajar));

                //update detail surat rekomendasi tugas belajar
                $this->izinbelajar->update("belajar_trans_rekomendasi_detail", array("rekomendasi_reg_id" => $rekomendasi_reg_id),
                    array(
                        "nomor_rekomendasi" => $nomor,
                        "nama_verifikator" => $nama,
                        "jabatan_verifikator" => $jabatan,
                        "updated_at" => date("Y-m-d H:i:s"),
                        "updated_by" => $this->ion_auth->get_user_id(),
                    ));

                if(isset($keterangan) && ($status_surat == 2 || $status_surat == 9)){
                    $tolak = array(
                        "belajar_reg_id" => $belajar_reg_id,
                        "tolak_comment" => $keterangan,
                        "tolak_jenis_belajar" => 1,
                        "tolak_jenis" => ($status_surat == 2 ? 1 : 0),
                        "created_at" => date("y-m-d h:i:s"),
                        "created_by" => $this->ion_auth->get_user_id()
                    );
                    $this->izinbelajar->save("belajar_trans_tolak",$tolak);
                }
                if($status_surat == 2){
                    $this->session->set_flashdata("error_message","Surat rekomendasi tugas belajar ditangguhkan.");
                } else if($status_surat == 9){
                    $this->session->set_flashdata("error_message","Surat rekomendasi tugas belajar ditolak.");
                }else {
                    $this->session->set_flashdata("success","Surat rekomendasi tugas belajar berhasil disetujui.");
                }
                redirect("admin/izinbelajar/rekomendasi");
            else:
                $this->session->set_flashdata("error_message","Terjadi kesalahan.");
                redirect("admin/izinbelajar/rekomendasi/");
            endif;
        } else{
            redirect("admin/auth/login");
        }
    }

    private function upload_files($belajar_reg_id,$proses,$posisi)
    {
        $uploaded = array();
        $success = true;
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['file_kepwal']['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['file_kepwal']['name']= $files['file_kepwal']['name'][$i];
            $_FILES['file_kepwal']['type']= $files['file_kepwal']['type'][$i];
            $_FILES['file_kepwal']['tmp_name']= $files['file_kepwal']['tmp_name'][$i];
            $_FILES['file_kepwal']['error']= $files['file_kepwal']['error'][$i];
            $_FILES['file_kepwal']['size']= $files['file_kepwal']['size'][$i];
            $extension = pathinfo($_FILES['file_kepwal']['name'], PATHINFO_EXTENSION);
            $custom_file_name = "IzinBelajar-KepWal-".$belajar_reg_id.'-'.$this->ion_auth->get_user_id().'-'.md5(date("y-m-d h:i:s"));

            $this->upload->initialize($this->set_upload_options($custom_file_name));

            if ($this->upload->do_upload('file_kepwal')) {
                $data = $this->upload->data();
                $data_diklat_trans_upload = array(
                    "belajar_reg_id"=> $belajar_reg_id,
                    "upload_proses"=> $proses,
                    "upload_posisi"=> $posisi,
                    "upload_file" => 'uploads/persyaratan/'.$data["raw_name"].".".$extension,
                    "created_at" => date("y-m-d h:i:s"),
                    "created_by" => $this->ion_auth->get_user_id()
                );
                array_push($uploaded,$data_diklat_trans_upload);
            } else {
                $success = false;
                break;
            }
        }
        if($success){
            foreach ($uploaded as $save){
                $this->izinbelajar->save("belajar_trans_upload",$save);
            }
            return true;
        } else {
            return false;
        }
    }

    private function set_upload_options($file_name)
    {
        //UPLOAD AN IMAGE OPTIONS
        $config = array();
        $config['upload_path'] = './uploads/lampiran/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '1024';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

    public function cetak($id)
    {
        if($this->ion_auth->logged_in()):
            $data["data_print"] = $this->izinbelajar->data_tugas_belajar($id);
            
            if(!empty($data["data_print"])):
                if($this->ion_auth->in_group(3)):
                    $data["instansi"] = "BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN";
                elseif($this->ion_auth->in_group(4)):
                    $data["instansi"] = strtoupper($data["data_print"][0]->instansi);
                endif;
                $this->load->view('adminpanel/izinbelajar/print_surat_rekomendasi', $data);
            endif;
        else:
            redirect("admin/auth/login");
        endif;
    }
}