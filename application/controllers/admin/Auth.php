<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->library(array('session','ion_auth'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && ($this->ion_auth->is_admin() || $this->ion_auth->in_group(3))){
            redirect("admin/home");
        } else{
            $this->load->view('adminpanel/login');
        }
    }

    public function login(){
        if($this->ion_auth->logged_in() && ($this->ion_auth->is_admin() || $this->ion_auth->in_group(3))){
            redirect("admin/home");
        } else{
            if(!is_null($this->input->post("username"))){
                $username = $this->input->post("username");
                $password = $this->input->post("password");
                $remember = ($this->input->post("remember") == "on" ? TRUE: 0);
                $output = $this->ion_auth->login($username,$password,$remember);

                $bkpp_or_skpd = array(3,4);
                if( $output == TRUE && $this->ion_auth->in_group($bkpp_or_skpd) ){
                    redirect("admin/home");
                } else if($output == TRUE && (!$this->ion_auth->in_group(1) || !$this->ion_auth->in_group(3)) ){
                    $this->session->set_flashdata("error_message","Cannot Login using provided Credential.");
                    redirect("admin/auth","refresh");
                } else if($output == FALSE && $this->ion_auth->is_max_login_attempts_exceeded($username)) {
                    $this->session->set_flashdata("error_message", "Cannot Login max Login Attempts Exceeded.");
                    redirect("admin/auth", "refresh");
                } else{
                    $this->session->set_flashdata("error_message","Your Username are not found.");
                    redirect("admin/auth","refresh");
                }
            }else{
                $this->load->view('adminpanel/login');
            }
        }
    }

    public function logout(){
        if($this->ion_auth->logged_in() && ($this->ion_auth->is_admin() || $this->ion_auth->in_group(3))){
            $this->ion_auth->logout();
            redirect("home");
        } else{
            redirect("admin/auth/login");
        }
    }

    public function createuser(){
        $username = 'takahashi';
        $password = '12345678';
        $email = 'takittydan88@yahoo.com';
        $additional_data = array(
            'first_name' => 'Takahashi',
            'last_name' => 'Dandy',
        );
        $group = array('1'); // Sets user to admin.

        $this->ion_auth->register($username, $password, $email, $additional_data, $group);
    }
}