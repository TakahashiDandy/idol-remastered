<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form"));
        $this->load->model("registrasi_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Registrasi";
            $data["tablename"] = "List Registrasi Diklat";
            $data["field"] = $this->registrasi_model->listfield("registrasi_master_reg_online");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/registrasi/index/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->registrasi_model->listregistrasi($config["per_page"],$page);

            $this->load->view("adminpanel/registrasi/list_registrasi_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function listregistrasi($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Registrasi";
            $data["tablename"] = "Input Registrasi Diklat";
            $data_ = $this->registrasi_model->detailrejectedregistrasi($id)->result();
            foreach ($data_ as $val) {

                switch ($val->status_registrant) {
                    case 0:
                        $status = "Proses Validasi";
                        break;
                    case 1:
                        $status = "Gagal Registrasi";
                        break;
                    case 2:
                        $status = "Tunda Registrasi";
                        break;
                    case 3:
                        $status = "Registrasi Berhasil";
                        break;
                    default:
                        $status = "Status Tidak Ditampilkan";
                        break;
                }


                $data["rows"][] = array(
                    "nip" => $val->nip,
                    "nama_lengkap" => $val->nama_lengkap,
                    "id_registrasi" => $val->id_registrasi,
                    "status_registrant" =>$status
                );
            }
            $this->load->view("adminpanel/registrasi/input_registrasi_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function updateregistrasi(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $id = $this->input->post("id_registrasi");
            $status = $this->input->post("status");
            $keterangan=$this->input->post('keterangan');
            $accdata = array();
            for ($i=0; $i < count($id); $i++) { 
                if(!is_null($id)):
                    $status_diklat = 0;
                    if($status[$i] == "Proses Validasi"):
                        $status_diklat = 0;
                    elseif($status[$i] == "Gagal Registrasi"):
                        $status_diklat = 1;
                    elseif($status[$i] == "Tunda Registrasi"):
                        $status_diklat = 2;
                    elseif($status[$i] == "Registrasi Berhasil"):
                        $status_diklat = 3;
                    endif;
                    $databatch = array(
                        "id_registrasi" => $id[$i],
                        "status_registrant" => $status_diklat,
                        "keterangan" => $keterangan[$i]
                    );
                    array_push($accdata,$databatch);
                    $this->registrasi_model->updateregistrasi($accdata);
                endif;
            }
            redirect("admin/registrasi/");
        } else{
            redirect("admin/auth/login");
        }
    }

    public function alumni()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Alumni";
            $data["tablename"] = "List Alumni Diklat";

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/registrasi/alumni/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->registrasi_model->listalumni($config["per_page"],$page);

            $this->load->view("adminpanel/registrasi/list_alumni_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function listprosesdiklat($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->form_validation->set_rules('sttp_tgl', 'STTP Tgl', 'required');
            $this->form_validation->set_rules('sttp_pej', 'STTP Pejabat', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("sttp_tgl") && !$this->session->set_userdata("sttp_pej")){
                    $savepoint = array(
                        "sttp_tgl" => $this->input->post("sttp_tgl"),
                        "sttp_pej" => $this->input->post("sttp_pej"),
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Detail Data Diklat";
                $data["tablename"] = "Input Proses Diklat";
                $data["rows1"] = $this->registrasi_model->detailalumni($id);
                $this->load->view("adminpanel/registrasi/input_proses_diklat",$data);
            } else{
                $this->session->unset_userdata(array("sttp_tgl","sttp_pej"));
                $this->updatealumni();
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function listalumni($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->form_validation->set_rules('sttp_tgl', 'STTP Tgl', 'required');
            $this->form_validation->set_rules('sttp_pej', 'STTP Pejabat', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("sttp_tgl") && !$this->session->set_userdata("sttp_pej")){
                    $savepoint = array(
                        "sttp_tgl" => $this->input->post("sttp_tgl"),
                        "sttp_pej" => $this->input->post("sttp_pej"),
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Alumni";
                $data["tablename"] = "Detail Alumni Diklat";
                $data["rows1"] = $this->registrasi_model->detailalumni($id);
                $this->load->view("adminpanel/registrasi/input_alumni_diklat",$data);
            } else{
                $this->session->unset_userdata(array("sttp_tgl","sttp_pej"));
                $this->updatealumni();
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function updatealumni(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $sttp_pej = $this->input->post("arr_sttp_pejabat");
            $sttp_tgl = $this->input->post("arr_sttp_tgl");
            $sttp_no = $this->input->post("arr_sttp_no");

            $id = $this->input->post("id_registrasi");
            $status = $this->input->post("status");
            $keterangan=$this->input->post('keterangan');
            $accdata = array();
            $rwdata = array();
            for ($i=0; $i < count($id); $i++) { 
                if(!is_null($id[$i])):
                    $status_diklat = 0;
                    if($status[$i] == "Proses Validasi"):
                        $status_diklat = 0;
                    elseif($status[$i] == "Gagal Registrasi"):
                        $status_diklat = 1;
                    elseif($status[$i] == "Tunda Registrasi"):
                        $status_diklat = 2;
                    elseif($status[$i] == "Registrasi Berhasil"):
                        $status_diklat = 3;
                    elseif($status[$i] == "Gagal Diklat"):
                        $status_diklat = 4;
                    elseif($status[$i] == "Tunda Kelulusan Diklat"):
                        $status_diklat = 5;
                    elseif($status[$i] == "Lulus Diklat"):
                        $status_diklat = 6;
                        $rwbatch = array(
                            "id_registrasi" => $id[$i],
                            "diklat_sttp_no" => $sttp_no[$i],
                            "diklat_sttp_tgl" => $sttp_tgl[$i],
                            "diklat_sttp_pej" => $sttp_pej[$i],
                        );
                        array_push($rwdata,$rwbatch);
                        $this->registrasi_model->insertriwayat($rwdata);
                    endif;
                    $databatch = array(
                        "id_registrasi" => $id[$i],
                        "status_registrant" => $status_diklat,
                        "keterangan" => $keterangan[$i]
                    );
                    array_push($accdata,$databatch);

                    $this->registrasi_model->updateregistrasi($accdata);
                endif;
            }
            redirect("admin/registrasi");
        } else{
            redirect("admin/auth/login");
        }
    }

    public function listdetailupload(){
        $id_reg = $this->input->post('id_reg');
        $id_jadwal_diklat = $this->input->post('id_jadwal_diklat');
        $check = $this->registrasi_model->checkstatusregistrasi($id_reg, $id_jadwal_diklat);
        if($check->status_registrant == 3){
            $condition = "'%LAPORAN-HASIL-DIKLAT-IDOL%'";
            $detail_uploads = $this->registrasi_model->list_registrasi_trans_uploads(intval($id_reg), $condition);
        }else if($check->status_registrant == 0 or $check->status_registrant == 2){
            $condition = "'%LAMPIRAN-IDOL%'";
            $detail_uploads = $this->registrasi_model->list_registrasi_trans_uploads(intval($id_reg), $condition);
        }
        echo json_encode(array("status" => $check, "detail_uploads" => $detail_uploads));
    }

    public function listdetailuploadalumni($id_reg){
        $condition = "'%HASIL-DIKLAT%'";
        $detail_uploads = $this->registrasi_model->list_registrasi_trans_uploads(intval($id_reg), $condition);
        $detail_riwayat_diklat = $this->registrasi_model->list_trans_riwayat_diklat(intval($id_reg));

        echo json_encode(
            array(
            "detail_upload"=>$detail_uploads,
            "detail_riwayat"=>$detail_riwayat_diklat
            )
        );
    }

    public function updatestatusjadwaldiklat(){
        $jadwal_diklat_id = $_POST['id'];
        $status_event = $_POST['status_event'];

        $data = array(
            "status_event" => $status_event
        );

        $where = array(
            "diklat_jadwal_id" => $jadwal_diklat_id
        );

        //update status_event jadwal diklat
        $status = $this->registrasi_model->update_status_jadwal_diklat($data, $where);
        echo $status;
    }
}
