<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tugasbelajar extends CI_Controller {

    // private static $total_jpl = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->helper('dateformat_helper');
        $this->load->model("integrasi_api_model");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model("Universitas_model","universitas");
        $this->load->model("Tugasbelajar_model","tugasbelajar");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Tugas Belajar";
            $data["tablename"] = "Daftar Pengajuan Tugas Belajar";
            $data["field"] = $this->tugasbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/jadwal/index/");
            $config["total_rows"] = $this->tugasbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->tugasbelajar->list_tugas_belajar($config["per_page"],$page);
            } else{
                $data["row"] = $this->tugasbelajar->list_tugas_belajar($config["per_page"],$page,$this->session->userdata("username"));
            }

            $this->load->view('adminpanel/tugasbelajar/list_tugas_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }


    public function rekomendasi()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Surat Rekomendasi Tugas Belajar";
            $data["tablename"] = "Daftar Pengajuan Surat Rekomendasi Tugas Belajar";
            $data["field"] = $this->tugasbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/jadwal/index/");
            $config["total_rows"] = $this->tugasbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->tugasbelajar->list_surat_rekomendasi($config["per_page"],$page);
            } else{
                $data["row"] = $this->tugasbelajar->list_surat_rekomendasi($config["per_page"],$page,$this->session->userdata("username"));
            }
            $this->load->view('adminpanel/tugasbelajar/list_surat_rekomendasi',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function aktifasi()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Permohonan Pengaktifan Pegawai";
            $data["tablename"] = "Daftar Pengajuan Pengaktifan Pegawai Tugas Belajar";
            $data["field"] = $this->tugasbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/jadwal/index/");
            $config["total_rows"] = $this->tugasbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            if($this->ion_auth->is_admin()){
                $data["row"] = $this->tugasbelajar->list_aktifasi_tugas_belajar($config["per_page"],$page);
            } else{
                $data["row"] = $this->tugasbelajar->list_aktifasi_tugas_belajar($config["per_page"],$page,$this->session->userdata("username"));
            }
            $this->load->view('adminpanel/tugasbelajar/list_aktifasi_tugas_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function addSuratRekomendasi(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)):
            $data["pagename"] = "Tugas Belajar";
            $data['content'] = 'Ini Content 1';

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }

            $this->load->view('adminpanel/tugasbelajarinput_surat_rekomendasi',$data);
        else:
            redirect("user/auth/login");
        endif;
    }

    public function insertSuratRekomendasi(){
        $id_user = $this->ion_auth->get_user_id();
        $data_detail = $this->detail_user->get_by_id($id_user);
        $nama_pendidikan = ($this->input->post('nama_universitas') !== null) ? $this->input->post('nama_universitas') : null;
        $id_universitas = ($this->input->post('id_universitas')!== null) ? $this->input->post('id_universitas') : 0;
        $id_tingpend = ($this->input->post('tingpend_id')!== null) ? $this->input->post('tingpend_id') : 0;
        // $id_sekolah = ($this->input->post('id_sekolah')!== null) ? $this->input->post('id_sekolah') : 0;
        $id_fakultas = ($this->input->post('fakultas_id')!== null) ? $this->input->post('fakultas_id') : 0;
        $kota = ($this->input->post('kota_universitas')!== null) ? $this->input->post('kota_universitas') : '';
        $id_jurusan = ($this->input->post('id_program_studi')!== null) ? $this->input->post('id_program_studi') : 0;
        $beasiswa = ($this->input->post('beasiswa')!== null) ? $this->input->post('beasiswa') : null;
        $jenis_pengiriman = ($this->input->post('jenis_pengiriman')!== null) ? $this->input->post('jenis_pengiriman') : null;

        $dataSuratRekomendasi = array(
            "nip" => $data_detail->nip,
            "rekomendasi_pendidikan_nm" => $nama_pendidikan,
            "rekomendasi_univ_id" => $id_universitas,
            "rekomendasi_jurusan_id" => $id_jurusan,
            "rekomendasi_tingpend_id" => $id_tingpend,
            // "rekomendasi_sekolah_id" => $id_sekolah,
            "rekomendasi_fakultas_id" => $id_fakultas,
            "rekomendasi_pend_lokasi" => $kota,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $data_detail->nama_lengkap,
            // "updated_at" => date("Y-m-d H:i:s"),
            // "updated_by" => $data_detail->nama_lengkap,
            "status_rekomendasi" => 0,
            "rekomendasi_beasiswa" => $beasiswa,
            "jenis_pengiriman" => $jenis_pengiriman
        );

        $rekomendasi_reg_id = $this->universitas->save("belajar_trans_rekomendasi", $dataSuratRekomendasi);

        $dataRegistrasiBelajar = array(
            "rekomendasi_reg_id" => $rekomendasi_reg_id,
            "jenis_belajar" => 1,
            "nip" => $data_detail->nip,
            "belajar_pendidikan_nm" => $nama_pendidikan,
            "belajar_univ_id" => $id_universitas,
            "belajar_jurusan_id" => $id_jurusan,
            "belajar_tingpend_id" => $id_tingpend,
            // "belajar_sekolah_id" => $id_sekolah,
            "belajar_fakultas_id" => $id_fakultas,
            "belajar_pend_lokasi" => $kota,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $data_detail->nama_lengkap,
            "status_reg_belajar" => 0
        );

        $this->universitas->save("registrasi_master_belajar", $dataRegistrasiBelajar);

        $this->session->set_flashdata("success","Anda telah mengajukan surat rekomendasi tugas belajar.");
        redirect("admin/tugasbelajar/rekomendasi");
    }

    public function detailVerifikasi($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Detail Verifikasi";
            $data["content"] = 'ini Content 1';
            $data["tablename"] = "Verifikasi Persetujuan Surat Rekomendasi";

            //data registrasi belajar
            $data["data_rows"] = $this->tugasbelajar->data_rekomendasi($belajar_reg_id);
            foreach ($data["data_rows"] as $detail){
                if($detail->jenis_pengiriman == 0){
                    $skpd = $this->tugasbelajar->getKepalaSKPD($detail->instansi);
                    $data["kepala_dinas"] = $skpd->kepala_dinas;
                    $data["jabatan_kepala"] = $skpd->jabatan_kepala;
                } else{
                    $skpd = $this->tugasbelajar->getBKPP();
                    $data["kepala_dinas"] = $skpd->kepala_dinas;
                    $data["jabatan_kepala"] = $skpd->jabatan_kepala;
                }
            }
            // print_r($data["data_rows"][0]);die();
            $this->load->view('adminpanel/tugasbelajar/update_status_registrasi',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function detailverifikasiopd($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $this->form_validation->set_rules('jenis_pengiriman', 'Status Pengajuan', 'required');
            $this->form_validation->set_rules('no_permohonan', 'Nomor Surat Permohonan Tugas Belajar', 'required');
            $this->form_validation->set_rules('no_abk', 'Nomor Surat Analisa Kebutuhan Pegawai', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Permohonan Tugas Belajar";

                //data registrasi belajar
                $data["data_rows"] = $this->tugasbelajar->data_tugas_belajar($belajar_reg_id);
                $data["data_surat_p"] = $this->tugasbelajar->getsuratbyposition($belajar_reg_id,0);
                $data["data_surat_abk"] = $this->tugasbelajar->getsuratbyposition($belajar_reg_id,1);
                $data["data_attachment"] = $this->tugasbelajar->getattachmentbyposition($belajar_reg_id,0);
                $data["array_dokumen_upload"] = $this->config->item("dokumen_tb_1");
                $this->load->view('adminpanel/tugasbelajar/update_verifikasi_opd_tugasbelajar',$data);
            } else{
                $datainsert = array();
                for($i = 0; $i <=1; $i++){
                    switch ($i){
                        case 1:
                            $no_surat = $this->input->post("no_abk");
                            break;
                        case 0:
                            $no_surat = $this->input->post("no_permohonan");
                            break;
                    }
                    $insertData = array(
                        "belajar_trans_posisi" => $i,
                        "no_surat" => $no_surat,
                        "reg_belajar_id" => $belajar_reg_id,
                        "created_by" => $this->ion_auth->get_user_id(),
                        "created_at" => date("y-m-d h:i:s")
                    );
                    array_push($datainsert,$insertData);
                }
                $this->tugasbelajar->insertSurat("belajar_trans_surat",$datainsert,$belajar_reg_id);

                $status = $this->input->post("jenis_pengiriman");
                $updatedata = array(
                    "updated_at" => date("Y-m-d h:i:s"),
                    "updated_by" => $this->ion_auth->get_user_id(),
                    "status_reg_belajar" => $status
                );
                $this->tugasbelajar->update_tugasbelajar($belajar_reg_id, $updatedata);
                redirect("admin/tugasbelajar");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function detailberkaspermohonan($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $this->form_validation->set_rules('jenis_pengiriman', 'Status Pengajuan', 'required');
            $this->form_validation->set_rules('no_kepwal', 'Nomor Surat Tugas Belajar', 'required');
            $this->form_validation->set_rules('no_loa', 'Nomor Surat Keterangan Perguruan Tinggi', 'required');

            if($this->form_validation->run() == FALSE){
                $data["pagename"] = "Detail Verifikasi";
                $data["content"] = 'ini Content 1';
                $data["tablename"] = "Verifikasi Berkas Permohonan Surat Tugas Belajar";

                //data registrasi belajar
                $data["data_rows"] = $this->tugasbelajar->data_tugas_belajar($belajar_reg_id);
                $data["data_attachment"] = $this->tugasbelajar->getattachmentbyposition($belajar_reg_id);
                $data["data_surat_tb"] = $this->tugasbelajar->getsuratbyposition($belajar_reg_id,2);
                $data["data_surat_loa"] = $this->tugasbelajar->getsuratbyposition($belajar_reg_id,3);
                $data["array_dokumen_upload"] = $this->config->item("dokumen_tb_1");
                $this->load->view('adminpanel/tugasbelajar/update_verifikasi_tugasbelajar',$data);
            } else{
                $datainsert = array();
                for($i = 0; $i <=1; $i++){
                    switch ($i){
                        case 0:
                            $no_surat = $this->input->post("no_kepwal");
                            break;
                        case 1:
                            $no_surat = $this->input->post("no_loa");
                            break;
                    }
                    $insertData = array(
                        "belajar_trans_posisi" => ($i == 0 ? 2 : 3 ),
                        "no_surat" => $no_surat,
                        "reg_belajar_id" => $belajar_reg_id,
                        "created_by" => $this->ion_auth->get_user_id(),
                        "created_at" => date("y-m-d h:i:s")
                    );
                    array_push($datainsert,$insertData);
                }
                $this->tugasbelajar->insertSurat("belajar_trans_surat",$datainsert,$belajar_reg_id);

                $status = $this->input->post("jenis_pengiriman");
                $updatedata = array(
                    "updated_at" => date("Y-m-d h:i:s"),
                    "updated_by" => $this->ion_auth->get_user_id(),
                    "status_reg_belajar" => $status
                );
                $this->tugasbelajar->update_tugasbelajar($belajar_reg_id, $updatedata);
                redirect("admin/tugasbelajar");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    function detailprogresstugasbelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){
            $data["pagename"] = "Detail Progress Belajar";
            $data["content"] = 'ini Content 1';
            $data["tablename"] = "Detail Pelaporan Per Semester";

            //data registrasi belajar
            $data["data_rows"] = $this->tugasbelajar->detailverifikasi_bkpp($belajar_reg_id);
            $data["data_attachment"] = $this->tugasbelajar->getprogresslaporan($belajar_reg_id);
            $data["data_riwayat"] = $this->tugasbelajar->getriwayatlulus($belajar_reg_id);

            // print_r($data["data_rows"][0]);die();
            $this->load->view('adminpanel/tugasbelajar/detail_progress_tugas_belajar',$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function updateSuratRekomendasi($belajar_reg_id){
        //ADMIN BKPP atau ADMIN SKPD
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(1,3))){

            // $id_user = $this->ion_auth->get_user_id();
            // $data_detail = $this->detail_user->get_by_id($id_user);
            $nama = $this->input->post('nama_verifikator');
            $nomor = $this->input->post('no_rekomendasi');
            $jabatan = $this->input->post('jabatan_verifikator');
            $status_surat = $this->input->post('jenis_pengiriman');
            $rekomendasi_reg_id = $this->input->post('rekomendasi_reg_id');
            $keterangan = $this->input->post('keterangan');

            if(!empty($rekomendasi_reg_id) && !empty($status_surat)):
                if($status_surat == 2){
                    $status_belajar = 9;
                } else if($status_surat == 1){
                    $status_belajar = 0;
                } else {
                    $status_belajar = 0;
                }
                //update status surat rekomendasi tugas belajar
                $this->tugasbelajar->update("belajar_trans_rekomendasi", array("rekomendasi_reg_id" => $rekomendasi_reg_id),
                    array("status_rekomendasi" => $status_surat));

                //update status registrasi tugas belajar
                $this->tugasbelajar->update("registrasi_master_belajar", array("belajar_reg_id" => $belajar_reg_id),
                    array("status_reg_belajar" => $status_belajar));

                //update detail surat rekomendasi tugas belajar
                $this->tugasbelajar->update("belajar_trans_rekomendasi_detail", array("rekomendasi_reg_id" => $rekomendasi_reg_id),
                    array(
                        "nomor_rekomendasi" => $nomor,
                        "nama_verifikator" => $nama,
                        "jabatan_verifikator" => $jabatan,
                        "updated_at" => date("Y-m-d H:i:s"),
                        "updated_by" => $this->ion_auth->get_user_id(),
                    ));

                if(isset($keterangan) && ($status_surat == 2 || $status_surat == 9)){
                    $tolak = array(
                        "belajar_reg_id" => $belajar_reg_id,
                        "tolak_comment" => $keterangan,
                        "tolak_jenis_belajar" => 1,
                        "tolak_jenis" => ($status_surat == 2 ? 1 : 0),
                        "created_at" => date("y-m-d h:i:s"),
                        "created_by" => $this->ion_auth->get_user_id()
                    );
                    $this->tugasbelajar->save("belajar_trans_tolak",$tolak);
                }
                if($status_surat == 2){
                    $this->session->set_flashdata("error_message","Surat rekomendasi tugas belajar ditangguhkan.");
                } else if($status_surat == 9){
                    $this->session->set_flashdata("error_message","Surat rekomendasi tugas belajar ditolak.");
                }else {
                    $this->session->set_flashdata("success","Surat rekomendasi tugas belajar berhasil disetujui.");
                }
                redirect("admin/tugasbelajar/rekomendasi");
            else:
                $this->session->set_flashdata("error_message","Terjadi kesalahan.");
                redirect("admin/tugasbelajar/rekomendasi");
            endif;
        } else{
            redirect("admin/auth/login");
        }
    }

    public function cetak($id)
    {
        if($this->ion_auth->logged_in()):
            $data["data_print"] = $this->tugasbelajar->data_tugas_belajar($id);
            
            if(!empty($data["data_print"])):
                if($this->ion_auth->in_group(3)):
                    $data["instansi"] = "BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN";
                elseif($this->ion_auth->in_group(4)):
                    $data["instansi"] = strtoupper($data["data_print"][0]->instansi);
                endif;
                $this->load->view('adminpanel/tugasbelajar/print_surat_rekomendasi', $data);
            endif;
        else:
            redirect("admin/auth/login");
        endif;
    }
}