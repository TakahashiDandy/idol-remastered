<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form"));
        $this->load->model("event_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation', 'Detektif'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->autoupdatejadwal();
            $data["pagename"] = "Jadwal Diklat";
            $data["tablename"] = "List Jadwal Diklat";
            $data["field"] = $this->event_model->listfield("diklat_trans_jadwal");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/jadwal/index/");
            $config["total_rows"] = $this->event_model->gettotalrows("diklat_trans_jadwal");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->event_model->listjadwalAdmin($config["per_page"],$page);

            $this->load->view("adminpanel/jadwal/list_jadwal_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function tambah_jadwal(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("diklat_jenis")){
                    $savepoint = array(
                        "diklat_jenis" => $this->input->post("diklat_jenis"),
                        "id_diklat" => $this->input->post("id_diklat"),
                        "diklat_nama" => $this->input->post("diklat_nama"),
                        "diklat_mulai" => $this->input->post("diklat_mulai"),
                        "diklat_selesai" => $this->input->post("diklat_selesai"),
                        "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                        "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                        "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                        "diklat_tempat" => $this->input->post("diklat_tempat"),
                        "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                        "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                        "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Jadwal Diklat";
                $data["tablename"] = "Tambah Jadwal Diklat";
                $this->load->view("adminpanel/jadwal/input_jadwal_diklat",$data);
            } else{
                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }

                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();

                //SAVE DATA JADWAL DIKLAT TO DATABASE
                $id_diklat_trans_jadwal = $this->event_model->insertjadwal($savepoint);

                //UPLOAD DATA TO SERVER AND SAVE TO DATABASE
                $this->upload_files($id_diklat_trans_jadwal, $id_user);

                redirect("admin/jadwal");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function delete_jadwal($id){
        $ids = $this->detektif->buka($id);
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->event_model->deletejadwal($ids);
            redirect("admin/jadwal/");
        }else{
            redirect("admin/auth/login");
        }

    }

    public function edit_jadwal($id){
        $ids = $this->detektif->buka($id);
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                foreach ($this->event_model->getonejadwal($ids)->result_array() as $data){
                    $savepoint = array(
                        "diklat_jadwal_id" => $data["diklat_jadwal_id"],
                        "diklat_nama" => $data["diklat_nama"],
                        "diklat_jenis" => $data["diklat_jenis"],
                        "diklat_mulai" => $data["diklat_mulai"],
                        "diklat_selesai" => $data["diklat_selesai"],
                        "diklat_deskripsi" => $data["diklat_deskripsi"],
                        "diklat_jumlah_jam" => $data["diklat_jumlah_jam"],
                        "diklat_jumlah_peserta" => $data["diklat_jumlah_peserta"],
                        "diklat_tempat" => $data["diklat_tempat"],
                        "diklat_usul_no" => $data["diklat_usul_no"],
                        "diklat_usul_tgl" => $data["diklat_usul_tgl"],
                        "diklat_penyelenggara" => $data["diklat_penyelenggara"]
                    );
                    switch ($data["diklat_jenis"]){
                        case 1:
                            $savepoint["id_diklat"] = $data["kategori_id"];
                            break;
                        case 2:
                            $savepoint["id_diklat"] = $data["diklat_fungsional_id"];
                            break;
                        case 3:
                            $savepoint["id_diklat"] = $data["diklat_teknis_id"];
                            break;
                    }
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Jadwal Diklat";
                $data["tablename"] = "Edit Jadwal Diklat";
                $data["attachment"] = $this->event_model->getattachment($ids)->result_array();
                $this->load->view("adminpanel/jadwal/edit_jadwal_diklat",$data);
            } else{
                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }
                $this->event_model->updatejadwal($ids,$savepoint);
                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();

                //UPLOAD DATA TO SERVER AND SAVE TO DATABASE
                if(!empty($_FILES)){
                    $this->upload_files($ids, $id_user);
                }

                redirect("admin/jadwal");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function autoupdatejadwal(){
        $this->event_model->autoupdatejadwal();
    }


    private function removesessionjadwal(){
        $session = array(
            "diklat_jenis",
            "id_diklat",
            "nama_diklat",
            "diklat_mulai",
            "diklat_selesai",
            "diklat_jumlah_jam",
            "diklat_jumlah_peserta",
            "diklat_tempat",
            "diklat_usul_no",
            "diklat_usul_tgl",
            "diklat_penyelenggara"
        );
        $this->session->unset_userdata($session);
    }

    private function upload_files($id_diklat_trans_jadwal, $id_user)
    {
        $uploaded = array();
        $success = true;
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['lampiran']['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['lampiran']['name']= $files['lampiran']['name'][$i];
            $_FILES['lampiran']['type']= $files['lampiran']['type'][$i];
            $_FILES['lampiran']['tmp_name']= $files['lampiran']['tmp_name'][$i];
            $_FILES['lampiran']['error']= $files['lampiran']['error'][$i];
            $_FILES['lampiran']['size']= $files['lampiran']['size'][$i];
            $extension = pathinfo($_FILES['lampiran']['name'], PATHINFO_EXTENSION);
            $custom_file_name = "JADWAL-IDOL-".$id_user.'-'.md5(date("y-m-d h:i:s"));

            $this->upload->initialize($this->set_upload_options($custom_file_name));

            if ($this->upload->do_upload('lampiran')) {
                $data = $this->upload->data();

                $data_diklat_trans_upload = array(
                    "id_jadwal"=> $id_diklat_trans_jadwal,
                    "filepath" => 'uploads/lampiran/'.$data["raw_name"].".".$extension,
                    "id_option" => 2,
                    "extension" => $extension,
                    "created_at" => date("y-m-d h:i:s"),
                    "created_by" => $id_user
                );
                array_push($uploaded,$data_diklat_trans_upload);
            } else {
                $success = false;
                break;
            }
        }
        if($success){
            foreach ($uploaded as $save){
                $this->event_model->save_diklat_trans_upload($save);
            }
            return true;
        } else {
            return false;
        }
    }

    private function set_upload_options($file_name)
    {
        //UPLOAD AN IMAGE OPTIONS
        $config = array();
        $config['upload_path'] = './uploads/lampiran/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '1024';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

    public function deleteattachment($id_attachment){
        $data = $this->event_model->removeattachment($id_attachment);
        foreach ($data as $result){
            foreach ($result as $attach){
                unlink($attach["filepath"]);
            }
        }
        return true;
    }


}