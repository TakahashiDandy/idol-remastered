<?php


class Integrasicontroller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("integrasi_api_model");
        $this->load->helper('url');
        $this->load->library(array('session','ion_auth', 'Detektif','datatables'));
    }

    public function index(){
        $data["pagename"] = "Settings";
        $this->load->view("adminpanel/settings/input_settings",$data);
    }

    public function indexx(){
        try{
            $this->datatables->select("kode_skpd,nama_dinas,tahun,kepala_dinas");
            $this->datatables->from("integrasi_master_skpd");
            return print_r($this->datatables->generate());

        } catch (Exception $e){
            echo false;
        }

    }

    public function datatables(){
        $this->load->view("adminpanel/include/tables");
    }

    public function integrasiMasterBelajar(){
        try {
            $this->getSekolah();
            $this->getTingPend();
            $this->getFakultas();
            $this->getJurusan();
            $this->getUniversitas();
            echo true;
        } catch (Exception $ex) {
            echo false;
        }
    }

    public function integrasiMasterDiklat(){
        try {
            $this->getDiklatStruktural();
            $this->getDiklatFungsional();
            $this->getDiklatTeknis();
            echo true;
        } catch (Exception $ex) {
            echo false;
        }
    }

    public function getDiklatStruktural(){
        $maxpage = $this->integrasi_api_model->getMasterPages("diklat/struktural");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterDiklatStruktural($loop);
        }
        $this->integrasi_api_model->getUpdatedDiklatStruktural();
    }

    public function getDiklatFungsional(){
        $maxpage = $this->integrasi_api_model->getMasterPages("diklat/fungsional");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterDiklatFungsional($loop);
        }
        $this->integrasi_api_model->getUpdatedDiklatFungsional();
    }

    public function getDiklatTeknis(){
        $maxpage = $this->integrasi_api_model->getMasterPages("diklat/teknis");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterDiklatTeknis($loop);
        }
        $this->integrasi_api_model->getUpdatedDiklatTeknis();
    }

    public function getSekolah(){
        $maxpage = $this->integrasi_api_model->getMasterPages("sekolah");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterSekolah($loop);
        }
        $this->integrasi_api_model->getUpdatedSekolah();
        return true;
    }

    public function getTingPend(){
        $maxpage = $this->integrasi_api_model->getMasterPages("tingpend");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterTingPend($loop);
        }
        $this->integrasi_api_model->getUpdatedTingPend();
        return true;
    }

    public function getFakultas(){
        $maxpage = $this->integrasi_api_model->getMasterPages("fakultas");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterFakultas($loop);
        }
        $this->integrasi_api_model->getUpdatedFakultas();
        return true;
    }

    public function getJurusan(){
        $maxpage = $this->integrasi_api_model->getMasterPages("jurusan");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterJurusan($loop);
        }
        $this->integrasi_api_model->getUpdatedJurusan();
        return true;
    }

    public function getUniversitas(){
        $maxpage = $this->integrasi_api_model->getMasterPages("universitas");
        for($loop = 1; $loop<=$maxpage; $loop++ ){
            $this->integrasi_api_model->getMasterUniversitas($loop);
        }
        $this->integrasi_api_model->getUpdatedUniversitas();
        return true;
    }
}