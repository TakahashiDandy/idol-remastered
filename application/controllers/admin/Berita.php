<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('session','pagination','ion_auth'));
    }

    public function index()
    {
        $this->load->view('adminpanel/berita/list_berita');
    }

    public function databerita(){
        $this->load->view('adminpanel/berita/data_berita');
    }

    public function kategori(){
        $this->load->view('adminpanel/berita/data_berita');
    }
}
