<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","download"));
        $this->load->model(array("report_model","registrasi_model"));
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $settings = new PhpOffice\PhpWord\Settings;
        $settings::setTempDir("uploads/reports/");
    }

    public function viewReportRegistrant()
{
    if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
        $data["pagename"] = "Report Registrasi";
        $data["tablename"] = "View Report Registrasi Diklat";
        $data["field"] = $this->registrasi_model->listfield("registrasi_master_reg_online");

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url("admin/report/viewReportRegistrant/");
        $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
        $config["per_page"] = 10;
        $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->report_model->reportregistrasi($config["per_page"], $page,0);

        $this->load->view("adminpanel/report/report_registrasi_diklat", $data);
    } else {
        redirect("admin/auth/login");
    }
}

    public function viewReportPenerimaanPeserta()
    {
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            $data["pagename"] = "Report Registrasi";
            $data["tablename"] = "View Report Penerimaan Peserta Diklat";
            $data["field"] = $this->registrasi_model->listfield("registrasi_master_reg_online");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/report/viewReportRegistrant/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->report_model->reportregistrasi($config["per_page"], $page,0);

            $this->load->view("adminpanel/report/report_penerimaan_diklat", $data);
        } else {
            redirect("admin/auth/login");
        }
    }

    public function viewReportAlumni()
    {
        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
            $data["pagename"] = "Report Alumni";
            $data["tablename"] = "View Report Alumni Peserta Diklat";
            $data["field"] = $this->registrasi_model->listfield("registrasi_master_reg_online");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/report/viewReportRegistrant/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->report_model->reportregistrasi($config["per_page"], $page,2);

            $this->load->view("adminpanel/report/report_lulus_diklat", $data);
        } else {
            redirect("admin/auth/login");
        }
    }

    public function getReportRegistrant($jadwal_id){

        $filename = glob("uploads/reports/*.docx");
        foreach ($filename as $data){
            unlink($data);
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/static-files/template1-registrasi.docx');
        $headerContent = $this->report_model->generateHeaderReport($jadwal_id);

        foreach ($headerContent->result() as $item){
            $templateProcessor->setValue('date', date("d M Y"));
            $templateProcessor->setValue('diklat_no_usul', $item->diklat_usul_no);
            $templateProcessor->setValue('diklat_penyelenggara', $item->diklat_penyelenggara);
            $templateProcessor->setValue('diklat_nama', $item->diklat_nama);
            $templateProcessor->setValue('diklat_tempat', $item->diklat_tempat);
            $templateProcessor->setValue('diklat_mulai', $item->diklat_mulai);
            $templateProcessor->setValue('diklat_selesai', $item->diklat_selesai);
            $templateProcessor->setValue('diklat_jumlah_jam', $item->diklat_jumlah_jam);
            $templateProcessor->setValue('diklat_jumlah_peserta', $item->diklat_jumlah_peserta);
            $templateProcessor->setValue('jumlah_peserta', $item->jumlah_pendaftar);
        }


        $repContent = $this->report_model->generateContentReport($jadwal_id);

        $templateProcessor->cloneRow('nip', $repContent->num_rows());
        foreach($repContent->result() as $key=>$item){
            $pos = $key+1;
            $templateProcessor->setValue("nip#". $pos,$item->nip);
            $templateProcessor->setValue("nama_lengkap#".$pos,$item->nama_lengkap);
        }

        $filename = "bkpp-report-registrasi-".md5(date('d-m-Y')).".docx";
        $templateProcessor->saveAs('uploads/reports/'. $filename);

        force_download("uploads/reports/".$filename,NULL,NULL);
    }

    public function getReportAccepted($jadwal_id){

        $filename = glob("uploads/reports/*.docx");
        foreach ($filename as $data){
            unlink($data);
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/static-files/template2-penerimaan.docx');
        $headerContent = $this->report_model->generateHeaderReport($jadwal_id);

        foreach ($headerContent->result() as $item){
            $templateProcessor->setValue('date', date("d M Y"));
            $templateProcessor->setValue('diklat_no_usul', $item->diklat_usul_no);
            $templateProcessor->setValue('diklat_penyelenggara', $item->diklat_penyelenggara);
            $templateProcessor->setValue('diklat_nama', $item->diklat_nama);
            $templateProcessor->setValue('diklat_tempat', $item->diklat_tempat);
            $templateProcessor->setValue('diklat_mulai', $item->diklat_mulai);
            $templateProcessor->setValue('diklat_selesai', $item->diklat_selesai);
            $templateProcessor->setValue('diklat_jumlah_jam', $item->diklat_jumlah_jam);
            $templateProcessor->setValue('diklat_jumlah_peserta', $item->diklat_jumlah_peserta);
            $templateProcessor->setValue('jumlah_peserta', $item->jumlah_pendaftar);
        }


        $accContent = $this->report_model->generateAcceptedContentReport($jadwal_id);
        $rejContent = $this->report_model->generateRejectedContentReport($jadwal_id);

        $templateProcessor->cloneRow('terima-nip', $accContent->num_rows());
        $templateProcessor->cloneRow('tolak-nip', $rejContent->num_rows());

        foreach($accContent->result() as $key=>$item){
            $pos = $key+1;
            $templateProcessor->setValue("terima-nip#". $pos,$item->nip);
            $templateProcessor->setValue("terima-nama_lengkap#".$pos,$item->nama_lengkap);
        }

        foreach($rejContent->result() as $key=>$item){
            $pos = $key+1;
            $templateProcessor->setValue("tolak-nip#". $pos,$item->nip);
            $templateProcessor->setValue("tolak-nama_lengkap#".$pos,$item->nama_lengkap);
        }

        $filename = "bkpp-report-penerimaan-".md5(date('d-m-Y')).".docx";
        $templateProcessor->saveAs('uploads/reports/'. $filename);

        force_download("uploads/reports/".$filename,NULL,NULL);
    }

    public function getReportPassed($jadwal_id){

        $filename = glob("uploads/reports/*.docx");
        foreach ($filename as $data){
            unlink($data);
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/static-files/template3-alumni.docx');
        $headerContent = $this->report_model->generateHeaderReport($jadwal_id);

        foreach ($headerContent->result() as $item){
            $templateProcessor->setValue('date', date("d M Y"));
            $templateProcessor->setValue('diklat_no_usul', $item->diklat_usul_no);
            $templateProcessor->setValue('diklat_penyelenggara', $item->diklat_penyelenggara);
            $templateProcessor->setValue('diklat_nama', $item->diklat_nama);
            $templateProcessor->setValue('diklat_tempat', $item->diklat_tempat);
            $templateProcessor->setValue('diklat_mulai', $item->diklat_mulai);
            $templateProcessor->setValue('diklat_selesai', $item->diklat_selesai);
            $templateProcessor->setValue('diklat_jumlah_jam', $item->diklat_jumlah_jam);
            $templateProcessor->setValue('diklat_jumlah_peserta', $item->diklat_jumlah_peserta);
            $templateProcessor->setValue('jumlah_peserta', $item->jumlah_pendaftar);
        }


        $accContent = $this->report_model->generatePassedContentReport($jadwal_id);

        $templateProcessor->cloneRow('lulus-nip', $accContent->num_rows());

        foreach($accContent->result() as $key=>$item){
            $pos = $key+1;
            $templateProcessor->setValue("lulus-no_sttp#". $pos,$item->diklat_sttp_no);
            $templateProcessor->setValue("lulus-nip#". $pos,$item->nip);
            $templateProcessor->setValue("lulus-nama_lengkap#".$pos,$item->nama_lengkap);
        }

        $filename = "bkpp-report-lulus-".md5(date('d-m-Y')).".docx";
        $templateProcessor->saveAs('uploads/reports/'. $filename);

        force_download("uploads/reports/".$filename,NULL,NULL);
    }


}