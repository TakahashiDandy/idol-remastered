<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('session','ion_auth'));
        $this->load->model(array('izinbelajar_model','tugasbelajar_model'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Beranda";
            $data["data_ib"] = $this->izinbelajar_model->notification();
            $data["data_tb"] = $this->tugasbelajar_model->notification();
            $this->session->set_userdata("notif_ib",$data["data_ib"]);
            $this->session->set_userdata("notif_tb",$data["data_tb"]);
            $this->load->view('adminpanel/home', $data);
        } else if($this->ion_auth->logged_in() && $this->ion_auth->in_group(array(2,3))){
            if($this->ion_auth->in_group(2)){
                $data["data_ib"] = $this->izinbelajar_model->notification(null,$this->session->userdata("username"));
                $data["data_tb"] = $this->tugasbelajar_model->notification(null,$this->session->userdata("username"));
            } else{
                $data["data_ib"] = $this->izinbelajar_model->notification($this->session->userdata("username"));
                $data["data_tb"] = $this->tugasbelajar_model->notification($this->session->userdata("username"));
            }
            $data["pagename"] = "Beranda";
            $this->session->set_userdata("notif_ib",$data["data_ib"]);
            $this->session->set_userdata("notif_tb",$data["data_tb"]);
            $this->load->view('adminpanel/home', $data);
        }else{
            redirect("admin/auth/login");
        }
    }
}
