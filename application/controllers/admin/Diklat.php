<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Diklat extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->model(array("diklat_model","event_model"));
        $this->load->library(array('session','pagination','ion_auth'));
    }

    public function detail_diklat(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Beranda";
            $this->load->view("frontpanel/diklat/detail_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function diklat_struktural(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Diklat Struktural";
            $data["tablename"] = "List Diklat Struktural";
            $data["field"] = $this->diklat_model->listfield("diklat_master_struktural_kategori");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_struktural";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_struktural_kategori");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->liststruk($config["per_page"],$page);

            $this->load->view("adminpanel/diklat/list_diklat_struktural",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function diklat_fungsional(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Diklat Fungsional";
            $data["tablename"] = "List Diklat Fungsional";
            $data["field"] = $this->diklat_model->listfield("diklat_master_diklat_fungsional");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_fungsional";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_diklat_fungsional");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->listfunc($config["per_page"],$page);


            $this->load->view("adminpanel/diklat/list_diklat_fungsional",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function diklat_teknis(){
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Diklat Teknis";
            $data["tablename"] = "List Diklat Teknis";
            $data["field"] = $this->diklat_model->listfield("diklat_master_diklat_teknis");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_teknis";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_diklat_teknis");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->listtek($config["per_page"],$page);

            $this->load->view("adminpanel/diklat/list_diklat_teknis",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function searchdiklatbyname($id,$name){
        $html ="";
        switch ($id){
            case 1:
                if($this->event_model->searchdiklatbyname(1,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" class='align-self-center' colspan='3'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else{
                    foreach($this->event_model->searchdiklatbyname(1,$name)->result() as $rows){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->kategori_id</td>";
                        $html .= "<td scope=\"col\">$rows->kategori_nama</td>";
                        $html .= "<td scope=\"col\">$rows->kategori_parent</td>";
                        $html .= "</tr>";
                    };
                }
                break;
            case 2:
                if($this->event_model->searchdiklatbyname(2,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" class='align-self-center' colspan='2'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else {
                    foreach ($this->event_model->searchdiklatbyname(2, $name)->result() as $rows) {
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->diklat_fungsional_id</td>";
                        $html .= "<td scope=\"col\">$rows->diklat_fungsional_nm</td>";
                        $html .= "</tr>";
                    };
                }
                break;
            case 3:
                if($this->event_model->searchdiklatbyname(3,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" colspan='2'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else {
                    foreach ($this->event_model->searchdiklatbyname(3, $name)->result() as $rows) {
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->diklat_teknis_id</td>";
                        $html .= "<td scope=\"col\">$rows->diklat_teknis_nm</td>";
                        $html .= "</tr>";
                    };
                }
                break;
        }
        echo $html;
    }

    public function modalsearchdiklatbyname($id,$name){
        $html ="";
        switch ($id){
            case 1:
                if($name == "all"){
                    foreach($this->diklat_model->liststruk(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->kategori_id)'>";
                        $html .= "<td id='diklat_id_$rows->kategori_id' scope=\"col\">$rows->kategori_id</td>";
                        $html .= "<td id='diklat_nama_$rows->kategori_id' scope='col'>$rows->kategori_nama</td>";
                        $html .= "</tr>";
                    };
                } else{
                    if($this->event_model->searchdiklatbyname(1,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" class='align-self-center' colspan='3'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    }
                    else{
                        foreach($this->event_model->searchdiklatbyname(1,$name)->result() as $rows){
                            $html .= "<tr onclick='modal_diklat_data($rows->kategori_id)'>";
                            $html .= "<td id='diklat_id_$rows->kategori_id' scope=\"col\">$rows->kategori_id</td>";
                            $html .= "<td id='diklat_nama_$rows->kategori_id' scope='col'>$rows->kategori_nama</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
            case 2:
                if($name == "all"){
                    foreach($this->diklat_model->listfunc(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->diklat_fungsional_id)'>";
                        $html .= "<td id='diklat_id_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_id</td>";
                        $html .= "<td id='diklat_nama_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_nm</td>";
                        $html .= "</tr>";
                    };
                } else {
                    if($this->event_model->searchdiklatbyname(2,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" class='align-self-center' colspan='2'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    } else{
                        foreach ($this->event_model->searchdiklatbyname(2, $name)->result() as $rows) {
                            $html .= "<tr onclick='modal_diklat_data($rows->diklat_fungsional_id)'>";
                            $html .= "<td id='diklat_id_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_id</td>";
                            $html .= "<td id='diklat_nama_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_nm</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
            case 3:
                if($name == "all"){
                    foreach($this->diklat_model->listtek(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->diklat_teknis_id)'>";
                        $html .= "<td id='diklat_id_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_id</td>";
                        $html .= "<td id='diklat_nama_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_nm</td>";
                        $html .= "</tr>";
                    };
                } else {
                    if($this->event_model->searchdiklatbyname(3,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" colspan='2'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    }
                    else {
                        foreach ($this->event_model->searchdiklatbyname(3, $name)->result() as $rows) {
                            $html .= "<tr onclick='modal_diklat_data($rows->diklat_teknis_id)'>";
                            $html .= "<td id='diklat_id_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_id</td>";
                            $html .= "<td id='diklat_nama_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_nm</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
        }
        echo $html;
    }
}
