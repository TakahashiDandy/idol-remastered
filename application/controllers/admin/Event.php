<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form"));
        $this->load->model("event_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()){
            $data["pagename"] = "Jadwal Diklat";
            $data["tablename"] = "List Jadwal Diklat";
            $data["field"] = $this->event_model->listfield("diklat_trans_jadwal");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("admin/jadwal/index/");
            $config["total_rows"] = $this->event_model->gettotalrows("diklat_trans_jadwal");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->event_model->listevent($config["per_page"],$page);

            $this->load->view("adminpanel/event/list_event_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

}