<?php


class Belajar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form"));
        $this->load->model("belajar_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        $data["pagename"] = "Pengajuan Surat Rekomendasi";
        $data["tablename"] = "List Pengajuan Surat Rekomendasi";
        $data["pagination"] = "abcdef";
        $data["field"] = $this->belajar_model->listfield("belajar_trans_rekomendasi");

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url("admin/registrasi/index/");
        $config["total_rows"] = $this->belajar_model->gettotalrows("belajar_trans_rekomendasi");
        $config["per_page"] = 10;
        $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->belajar_model->listrekomendasi($config["per_page"], $page);

        $this->load->view("adminpanel/belajar/list_registrasi_diklat", $data);
    }

    public function detailrekomendasi($id){
        $data["pagename"] = "Cek Permohonan Surat Rekomendasi";
        $data["tablename"] = "Detail Permohonan Surat Rekomendasi";
        $data["detail"] = "null";
        $this->load->view("adminpanel/belajar/input_rekomendasi_belajar", $data);
    }
}