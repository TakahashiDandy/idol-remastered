<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailEng extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->library(array('session','ion_auth','email'));
    }

    public function index()
    {
        $emailconf = array(
            "image" => base_url("/assets/courseware/img/") . "logo-idol.png",
            "header_message" => "Reset Password",
            "content" => "Anda membuat request penggantian password. Klik Link di bawah (Link Aktif 15 menit)",
            "attachment" => base_url("")
        );
        $this->load->view("email/view-email",$emailconf,true);
    }

    public function sendmail(){
        $emailconf = array(
            "image" => base_url("/assets/courseware/img/") . "logo-idol.png",
            "header_message" => "Reset Password",
            "content" => "Anda membuat request penggantian password. Klik Link di bawah (Link Aktif 15 menit)",
            "attachment" => base_url("")
        );

        $this->email->to("takittydan88@yahoo.com");
        $this->email->from('your@example.com');
        $this->email->subject('IDOL Reset Password');
        $this->email->message($this->load->view("email/view-email",$emailconf,true));
        $this->email->set_newline("\r\n");
        if($this->email->send()){
            echo TRUE;
        } else{
            echo FALSE;
        }
        exit;
    }
}