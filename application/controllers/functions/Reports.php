<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->model("Izinbelajar_model","izinbelajar");
        $this->load->model("Report_model","report_model");
        $this->load->library(array('session','ion_auth'));
        if(!$this->ion_auth->logged_in()){
            redirect("home");
        }
    }

    // report surat rekomendasi tugas belajar dari kepala opd atau bkpp
    public function rekomopd($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,0);
        foreach ($data["data_rows"] as $rekom){
            if($rekom->jenis_pengiriman == 0){
                $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
                $data["nama_skpd"] = $skpd->nama_dinas;
                $data["kepala_dinas"] = $skpd->kepala_dinas;
                $data["nip_kepala"] = $skpd->nip_kepala;
                $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            } else{
                $skpd = $this->report_model->searchKepalaBKPP();
                $data["nama_skpd"] = $skpd->nama_dinas;
                $data["kepala_dinas"] = $skpd->kepala_dinas;
                $data["nip_kepala"] = $skpd->nip_kepala;
                $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            }
        }
        $this->load->view("reports/lampiran-surat-rekomendasi.php",$data);
    }

    // report surat rekomendasi dari opd izin belajar
    public function surekomopd($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,0);
        $this->load->view("reports/lampiran-form-surat-rekomendasi-izin-belajar.php",$data);
    }

    // report surat keterangan dari opd izin belajar
    public function suketopd($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,1);
        $this->load->view("reports/lampiran-form-surat-keterangan.php",$data);
    }

    // report surat analisa kebutuhan kepegawaian
    public function suabk($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
            $data["jenis_belajar"] = $rekom->jenis_belajar;
        }
        if($data["jenis_belajar"] == 0){
            $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,2);
        } else {
            $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,1);
        }
        $this->load->view("reports/lampiran-form-analisa-kebutuhan-pegawai.php",$data);
    }

    // report surat permohonan izin belajar
    public function sumoizin($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,3);
        $this->load->view("reports/lampiran-form-surat-permohonan-izin-belajar.php",$data);
    }

    // report surat permohonan tugas belajar
    public function sumotugas($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,0);
        $this->load->view("reports/lampiran-form-surat-permohonan-tugas-belajar2.php",$data);
    }

    // report surat permohonan tugas belajar pribadi
    public function sumotugasprib($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $this->load->view("reports/lampiran-form-surat-permohonan-tugas-belajar.php",$data);
    }

    // report surat pernyataan izin belajar
    public function sunyaizin($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        foreach ($data["data_rows"] as $rekom){
            $skpd = $this->report_model->searchKepalaOPD($rekom->instansi);
            $data["nama_skpd"] = $skpd->nama_dinas;
            $data["kepala_dinas"] = $skpd->kepala_dinas;
            $data["nip_kepala"] = $skpd->nip_kepala;
            $data["jabatan_kepala"] = $skpd->jabatan_kepala;
            $data["golongan_kepala"] = $skpd->golongan_kepala;
            $data["pangkat_kepala"] = $skpd->pangkat_kepala;
        }
        $data["data_surat"] = $this->report_model->searchSuratByID($belajar_reg_id,4);
        $this->load->view("reports/lampiran-form-surat-pernyataan-izin-belajar.php",$data);
    }

    // report surat pernyataan tugas belajar
    public function sunyatugas($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        $this->load->view("reports/lampiran-form-surat-permohonan-tugas-belajar.php",$data);
    }

    // report surat penerbitan izin belajar
    public function suterizin($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        $data["data_surat_opd"] = $this->report_model->searchSuratByID($belajar_reg_id,0);
        $skpd = $this->report_model->searchKepalaSetda();
        $data["kepala_dinas"] = $skpd->kepala_dinas;
        $data["nama_skpd"] = $skpd->nama_dinas;
        $data["nip_kepala"] = $skpd->nip_kepala;
        $data["data_surat_kepwal"] = $this->report_model->searchSuratByID($belajar_reg_id,5);
        $data["data_surat_mahasiswa"] = $this->report_model->searchSuratByID($belajar_reg_id,4);
        $this->load->view("reports/surat-penerbitan-izin-belajar.php",$data);
    }

    // report surat penerbitan tugas belajar
    public function suterbelajar($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        $skpd = $this->report_model->searchKepalaSetda();
        $data["kepala_dinas"] = $skpd->kepala_dinas;
        $data["nama_skpd"] = $skpd->nama_dinas;
        $data["nip_kepala"] = $skpd->nip_kepala;
        $data["data_surat_kepwal"] = $this->report_model->searchSuratByID($belajar_reg_id,0);
        $data["data_surat_mahasiswa"] = $this->report_model->searchSuratByID($belajar_reg_id,1);
        $this->load->view("reports/surat-kepwal-tugas-belajar.php",$data);
    }

    // report surat pernyataan ganti rugi
    public function sunyatigi($belajar_reg_id){
        $data["data_rows"] = $this->report_model->report_rekomendasi($belajar_reg_id,1);
        $skpd = $this->report_model->searchKepalaSetda();
        $data["kepala_dinas"] = $skpd->kepala_dinas;
        $data["nama_skpd"] = $skpd->nama_dinas;
        $data["nip_kepala"] = $skpd->nip_kepala;
        $data["data_surat_opd"] = $this->report_model->searchSuratByID($belajar_reg_id,0);
        $data["data_surat_kepwal"] = $this->report_model->searchSuratByID($belajar_reg_id,6);
        $data["data_surat_mahasiswa"] = $this->report_model->searchSuratByID($belajar_reg_id,5);
        $this->load->view("reports/lampiran-form-surat-ganti-rugi-tugas-belajar.php",$data);
    }
}