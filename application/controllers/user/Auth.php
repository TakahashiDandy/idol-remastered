<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form','captcha'));
        $this->load->model(array("trans_detail_user_model","master_user_model","integrasi_api_model","registrasi_model"));
        $this->load->library(array('session','ion_auth','encryption','form_validation','email_verifier','phpmailers'));
        $this->config->load('email');
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            redirect("user/home");
        } else{
            $this->load->view('userpanel/login');
        }
    }

    public function login(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data_user = $this->master_user_model->get_detail_user_by_username($this->session->userdata("detailuser")->username);
            $this->session->set_userdata("datauser",$data_user);
            redirect("user/home");
        } else{
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');

            if($this->form_validation->run()){
                $username = $this->input->post("username");
                $password = $this->input->post("password");
                $remember = ($this->input->post("remember") == "on" ? TRUE: FALSE);

                $output = $this->ion_auth->login($username,$password,$remember);
                if($output == TRUE && $this->ion_auth->in_group(2)){
                    $nip = $this->master_user_model->get_detail_user_by_username($username)->nip;
                    $this->sinkronisasi_data_pegawai($nip);
                    $data_user = $this->master_user_model->get_detail_user_by_username($username);
                    $this->session->set_userdata("datauser",$data_user);
                    redirect("user/home");
                }else if($output == TRUE && !$this->ion_auth->in_group(2)){
                    $this->session->set_flashdata("error_message","Cannot Login using provided Credential.");
                    redirect("user/auth","refresh");
                } else if($output == FALSE && $this->ion_auth->is_max_login_attempts_exceeded($username)){
                    $this->session->set_flashdata("error_message","Cannot Login max Login Attempts Exceeded.");
                    redirect("user/auth","refresh");
                }else if($output == FALSE){
                    $this->session->set_flashdata("error_message","Your Credential are not found.");
                    redirect("user/auth","refresh");
                }

            } else{
                $this->session->set_flashdata("error_message",$this->form_validation->error_string());
                redirect("user/auth","refresh");
            }
        }
    }

    public function logout(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->ion_auth->logout();
            redirect("home");
        } else{
            redirect("user/auth/login");
        }
    }

    public function resetpassword(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->ion_auth->logout();
            redirect("home");
        } else{
            if(!is_null($this->input->post("username"))){
                $username = $this->input->post("username");
                $output = $this->ion_auth->username_check($username);
                if($output == TRUE){
                    $forgotten = $this->ion_auth->forgotten_password($username);
                    if($forgotten){
                        $this->sendResetEmail($forgotten);
                        $this->session->set_flashdata("success_message","Please check your Email to reset the password.");
                        redirect("user/auth/resetpassword","refresh");
                    } else{
                        $this->session->set_flashdata("error_message","Cannot send reset password Email.");
                        redirect("user/auth/resetpassword","refresh");
                    }
                } else {
                    $this->session->set_flashdata("error_message","Your Email are not registered.");
                    redirect("user/auth/resetpassword","refresh");
                }
            } else{
                $this->load->view('userpanel/resetpassword');
            }
        }
    }

    public function verify($token){
    if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
        $this->ion_auth->logout();
        redirect("home");
    } else{
        if(!is_null($token)){
            $output = $this->ion_auth->forgotten_password_check($token);
            if($output == true){
                $data["ids"] = $this->encryption->encrypt($output->id);
                $this->load->view("userpanel/changepassword",$data);
            } else {
                $this->session->set_flashdata("error_message","Token Unavailable.");
                redirect("user/auth/login","refresh");
            }
        } else{
            $this->session->set_flashdata("error_message","Token Unavailable.");
            redirect("user/auth/resetpassword","refresh");
        }
    }
}

    public function activate($token){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->ion_auth->logout();
            redirect("home");
        } else{
            if(!is_null($token)){
                $returnbase = base64_decode($token);
                $decrypt = $this->encryption->decrypt($returnbase);
                $userdetail = json_decode($decrypt);
                if($this->trans_detail_user_model->isActivate($userdetail->nip, $userdetail->id) < 1){
                    $ionauthdata = array(
                        "activation_code" => NULL,
                        "activation_selector" => NULL,
                        "active"=>1
                    );
                    $succ1 = $this->ion_auth->update($userdetail->id,$ionauthdata);
                    if($succ1 == TRUE){
                        $this->session->set_flashdata("success_message","Registered Successfuly.");
                        redirect("user/auth/","refresh");
                    } else{
                        $this->session->set_flashdata("error_message","Cannot Registered");
                        redirect("user/auth/","refresh");
                    }
                } else{
                    $this->session->set_flashdata("error_message","Token tak berlaku. Silahkan Login");
                    redirect("user/auth/","refresh");
                }
            } else{
                $this->session->set_flashdata("error_message","Token Unavailable.");
                redirect("user/auth/","refresh");
            }
        }
    }

    public function changepassword(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->ion_auth->logout();
            redirect("home");
        } else{
            if(!is_null($this->input->post("password") && !is_null($this->input->post("check"))
                && is_null($this->input->post("token")))){
                $issame = ($this->input->post("password") == $this->input->post("check") ? TRUE : FALSE);
                if($issame){
                    $id = $this->encryption->decrypt($this->input->post("token"));
                    $data = array(
                          'password' => $this->input->post("password"),
                    );
                    $output = $this->ion_auth->update($id, $data);

                    if($output == TRUE){
                        $this->session->set_flashdata("success_message","Password Changed Successfully, 
                        Please login with your new credential");
                        redirect("user/auth/login","refresh");
                    }
                } else{
                    $this->session->set_flashdata("error_message","Password do not match");
                    redirect("user/auth/changepassword","refresh");
                }
            } else{
                $this->session->set_flashdata("error_message","Password do not match");
                redirect("user/auth/changepassword","refresh");
            }
        }
    }

    private function sendResetEmail($data){
        $emailconf = array(
            "image" => base_url("/assets/courseware/img/") . "logo-idol.png",
            "header_message" => "Reset Password",
            "content" => "Hai, ".$data["identity"].". Anda membuat request penggantian password. Klik Link di bawah (Link Aktif 15 menit)",
            "attachment" => base_url("user/auth/verify/") . $data["forgotten_password_code"]
        );
        $this->email->to("takittydan88@yahoo.com");
        $this->email->from($this->config->item("default_email_from"),$this->config->item("default_name_from"));
        $this->email->subject('IDOL Reset Password');
        $this->email->message($this->load->view("email/email-resetpassword",$emailconf,true));
        $this->email->set_newline("\r\n");
        if($this->email->send()){
            return TRUE;
        } else{
            show_error($this->email->print_debugger());
        }
    }

    private function sendActiveEmail($data){
        $encrypted = base64_encode($this->encryption->encrypt(json_encode($data)));
        $emailconf = array(
            "image" => base_url("/assets/courseware/img/") . "logo-idol.png",
            "header_message" => "Activate Account",
            "content" => "Hai, ".$data["identity"].". Anda mendaftarkan diri untuk user akun IDOL. Klik Link di bawah (Link Aktif 15 menit)",
            "attachment" => base_url("user/auth/activate/") . $encrypted
        );

        $this->phpmailers->sendEmail($data["email"],
            null,
            'IDOL Activation Account',
            $this->load->view("email/email-activateuser",$emailconf,true)
        );
    }

    public function registeraccount(){
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[user_master_users.username]');
        $this->form_validation->set_rules('firstname', 'Firstname', 'required');
        $this->form_validation->set_rules('lastname', 'Lastname', 'required');
        $this->form_validation->set_rules('nip', 'NIP', 'required|is_unique[user_trans_detail_user.nip]|min_length[18]|max_length[18]');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user_master_users.email]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');
        $this->form_validation->set_rules('passwordcheck', 'Verify Password', 'required|matches[password]|alpha_numeric');

        if($this->form_validation->run() == FALSE){
            $this->load->view('userpanel/signup');
        }else{
            $username = $this->input->post("username");
            $firstname = $this->input->post("firstname");
            $lastname = $this->input->post("lastname");
            $nip = $this->input->post("nip");
            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $additional_data = array(
                "first_name" => $firstname,
                "last_name" => $lastname,
                "active"=>0
            );

//            $this->email_verifier->setStreamTimeoutWait(5);
//            $this->email_verifier->setEmailFrom("admin-idol@bandung.go.id");
//
//            if($this->email_verifier->check($email)){
                $group = array('2'); // Sets user to user.
                $data = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
                if(is_array($data)){
                    $data["nip"] = $nip;
                    $userdata = array(
                        "id_user" => $data["id"],
                        "nip"=> $data["nip"]
                    );
                } else {
                    $userdata = array(
                        "id_user" => $data,
                        "nip"=> $nip
                    );
                }
                $this->trans_detail_user_model->save($userdata);
                $this->session->set_flashdata("success_message","Pendaftaran Selesai, Silahkan Cek Email anda untuk Aktifasi");
                redirect("user/auth","refresh");
//            } else{
//                $data["message"]= "email diketahui tidak aktif. pastikan email anda aktif";
//                $this->load->view('userpanel/signup');
//            }
        }
    }

    private function sinkronisasi_data_pegawai($nip_pegawai){
        $result = json_decode($this->integrasi_api_model->get_detail_by_nip($nip_pegawai));
        if($result !== null){
            $response = $result->response;
            if($response->status == 1) {
                $data = $response->data->data;
                if(!empty($data)){
                    $detail_user = array(
                        "nama_lengkap" => $data->nama,
                        "golongan" => $data->golongan_akhir,
                        "tempat_lahir" => $data->tempat_lahir,
                        "tanggal_lahir" => $data->tanggal_lahir,
                        "jenis_kelamin" => $data->jenis_kelamin,
                        "nama_pangkat" => $data->nama_pangkat,
                        "jabatan" => $data->jabatan_nama,
                        "no_kontak" => $data->no_hp,
                        "email" => $data->email == "" ? "-@gmail.com" : $data->email,
                        "instansi" => $data->instansi,
                        "pendidikan_terakhir" => $data->pendidikan_akhir,
                        "kotakab" => "Bandung",
                        "provinsi" => "Jawa Barat",
                        "telpfax" => $data->no_hp,
                        "alamat_rumah" => $data->alamat,
                        "alamat_kantor" => $data->alamat,
                        "foto" =>$data->foto
                    );

                    if(!empty($data->nama)):
                        $this->trans_detail_user_model->update(array("nip" => $nip_pegawai), $detail_user);
                    endif;

                    return true;
                } else{
                    return false;
                }
            }
        }
    }
}