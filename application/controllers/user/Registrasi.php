<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->model("event_model");
        $this->load->model("registrasi_model");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Registrasi";
            $data["tablename"] = "List Registrasi Diklat";
            $data["field"] = $this->registrasi_model->listfield("registrasi_master_reg_online");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/registrasi/index/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->registrasi_model->listregistrasi($config["per_page"],$page);

            $this->load->view("userpanel/registrasi/list_registrasi_diklat",$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function listregistrasi($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Registrasi";
            $data["tablename"] = "Input Registrasi Diklat";
            $data["rows1"] = $this->registrasi_model->detailrejectedregistrasi($id);
            $data["rows2"] = $this->registrasi_model->detailacceptedregistrasi($id);
            $this->load->view("userpanel/registrasi/input_registrasi_diklat",$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function updateregistrasi(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            // $this->upload_files();
            redirect("user/registrasi/");
        } else{
            redirect("user/auth/login");
        }
    }

    public function alumni()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Alumni";
            $data["tablename"] = "List Alumni Diklat";

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/registrasi/alumni/");
            $config["total_rows"] = $this->registrasi_model->gettotalrows("registrasi_master_reg_online");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->registrasi_model->listalumni($config["per_page"],$page);

            $this->load->view("userpanel/registrasi/list_alumni_diklat",$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function listalumni($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->form_validation->set_rules('sttp_tgl', 'STTP Tgl', 'required');
            $this->form_validation->set_rules('sttp_pej', 'STTP Pejabat', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("sttp_tgl") && !$this->session->set_userdata("sttp_pej")){
                    $savepoint = array(
                        "sttp_tgl" => $this->input->post("sttp_tgl"),
                        "sttp_pej" => $this->input->post("sttp_pej"),
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Alumni";
                $data["tablename"] = "Input Alumni Diklat";
                $data["rows1"] = $this->registrasi_model->detailgagalalumni($id);
                $data["rows2"] = $this->registrasi_model->detaillulusalumni($id);
                $this->load->view("userpanel/registrasi/input_alumni_diklat",$data);
            } else{
                $this->session->unset_userdata(array("sttp_tgl","sttp_pej"));
                $this->updatealumni();
            }
        } else{
            redirect("user/auth/login");
        }
    }

    public function updatealumni(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $acc = $this->input->post("chkaccept");
            $rej = $this->input->post("chkreject");
            $sttp_pej = $this->input->post("sttp_pej");
            $sttp_tgl = $this->input->post("sttp_tgl");

            if(!is_null($acc)){
                $accdata = array();
                $rwdata = array();
                for($i=0;$i < count($acc);$i++){
                    $diklat_id = $acc[$i];

                    $databatch = array(
                        "id_registrasi" => $diklat_id,
                        "status_registrant" => 4,
                    );
                    array_push($accdata,$databatch);

                    $rwbatch = array(
                        "id_registrasi" => $diklat_id,
                        "diklat_sttp_no" => $this->input->post("asttpno-".$diklat_id),
                        "diklat_sttp_tgl" => $sttp_tgl,
                        "diklat_sttp_pej" => $sttp_pej,
                    );
                    array_push($rwdata,$rwbatch);

                }
                $this->registrasi_model->updateregistrasi($accdata);
                $this->registrasi_model->insertriwayat($rwdata);
            }
            if(!is_null($rej)){
                $rejdata = array();
                for($i=0;$i < count($rej);$i++){
                    $diklat_id = $rej[$i];
                    $databatch = array(
                        "id_registrasi" => $diklat_id,
                        "status_registrant" => 3,
                    );
                    array_push($rejdata,$databatch);
                }
                $this->registrasi_model->updateregistrasi($rejdata);
            }
            redirect("user/registrasi/alumni");
        } else{
            redirect("user/auth/login");
        }
    }

    public function tambah_user(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("diklat_jenis")){
                    $savepoint = array(
                        "diklat_jenis" => $this->input->post("diklat_jenis"),
                        "id_diklat" => $this->input->post("id_diklat"),
                        "diklat_nama" => $this->input->post("diklat_nama"),
                        "diklat_mulai" => $this->input->post("diklat_mulai"),
                        "diklat_selesai" => $this->input->post("diklat_selesai"),
                        "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                        "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                        "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                        "diklat_tempat" => $this->input->post("diklat_tempat"),
                        "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                        "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                        "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Jadwal Diklat";
                $data["tablename"] = "Tambah Jadwal Diklat";
                $this->load->view("adminpanel/jadwal/input_jadwal_diklat",$data);
            } else{
                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }
                $this->event_model->insertjadwal($savepoint);

                redirect("admin/jadwal");
            }
        } else{
            redirect("admin/auth/login");
        }
    }

    public function userregistrasi($id){        
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){

            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                //get data jadwal user diklat
                $diklat_jadwal_id = $this->uri->segment(4);
                $data["jadwal_diklat"] = $this->event_model->getonejadwal($diklat_jadwal_id,0)->result_array();

                //set data jadwal diklat session
                foreach ($this->event_model->getonejadwal($diklat_jadwal_id,0)->result_array() as $data){
                    $savepoint = array(
                        "diklat_jadwal_id" => $data["diklat_jadwal_id"],"diklat_nama" => $data["diklat_nama"],
                        "diklat_jenis" => $data["diklat_jenis"],"diklat_mulai" => $data["diklat_mulai"],
                        "diklat_selesai" => $data["diklat_selesai"],"diklat_deskripsi" => $data["diklat_deskripsi"],
                        "diklat_jumlah_jam" => $data["diklat_jumlah_jam"],"diklat_jumlah_peserta" => $data["diklat_jumlah_peserta"],
                        "diklat_tempat" => $data["diklat_tempat"],"diklat_usul_no" => $data["diklat_usul_no"],
                        "diklat_usul_tgl" => $data["diklat_usul_tgl"],"diklat_penyelenggara" => $data["diklat_penyelenggara"]
                    );
                    switch ($data["diklat_jenis"]){
                        case 1:
                            $savepoint["id_diklat"] = $data["kategori_id"];
                            break;
                        case 2:
                            $savepoint["id_diklat"] = $data["diklat_fungsional_id"];
                            break;
                        case 3:
                            $savepoint["id_diklat"] = $data["diklat_teknis_id"];
                            break;
                    }
                    $this->session->set_userdata($savepoint);
                }

                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();
                $data_detail = $this->detail_user->get_by_id($id_user);
                $data["detail_user"] = $data_detail;
                $data["pagename"] = "Registrasi User";
                
                $this->load->view("userpanel/registrasi/user_registrasi_diklat",$data);
            }else{
                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();

                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }
                $this->upload_files($id,$id_user);
                $this->event_model->updatejadwal($id,$savepoint);
                redirect("user/jadwal");
            }
        } else{
            redirect("user/auth/login");
        }
    }

    public function getlistjadwal(){
        $data["jadwal"]= $this->event_model->listjadwal(10,0);
        $view = $this->load->view("frontpanel/loader/selectdatajadwal",$data,true);
        print($view);
    }

    public function checkregistrant($id,$nip){
        $data = $this->registrasi_model->checkregistrant($id,$nip)->row();
        if($data->total > 0){
            echo $data->total;
        } else{
            echo "0";
        }
    }

    public function checkcaptcha(){
        $captcha = $this->input->get("captcha");
        if($captcha == $this->session->userdata("captchaword")){
            echo true;
        } else{
            echo false;
        }
        exit();
    }

    private function upload_files($id_jadwal,$id_user)
    {       
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['lampiran']['name']);
        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['lampiran']['name']= $files['lampiran']['name'][$i];
            $_FILES['lampiran']['type']= $files['lampiran']['type'][$i];
            $_FILES['lampiran']['tmp_name']= $files['lampiran']['tmp_name'][$i];
            $_FILES['lampiran']['error']= $files['lampiran']['error'][$i];
            $_FILES['lampiran']['size']= $files['lampiran']['size'][$i];    
            $custom_file_name = "LAMPIRAN-IDOL-".$id_user.'-'.$id_jadwal.'-'.md5(date("y-m-d h:i:s"));
            $extension = pathinfo($_FILES['lampiran']['name'], PATHINFO_EXTENSION);

            $this->upload->initialize($this->set_upload_options($custom_file_name));

            if ($this->upload->do_upload('lampiran')) {
                $success = array(
                    "file" => $this->upload->data("client_name"),
                    "uploaded" => $this->upload->data("file_name"),
                    "message" => "Success Uploaded"
                );
            } else {
                $success = array(
                    "file" =>$this->upload->data("client_name"),
                    "uploaded" => $this->upload->data("file_name"),
                    "message" => "Error :" . $this->upload->display_errors()
                );
            }
        }
    }

    private function set_upload_options($file_name)
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/lampiran/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '5128';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

    private function removesessionjadwal(){
        $session = array(
            "diklat_jenis",
            "id_diklat",
            "nama_diklat",
            "diklat_mulai",
            "diklat_selesai",
            "diklat_jumlah_jam",
            "diklat_jumlah_peserta",
            "diklat_tempat",
            "diklat_usul_no",
            "diklat_usul_tgl",
            "diklat_penyelenggara"
        );
        $this->session->unset_userdata($session);
    }

}
