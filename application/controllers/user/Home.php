<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("Tugasbelajar_model","tugasbelajar");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model("izinbelajar_model","izinbelajar");
        $this->load->library(array('session','pagination','ion_auth','form_validation','upload'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Beranda";
            $nip = $this->session->userdata("datauser")->nip;
            $data["count_of_izinbelajar"] = $this->izinbelajar->gettotalrowsbyNIP('registrasi_master_belajar',$nip);
            $data["count_of_tugasbelajar"] = $this->tugasbelajar->gettotalrowsbyNIP('registrasi_master_belajar', $nip);
            $this->load->view('userpanel/home', $data);
        } else{
            redirect("user/auth/login");
        }
    }
}
