<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Riwayat extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array("url","dateformat_helper"));
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model(array("diklat_model","event_model","registrasi_model"));
        $this->load->library(array('session','pagination','ion_auth'));
    }

    public function index_riwayat(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Riwayat Diklat";
            $data["tablename"] = "Riwayat Diklat";
            $data["field"] = $this->diklat_model->listfield("diklat_trans_riwayat_diklat");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/riwayat/index_riwayat/");
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_trans_riwayat_diklat");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);

            if(!empty($this->session->userdata("datauser")->nip)){
                $nip = $this->session->userdata("datauser")->nip;
            }

            $data["row"] = $this->diklat_model->listriwayatdiklat($config["per_page"],$page, $nip);
            $this->load->view("userpanel/diklat/index",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function index_pendidikan(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Riwayat Pendidikan";
            $data["tablename"] = "Riwayat Pendidikan";
            $data["field"] = $this->diklat_model->listfield("belajar_trans_riwayat_pendidikan");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/riwayat/index_pendidikan/");
            $config["total_rows"] = $this->diklat_model->gettotalrows("belajar_trans_riwayat_pendidikan");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);

            if(!empty($this->session->userdata("datauser")->nip)){
                $nip = $this->session->userdata("datauser")->nip;
            }

            $data["row"] = $this->diklat_model->listriwayatpendidikan($config["per_page"],$page, $nip);
            $this->load->view("userpanel/pendidikan/index",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function getRiwayatDiklat(){
        $client = new GuzzleHttp\Client(); $url = $AccessKey = '';
        
        $id_user = $this->ion_auth->get_user_id();
        $detail_user = $this->detail_user->get_by_id($id_user);
        $data_skpd = $this->detail_user->get_data_skpd($detail_user->instansi);

        $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/idolriwayat/kode_skpd='.$data_skpd->kode_skpd.'&nip='.$detail_user->nip.'&jenis_riwayat=diklat';
        $AccessKey = 'ksn685pkyz';
    
        //request the data
        $res = $client->request('GET', $url, [
            'headers' => [
                'User-Agent'    => 'MANTRA',
                'AccessKey'     => $AccessKey       
            ]
        ]);

        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            if(!empty($data->response->data->data)):

                //truncate cache
                $this->db->truncate("cache_trans_riwayat_diklat");

                //collecting data
                foreach ($data->response->data->data as $value) {
                    $insertdata = array(
                        "diklat_id"=>$value->diklat_id,
                        "kategori_id"=>$value->kategori_id,
                        "kategori_nama"=>$value->kategori_nama,
                        "diklat_fungsional_id" => $value->diklat_fungsional_id,
                        "diklat_fungsional_nm" => $value->diklat_fungsional_nm,
                        "diklat_teknis_id" => $value->diklat_teknis_id,
                        "diklat_teknis_nm" => $value->diklat_teknis_nm,
                        "diklat_mulai" => $value->diklat_mulai,
                        "diklat_selesai" => $value->diklat_selesai,
                        "diklat_penyelenggara" => $value->diklat_penyelenggara,
                        "diklat_tempat" => $value->diklat_tempat,
                        "diklat_jumlah_jam" => $value->diklat_jumlah_jam,
                        "diklat_usul_no" => $value->diklat_usul_no,
                        "diklat_usul_tgl" => $value->diklat_usul_tgl,
                        "diklat_nama" => $value->diklat_nama,
                        "diklat_sttp_no" => $value->diklat_sttp_no,
                        "diklat_sttp_tgl" => $value->diklat_sttp_tgl,
                        "diklat_sttp_pej" => $value->diklat_sttp_pej
                    );
                    $this->db->insert("cache_trans_riwayat_diklat",$insertdata);
                }
            endif;

            //Migrating begin
            $this->getUpdatedRiwayatDiklat();

            echo true;

        } else {
            echo false;
        }
    }

    public function getUpdatedRiwayatDiklat(){

        $diklat_jadwal_id = $registrasi_reg_online_id = $diklat_jenis = 0;

        //insert to diklat trans jadwal
        $dat = $this->db->query("SELECT b.* FROM diklat_trans_riwayat_diklat a RIGHT JOIN cache_trans_riwayat_diklat b
                ON a.diklat_sttp_no = b.diklat_sttp_no
                WHERE (a.diklat_sttp_no !=  b.diklat_sttp_no OR a.diklat_sttp_tgl != b.diklat_sttp_tgl OR a.diklat_sttp_pej != b.diklat_sttp_pej) 
                OR (a.diklat_sttp_no IS NULL AND a.diklat_sttp_tgl IS NULL AND a.diklat_sttp_pej IS NULL)");
        
        foreach ($dat->result() as $value) {
            if(!empty($value->kategori_id)){
                $diklat_jenis = 1;
            }else if(!empty($value->diklat_fungsional_id)){
                $diklat_jenis = 2;
            }else {
                $diklat_jenis = 3;
            }
            $dataDiklatTransJadwal = array(
                "kategori_id"=>$value->kategori_id,
                "diklat_teknis_id" => $value->diklat_teknis_id,
                "diklat_usul_no" => $value->diklat_usul_no,
                "diklat_usul_tgl" => $value->diklat_usul_tgl,
                "diklat_jenis" => $diklat_jenis,
                "diklat_nama" => $value->diklat_nama,
                "diklat_mulai" => $value->diklat_mulai,
                "diklat_selesai" => $value->diklat_selesai,
                "diklat_penyelenggara" => $value->diklat_penyelenggara,
                "diklat_tempat" => $value->diklat_tempat,
                "diklat_jumlah_jam" => $value->diklat_jumlah_jam,
                "status_event" => 2,
            );

            $this->db->where("diklat_usul_no",$value->diklat_usul_no);
            $query = $this->db->get("diklat_trans_jadwal");
            if ($query->num_rows() > 0){
                $this->db->update("diklat_trans_jadwal",$dataDiklatTransJadwal);
            }
            else{
                $this->db->insert("diklat_trans_jadwal",$dataDiklatTransJadwal);
                $diklat_jadwal_id = $this->db->insert_id();

                //insert to registrasi online
                $id_user = $this->ion_auth->get_user_id();
                $detail_user = $this->detail_user->get_by_id($id_user);
                $data_reg_online = array(
                    "nip" => $detail_user->nip,
                    "nama_lengkap" => $detail_user->nama_lengkap,
                    "jenis_kelamin" => $detail_user->jenis_kelamin,
                    "jabatan" => $detail_user->jabatan,
                    "golongan" => $detail_user->golongan,
                    "instansi" => $detail_user->instansi,
                    "pendidikan_terakhir" => $detail_user->pendidikan_terakhir,
                    "tempat_lahir" => $detail_user->tempat_lahir,
                    "tanggal_lahir" => $detail_user->tanggal_lahir,
                    "alamat_rumah" => $detail_user->alamat_rumah,
                    "alamat_kantor" => $detail_user->alamat_kantor,
                    "kabkota" => $detail_user->kotakab,
                    "provinsi" => $detail_user->provinsi,
                    "no_kontak" => $detail_user->no_kontak,
                    "no_tlp_kantor" => $detail_user->no_kontak,
                    "email" => $detail_user->email,
                    "diklat_jadwal_id" => $diklat_jadwal_id,
                    "keterangan" => 'Data hasil sinkronisasi dengan SIMPEG.',
                    "status_registrant" => 6,
                );

                $this->db->insert("registrasi_master_reg_online",$data_reg_online);
                $registrasi_reg_online_id = $this->db->insert_id();

                //insert to trans riwayat diklat
                $data = $this->db->query("SELECT 
                a.diklat_sttp_no as diklat_sttp_no, a.diklat_sttp_tgl as diklat_sttp_tgl, a.diklat_sttp_pej as diklat_sttp_pej,
                b.diklat_sttp_no as c_diklat_sttp_no, b.diklat_sttp_tgl as c_diklat_sttp_tgl, b.diklat_sttp_pej as c_diklat_sttp_pej
                FROM diklat_trans_riwayat_diklat a
                RIGHT JOIN cache_trans_riwayat_diklat b
                ON a.diklat_sttp_no = b.diklat_sttp_no
                WHERE (a.diklat_sttp_no !=  b.diklat_sttp_no OR a.diklat_sttp_tgl != b.diklat_sttp_tgl OR a.diklat_sttp_pej != b.diklat_sttp_pej) 
                OR (a.diklat_sttp_no IS NULL AND a.diklat_sttp_tgl IS NULL AND a.diklat_sttp_pej IS NULL)");

                //update kalau ada perbedaan dari data cache ke data trans riwayat diklat.
                foreach ($data->result() as $item){
                    $data_diklat_trans_riwayat_diklat = array(
                        "diklat_sttp_no" => $item->c_diklat_sttp_no,
                        "diklat_sttp_tgl" => $item->c_diklat_sttp_tgl,
                        "diklat_sttp_pej" => $item->c_diklat_sttp_pej,
                        "id_registrasi" => $registrasi_reg_online_id
                    );
                
                    $this->db->where("diklat_sttp_no",$item->c_diklat_sttp_no);
                    $query = $this->db->get("diklat_trans_riwayat_diklat");
                    if ($query->num_rows() > 0){
                        $this->db->update("diklat_trans_riwayat_diklat", $data_diklat_trans_riwayat_diklat);
                    }
                    else{
                        $this->db->insert("diklat_trans_riwayat_diklat", $data_diklat_trans_riwayat_diklat);
                    }
                }
            }
        }
        $this->db->query("DELETE FROM cache_trans_riwayat_diklat");
    }


    //========================

    public function getRiwayatPendidikan(){
        $client = new GuzzleHttp\Client(); $url = $AccessKey = '';
        
        $id_user = $this->ion_auth->get_user_id();
        $detail_user = $this->detail_user->get_by_id($id_user);
        $data_skpd = $this->detail_user->get_data_skpd($detail_user->instansi);

        $url = 'https://mantra.bandung.go.id/mantra/json/bkd/simpeg/idolriwayat/kode_skpd='.$data_skpd->kode_skpd.'&nip='.$detail_user->nip.'&jenis_riwayat=pendidikan';
        $AccessKey = 'ksn685pkyz';
    
        //request the data
        $res = $client->request('GET', $url, [
            'headers' => [
                'User-Agent'    => 'MANTRA',
                'AccessKey'     => $AccessKey       
            ]
        ]);

        if($res->getStatusCode() == 200){
            $data = json_decode($res->getBody());
            if(!empty($data->response->data->data)):

                //truncate cache
                $this->db->truncate("cache_trans_riwayat_pendidikan");
                
                //collecting data
                foreach ($data->response->data->data as $value) {
                    $insertdata = array(
                        "riw_pendidikan_sttb_ijazah" => $value->riw_pendidikan_sttb_ijazah,
                        "riw_pendidikan_tgl" => $value->riw_pendidikan_tgl,
                        "riw_pendidikan_pejabat" => $value->riw_pendidikan_pejabat,
                        "riw_pendidikan_nm" => $value->riw_pendidikan_nm,
                        "riw_pendidikan_lokasi" => $value->riw_pendidikan_lokasi,
                        "universitas" => $value->universitas,
                        "universitas_lokasi" => $value->universitas_lokasi,
                        "jurusan" => $value->jurusan,
                        "tingpend_id" => $value->tingpend_id,
                        "fakultas_id" => $value->fakultas_id,
                        "nm_tingpend" => $value->nm_tingpend,
                        "fakultas_nm" => $value->fakultas_nm
                    );
                    $this->db->insert("cache_trans_riwayat_pendidikan",$insertdata);
                }
            endif;

            //Migrating begin
            $this->getUpdatedRiwayatPendidikan();

            echo true;

        } else {
            echo false;
        }
    }

    public function getUpdatedRiwayatPendidikan(){

        $belajar_reg_id = 0;
        //Get NIP
        $id_user = $this->ion_auth->get_user_id();
        $data_detail = $this->detail_user->get_by_id($id_user);

        //insert to tabel registrasi master belajar
        $dat = $this->db->query("SELECT b.* FROM belajar_trans_riwayat_pendidikan a RIGHT JOIN cache_trans_riwayat_pendidikan b
                ON a.belajar_trans_riwayat_sttb_ijazah = b.riw_pendidikan_sttb_ijazah
                WHERE (a.belajar_trans_riwayat_sttb_ijazah != b.riw_pendidikan_sttb_ijazah OR a.belajar_trans_riwayat_pendidikan_tgl !=  b.riw_pendidikan_tgl OR a.belajar_trans_riwayat_pejabat != b.riw_pendidikan_pejabat) 
                OR (a.belajar_trans_riwayat_sttb_ijazah IS NULL AND a.belajar_trans_riwayat_pendidikan_tgl IS NULL AND a.belajar_trans_riwayat_pejabat IS NULL)");
        
        foreach ($dat->result() as $value) {
            if(empty($value->universitas)):
                $belajar_pendidikan_nm = $value->riw_pendidikan_nm;
            else:
                $belajar_pendidikan_nm = $value->nm_tingpend.' '.$value->universitas;
            endif;
            $dataRegistrasiMasterBelajar = array(
                "rekomendasi_reg_id" => null,
                "jenis_belajar" => null,
                "nip" => $data_detail->nip,
                "belajar_pendidikan_nm" => $belajar_pendidikan_nm,
                "belajar_univ_id" => null,
                "belajar_jurusan_id" => null,
                "belajar_tingpend_id" => $value->tingpend_id,
                "belajar_sekolah_id" => null,
                "belajar_fakultas_id" => $value->fakultas_id,
                "belajar_pend_lokasi" => $value->riw_pendidikan_lokasi,
                "created_at" => $value->riw_pendidikan_sttb_ijazah,
                "created_by" => $id_user,
                "updated_at" => $value->riw_pendidikan_sttb_ijazah,
                "updated_by" => $id_user,
                "status_reg_belajar" => 7
            );
            
            $this->db->where("belajar_pendidikan_nm",$value->riw_pendidikan_nm);
            $this->db->or_where("belajar_pendidikan_nm",$value->universitas);
            $query = $this->db->get("registrasi_master_belajar");
            if ($query->num_rows() > 0){
                $this->db->update("registrasi_master_belajar",$dataRegistrasiMasterBelajar);
            }
            else{
                $this->db->insert("registrasi_master_belajar",$dataRegistrasiMasterBelajar);
                $belajar_reg_id = $this->db->insert_id();

                //insert to tabel belajar_trans_riwayat_pendidikan
                $data_belajar_trans_riwayat_pendidikan = array(
                    "registrasi_belajar_id"                 => $belajar_reg_id,
                    "belajar_trans_riwayat_sttb_ijazah"     => $value->riw_pendidikan_sttb_ijazah,
                    "belajar_trans_riwayat_pendidikan_tgl"  => $value->riw_pendidikan_tgl,
                    "belajar_trans_riwayat_pejabat"         => $value->riw_pendidikan_pejabat
                );

                $this->db->insert("belajar_trans_riwayat_pendidikan", $data_belajar_trans_riwayat_pendidikan);
            }
        }
        $this->db->query("DELETE FROM cache_trans_riwayat_pendidikan");
    }
}