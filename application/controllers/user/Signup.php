<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->model("Master_user_model","signup_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $this->load->database();
    }

    public function index()
    {
        $this->load->view('userpanel/signup');
    }


}