<?php

use PHPUnit\Runner\Exception;

defined('BASEPATH') OR exit('No direct script access allowed');

class Izinbelajar extends CI_Controller {

    // private static $total_jpl = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->helper('dateformat_helper');
        $this->load->model("integrasi_api_model");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model("Universitas_model","universitas");
        $this->load->model("izinbelajar_model","izinbelajar");
        $this->load->library(array('session','pagination','ion_auth','form_validation','upload'));
        $this->load->database();
    }

    public function index(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Data Izin Belajar";
            $data['content'] = 'Daftar Izin Belajar';
            $data["field"] = $this->izinbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/izinbelajar");
            $config["total_rows"] = $this->izinbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $nip = $this->session->userdata("datauser")->nip;
            //Pagination Config

            $data["row"] = $this->izinbelajar->list_izin_belajar($config["per_page"],$page,null, $nip);

            /*
            $this->db->select('*');
            $this->db->from('registrasi_master_belajar');
            $this->db->where('jenis_belajar', '0');
            //$this->db->join('comments', 'comments.id = articles.id');
            $query = $this->db->get();
            $data['data_master_belajar'] = $query;*/
            $this->load->view('userpanel/izinbelajar/view_index',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function pengajuanIzinBelajar()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Pengajuan Izin Belajar";
            $data['content'] = 'Halaman Pengajuan Izin Belajar';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $this->load->view('userpanel/izinbelajar/form_input_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesPengajuan(){
        $this->db->select('*');
        $this->db->from('belajar_master_tingkat_pend');
        $this->db->where('tingpend_id',$this->input->post('tingpend_id'));
        $query = $this->db->get()->result();

        $this->db->select('*');
        $this->db->from('belajar_master_universitas');
        $this->db->where('univ_id',$this->input->post('id_universitas'));
        $query_universitas = $this->db->get()->result();

        $id_user = $this->ion_auth->get_user_id();
        $data_rekomendasi = array(
            'nip'=> $this->input->post('nip'),
            'rekomendasi_pendidikan_nm'=> 'Program'.' '.$query[0]->print_tingpend.' '.$this->input->post('nama_universitas'),
            'rekomendasi_univ_id'=> $this->input->post('id_universitas'),
            'rekomendasi_jurusan_id'=> $this->input->post('id_program_studi'),
            'rekomendasi_tingpend_id'=> $this->input->post('tingpend_id'),
            'rekomendasi_sekolah_id'=> NULL,
            'rekomendasi_fakultas_id'=> $this->input->post('fakultas_id'),
            'rekomendasi_pend_lokasi'=> $query_universitas[0]->univ_kota,
            'rekomendasi_beasiswa'=> $this->input->post('inp_beasiswa'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $id_user,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $id_user,
            'status_rekomendasi'=> 0,
            'jenis_pengiriman'=>0
        );
        if($this->db->insert('belajar_trans_rekomendasi', $data_rekomendasi)){
            $id_rekom = $this->db->insert_id();
            $data = array(
                'rekomendasi_reg_id' => $id_rekom,
                'jenis_belajar' => 0,
                'nip' => $this->input->post('nip'),
                'belajar_pendidikan_nm' => $this->input->post('nama_universitas'),
                'belajar_univ_id' => $this->input->post('id_universitas'),
                'belajar_jurusan_id' => $this->input->post('id_program_studi'),
                'belajar_tingpend_id' => $this->input->post('tingpend_id'),
                'belajar_sekolah_id' => NULL,
                'belajar_fakultas_id' => $this->input->post('fakultas_id'),
                'belajar_pend_lokasi' => $query_universitas[0]->univ_kota,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $id_user,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => $id_user,
                'status_reg_belajar' => '0',
                'status_disposisi' => '0'
            );
            if($this->db->insert('registrasi_master_belajar', $data)){
                $this->session->set_flashdata('success_message', 'Telah berhasil membuat izin belajar, silahkan menuju proses selanjutnya dengan klik tombol proses');
                redirect("user/izinbelajar");
            }else{
                $this->db->where('rekomendasi_reg_id', $id_rekom);
                $this->db->delete('belajar_trans_rekomendasi');
                $this->session->set_flashdata('error_message', 'Proses pengajuan bermasalah, silahkan coba lagi.');
                redirect("user/izinbelajar");
            }
        }else{
            $this->session->set_flashdata('error_message', 'Proses pengajuan bermasalah, silahkan coba lagi.');
            redirect("user/izinbelajar");
        }
        
    }

    public function prosesIzinBelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Proses Izin Belajar";
            $data['content'] = 'Melihat data proses izin belajar';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            //reg belajar
            $this->db->select('a.*, b.*');
            $this->db->from('registrasi_master_belajar a');
            $this->db->join("belajar_trans_rekomendasi b","a.rekomendasi_reg_id = b.rekomendasi_reg_id");
            $this->db->where('a.belajar_reg_id', $belajar_reg_id);
            $query = $this->db->get();
            
            //reg belajar upload
            $this->db->select('*');
            $this->db->from('belajar_trans_upload');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('upload_proses', 0);
            $queryTransUpload = $this->db->get();

            //reg belajar tolak
            $this->db->select('*');
            $this->db->from('belajar_trans_tolak');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('tolak_proses', 1);
            $this->db->where('tolak_jenis', 1);
            $queryTransTolak = $this->db->get();

            $this->db->select('*');
            $this->db->from('belajar_trans_tolak');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('tolak_proses', 4);
            $this->db->where('tolak_jenis', 1);
            $queryTransTolak2 = $this->db->get();

            //reg belajar upload
            $this->db->select('*');
            $this->db->from('belajar_trans_upload');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('upload_proses', 2);
            $queryTransUpload2 = $this->db->get();

            //reg belajar laporan
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $queryTransLaporan = $this->db->get();

            //data pengajuan perpanjangan izin belajar
            $this->db->select('*');
            $this->db->from('belajar_trans_perpanjangan');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $queryTransPerpanjangan= $this->db->get();

            $data['data_reg_belajar_id'] = $belajar_reg_id;
            $data['data_master_belajar'] = $query;
            $data['data_master_belajar_trans_upload'] = $queryTransUpload;
            $data['data_master_belajar_trans_tolak'] = $queryTransTolak;
            $data['data_master_belajar_trans_tolak_2'] = $queryTransTolak2;
            $data['data_master_belajar_trans_upload_2'] = $queryTransUpload2;
            $data['data_master_belajar_trans_laporan'] = $queryTransLaporan;
            $data['data_master_belajar_trans_perpanjangan'] = $queryTransPerpanjangan;
            $this->load->view('userpanel/izinbelajar/view_proses_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function pengajuanBerkas($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Pengajuan Berkas Izin Belajar ke OPD";
            $data['content'] = 'Mengarah pada form pengajuan berkas izin belajar ke OPD';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($belajar_reg_id)){
                $data["belajar_reg_id"] = $belajar_reg_id;
            }
            $data['status'] = 0;
            $this->load->view('userpanel/izinbelajar/form_pengajuan_opd_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesOpd(){
        $SUM_OF_FILE = 11;
        $count_file = 0;
        
        $INP_REG_BELAJAR_ID = $_POST['belajar_reg_id'];
        $INP_PROSES_REG_BELAJAR_ID = $_POST['proses_izin_belajar'];
        for($i = 0; $i < $SUM_OF_FILE; $i++){
            $count_file = $count_file + 1;
            $nama_file = 'inp_dokumen_'.$count_file;
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){
                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_izin_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_izin_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $result_delete = $this->deleteFileOnReuplod($INP_REG_BELAJAR_ID, $INP_PROSES_REG_BELAJAR_ID);
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/izinbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_REG_BELAJAR_ID,
                        "upload_proses" => $INP_PROSES_REG_BELAJAR_ID,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->izinbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/izinbelajar");
            }
        }
        $this->izinbelajar->updateProsesRegistrasiMasterBelajar('1', $INP_REG_BELAJAR_ID);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/izinbelajar");
    }

    public function perbaikanberkas($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Perbaiki Unggahan Berkas Izin Belajar";
            $data['content'] = 'Proses perbaikan unggahan berkas izin belajar';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($belajar_reg_id)){
                $data["belajar_reg_id"] = $belajar_reg_id;
            }
            $output = $this->deleteFileOnReuplod($belajar_reg_id, 0);
            if($output['status'] == 0){
                $this->session->set_flashdata("error_message","Terjadi kesalahan, coba lagi. Jika masih belum, silahkan hubungi Operator");
                redirect("user/izinbelajar");
            }
            $data['status'] = 1;
            $this->load->view('userpanel/izinbelajar/form_pengajuan_opd_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function pengajuanbkpp($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Pengajuan Berkas Izin Belajar ke BKPP";
            $data['content'] = 'Mengarah pada form pengajuan berkas izin belajar ke BKPP';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($belajar_reg_id)){
                $data["belajar_reg_id"] = $belajar_reg_id;
            }
            $data['status'] = 0;
            $this->load->view('userpanel/izinbelajar/form_pengajuan_bkpp_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesbkpp(){
        $SUM_OF_FILE = 3;
        $count_file = 0;
        
        $INP_REG_BELAJAR_ID = $_POST['belajar_reg_id'];
        $INP_PROSES_REG_BELAJAR_ID = $_POST['proses_izin_belajar'];
        for($i = 0; $i < $SUM_OF_FILE; $i++){
            $count_file = $count_file + 1;
            $nama_file = 'inp_dokumen_'.$count_file;
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){
                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_izin_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_izin_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $result_delete = $this->deleteFileOnReuplod($INP_REG_BELAJAR_ID, $INP_PROSES_REG_BELAJAR_ID);
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/izinbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_REG_BELAJAR_ID,
                        "upload_proses" => $INP_PROSES_REG_BELAJAR_ID,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->izinbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/izinbelajar");
            }
        }
        $this->izinbelajar->updateProsesRegistrasiMasterBelajar('4', $INP_REG_BELAJAR_ID);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/izinbelajar");
    }

    public function perbaikanBerkasBkpp($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Perbaiki Unggahan Berkas Izin Belajar";
            $data['content'] = 'Proses perbaikan unggahan berkas izin belajar';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($belajar_reg_id)){
                $data["belajar_reg_id"] = $belajar_reg_id;
            }
            $output = $this->deleteFileOnReuplod($belajar_reg_id, 0);
            $output = $this->deleteFileOnReuplod($belajar_reg_id, 2);
            if($output['status'] == 0){
                $this->session->set_flashdata("error_message","Terjadi kesalahan, coba lagi. Jika masih belum, silahkan hubungi Operator");
                redirect("user/izinbelajar");
            }
            $data['status'] = 1;
            $this->load->view('userpanel/izinbelajar/form_perbaikan_berkas_izin_belajar_bkpp',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesPerbaikanBkpp(){
        $SUM_OF_FILE = 14;
        $count_file = 0;
        
        $INP_REG_BELAJAR_ID = $_POST['belajar_reg_id'];
        for($i = 0; $i < $SUM_OF_FILE; $i++){
            $count_file = $count_file + 1;
            $nama_file = 'inp_dokumen_'.$count_file;
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){
                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_izin_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_izin_belajar'][$i] ;
                     $INP_PROSES_REG_BELAJAR_ID = $_POST['proses_izin_belajar'][$i];
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $result_delete = $this->deleteFileOnReuplod($INP_REG_BELAJAR_ID, $INP_PROSES_REG_BELAJAR_ID);
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/izinbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_REG_BELAJAR_ID,
                        "upload_proses" => $INP_PROSES_REG_BELAJAR_ID,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->izinbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/izinbelajar");
            }
        }
        $this->izinbelajar->updateProsesRegistrasiMasterBelajar('4', $INP_REG_BELAJAR_ID);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/izinbelajar");
    }

    public function perpanjangan($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Pengajuan Perpanjangan Izin Belajar";
            $data['content'] = 'Mengarah pada form pengajuan perpanjangan izin belajar';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($belajar_reg_id)){
                $data["belajar_reg_id"] = $belajar_reg_id;
            }
            $this->load->view('userpanel/izinbelajar/form_pengajuan_perpanjangan',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesPerpanjangan(){
        if(isset($_POST['belajar_reg_id']) && isset($_POST['inp_alasan'])){
            $INP_REG_BELAJAR_ID = $_POST['belajar_reg_id'];
            $INP_ALASAN = $_POST['inp_alasan'];
            $INP_STATUS = 0;
            $data_input = array(
                "belajar_reg_id" => $INP_REG_BELAJAR_ID,
                "belajar_alasan" => $INP_ALASAN,
                "status_perpanjangan" => $INP_STATUS
            );
            if($this->izinbelajar->save("belajar_trans_perpanjangan", $data_input) != null){
                $this->session->set_flashdata("success_message","Pengajuan Perpanjangan Berhasil, Silahkan menunggu konfirmasi BKPP");
                redirect("user/izinbelajar");
            }else{
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/izinbelajar");
            }
        }else{
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
            redirect("user/izinbelajar");
        }
    }

    /* ----- FUNGSI -----*/
    private function upload_files($jenis, $posisi, $namafile, $iduser)
    {       
        $this->load->library('upload');
        $file_upload = $_FILES;
        $_FILES[$namafile]['name']= $file_upload[$namafile]['name'];
        $_FILES[$namafile]['type']= $file_upload[$namafile]['type'];
        $_FILES[$namafile]['tmp_name']= $file_upload[$namafile]['tmp_name'];
        $_FILES[$namafile]['error']= $file_upload[$namafile]['error'];
        $_FILES[$namafile]['size']= $file_upload[$namafile]['size'];
        $custom_file_name ="IzinBelajar-".$jenis."-".$posisi."-".$iduser.'-'.md5(date("y-m-d h:i:s"));
        $extension = pathinfo($_FILES[$namafile]['name'], PATHINFO_EXTENSION);

        $this->upload->initialize($this->set_upload_options($custom_file_name));
        if ($this->upload->do_upload($namafile)) {
            $success = array(
                "status"=> 1,
                "filename"=>'uploads/persyaratan/'.$custom_file_name.".".$extension,
                "file" => $this->upload->data("client_name"),
                "uploaded" => $this->upload->data("file_name"),
                "message" => "Success Uploaded"
            );
        } else {
            $success = array(
                "status"=> 0,
                "filename"=>'uploads/persyaratan/'.$custom_file_name.".".$extension,
                "file" =>$this->upload->data("client_name"),
                "uploaded" => $this->upload->data("file_name"),
                "message" => "Error :" . $this->upload->display_errors()
            );
        }
        return $success;
    }

    private function set_upload_options($file_name)
    {
        //UPLOAD AN IMAGE OPTIONS
        $config = array();
        $config['upload_path'] = './uploads/persyaratan/';
        $config['allowed_types'] = 'jpg|png|pdf|rar|zip';
        $config['max_size']      = '2048';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

    //Fungsi delete file dan hapus DB, ketika perbaiki
    private function deleteFileOnReuplod($idregbelajar, $proses){
        //get All Data from belajar trans upload with where id reg belajar and proses
        $this->db->select('*');
        $this->db->from('belajar_trans_upload');
        $this->db->where('belajar_reg_id',$idregbelajar);
        $this->db->where('upload_proses',$proses);
        $query = $this->db->get();
        $data_file = $query->result();
        if(count($data_file) > 0){
            foreach ($data_file as $file) {
                if (unlink($file->upload_file)){
                    $this->db->where('upload_id', $file->upload_id);
                    $this->db->delete('belajar_trans_upload');
                    $output = array(
                        "status"=> 1,
                        "message" => "Success"
                    );
                }else{
                    $output = array(
                        "status"=> 0,
                        "message" => "Error"
                    );
                    break;
                }
            }
        }else{
            $output = array(
                "status"=> 1,
                "message" => "Success"
            );
        }
        return $output;
    }


    public function modalSearchUniversitasByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_universitas()->result() as $rows){
                $html .= "<tr onclick='modal_universitas_data($rows->univ_id)'>";
                $html .= "<td id='univ_id_$rows->univ_id' scope=\"col\">$rows->univ_id</td>";
                $html .= "<td width='15%' id='univ_nama_$rows->univ_id' scope=\"col\">$rows->univ_nmpti</td>";
                $html .= "<td id='univ_kota_$rows->univ_id' scope='col'>$rows->univ_kota</td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_universitas_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_universitas_data($rows->univ_id)'>";
                $html .= "<td id='univ_id_$rows->univ_id' scope=\"col\">$rows->univ_id</td>";
                $html .= "<td width='15%' id='univ_nama_$rows->univ_id' scope=\"col\">$rows->univ_nmpti</td>";
                $html .= "<td id='univ_kota_$rows->univ_id' scope='col'>$rows->univ_kota</td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }

    public function modalSearchProgramStudiByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_jurusan()->result() as $rows){
                $html .= "<tr onclick='modal_jurusan_data($rows->jurusan_id)'>";
                $html .= "<td id='jurusan_id_$rows->jurusan_id' scope=\"col\"><a href='#'>$rows->jurusan_id</a></td>";
                $html .= "<td id='jurusan_nama_$rows->jurusan_id' scope=\"col\"><a href='#'>$rows->jurusan_nm</a></td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_jurusan_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_jurusan_data($rows->jurusan_id)'>";
                $html .= "<td id='jurusan_id_$rows->jurusan_id' scope=\"col\"><a href='#'>$rows->jurusan_id</a></td>";
                $html .= "<td id='jurusan_nama_$rows->jurusan_id' scope=\"col\"><a href='#'>$rows->jurusan_nm</a></td>";
                $html .= "</tr>";
            };
        }
        echo $html;       
    }

    public function modalSearchFakultasByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_fakultas()->result() as $rows){
                $html .= "<tr onclick='modal_fakultas_data($rows->fakultas_id)'>";
                $html .= "<td id='fakultas_id_$rows->fakultas_id' scope=\"col\"><a href='#'>$rows->fakultas_id</a></td>";
                $html .= "<td id='fakultas_nama_$rows->fakultas_id' scope=\"col\"><a href='#'>$rows->fakultas_nm</a></td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_fakultas_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_fakultas_data($rows->fakultas_id)'>";
                $html .= "<td id='fakultas_id_$rows->fakultas_id' scope=\"col\"><a href='#'>$rows->fakultas_id</a></td>";
                $html .= "<td id='fakultas_nama_$rows->fakultas_id' scope=\"col\"><a href='#'>$rows->fakultas_nm</a></td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }

    public function modalSearchTingkatPendidikanByName($name){
        $html ="";
        if($name == "all"){
            $i = 1;
            foreach($this->universitas->list_tingkat_pend()->result() as $rows){
                $html .= "<tr onclick='modal_tingkat_pendidikan_data($rows->tingpend_id)'>";
                $html .= "<td scope=\"col\"><input type='hidden' id='tingpend_id_$rows->tingpend_id' value=".$rows->tingpend_id."> ".$i++." </td>";
                $html .= "<td id='tingpend_nama_$rows->tingpend_id' scope=\"col\"><a href='#'>$rows->nm_tingpend ($rows->print_tingpend)</a></td>";
                $html .= "</tr>";
            };
        } else{
            $i = 1;
            foreach($this->universitas->search_list_tingkat_pend_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_tingkat_pendidikan_data($rows->tingpend_id)'>";
                $html .= "<td scope=\"col\"><input type='hidden' id='tingpend_id_$rows->tingpend_id' value=".$rows->tingpend_id."> ".$i++." </td>";
                $html .= "<td id='tingpend_nama_$rows->tingpend_id' scope=\"col\"><a href='#'>$rows->nm_tingpend ($rows->print_tingpend)</a></td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }
    /*
        --- PERUBAHAN PERLU DIHAPUS ---
    public function tambahLaporanIzinBelajar($belajarregid)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Unggah Surat Izin Belajar";
            $data['content'] = 'ini Content 1';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data["belajar_reg_id"] = $belajarregid;
            $this->load->view('userpanel/izinbelajar/tambah_laporan_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesTambahLaporan(){
        $POST_NIP = $_POST['nip'];
        $POST_BELAJAR_ID = $_POST['belajarid'];
        $PARAM_NAMA_FILE = 'inp_dok_lap';
        $PARAM_NAMA_JENIS = str_replace(' ', '', $_POST['inp_namalap']);
        $PARAM_JENIS = 'Laporan_'.$PARAM_NAMA_JENIS;
        $PARAM_IDUSER = $this->ion_auth->get_user_id();
        try {
            if($_FILES[$PARAM_NAMA_FILE]['size'] == 0 && $_FILES[$PARAM_NAMA_FILE]['error'] == 4 ){
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah Laporan");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$POST_BELAJAR_ID);
            }else{
                $result = $this->upload_files($PARAM_JENIS, '0', $PARAM_NAMA_FILE, $PARAM_IDUSER);
                if($result['status'] == 0){
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                    redirect("user/izinbelajar");
                }
                $data_upload = array(
                    "belajar_reg_id" => $POST_BELAJAR_ID,
                    "laporan_nama" => $_POST['inp_namalap'],
                    "laporan_file" => $result['filename'],
                    "created_at" => date("Y-m-d H:i:s"),
                    "created_by" => $PARAM_IDUSER
                );
                $this->izinbelajar->save("belajar_trans_laporan", $data_upload);
            }
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah Laporan");
                redirect("user/izinbelajar/detailProsesIzinBelajar/".$POST_BELAJAR_ID);
        }
        $this->session->set_flashdata("success_message","Laporan telah ditambahkan.");
        redirect("user/izinbelajar/detailProsesIzinBelajar/".$POST_BELAJAR_ID);
    }

    public function ubahLaporan($laporan_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Laporan Izin Belajar";
            $data['content'] = 'ini Content 1';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('laporan_id',$laporan_id);
            $query = $this->db->get()->result();
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data["belajar_reg_id"] = $query[0]->belajar_reg_id;
            $data["data_laporan"] = $query;
            $this->load->view('userpanel/izinbelajar/edit_laporan_izin_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesHapusLaporan(){
        try {
            $INP_LAPORAN_ID = $_POST['laporanid'];
            $INP_REG_BELAJAR_ID = $_POST['regbelajarid'];
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('laporan_id',$INP_LAPORAN_ID);
            $query = $this->db->get()->result();

            if (unlink($query[0]->laporan_file)){
                $this->db->where('laporan_id', $INP_LAPORAN_ID);
                $this->db->delete('belajar_trans_laporan');
            }else{
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                    redirect("user/IzinBelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
            }
            $this->session->set_flashdata("success_message","Laporan telah dihapus.");
                redirect("user/IzinBelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                redirect("user/IzinBelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
        }
    }

    public function prosesUbahLaporan(){
        try {
            $INP_LAPORAN_ID = $_POST['laporanid'];
            $INP_REG_BELAJAR_ID = $_POST['belajarid'];
            if($_FILES['inp_dok_lap']['size'] == 0 && $_FILES['inp_dok_lap']['error'] == 4 ){
                //proses ubah tanpa dokumen
                $INP_DATA_NAMA = $_POST['inp_namalap'];
                $INP_DATA_DOKUMEN = NULL;
                $INP_USER_DATA = $this->ion_auth->get_user_id();
                $result = $this->izinbelajar->updateDataLaporan($INP_LAPORAN_ID,$INP_DATA_NAMA,$INP_DATA_DOKUMEN, $INP_USER_DATA );
                if($result){
                    $this->session->set_flashdata("success_message","Laporan berhasil diubah.");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }
            }else{
                //proses ubah dengan dokumen

                //proses hapus file sebelumnya
                $this->db->select('*');
                $this->db->from('belajar_trans_laporan');
                $this->db->where('laporan_id',$INP_LAPORAN_ID);
                $queryLaporan = $this->db->get()->result();
                if (unlink($queryLaporan[0]->laporan_file)){
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                        redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }

                $INP_DATA_NAMA = $_POST['inp_namalap'];
                $PARAM_NAMA_FILE = 'inp_dok_lap';
                $PARAM_NAMA_JENIS = str_replace(' ', '', $_POST['inp_namalap']);
                $PARAM_JENIS = 'Laporan_'.$PARAM_NAMA_JENIS;
                $INP_USER_DATA = $this->ion_auth->get_user_id();
                $result = $this->upload_files($PARAM_JENIS, '0', $PARAM_NAMA_FILE, $INP_USER_DATA);
                if($result['status'] == 0){
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }
                $INP_DATA_DOKUMEN = $result['filename'];
                $result2 = $this->izinbelajar->updateDataLaporan($INP_LAPORAN_ID,$INP_DATA_NAMA,$INP_DATA_DOKUMEN, $INP_USER_DATA );
                if($result2){
                    $this->session->set_flashdata("success_message","Laporan berhasil diubah.");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                    redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
                }
            }
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                redirect("user/izinbelajar/detailProsesIzinBelajar/".$INP_REG_BELAJAR_ID);
        }
    }
   */
}