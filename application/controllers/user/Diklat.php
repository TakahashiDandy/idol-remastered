<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Diklat extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->model(array("diklat_model","event_model","registrasi_model"));
        $this->load->library(array('session','pagination','ion_auth'));
    }

    public function index_user(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Riwayat Diklat";
            $data["tablename"] = "Riwayat Diklat";
            $data["field"] = $this->diklat_model->listfield("diklat_trans_riwayat_diklat");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/diklat/index_user/");
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_trans_riwayat_diklat");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);

            if(!empty($this->session->userdata("datauser")->nip)){
                $nip = $this->session->userdata("datauser")->nip;
            }

            $data["row"] = $this->diklat_model->listriwayatdiklat($config["per_page"],$page, $nip);
            $this->load->view("userpanel/diklat/index",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function detail_riwayat_diklat($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Detil Riwayat Diklat";
            //get data user diklat
            $id_user = $this->ion_auth->get_user_id();
            $data_detail_user = $this->registrasi_model->get_detail_user_diklat_by_id($id_user, $id);
            $data["detail_user"] = $data_detail_user;
            
            if($data_detail_user->status_registrant == 2){
                //ambil file upload yang statusnya ditunda = 1
                $data["attachment"] = $this->registrasi_model->getattachment($id, 1)->result_array();
            }else{
                //ambil file upload yang statusnya hasil diklat = 2
                $data["attachment"] = $this->registrasi_model->getattachment($id, 2)->result_array();
            }
            $data["rows1"] = $this->registrasi_model->get_detail_alumni_riwayat_diklat($id);
            $this->load->view("userpanel/diklat/detail_riwayat_diklat",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function update_riwayat_diklat($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){

            //get data user diklat
            $id_user = $this->ion_auth->get_user_id();
            $data_detail_user = $this->registrasi_model->get_detail_user_diklat_by_id($id_user, $id);
            $id_jadwal = $data_detail_user->diklat_jadwal_id;
            $id_user = $data_detail_user->id_user;
            $status_registrant = $this->input->post('status_registrant');

            //UPLOAD DATA TO SERVER AND SAVE TO DATABASE
            $this->upload_files($id_jadwal, $id_user, $id, $status_registrant);
            if($status_registrant == 2){
                $this->session->set_flashdata("success_message", "Berkas diklat sudah diperbaharui.");
            }
            if($status_registrant == 3 || $status_registrant == 5){
                $this->session->set_flashdata("success_message", "Berkas hasil diklat sudah dikirim.");
            }
            redirect("user/diklat/index_user");
        } else{
            redirect("user/auth/login");
        }   
    }

    public function diklat_struktural(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Diklat Struktural";
            $data["tablename"] = "List Diklat Struktural";
            $data["field"] = $this->diklat_model->listfield("diklat_master_struktural_kategori");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_struktural";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_struktural_kategori");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->liststruk($config["per_page"],$page);

            $this->load->view("adminpanel/diklat/list_diklat_struktural",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function diklat_fungsional(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Diklat Fungsional";
            $data["tablename"] = "List Diklat Fungsional";
            $data["field"] = $this->diklat_model->listfield("diklat_master_diklat_fungsional");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_fungsional";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_diklat_fungsional");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->listfunc($config["per_page"],$page);


            $this->load->view("adminpanel/diklat/list_diklat_fungsional",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function diklat_teknis(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Diklat Teknis";
            $data["tablename"] = "List Diklat Teknis";
            $data["field"] = $this->diklat_model->listfield("diklat_master_diklat_teknis");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url() . "admin/diklat/diklat_teknis";
            $config["total_rows"] = $this->diklat_model->gettotalrows("diklat_master_diklat_teknis");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $data["row"] = $this->diklat_model->listtek($config["per_page"],$page);

            $this->load->view("adminpanel/diklat/list_diklat_teknis",$data);
        } else{
            redirect("admin/auth/login");
        }
    }

    public function searchdiklatbyname($id,$name){
        $html ="";
        switch ($id){
            case 1:
                if($this->event_model->searchdiklatbyname(1,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" class='align-self-center' colspan='3'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else{
                    foreach($this->event_model->searchdiklatbyname(1,$name)->result() as $rows){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->kategori_id</td>";
                        $html .= "<td scope=\"col\">$rows->kategori_nama</td>";
                        $html .= "<td scope=\"col\">$rows->kategori_parent</td>";
                        $html .= "</tr>";
                    };
                }
                break;
            case 2:
                if($this->event_model->searchdiklatbyname(2,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" class='align-self-center' colspan='2'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else {
                    foreach ($this->event_model->searchdiklatbyname(2, $name)->result() as $rows) {
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->diklat_fungsional_id</td>";
                        $html .= "<td scope=\"col\">$rows->diklat_fungsional_nm</td>";
                        $html .= "</tr>";
                    };
                }
                break;
            case 3:
                if($this->event_model->searchdiklatbyname(3,$name)->num_rows() < 1){
                    $html .= "<tr>";
                    $html .= "<td scope=\"col\" colspan='2'>DATA NOT FOUND</td>";
                    $html .= "</tr>";
                } else {
                    foreach ($this->event_model->searchdiklatbyname(3, $name)->result() as $rows) {
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\">$rows->diklat_teknis_id</td>";
                        $html .= "<td scope=\"col\">$rows->diklat_teknis_nm</td>";
                        $html .= "</tr>";
                    };
                }
                break;
        }
        echo $html;
    }

    public function modalsearchdiklatbyname($id,$name){
        $html ="";
        switch ($id){
            case 1:
                if($name == "all"){
                    foreach($this->diklat_model->liststruk(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->kategori_id)'>";
                        $html .= "<td id='diklat_id_$rows->kategori_id' scope=\"col\">$rows->kategori_id</td>";
                        $html .= "<td id='diklat_nama_$rows->kategori_id' scope='col'>$rows->kategori_nama</td>";
                        $html .= "</tr>";
                    };
                } else{
                    if($this->event_model->searchdiklatbyname(1,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" class='align-self-center' colspan='3'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    }
                    else{
                        foreach($this->event_model->searchdiklatbyname(1,$name)->result() as $rows){
                            $html .= "<tr onclick='modal_diklat_data($rows->kategori_id)'>";
                            $html .= "<td id='diklat_id_$rows->kategori_id' scope=\"col\">$rows->kategori_id</td>";
                            $html .= "<td id='diklat_nama_$rows->kategori_id' scope='col'>$rows->kategori_nama</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
            case 2:
                if($name == "all"){
                    foreach($this->diklat_model->listfunc(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->diklat_fungsional_id)'>";
                        $html .= "<td id='diklat_id_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_id</td>";
                        $html .= "<td id='diklat_nama_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_nm</td>";
                        $html .= "</tr>";
                    };
                } else {
                    if($this->event_model->searchdiklatbyname(2,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" class='align-self-center' colspan='2'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    } else{
                        foreach ($this->event_model->searchdiklatbyname(2, $name)->result() as $rows) {
                            $html .= "<tr onclick='modal_diklat_data($rows->diklat_fungsional_id)'>";
                            $html .= "<td id='diklat_id_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_id</td>";
                            $html .= "<td id='diklat_nama_$rows->diklat_fungsional_id' scope=\"col\">$rows->diklat_fungsional_nm</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
            case 3:
                if($name == "all"){
                    foreach($this->diklat_model->listtek(10,10)->result() as $rows){
                        $html .= "<tr onclick='modal_diklat_data($rows->diklat_teknis_id)'>";
                        $html .= "<td id='diklat_id_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_id</td>";
                        $html .= "<td id='diklat_nama_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_nm</td>";
                        $html .= "</tr>";
                    };
                } else {
                    if($this->event_model->searchdiklatbyname(3,$name)->num_rows() < 1){
                        $html .= "<tr>";
                        $html .= "<td scope=\"col\" colspan='2'>DATA NOT FOUND</td>";
                        $html .= "</tr>";
                    }
                    else {
                        foreach ($this->event_model->searchdiklatbyname(3, $name)->result() as $rows) {
                            $html .= "<tr onclick='modal_diklat_data($rows->diklat_teknis_id)'>";
                            $html .= "<td id='diklat_id_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_id</td>";
                            $html .= "<td id='diklat_nama_$rows->diklat_teknis_id' scope=\"col\">$rows->diklat_teknis_nm</td>";
                            $html .= "</tr>";
                        };
                    }
                }
                break;
        }
        echo $html;
    }

    private function upload_files($id_diklat_trans_jadwal, $id_user, $id_reg, $status_registrant)
    {
        //Hapus data sebelum upload yang baru
        if($status_registrant == 2 || $status_registrant == 5){
            $option = ($status_registrant == 2 ? 1: 2);
            $data = $this->registrasi_model->delete_registrasi_trans_upload($id_diklat_trans_jadwal,$id_reg,$option);
            if(sizeof($data["attachment"]) > 0){
                foreach ($data as $item){
                    foreach ($item as $attachment){
                        unlink($attachment["filepath"]);
                    }
                }
            }
        }

        $uploaded = array();
        $success = true;
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['laporanhasildiklat']['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['laporanhasildiklat']['name']= $files['laporanhasildiklat']['name'][$i];
            $_FILES['laporanhasildiklat']['type']= $files['laporanhasildiklat']['type'][$i];
            $_FILES['laporanhasildiklat']['tmp_name']= $files['laporanhasildiklat']['tmp_name'][$i];
            $_FILES['laporanhasildiklat']['error']= $files['laporanhasildiklat']['error'][$i];
            $_FILES['laporanhasildiklat']['size']= $files['laporanhasildiklat']['size'][$i];
            $extension = pathinfo($_FILES['laporanhasildiklat']['name'], PATHINFO_EXTENSION);
            if($status_registrant == 2){
                $custom_file_name = "LAMPIRAN-IDOL-".$id_user.'-'.$id_diklat_trans_jadwal.'-'.md5(date("y-m-d h:i:s"));
            } else{
                $custom_file_name = "LAPORAN-HASIL-DIKLAT-IDOL-".$id_user.'-'.$id_diklat_trans_jadwal.'-'.md5(date("y-m-d h:i:s"));
            }

            $this->upload->initialize($this->set_upload_options($custom_file_name));

            if ($this->upload->do_upload('laporanhasildiklat')) {
                $data = $this->upload->data();

                $data_diklat_trans_upload = array(
                    "id_jadwal" => $id_diklat_trans_jadwal,
                    "id_registrasi" => $id_reg,
                    "id_option" => ($status_registrant == 2 ? 1: 2),
                    "filepath" => 'uploads/lampiran/'.$data["raw_name"].".".$extension,
                    "extension" => $extension,
                    "created_at" => date("y-m-d h:i:s"),
                    "created_by" => $id_user
                );

                array_push($uploaded,$data_diklat_trans_upload);

            } else {
                $success = false;
                break;
            }
        }
        
        if($success){
            foreach ($uploaded as $save){
                $this->registrasi_model->save_registrasi_trans_upload($save);
                
            }
            return true;
        } else {
            return false;
        }
    }

    
    private function set_upload_options($file_name)
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/lampiran/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '1024';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

    public function deleteattachment($id_attachment){
        $data = $this->registrasi_model->removeattachment($id_attachment);
        foreach ($data as $result){
            foreach ($result as $attach){
                unlink($attach["filepath"]);
            }
        }
        return true;
    }

    public function deleteattachmentsyaratdiklat($id_attachment){
        $data = $this->registrasi_model->removeattachmentsyaratdiklat($id_attachment);
        foreach ($data as $result){
            foreach ($result as $attach){
                unlink($attach["filepath"]);
            }
        }
        return true;
    }
}
