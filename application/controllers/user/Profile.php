<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Profile - ".$this->session->username;
            
            //Get NIP
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            if(!empty($data_detail->nip)){
                $data["nip"] = $data_detail->nip;
            }
            
            //return view Profile
            $this->load->view('userpanel/profile/index',$data);
        }
    }
}