<?php

use PHPUnit\Framework\MockObject\Stub\Exception;

use function GuzzleHttp\json_encode;

defined('BASEPATH') OR exit('No direct script access allowed');

class Tugasbelajar extends CI_Controller {

    // private static $total_jpl = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->helper('dateformat_helper');
        $this->load->model("integrasi_api_model");
        $this->load->model("Trans_detail_user_model","detail_user");
        $this->load->model("Universitas_model","universitas");
        $this->load->model("Tugasbelajar_model","tugasbelajar");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Data Tugas Belajar";
            $data["tablename"] = "Daftar Tugas Belajar";
            $data["field"] = $this->tugasbelajar->listfield("registrasi_master_belajar");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/tugasbelajar");
            $config["total_rows"] = $this->tugasbelajar->gettotalrows("registrasi_master_belajar");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $nip = $this->session->userdata("datauser")->nip;
            $data["row"] = $this->tugasbelajar->list_tugas_belajar($config["per_page"],$page,null, $nip);
            // print_r($nip);die();
            //print_r(json_encode($data["row"]->result()));die();

            $this->load->view('userpanel/tugasbelajar/index',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function addSuratRekomendasi(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)):
            $data["pagename"] = "Tugas Belajar";
            $data['content'] = 'Ini Content 1';

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }

            $this->load->view('userpanel/tugasbelajar/input_surat_rekomendasi',$data);
        else:
            redirect("user/auth/login");
        endif;
    }

    public function ajukanTugasBelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)):
            $data["pagename"] = "Tugas Belajar";
            $data['content'] = 'Ini Content 1';

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data['belajar_reg_id'] = $belajar_reg_id;
            $this->load->view('userpanel/tugasbelajar/ajukan_tugas_belajar',$data);
        else:
            redirect("user/auth/login");
        endif;
    }

    public function perbaikiTugasBelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)):
            $data["pagename"] = "Tugas Belajar";
            $data['content'] = 'Ini Content 1';

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data['belajar_reg_id'] = $belajar_reg_id;
            $this->load->view('userpanel/tugasbelajar/perbaiki_tugas_belajar',$data);
        else:
            redirect("user/auth/login");
        endif;
    }

    public function unggahDokumenTugasBelajar($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)):
            $data["pagename"] = "Tugas Belajar";
            $data['content'] = 'Ini Content 1';

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data['belajar_reg_id'] = $belajar_reg_id;
            $this->load->view('userpanel/tugasbelajar/unggah_dokumen',$data);
        else:
            redirect("user/auth/login");
        endif;
    }

    public function insertSuratRekomendasi(){
        $id_user = $this->ion_auth->get_user_id();
        $data_detail = $this->detail_user->get_by_id($id_user);
        $nama_pendidikan = ($this->input->post('nama_universitas') !== null) ? $this->input->post('nama_universitas') : null;
        $id_universitas = ($this->input->post('id_universitas')!== null) ? $this->input->post('id_universitas') : 0;
        $id_tingpend = ($this->input->post('tingpend_id')!== null) ? $this->input->post('tingpend_id') : 0;
        // $id_sekolah = ($this->input->post('id_sekolah')!== null) ? $this->input->post('id_sekolah') : 0;
        $id_fakultas = ($this->input->post('fakultas_id')!== null) ? $this->input->post('fakultas_id') : 0;
        $kota = ($this->input->post('kota_universitas')!== null) ? $this->input->post('kota_universitas') : '';
        $id_jurusan = ($this->input->post('id_program_studi')!== null) ? $this->input->post('id_program_studi') : 0;
        $jenis_pengiriman = ($this->input->post('jenis_pengiriman')!== null) ? $this->input->post('jenis_pengiriman') : null;
        $beasiswa = ($this->input->post('beasiswa')!== null) ? $this->input->post('beasiswa') : null;

        $dataSuratRekomendasi = [
            "nip" => $data_detail->nip,
            "rekomendasi_pendidikan_nm" => $nama_pendidikan,
            "rekomendasi_univ_id" => $id_universitas,
            "rekomendasi_jurusan_id" => $id_jurusan,
            "rekomendasi_tingpend_id" => $id_tingpend,
            // "rekomendasi_sekolah_id" => $id_sekolah,
            "rekomendasi_fakultas_id" => $id_fakultas,
            "rekomendasi_pend_lokasi" => $kota,
            "rekomendasi_beasiswa" => $beasiswa,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $data_detail->nama_lengkap,
            // "updated_at" => date("Y-m-d H:i:s"),
            // "updated_by" => $data_detail->nama_lengkap,
            "status_rekomendasi" => 0,
            "jenis_pengiriman" => $jenis_pengiriman
        ];

        $rekomendasi_reg_id = $this->universitas->save("belajar_trans_rekomendasi", $dataSuratRekomendasi);
        $this->universitas->save("belajar_trans_rekomendasi_detail",[
            "rekomendasi_reg_id" => $rekomendasi_reg_id,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $data_detail->nama_lengkap
        ]);

        $dataRegistrasiBelajar = [
            "rekomendasi_reg_id" => $rekomendasi_reg_id,
            "jenis_belajar" => 1,
            "nip" => $data_detail->nip,
            "belajar_pendidikan_nm" => $nama_pendidikan,
            "belajar_univ_id" => $id_universitas,
            "belajar_jurusan_id" => $id_jurusan,
            "belajar_tingpend_id" => $id_tingpend,
            // "belajar_sekolah_id" => $id_sekolah,
            "belajar_fakultas_id" => $id_fakultas,
            "belajar_pend_lokasi" => $kota,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $data_detail->nama_lengkap,
            "status_reg_belajar" => 0
        ];

        $this->universitas->save("registrasi_master_belajar", $dataRegistrasiBelajar);

        $this->session->set_flashdata("success","Anda telah mengajukan surat rekomendasi tugas belajar.");
        redirect("user/tugasbelajar");
    }

    public function modalSearchUniversitasByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_universitas()->result() as $rows){
                $html .= "<tr onclick='modal_universitas_data($rows->univ_id)'>";
                $html .= "<td id='univ_id_$rows->univ_id' scope=\"col\">$rows->univ_id</td>";
                $html .= "<td width='15%' id='univ_nama_$rows->univ_id' scope=\"col\">$rows->univ_nmpti</td>";
                $html .= "<td id='univ_kota_$rows->univ_id' scope='col'>$rows->univ_kota</td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_universitas_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_universitas_data($rows->univ_id)'>";
                $html .= "<td id='univ_id_$rows->univ_id' scope=\"col\">$rows->univ_id</td>";
                $html .= "<td width='15%' id='univ_nama_$rows->univ_id' scope=\"col\">$rows->univ_nmpti</td>";
                $html .= "<td id='univ_kota_$rows->univ_id' scope='col'>$rows->univ_kota</td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }

    public function modalSearchProgramStudiByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_jurusan()->result() as $rows){
                $html .= "<tr onclick='modal_jurusan_data($rows->jurusan_id)'>";
                $html .= "<td id='jurusan_id_$rows->jurusan_id' scope=\"col\">$rows->jurusan_id</td>";
                $html .= "<td id='jurusan_nama_$rows->jurusan_id' scope=\"col\">$rows->jurusan_nm</td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_jurusan_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_jurusan_data($rows->jurusan_id)'>";
                $html .= "<td id='jurusan_id_$rows->jurusan_id' scope=\"col\">$rows->jurusan_id</td>";
                $html .= "<td id='jurusan_nama_$rows->jurusan_id' scope=\"col\">$rows->jurusan_nm</td>";
                $html .= "</tr>";
            };
        }
        echo $html;       
    }

    public function modalSearchFakultasByName($name){
        $html ="";
        if($name == "all"){
            foreach($this->universitas->list_fakultas()->result() as $rows){
                $html .= "<tr onclick='modal_fakultas_data($rows->fakultas_id)'>";
                $html .= "<td id='fakultas_id_$rows->fakultas_id' scope=\"col\">$rows->fakultas_id</td>";
                $html .= "<td id='fakultas_nama_$rows->fakultas_id' scope=\"col\">$rows->fakultas_nm</td>";
                $html .= "</tr>";
            };
        } else{
            foreach($this->universitas->search_list_fakultas_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_fakultas_data($rows->fakultas_id)'>";
                $html .= "<td id='fakultas_id_$rows->fakultas_id' scope=\"col\">$rows->fakultas_id</td>";
                $html .= "<td id='fakultas_nama_$rows->fakultas_id' scope=\"col\">$rows->fakultas_nm</td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }

    public function modalSearchTingkatPendidikanByName($name){
        $html ="";
        if($name == "all"){
            $i = 1;
            foreach($this->universitas->list_tingkat_pend()->result() as $rows){
                $html .= "<tr onclick='modal_tingkat_pendidikan_data($rows->tingpend_id)'>";
                $html .= "<td scope=\"col\"><input type='hidden' id='tingpend_id_$rows->tingpend_id' value=".$rows->tingpend_id."> ".$i++." </td>";
                $html .= "<td id='tingpend_nama_$rows->tingpend_id' scope=\"col\">$rows->nm_tingpend ($rows->print_tingpend)</td>";
                $html .= "</tr>";
            };
        } else{
            $i = 1;
            foreach($this->universitas->search_list_tingkat_pend_by_name(urldecode($name))->result() as $rows){
                $html .= "<tr onclick='modal_tingkat_pendidikan_data($rows->tingpend_id)'>";
                $html .= "<td scope=\"col\"><input type='hidden' id='tingpend_id_$rows->tingpend_id' value=".$rows->tingpend_id."> ".$i++." </td>";
                $html .= "<td id='tingpend_nama_$rows->tingpend_id' scope=\"col\">$rows->nm_tingpend ($rows->print_tingpend)</td>";
                $html .= "</tr>";
            };
        }
        echo $html;
    }

    public function detailProsesTugasBelajar($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Detail Proses Pengajuan Tugas Belajar";
            $data['content'] = 'ini Content 1';
            //$data["status_reg_belajar"] = $status_reg_belajar;
            $data["data_reg_belajar_id"] = $belajar_reg_id;

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            //reg belajar
            $this->db->select('*');
            $this->db->from('registrasi_master_belajar');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $query = $this->db->get();

            //reg belajar rekomendasi
            $this->db->select('belajar_trans_rekomendasi.*, belajar_trans_rekomendasi_detail.*');
            $this->db->from('belajar_trans_rekomendasi');
            $this->db->join('belajar_trans_rekomendasi_detail', 
            'belajar_trans_rekomendasi.rekomendasi_reg_id ='
            .' belajar_trans_rekomendasi_detail.rekomendasi_reg_id');
            $this->db->join('registrasi_master_belajar', 
            'belajar_trans_rekomendasi.rekomendasi_reg_id ='
            .' registrasi_master_belajar.rekomendasi_reg_id');
            $this->db->where('registrasi_master_belajar.belajar_reg_id', $belajar_reg_id);
            $queryRekomendasi = $this->db->get();

            //reg belajar upload 0
            $this->db->select('*');
            $this->db->from('belajar_trans_upload');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('upload_proses', 0);
            $queryTransUpload = $this->db->get();

            //reg belajar tolak
            $this->db->select('*');
            $this->db->from('belajar_trans_tolak');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('tolak_proses', 2);
            $this->db->where('tolak_jenis', 1);
            $queryTransTolak = $this->db->get();

            //reg belajar upload
            $this->db->select('*');
            $this->db->from('belajar_trans_upload');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $this->db->where('upload_proses', 2);
            $queryTransUpload2 = $this->db->get();

            //reg belajar laporan
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $queryTransLaporan = $this->db->get();


            $data['data_master_belajar'] = $query;
            $data['data_master_belajar_trans_upload'] = $queryTransUpload;
            $data['data_master_belajar_trans_tolak'] = $queryTransTolak;
            $data['data_master_belajar_trans_upload_2'] = $queryTransUpload2;
            $data['data_master_belajar_trans_laporan'] = $queryTransLaporan;
            $data['data_master_rekomendasi'] = $queryRekomendasi;
            $this->load->view('userpanel/tugasbelajar/proses_tugas_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function laporan($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Laporan Tugas Belajar";
            $data['content'] = 'ini Content 1';
            $data["data_reg_belajar_id"] = $belajar_reg_id;

            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }

            //reg belajar laporan
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('belajar_reg_id', $belajar_reg_id);
            $queryTransLaporan = $this->db->get()->result();

            $query = $this->tugasbelajar->data_tugas_belajar($belajar_reg_id);
            $data['data_master_belajar'] = $query;
            $data['data_laporan'] = $queryTransLaporan;
            $this->load->view('userpanel/tugasbelajar/laporan_tugas_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function ajukanPermohonanTugasBelajar($belajar_reg_id)
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Permohonan Tugas Belajar";
            $data['content'] = 'ini Content 1';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $this->load->view('userpanel/tugasbelajar/permohonan_tugas_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function tambahLaporan($belajar_reg_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Tambah laporan";
            $data['content'] = 'ini Content 1';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $data["belajar_reg_id"] = $belajar_reg_id;
            $this->load->view('userpanel/tugasbelajar/tambah_laporan_tugas_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function ubahLaporan($laporan_id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $data["pagename"] = "Ubah Laporan";
            $data['content'] = 'ini Content 1';
            $id_user = $this->ion_auth->get_user_id();
            $data_detail = $this->detail_user->get_by_id($id_user);
            if(!empty($data_detail)){
                $data["detail_user"] = $data_detail;
            }
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('laporan_id',$laporan_id);
            $query = $this->db->get()->result();

            $data["belajar_reg_id"] = $query[0]->belajar_reg_id;
            $data["data_laporan"] = $query;
            $this->load->view('userpanel/tugasbelajar/edit_laporan_tugas_belajar',$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function prosesTambahLaporan(){
        $POST_NIP = $_POST['nip'];
        $POST_BELAJAR_ID = $_POST['belajarid'];
        $PARAM_NAMA_FILE = 'inp_dok_lap';
        $PARAM_NAMA_JENIS = str_replace(' ', '', $_POST['inp_namalap']);
        $PARAM_JENIS = 'Laporan_'.$PARAM_NAMA_JENIS;
        $PARAM_IDUSER = $this->ion_auth->get_user_id();
        try {
            if($_FILES[$PARAM_NAMA_FILE]['size'] == 0 && $_FILES[$PARAM_NAMA_FILE]['error'] == 4 ){
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah Laporan");
                    redirect("user/tugasbelajar/laporan/".$POST_BELAJAR_ID);
            }else{
                $result = $this->upload_files($PARAM_JENIS, '0', $PARAM_NAMA_FILE, $PARAM_IDUSER);
                if($result['status'] == 0){
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                    redirect("user/tugasbelajar");
                }
                $data_upload = array(
                    "belajar_reg_id" => $POST_BELAJAR_ID,
                    "laporan_nama" => $_POST['inp_namalap'],
                    "laporan_file" => $result['filename'],
                    "created_at" => date("Y-m-d H:i:s"),
                    "created_by" => $PARAM_IDUSER
                );
                $this->tugasbelajar->save("belajar_trans_laporan", $data_upload);
            }
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah Laporan");
                redirect("user/tugasbelajar/laporan/".$POST_BELAJAR_ID);
        }
        $this->session->set_flashdata("success_message","Laporan telah ditambahkan.");
        redirect("user/tugasbelajar/laporan/".$POST_BELAJAR_ID);
    }

    public function prosesUbahLaporan(){
        try {
            $INP_LAPORAN_ID = $_POST['laporanid'];
            $INP_REG_BELAJAR_ID = $_POST['belajarid'];
            if($_FILES['inp_dok_lap']['size'] == 0 && $_FILES['inp_dok_lap']['error'] == 4 ){
                //proses ubah tanpa dokumen
                $INP_DATA_NAMA = $_POST['inp_namalap'];
                $INP_DATA_DOKUMEN = NULL;
                $INP_USER_DATA = $this->ion_auth->get_user_id();
                $result = $this->tugasbelajar->updateDataLaporan($INP_LAPORAN_ID,$INP_DATA_NAMA,$INP_DATA_DOKUMEN, $INP_USER_DATA );
                if($result){
                    $this->session->set_flashdata("success_message","Laporan berhasil diubah.");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }
            }else{
                //proses ubah dengan dokumen
                //proses hapus file sebelumnya
                $this->db->select('*');
                $this->db->from('belajar_trans_laporan');
                $this->db->where('laporan_id',$INP_LAPORAN_ID);
                $queryLaporan = $this->db->get()->result();
                if (unlink($queryLaporan[0]->laporan_file)){
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                        redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }

                $INP_DATA_NAMA = $_POST['inp_namalap'];
                $PARAM_NAMA_FILE = 'inp_dok_lap';
                $PARAM_NAMA_JENIS = str_replace(' ', '', $_POST['inp_namalap']);
                $PARAM_JENIS = 'Laporan_'.$PARAM_NAMA_JENIS;
                $INP_USER_DATA = $this->ion_auth->get_user_id();
                $result = $this->upload_files($PARAM_JENIS, '0', $PARAM_NAMA_FILE, $INP_USER_DATA);
                if($result['status'] == 0){
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }
                $INP_DATA_DOKUMEN = $result['filename'];
                $result2 = $this->tugasbelajar->updateDataLaporan($INP_LAPORAN_ID,$INP_DATA_NAMA,$INP_DATA_DOKUMEN, $INP_USER_DATA );
                if($result2){
                    $this->session->set_flashdata("success_message","Laporan berhasil diubah.");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
                }
            }
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses ubah laporan belajar.");
                redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
        }
    }

    public function prosesHapusLaporan(){
        try {
            $INP_LAPORAN_ID = $_POST['laporanid'];
            $INP_REG_BELAJAR_ID = $_POST['regbelajarid'];
            $this->db->select('*');
            $this->db->from('belajar_trans_laporan');
            $this->db->where('laporan_id',$INP_LAPORAN_ID);
            $query = $this->db->get()->result();

            if (unlink($query[0]->laporan_file)){
                $this->db->where('laporan_id', $INP_LAPORAN_ID);
                $this->db->delete('belajar_trans_laporan');
            }else{
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                    redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
            }
            $this->session->set_flashdata("success_message","Laporan telah dihapus.");
                redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses selesai belajar.");
                redirect("user/tugasbelajar/laporan/".$INP_REG_BELAJAR_ID);
        }
    }

    public function cetak($id)
    {
        if($this->ion_auth->logged_in()):
            $data["data_print"] = $this->tugasbelajar->data_tugas_belajar($id);
            
            if(!empty($data["data_print"])):
                if($this->ion_auth->in_group(3)):
                    $data["instansi"] = "BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN";
                else:
                    $data["instansi"] = strtoupper($data["data_print"][0]->instansi);
                endif;
                // $data["instansi"] = "BADAN KEPEGAWAIAN PENDIDIKAN DAN PELATIHAN";
                $this->load->view('adminpanel/tugasbelajar/print_surat_rekomendasi', $data);
            endif;
        else:
            redirect("admin/auth/login");
        endif;
    }

    public function prosesPerbaikiTugasBelajar(){
        $SUM_OF_FILE_1 = 10; // num of file
        $SUM_OF_FILE_2 = 3; // num of file
        $PROSES_REG_BELAJAR_1 = 0; //proses belajar
        $PROSES_REG_BELAJAR_2 = 2; //proses belajar
        $INP_POST_REG_BELAJAR_ID = $_POST['belajar_reg_id']; 
        //got data upload
            $this->db->select('*');
            $this->db->from('belajar_trans_upload');
            $this->db->where('belajar_reg_id', $INP_POST_REG_BELAJAR_ID);
            $queryTransUpload = $this->db->get()->result();
        //---
        //Proses Hapus File
        try {
            foreach ($queryTransUpload as $file) {
                if (unlink($file->upload_file)){
                    $this->db->where('upload_id', $file->upload_id);
                    $this->db->delete('belajar_trans_upload');
                }else{
                    $this->session->set_flashdata("error_message","Terjadi Error dalam proses reload dokumen 2, silahkan coba kembali.");
                     redirect("user/tugasbelajar");
                }
            }
        } catch (Exception $ex) {
            $this->session->set_flashdata("error_message","Terjadi Error dalam proses reload dokumen, silahkan coba kembali.");
            redirect("user/tugasbelajar");
        }
        //---
        //Proses Upload Ulang
        for($i = 0; $i < $SUM_OF_FILE_1; $i++){
            $count_file = $i + 1; 
            $nama_file = 'inp_dokumen_'.$count_file; //filename
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){//if upload file was null

                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_tugas_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_tugas_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/tugasbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_POST_REG_BELAJAR_ID,
                        "upload_proses" => $PROSES_REG_BELAJAR_1,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->tugasbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/tugasbelajar");
            }
        }

        for($i = 0; $i < $SUM_OF_FILE_2; $i++){
            $count_file = $i + 1; 
            $nama_file = 'inp_dokumen_2_'.$count_file; //filename
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){//if upload file was null

                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_tugas_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_tugas_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/tugasbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_POST_REG_BELAJAR_ID,
                        "upload_proses" => $PROSES_REG_BELAJAR_2,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->tugasbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/tugasbelajar");
            }
        }
        $this->tugasbelajar->update("registrasi_master_belajar", ["belajar_reg_id" => $INP_POST_REG_BELAJAR_ID], ["status_reg_belajar" => 3]);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/tugasbelajar");
    }
    
    public function prosesUnggahDokumenTugasBelajar(){
        $SUM_OF_FILE = 11; // num of file
        $INP_POST_REG_BELAJAR_ID = $_POST['belajar_reg_id']; // reg belajar id
        $INP_POST_PROSES = $_POST['proses_tugas_belajar']; // get post data about proses
        for($i = 0; $i < $SUM_OF_FILE; $i++){
            $count_file = $i + 1; 
            $nama_file = 'inp_dokumen_'.$count_file; //filename
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){//if upload file was null

                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_tugas_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_tugas_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/tugasbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_POST_REG_BELAJAR_ID,
                        "upload_proses" => $INP_POST_PROSES,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->tugasbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/tugasbelajar");
            }
        }
        $this->tugasbelajar->update("registrasi_master_belajar", ["belajar_reg_id" => $INP_POST_REG_BELAJAR_ID], ["status_reg_belajar" => 1]);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/tugasbelajar");
    }

    public function prosesAjukanTugasBelajar(){
        $SUM_OF_FILE = 3; // num of file
        $INP_POST_REG_BELAJAR_ID = $_POST['belajar_reg_id']; // reg belajar id
        $INP_POST_PROSES = $_POST['proses_tugas_belajar']; // get post data about proses
        for($i = 0; $i < $SUM_OF_FILE; $i++){
            $count_file = $i + 1; 
            $nama_file = 'inp_dokumen_'.$count_file; //filename
            try {
                if($_FILES[$nama_file]['size'] == 0 && $_FILES[$nama_file]['error'] == 4 ){//if upload file was null

                }else{
                    $PARAM_INP_UPLOAD_POSISI = $_POST['posisi_proses_tugas_belajar'][$i];
                    $PARAM_INP_UPLOAD_NAMAFILE = $nama_file;
                    $PARAM_JENIS = $_POST['jenis_dokumen_syarat_tugas_belajar'][$i] ;
                    $PARAM_IDUSER = $this->ion_auth->get_user_id();
                    $result = $this->upload_files($PARAM_JENIS,$PARAM_INP_UPLOAD_POSISI, $PARAM_INP_UPLOAD_NAMAFILE, $PARAM_IDUSER);
                    if($result['status'] == 0){
                        $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                        redirect("user/tugasbelajar");
                    }
                    $data_upload = array(
                        "belajar_reg_id" => $INP_POST_REG_BELAJAR_ID,
                        "upload_proses" => $INP_POST_PROSES,
                        "upload_posisi" => $PARAM_INP_UPLOAD_POSISI,
                        "upload_file" => $result['filename'],
                        "created_at" => date("Y-m-d H:i:s"),
                        "created_by" => $PARAM_IDUSER
                    );
                    $this->tugasbelajar->save("belajar_trans_upload", $data_upload);
                }
            } catch (Exception $ex) {
                $this->session->set_flashdata("error_message","Terjadi Error dalam proses unggah dokumen, mohon untuk cek file yang anda kirimkan dan juga unggah/upload kembali");
                redirect("user/tugasbelajar");
            }
        }
        $this->tugasbelajar->update("registrasi_master_belajar", ["belajar_reg_id" => $INP_POST_REG_BELAJAR_ID], ["status_reg_belajar" => 3]);
        $this->session->set_flashdata("success_message","Unggah Dokumen Berhasil, silahkan cek proses. Saat ini proses menunggu verifikasi OPD");
        redirect("user/tugasbelajar");
    }

    /* Saat INi tidak digunakan 
    public function unggah_persyaratan_permohonan($belajar_reg_id){
        if($this->ion_auth->logged_in()):
            //get user id
            $id_user = $this->ion_auth->get_user_id();

            //do upload persyaratan permohonan tugas belajar
            $result = $this->upload_files($belajar_reg_id, $id_user);

            if($result['status_uploads']):
                //update status registrasi tugas belajar
                $this->tugasbelajar->update("registrasi_master_belajar", ["belajar_reg_id" => $belajar_reg_id], ["status_reg_belajar" => 4]);

                $this->session->set_flashdata("success","Berkas Persyaratan Permohonan Tugas Belajar Sudah Dikirim.");
                redirect("user/tugasbelajar");
            else:
                $this->session->set_flashdata("error_message", $result['message']);
                redirect("user/tugasbelajar/ajukanPermohonanTugasBelajar/".$belajar_reg_id);
            endif;
        endif;
    }*/

    private function upload_files($jenis, $posisi, $namafile, $iduser)
    {       
        $this->load->library('upload');
        $file_upload = $_FILES;
        $_FILES[$namafile]['name']= $file_upload[$namafile]['name'];
        $_FILES[$namafile]['type']= $file_upload[$namafile]['type'];
        $_FILES[$namafile]['tmp_name']= $file_upload[$namafile]['tmp_name'];
        $_FILES[$namafile]['error']= $file_upload[$namafile]['error'];
        $_FILES[$namafile]['size']= $file_upload[$namafile]['size'];
        $custom_file_name ="TugasBelajar-".$jenis."-".$posisi."-".$iduser.'-'.md5(date("y-m-d h:i:s"));
        $extension = pathinfo($_FILES[$namafile]['name'], PATHINFO_EXTENSION);

        $this->upload->initialize($this->set_upload_options($custom_file_name));
        if ($this->upload->do_upload($namafile)) {
            $success = array(
                "status"=> 1,
                "filename"=>'uploads/persyaratan/'.$custom_file_name.".".$extension,
                "file" => $this->upload->data("client_name"),
                "uploaded" => $this->upload->data("file_name"),
                "message" => "Success Uploaded"
            );
        } else {
            $success = array(
                "status"=> 0,
                "filename"=>'uploads/persyaratan/'.$custom_file_name.".".$extension,
                "file" =>$this->upload->data("client_name"),
                "uploaded" => $this->upload->data("file_name"),
                "message" => "Error :" . $this->upload->display_errors()
            );
        }
        return $success;
    }

    private function set_upload_options($file_name)
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/persyaratan/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '1024';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }
}