<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bimteksosialisasi extends CI_Controller {

    // private static $total_jpl = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form","captcha"));
        $this->load->helper('dateformat_helper');
        $this->load->model("BimtekSosialisasi_model","bimteksosialisasi_model");
        $this->load->model("integrasi_api_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
        $this->load->database();
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $nip = $this->session->userdata("datauser")->nip;

            $data["pagename"] = "Bimtek/Sosialisasi/Workshop";
            $data["tablename"] = "Bimtek/Sosialisasi/Workshop";
            $data["field"] = $this->bimteksosialisasi_model->listfield("user_trans_bimtek");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/bimteksosialisasi/index/");
            $config["total_rows"] = $this->bimteksosialisasi_model->gettotalrows("user_trans_bimtek",$nip);
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();
            $page = $this->uri->segment(4);
            
            //call api and sync data bimtek, sosialisasi, workshop
            $this->load_data_bimtek($nip);

            //count total jpl
            $data["data_jpl"] = $this->bimteksosialisasi_model->count_total_jpl($nip);
            $data["rows"] = $this->bimteksosialisasi_model->get_all_data($config["per_page"],$page,$nip);

            $this->load->view('userpanel/bimteksosialisasi/index',$data);
        } else{
            redirect("user/auth/login");
        }
    }


    public function load_data_bimtek($nip){
        $result = json_decode($this->integrasi_api_model->get_bimtek_sosialisasi_by_nip($nip));
        if($result !== null){
            $data = $result->data;
            if(sizeof($data) > 0){
                $total_jpl = 0;
                foreach ($result->data as $data) {
                    $bimtek_sosialisasi[] = array(
                        "nip" => $data->nip,
                        "tanggal" => $data->tanggal,
                        "kojab" => $data->kojab,
                        "kode_aktifitas" => $data->kode_aktifitas,
                        "volume" => $data->volume,
                        "tingkat_kesulitan" => $data->tingkat_kesulitan,
                        "nama_aktifitas" => $data->nama_aktifitas,
                        "keterangan" => $data->keterangan,
                    );
                    $nip = $data->nip;
                    $total_jpl += $data->volume;
                }

                $data = $this->bimteksosialisasi_model->get_by_nip($nip);

                if(empty($data)):
                    for ($i=0; $i < count($bimtek_sosialisasi); $i++):
                        $this->bimteksosialisasi_model->save($bimtek_sosialisasi[$i]);
                    endfor;
                else:
                    if(count($bimtek_sosialisasi) > count($data)):
                        for ($i=0; $i < count($bimtek_sosialisasi); $i++):
                            $tanggal = $bimtek_sosialisasi[$i]['tanggal'];
                            $is_exist = $this->bimteksosialisasi_model->get_by_tanggal($tanggal);
                            if(empty($is_exist)):
                                $this->bimteksosialisasi_model->save($bimtek_sosialisasi[$i]);
                            endif;  
                        endfor;
                    endif;
                endif;
                return $total_jpl;
            } else{
                $this->session->set_flashdata("error_message","Data Tidak Ditemukan.");
                return array("Data Tidak Ditemukan");
            }
        } else{
            $message = "API Simpeg tidak dapat dilakukan, Periksa Kembali Internet Anda";
            $this->session->set_flashdata("error_message",$message);
            echo $message;
        }
    }


}