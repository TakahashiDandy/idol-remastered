<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url","form"));
        $this->load->model("event_model");
        $this->load->model("registrasi_model");
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->autoupdatejadwal();
            $data["pagename"] = "Jadwal Diklat";
            $data["tablename"] = "List Jadwal Diklat";
            $data["field"] = $this->event_model->listfield("diklat_trans_jadwal");

            //====PAGINATION CONFIG===
            $config["base_url"] = base_url("user/jadwal/index/");
            $config["total_rows"] = $this->event_model->gettotalrows("diklat_trans_jadwal");
            $config["per_page"] = 10;
            $config["full_tag_open"] = "<ul class='pagination justify-content-end'>";
            $config["full_tag_close"] = "</ul>";
            $config["next_link"] = "<i class='fa fa-angle-right'></i>";
            $config["next_tag_open"] = "<li class='page-item'>";
            $config["next_tag_close"] = "</li>";
            $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
            $config["prev_tag_open"] = "<li class='page-item'>";
            $config["prev_tag_close"] = "</li>";
            $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
            $config["cur_tag_close"] = "</a></li>";
            $config["num_tag_open"] = "<li class='page-item'>";
            $config["num_tag_close"] = "</li>";
            $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
            $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
            $config['attributes'] = array('class' => 'page-link');
            $this->pagination->initialize($config);
            $data["pagination"] = $this->pagination->create_links();

            $page = $this->uri->segment(4);
            $nip = $this->session->userdata("datauser")->nip;
            $data["row"] = $this->event_model->listjadwal($config["per_page"],$page, $nip);

            $this->load->view("userpanel/jadwal/list_jadwal_diklat",$data);
        } else{
            redirect("user/auth/login");
        }
    }

    public function tambah_jadwal(){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                if(!$this->session->set_userdata("diklat_jenis")){
                    $savepoint = array(
                        "diklat_jenis" => $this->input->post("diklat_jenis"),
                        "id_diklat" => $this->input->post("id_diklat"),
                        "diklat_nama" => $this->input->post("diklat_nama"),
                        "diklat_mulai" => $this->input->post("diklat_mulai"),
                        "diklat_selesai" => $this->input->post("diklat_selesai"),
                        "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                        "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                        "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                        "diklat_tempat" => $this->input->post("diklat_tempat"),
                        "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                        "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                        "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                    );
                    $this->session->set_userdata($savepoint);
                }
                $data["pagename"] = "Jadwal Diklat";
                $data["tablename"] = "Tambah Jadwal Diklat";
                $this->load->view("userpanel/jadwal/input_jadwal_diklat",$data);
            } else{
                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }
                $this->event_model->insertjadwal($savepoint);

                redirect("user/jadwal");
            }
        } else{
            redirect("user/auth/login");
        }
    }

    public function delete_jadwal($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->event_model->deletejadwal($id);
            redirect("user/jadwal/");
        }else{
            redirect("user/auth/login");
        }

    }

    public function edit_jadwal($id){
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){
            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');

            if($this->form_validation->run() == FALSE){
                foreach ($this->event_model->getonejadwal($id,1)->result_array() as $data){
                    $savepoint = array(
                        "diklat_jadwal_id" => $data["diklat_jadwal_id"],
                        "diklat_nama" => $data["diklat_nama"],
                        "diklat_jenis" => $data["diklat_jenis"],
                        "diklat_mulai" => $data["diklat_mulai"],
                        "diklat_selesai" => $data["diklat_selesai"],
                        "diklat_deskripsi" => $data["diklat_deskripsi"],
                        "diklat_jumlah_jam" => $data["diklat_jumlah_jam"],
                        "diklat_jumlah_peserta" => $data["diklat_jumlah_peserta"],
                        "diklat_tempat" => $data["diklat_tempat"],
                        "diklat_usul_no" => $data["diklat_usul_no"],
                        "diklat_usul_tgl" => $data["diklat_usul_tgl"],
                        "diklat_penyelenggara" => $data["diklat_penyelenggara"]
                    );
                    switch ($data["diklat_jenis"]){
                        case 1:
                            $savepoint["id_diklat"] = $data["kategori_id"];
                            break;
                        case 2:
                            $savepoint["id_diklat"] = $data["diklat_fungsional_id"];
                            break;
                        case 3:
                            $savepoint["id_diklat"] = $data["diklat_teknis_id"];
                            break;
                    }
                }
                $this->session->set_userdata($savepoint);
                $data["pagename"] = "Jadwal Diklat";
                $data["tablename"] = "Edit Jadwal Diklat";
                $this->load->view("userpanel/jadwal/edit_jadwal_diklat",$data);
            } else{
                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }
                $this->event_model->updatejadwal($id,$savepoint);
                redirect("user/jadwal");
            }
        } else{
            redirect("user/auth/login");
        }
    }

    public function autoupdatejadwal(){
        $this->event_model->autoupdatejadwal();
    }


    private function removesessionjadwal(){
        $session = array(
            "diklat_jenis",
            "id_diklat",
            "nama_diklat",
            "diklat_mulai",
            "diklat_selesai",
            "diklat_jumlah_jam",
            "diklat_jumlah_peserta",
            "diklat_tempat",
            "diklat_usul_no",
            "diklat_usul_tgl",
            "diklat_penyelenggara"
        );
        $this->session->unset_userdata($session);
    }

    public function jadwal_registrasi_diklat($id){        
        if($this->ion_auth->logged_in() && $this->ion_auth->in_group(2)){

            $this->form_validation->set_rules('diklat_jenis', 'Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('id_diklat', 'ID Jenis Diklat', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('diklat_nama', 'Nama Diklat', 'required');
            $this->form_validation->set_rules('diklat_mulai', 'Tanggal Mulai', 'required');
            $this->form_validation->set_rules('diklat_selesai', 'Tanggal Akhir', 'required');
            $this->form_validation->set_rules('diklat_deskripsi', 'Deskripsi Diklat', 'required');
            $this->form_validation->set_rules('diklat_jumlah_jam', 'Jumlah Jam', 'required|numeric');
            $this->form_validation->set_rules('diklat_jumlah_peserta', 'Jumlah Peserta', 'required|numeric');
            $this->form_validation->set_rules('diklat_tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('diklat_usul_no', 'No. Usul', 'required');
            $this->form_validation->set_rules('diklat_usul_tgl', 'Tanggal Usul', 'required');
            $this->form_validation->set_rules('diklat_penyelenggara', 'Penyelenggara', 'required');
            if(empty($_FILES['lampiran']['name'])){
                $this->form_validation->set_rules('lampiran', 'Lampiran', 'required');
            }

            if($this->form_validation->run() == FALSE){
                //get data jadwal user diklat
                $diklat_jadwal_id = $this->uri->segment(4);
                $data["jadwal_diklat"] = $this->event_model->getonejadwal($diklat_jadwal_id,0)->result_array();

                //set data jadwal diklat session
                foreach ($this->event_model->getonejadwal($diklat_jadwal_id,0)->result_array() as $data){
                    $savepoint = array(
                        "diklat_jadwal_id" => $data["diklat_jadwal_id"],"diklat_nama" => $data["diklat_nama"],
                        "diklat_jenis" => $data["diklat_jenis"],"diklat_mulai" => $data["diklat_mulai"],
                        "diklat_selesai" => $data["diklat_selesai"],"diklat_deskripsi" => $data["diklat_deskripsi"],
                        "diklat_jumlah_jam" => $data["diklat_jumlah_jam"],"diklat_jumlah_peserta" => $data["diklat_jumlah_peserta"],
                        "diklat_tempat" => $data["diklat_tempat"],"diklat_usul_no" => $data["diklat_usul_no"],
                        "diklat_usul_tgl" => $data["diklat_usul_tgl"],"diklat_penyelenggara" => $data["diklat_penyelenggara"]
                    );
                    switch ($data["diklat_jenis"]){
                        case 1:
                            $savepoint["id_diklat"] = $data["kategori_id"];
                            break;
                        case 2:
                            $savepoint["id_diklat"] = $data["diklat_fungsional_id"];
                            break;
                        case 3:
                            $savepoint["id_diklat"] = $data["diklat_teknis_id"];
                            break;
                    }
                    $this->session->set_userdata($savepoint);
                }

                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();
                $data_detail_user = $this->registrasi_model->get_detail_user_by_id($id_user);
                $data["detail_user"] = $data_detail_user;
                $data["pagename"] = "Registrasi Diklat";
                $data["attachment"] = $this->event_model->getattachment($diklat_jadwal_id)->result_array();
                $this->load->view("userpanel/jadwal/jadwal_registrasi_diklat",$data);
            }else{
                //get data user diklat
                $id_user = $this->ion_auth->get_user_id();

                $this->removesessionjadwal();
                $savepoint = array(
                    "diklat_jenis" => $this->input->post("diklat_jenis"),
                    "diklat_nama" => $this->input->post("diklat_nama"),
                    "diklat_mulai" => $this->input->post("diklat_mulai"),
                    "diklat_selesai" => $this->input->post("diklat_selesai"),
                    "diklat_deskripsi" => $this->input->post("diklat_deskripsi"),
                    "diklat_jumlah_jam" => $this->input->post("diklat_jumlah_jam"),
                    "diklat_jumlah_peserta" => $this->input->post("diklat_jumlah_peserta"),
                    "diklat_tempat" => $this->input->post("diklat_tempat"),
                    "diklat_usul_no" => $this->input->post("diklat_usul_no"),
                    "diklat_usul_tgl" => $this->input->post("diklat_usul_tgl"),
                    "diklat_penyelenggara" => $this->input->post("diklat_penyelenggara")
                );
                switch ($this->input->post("diklat_jenis")){
                    case 1:
                        $savepoint["kategori_id"]=$this->input->post("id_diklat");
                        break;
                    case 2:
                        $savepoint["diklat_fungsional_id"]=$this->input->post("id_diklat");
                        break;
                    case 3:
                        $savepoint["diklat_teknis_id"]=$this->input->post("id_diklat");
                        break;
                }

                //UPDATE JADWAL
                // $this->event_model->updatejadwal($id,$savepoint);

                //INSERT DATA MASTER RE
                $data_registrasi_master_reg_online = array(
                    "diklat_jadwal_id" => $id,
                    "nip" => $this->input->post("nip"),
                    "nama_lengkap" => $this->input->post("nama"),
                    "jenis_kelamin" => $this->input->post("jeniskelamin"),
                    "jabatan" => $this->input->post("jabatan"),
                    "golongan" => $this->input->post("golongan"),
                    "instansi" => $this->input->post("instansi"),
                    "pendidikan_terakhir" => $this->input->post("pendidikanterakhir"),
                    "tempat_lahir" => $this->input->post("tempatlahir"),
                    "tanggal_lahir" => $this->input->post("tgllahir"),
                    "alamat_rumah" => $this->input->post("alamatrumah"),
                    "alamat_kantor" => $this->input->post("alamatkantor"),
                    "kabkota" => $this->input->post("kabkota"),
                    "provinsi" => $this->input->post("provinsi"),
                    "no_kontak" => $this->input->post("nomorkontak"),
                    "no_tlp_kantor" => $this->input->post("telpfax"),
                    "email" => $this->input->post("email"),
                    "status_registrant" => 0,
                );
                $id_reg = $this->registrasi_model->insertregistrasi($data_registrasi_master_reg_online);

                $diklat = "";
                //UPLOAD BERKAS LAMPIRAN
                if($this->upload_files($id,$id_user,$id_reg)){
                    if(stripos($this->input->post("diklat_nama"), "Diklat") !== false){
                        $diklat = $this->input->post("diklat_nama");
                    }else{
                        $diklat = "Diklat ".$this->input->post("diklat_nama");
                    }
                    $this->session->set_flashdata("success_message", "Anda telah melakukan pendaftaran ".$diklat.".");
                    redirect("user/jadwal");
                }else{
                    $this->registrasi_model->deleteregistrasi($id_reg);
                    $this->session->set_flashdata("error_message", "File Uploaded are wrong");
                    redirect("user/jadwal/jadwal_registrasi_diklat/".$id);
                }
            }
        } else{
            redirect("user/auth/login");
        }
    }

    private function upload_files($id_jadwal,$id_user,$id_reg)
    {       
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['lampiran']['name']);
        $error = FALSE;
        $uploadedfiles = array();
        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['lampiran']['name']= $files['lampiran']['name'][$i];
            $_FILES['lampiran']['type']= $files['lampiran']['type'][$i];
            $_FILES['lampiran']['tmp_name']= $files['lampiran']['tmp_name'][$i];
            $_FILES['lampiran']['error']= $files['lampiran']['error'][$i];
            $_FILES['lampiran']['size']= $files['lampiran']['size'][$i];
            $extension = pathinfo($_FILES['lampiran']['name'], PATHINFO_EXTENSION);
            $custom_file_name = "LAMPIRAN-IDOL-".$id_user.'-'.$id_jadwal.'-'.md5(date("y-m-d h:i:s"));

            $this->upload->initialize($this->set_upload_options($custom_file_name));

            if ($this->upload->do_upload('lampiran')) {
                $data = $this->upload->data();
                $data_upload = array(
                    "id_jadwal" => $id_jadwal,
                    "id_registrasi" => $id_reg,
                    "id_option" => 1,
                    "filepath" => 'uploads/lampiran/'.$data["raw_name"].".".$extension,
                    "extension" => $extension,
                    "created_at" => date("y-m-d h:i:s"),
                    "created_by" => $id_user
                );
                array_push($uploadedfiles,$data_upload);
            } else {
                $error = TRUE;
                break;
            }
        }

        if($error == TRUE){
            foreach ($uploadedfiles as $data){
                var_dump($data);
                unlink($data->filepath);
            }
            $this->registrasi_model->delete_registrasi_trans_upload($id_jadwal,$id_reg);
            return false;
        } else{
            foreach ($uploadedfiles as $data_upload){
                $this->registrasi_model->save_registrasi_trans_upload($data_upload);
            }
            return true;
        }
    }

    private function set_upload_options($file_name)
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/lampiran/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc';
        $config['max_size']      = '1024';
        $config['overwrite']     = FALSE;
        $config['file_name'] = $file_name;

        return $config;
    }

}