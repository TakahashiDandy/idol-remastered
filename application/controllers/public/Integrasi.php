<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model("integrasi_api_model");
        $this->load->model("Trans_detail_user_model","detail_user");
    }

    function load_data($nip){

        $result = json_decode($this->integrasi_api_model->get_detail_by_nip($nip));
        if($result !== null){
            $response = $result->response;
            if($response->status == 1) {
                $data = $response->data->data;
                if(!empty($data)){
                    $detail_user = array(
                        "nama_lengkap" => $data->nama,
                        "golongan" => $data->golongan_akhir,
                        "tempat_lahir" => $data->tempat_lahir,
                        "tanggal_lahir" => $data->tanggal_lahir,
                        "jenis_kelamin" => $data->jenis_kelamin,
                        "nama_pangkat" => $data->nama_pangkat,
                        "jabatan" => $data->jabatan_nama,
                        "no_kontak" => $data->no_hp,
                        "email" => $data->email == "" ? "-@gmail.com" : $data->email,
                        "instansi" => $data->instansi,
                        "pendidikan_terakhir" => $data->pendidikan_akhir,
                        "kotakab" => "Bandung",
                        "provinsi" => "Jawa Barat",
                        "telpfax" => $data->no_hp,
                        "alamat_rumah" => $data->alamat,
                        "alamat_kantor" => $data->alamat,
                        "foto" =>$data->foto
                    );

                    $this->detail_user->update(array("nip" => $nip), $detail_user);

                    echo json_encode($data);
                } else{
                    echo "Data Tidak Ditemukan";
                }
            }
        } else{
            echo "API Simpeg tidak dapat dilakukan, Periksa Kembali Internet Anda";
        }

    }

    function getriwayatdiklat($nip){
        $data = $this->integrasi_api_model->riwayat_diklat($nip)->result();
        echo json_encode($data);
    }

    function getriwayatpendidikan($nip){
        $data = $this->integrasi_api_model->riwayat_pendidikan($nip);
        echo json_encode($data);
    }

    function gettmtbelajar($nip){
        $data = $this->integrasi_api_model->dataTransSurat($nip);
        if(!empty($data) && count($data) > 0){
            foreach ($data as $value) {
                if($value->tmt_mulai !== null){
                    $tanggal_tmt_mulai = date('Y-m-d', strtotime($value->tmt_mulai));
                    $tanggal_tmt_akhir = date('Y-m-d', strtotime($value->tmt_mulai. ' + '.$value->masa_tempuh.' years'));
                }else{
                    $tanggal_tmt_mulai = $tanggal_tmt_akhir = null;
                }
                $array_results[] = array(
                    "id_registrasi_belajar" => $value->belajar_reg_id,
                    "nip" => $nip,
                    "tmt_mulai" => $tanggal_tmt_mulai,
                    "tmt_akhir" => $tanggal_tmt_akhir,
                    "status_perpanjangan" => $value->status_perpanjangan == 1 ? true : false
                );
            }
            echo json_encode($array_results, true);
        }else{
            echo json_encode(array(), true);
        }
    }
}