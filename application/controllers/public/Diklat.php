<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diklat extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model(array('event_model','diklat_model'));
        $this->load->library(array('pagination'));
    }

    public function index()
    {
        $data["pagename"] = "List Semua Diklat";

        $datadiklat = $this->event_model->fplistjadwalbyjenis(0,0,0)->num_rows();

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url() . "public/diklat/diklat_struktural";
        $config["total_rows"] = $datadiklat;
        $config["per_page"] = 10;
        $config["full_tag_open"] = "<ul class='pagination'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->event_model->fplistjadwalbyjenis(0,$config["per_page"], $page);
        $data["totalrows"] = $config["total_rows"];
        $data["page"] = $config["total_rows"];

        $this->load->view("frontpanel/diklat/all_diklat",$data);
    }

    public function detail_diklat($id){
        $data["jadwal"] = $this->event_model->getonejadwal($id);
        $this->load->view('frontpanel/diklat/detail_diklat',$data);
    }

    public function diklat_struktural(){
        $data["pagename"] = "Diklat Struktural";

        $datadiklat = $this->event_model->fplistjadwalbyjenis(1,0,0)->num_rows();

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url() . "public/diklat/diklat_struktural";
        $config["total_rows"] = $datadiklat;
        $config["per_page"] = 10;
        $config["full_tag_open"] = "<ul class='pagination'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->event_model->fplistjadwalbyjenis(1,$config["per_page"], $page);
        $data["totalrows"] = $config["total_rows"];
        $data["page"] = $config["total_rows"];

        $this->load->view("frontpanel/diklat/all_diklat",$data);
    }

    public function diklat_fungsional(){
        $data["pagename"] = "Diklat Fungsional";

        $datadiklat = $this->event_model->fplistjadwalbyjenis(2,0,0)->num_rows();

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url() . "public/diklat/diklat_fungsional";
        $config["total_rows"] = $datadiklat;
        $config["per_page"] = 10;
        $config["full_tag_open"] = "<ul class='pagination'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->event_model->fplistjadwalbyjenis(2,$config["per_page"], $page);
        $data["totalrows"] = $config["total_rows"];
        $data["page"] = $config["total_rows"];


        $this->load->view("frontpanel/diklat/all_diklat",$data);
    }

    public function diklat_teknis(){
        $data["pagename"] = "Diklat Teknis";

        $datadiklat = $this->event_model->fplistjadwalbyjenis(3,0,0)->num_rows();

        //====PAGINATION CONFIG===
        $config["base_url"] = base_url() . "public/diklat/diklat_teknis";
        $config["total_rows"] = $datadiklat;
        $config["per_page"] = 12;
        $config["full_tag_open"] = "<ul class='pagination'>";
        $config["full_tag_close"] = "</ul>";
        $config["next_link"] = "<i class='fa fa-angle-right'></i>";
        $config["next_tag_open"] = "<li class='page-item'>";
        $config["next_tag_close"] = "</li>";
        $config["prev_link"] = "<i class='fa fa-angle-left'></i>";
        $config["prev_tag_open"] = "<li class='page-item'>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='page-item active'> <a class='page-link'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li class='page-item'>";
        $config["num_tag_close"] = "</li>";
        $config["last_link"] = "<i class='fa fa-angle-double-right'></i>";
        $config["first_link"] = "<i class='fa fa-angle-double-left'></i>";
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $page = $this->uri->segment(4);
        $data["row"] = $this->event_model->fplistjadwalbyjenis(3,$config["per_page"], $page);
        $data["totalrows"] = $config["total_rows"];
        $data["page"] = $config["total_rows"];

        $this->load->view("frontpanel/diklat/all_diklat",$data);
    }
}
