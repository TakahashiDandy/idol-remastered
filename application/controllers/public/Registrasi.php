<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','captcha','form'));
        $this->load->model(array("registrasi_model","event_model"));
        $this->load->library(array('session','pagination','ion_auth','form_validation'));
    }

    public function index()
    {
        redirect(base_url("user/home"));
        $this->form_validation->set_rules('nip', 'NIP', 'required|min_length[18]|max_length[18]');
        $this->form_validation->set_rules('jadwal_diklat', 'Jadwal Diklat', 'required');

        if(!$this->form_validation->run()){
            //Config Captcha untuk registrasi
            $config = array(
                'img_path'      => 'uploads/captcha/',
                'img_url'       => base_url('uploads/captcha/'),
                'font_path'     => 'system/fonts/texb.ttf',
                'img_width'     => 150,
                'img_height'    => 60,
                'expiration'    => 300,
                'word_length'   => 4,
                'font_size'     => 16,
                'img_id'        => 'capt',
                'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

                // White background and border, black text and red grid
                'colors'        => array(
                    'background' => array(255, 255, 255),
                    'border' => array(255, 255, 255),
                    'text' => array(0, 0, 0),
                    'grid' => array(0, 128, 255)
                )
            );
            $cap = create_captcha($config);

            $this->session->set_userdata("captchaword",$cap["word"]);
            $data["captcha"] = $cap["image"];

            $this->load->view('frontpanel/diklat/registrasi',$data);
        } else{
            $savedata = array(
                "nip"   => $this->input->post("nip"),
                "nama_lengkap"   => $this->input->post("nama"),
                "jenis_kelamin"   => $this->input->post("jeniskelamin"),
                "jabatan"   => $this->input->post("jabatan"),
                "golongan"   => $this->input->post("golongan"),
                "instansi"   => $this->input->post("instansi"),
                "pendidikan_terakhir"   => $this->input->post("pendidikanteraknir"),
                "tempat_lahir"   => $this->input->post("tempatlahir"),
                "tanggal_lahir"   => $this->input->post("tgllahir"),
                "alamat_rumah"   => $this->input->post("alamatrumah"),
                "alamat_kantor"   => $this->input->post("alamatkantor"),
                "kabkota"   => $this->input->post("kabkota"),
                "provinsi"   => $this->input->post("provinsi"),
                "no_kontak"   => $this->input->post("nomorkontak"),
                "no_tlp_kantor"   => $this->input->post("tlpfax"),
                "email"   => $this->input->post("email"),
                "diklat_jadwal_id"   => $this->input->post("jadwal_diklat")
            );
            $this->session->unset_userdata("captchaword");
            $this->registrasi_model->insertregistrasi($savedata);
            $this->session->set_flashdata("success_message","Terimakasih Telah Mendaftarkan Diri di IDOL");
            redirect("public/registrasi","refresh");
        }
    }

    public function getlistjadwal(){
        $data["jadwal"]= $this->event_model->listjadwal(10,0);
        $view = $this->load->view("frontpanel/loader/selectdatajadwal",$data,true);
        print($view);
    }

    public function checkregistrant($id,$nip){
        $data = $this->registrasi_model->checkregistrant($id,$nip)->row();
        if($data->total > 0){
            echo $data->total;
        } else{
            echo "0";
        }
    }

    public function checkcaptcha(){
        $captcha = $this->input->get("captcha");
        if($captcha == $this->session->userdata("captchaword")){
            echo true;
        } else{
            echo false;
        }
        exit();
    }



}
