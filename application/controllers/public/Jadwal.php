<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("event_model");
    }

    public function index()
    {

        $color = array("#5472d3","#4c8c4a", "#c9bc1f", "#718792" , "#49599a", "#519657");
        $json = $this->event_model->fplistjadwal(100);
        $output = array();
        foreach ($json->result() as $item){
            $index = rand(0,5);
            $pre = array(
                "title"=> $item->diklat_nama,
                "start"=> $item->diklat_mulai,
                "end"=> $item->diklat_selesai,
                "url"=> base_url("public/diklat/detail_diklat/").$item->diklat_jadwal_id,
                "backgroundColor"=>$color[$index],
                "borderColor"=>$color[$index],
                );
            array_push($output,$pre);
        }
        $data["calendar"] = json_encode($output);
        $this->load->view('frontpanel/diklat/jadwal_diklat',$data);
    }
}