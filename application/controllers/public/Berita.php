<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $this->load->view('frontpanel/berita/all_berita');
    }

    public function detail_berita(){
        $this->load->view('frontpanel/berita/detail_berita');
    }
}
