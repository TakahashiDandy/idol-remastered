//var base_url = window.location.origin;
//var path = window.location.pathname;
var urlregistrasi  = base_url + "/public/integrasi/load_data/";
var urlmodaldiklat = base_url + "/admin/diklat/modalsearchdiklatbyname/";
var urlsearchdata =  base_url + "/admin/diklat/searchdiklatbyname/";

var urlintegrasimasterbelajar = base_url + "admin/integrasicontroller/integrasimasterbelajar";
var urlintegrasimasterdiklat = base_url + "admin/integrasicontroller/integrasimasterdiklat";


$("#jenis_pengiriman_rekom").change(function () {
    var data = $(this).children("option:selected").val();
    switch (data) {
        case "1":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        default:
            $("#collector").fadeOut();
            $("#keterangan").fadeIn();
            break;
    }
});

$("#jenis_pengiriman_tb").change(function () {
    var data = $(this).children("option:selected").val();
    switch (data) {
        case "2":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        case "3":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        case "5":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        default:
            $("#collector").fadeOut();
            $("#keterangan").fadeIn();
            break;
    }
});

$("#jenis_pengiriman_ib").change(function () {
    var data = $(this).children("option:selected").val();
    switch (data) {
        case "2":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        case "7":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        default:
            $("#collector").fadeOut();
            $("#keterangan").fadeIn();
            break;
    }
});

$("#jenis_pengiriman_ak").change(function () {
    var data = $(this).children("option:selected").val();
    switch (data) {
        case "1":
            $("#collector").fadeIn();
            $("#keterangan").fadeIn();
            break;
        default:
            $("#collector").fadeOut();
            $("#keterangan").fadeIn();
            break;
    }
});

$('#integrasi_master').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": base_url + "admin/integrasicontroller/indexx",
        "type": "POST"
    },
    "columns": [
        { "data": "kode_skpd" },
        { "data": "nama_dinas" },
        { "data": "tahun" },
        { "data": "kepala_dinas" }
    ]
});

$('#list_tubel').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": base_url + "admin/integrasicontroller/indexx",
        "type": "POST"
    },
    "columns": [
        { "data": "kode_skpd" },
        { "data": "nama_dinas" },
        { "data": "tahun" },
        { "data": "kepala_dinas" }
    ]
});

function showHome(){
    $(".banner-section").show(100);
    $(".events").hide();
    $(".recent-posts").hide();
    $(".popular-courses").hide();
}
function showEvent(){
    $(".banner-section").hide();
    $(".events").show(100);
    $(".recent-posts").hide();
    $(".popular-courses").hide();
}
function showLive(){
    $(".banner-section").hide();
    $(".events").hide();
    $(".recent-posts").show(100);
    $(".popular-courses").hide();
}
function showClient(){
    $(".banner-section").hide();
    $(".events").hide();
    $(".recent-posts").hide();
    $(".popular-courses").show(100);
}

function phase1() {
    $("#modal-loading").hide();
    $("#phase1").show();
    $("#phase2").hide();
    $("#phase3").hide();
}

function phase2() {
    var nip = $("#nip").val();
    var nama = $("#nama").val();
    var norobot = $("input[name='captcha']").val();
    if(nama == ""){
        if(nip  &&  norobot && !isNaN(nip)) {
            $("#phase1").hide();
            $("#modal-loading").show();
            $.ajax({
                url: urlregistrasi + nip,
                success: function (result) {
                    try {
                        //memeriksa apakah data simpegnya ada
                        var data = JSON.parse(result);
                        $("#phase1").hide();
                        $("#phase2").show();
                        $("#phase3").hide();

                        $("#nama").val(data.nama);
                        $("#golongan").val(data.golongan_akhir);
                        $("#tempatlahir").val(data.tempat_lahir);
                        $("#tgllahir").val(data.tanggal_lahir);
                        $("#jeniskelamin").val(data.jenis_kelamin);
                        $("#jabatan").val(data.jabatan_nama);
                        $("#nomorkontak").val(data.no_hp);
                        $("#email").val((data.email == "" ? "-@gmail.com" : data.email ));
                        $("#instansi").val(data.instansi);
                        $("#pendidikanterakhir").val(data.pendidikan_akhir);
                        $("#kabkota").val("Bandung");
                        $("#provinsi").val("Jawa Barat");
                        $("#telpfax").val(data.no_hp);
                        $("#alamatkantor").val(data.alamat);
                        $("#alamatrumah").val(data.alamat);
                    } catch (e) {
                        alert(result);
                        phase1();
                    }
                },
                complete: function () {
                    $("#modal-loading").hide();
                }
            });
        } else {
            alert("Periksa Kembali Data NIP dan Captcha Anda.");
        }
    } else{
        $("#phase1").hide();
        $("#phase2").show();
        $("#phase3").hide();
    }
}

function phase3() {
    $("#phase1").hide();
    $("#phase2").hide();
    $("#phase3").show();
    $("#jadwaldiklat").load('registrasi/getlistjadwal');
}

function checkcaptcha(){
    var input = $("#captcha").val();
    $.ajax({
        type: "GET",
        url: base_url + "/idol-remastered/public/registrasi/checkcaptcha",
        data:{captcha:input},
        success: function(response){
            if(response == true){
                phase2()
            } else{
                alert("Captcha tidak sama, periksa kembali");
            }
        }
    });
}

function checkregistrant(){
    var nip = $("#nip").val();
    var option = $('#jadwal_diklat').find(":selected").val();
    $.ajax({
        url: base_url + "/idol-remastered/public/registrasi/checkregistrant/"+option+"/"+nip,
        success: function (result) {
        try {
            if(result > "0"){
                alert("Anda Sudah Mendaftarkan Diri");
            }else{
                $(".comment-form").submit();
            }
        } catch (e) {
            console.error(e);
        }
    }
});
}

function modal_diklat_data(prm){
    var diklat_id = document.getElementById("diklat_id_"+prm).innerText;
    var diklat_nama = document.getElementById("diklat_nama_"+prm).innerText;
    $("#id_diklat").val(diklat_id);
    $("#diklat_nama").val(diklat_nama);
    $("#exampleModal").modal('toggle');
    $("#textsearch").val("");
}

function getdatarow() {
    if($("#textsearch").val() == ""){
        $.ajax({
                url: urlmodaldiklat
            + $("input[type='radio'][name='diklat_jenis']:checked").val() + "/all",
            success: function (result) {
            try {
                $("#modaldata").html(result);
            } catch (e) {
                alert(result);
            }
        }
    });
    } else{
        $.ajax({
                url: urlmodaldiklat +
            $("input[type='radio'][name='diklat_jenis']:checked").val() +"/"+$("#textsearch").val(),
            success: function (result) {
            try {
                $("#modaldata").html(result);
            } catch (e) {
                alert(result);
            }
        }
    });
    }
}

function searchdata(prm){
    if($("#textsearch").val() == ""){
        inputan = "all";
    } else {
        inputan = $("#textsearch").val();
    }
    $.ajax({
        url: urlsearchdata + prm +"/" +inputan,
        success: function (result) {
            try {
                switch (prm){
                    case 1:
                        $("#datastruktural").html(result);
                        break;
                    case 2:
                        $("#datafungsional").html(result);
                        break;
                    case 3:
                        $("#datateknis").html(result);
                        break;
                }
                $("#pagination").hide();
            } catch (e) {
                alert(result);
            }
        }
    });
}

function integrasidata(e) {
    if(e == "master-belajar"){ _url = urlintegrasimasterbelajar; }else{ _url = urlintegrasimasterdiklat; }

    $.ajax({
        url: _url,
        beforeSend : function(){
            if(e == "master-belajar") { 
                $("#intb").addClass("fa-spin");

                //disabled button integrasi master diklat
                $("#btna").attr("disabled", true);
            }
            else{ 
                $("#inta").addClass("fa-spin");

                //disabled button integrasi master belajar
                $("#btnb").attr("disabled", true);
            }
        },
        success: function (result) {
            try {
                if(result == 1){
                    if(e == "master-belajar"){
                        $("#btnb").addClass("btn-success");
                        $("#intb").addClass("fa-check");
                        $("#spb").text("Integrasi selesai");
                        $("#btnb").attr("disabled", true);
                    }else{
                        $("#btna").addClass("btn-success");
                        $("#inta").addClass("fa-check");
                        $("#spa").text("Integrasi selesai");
                        $("#btna").attr("disabled", true);
                    }
                } else{
                    if(e == "master-belajar"){
                        $("#btnb").addClass("btn-danger");
                        $("#intb").addClass("fa-cross");
                        $("#spb").text("Integrasi gagal");
                        $("#btnb").attr("disabled", false);
                    }else{
                        $("#btna").addClass("btn-danger");
                        $("#inta").addClass("fa-cross");
                        $("#spa").text("Integrasi gagal");
                        $("#btna").attr("disabled", false);
                    }
                }
            } catch (e) {
                alert(e);
            }
        }, complete: function () {
            if(e == "master-belajar"){ 
                $("#intb").removeClass("fa-sync");
                $("#intb").removeClass("fa-spin");
                $("#btnb").removeClass("btn-primary");

                //disabled button integrasi master diklat
                $("#btna").attr("disabled", false);
            }else{
                $("#inta").removeClass("fa-spin");
                $("#inta").removeClass("fa-spin");
                $("#btna").removeClass("btn-primary");

                //disabled button integrasi master belajar
                $("#btnb").attr("disabled", false);
            }
        }
    });
}